module.exports = {
    env: {
        node: true,
        es6: true,
        browser: true
      },
    
      parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
          jsx: true,
          modules: true,
          experimentalObjectRestSpread: true
        }
      },
        // 
    // "extends": "airbnb",
    // extends: ["airbnb-base", "prettier"],
    // plugins: ["prettier"],
    // parserOptions: {
    //   ecmaVersion: 12,
    // },
    // rules: {
    //   "prefer-destructuring": "off",
    //   "prettier/prettier": ["error", { printWidth: 120, endOfLine: "auto" }],
    //   eqeqeq: "off",
    //   camelcase: "off",
    //   quotes: ["error", "double"],
    // },
};