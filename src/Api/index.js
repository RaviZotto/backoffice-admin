import axios from "axios";
import {  AxiosError } from "axios";
import notification from "../components/notification";
import { getErrorString } from "../utility/extras";



const getToken=()=>{ 
  var token = JSON.parse(localStorage.getItem("token_item"));
  return token['token'];
}
              
               const baseURL = "https://paymentz.z-pay.co.uk";
              // const baseURL="http://localhost:3006";
export const Axios = axios.create({
  baseURL,
  timeoutErrorMessage: "Couldnt reach the request",
});

Axios.interceptors.request.use(
  (config) => {
    // ** Get token from localStorage
    const accessToken = getToken();

    // ** If token is present add it to request's Authorization Header
    if (accessToken) {
      // ** eslint-disable-next-line no-param-reassign
      config.headers.Authorization = `Bearer ${accessToken}`;
    }else{ 
         window.history.pushState(null,null,'/login');
    }
    return config;
  },
  (error) => { 
    const {data,statusText,status}=error.response;
    if(status==400 || status==401){
      localStorage.clear();
      window.location.href='/login';
      
     
      return;
    }

    return Promise.reject(data.message?data.message:data);
  } 
);

Axios.interceptors.response.use(
  response => response,
  (error) => { 
    console.log(error.response,error);
    const {data,statusText,status}=error.response||{};
    if(
      error=='Error: Network Error' || 
     status==401 || statusText&&statusText.toLowerCase()=='unauthorized'  ){
      notification({type: 'error', message:data&&data.message?data.message:(data||statusText)});
      localStorage.clear();
       window.location.href='/login';
     return;
    }

    return Promise.reject(data.message?data.message:data);
  } 

)
export const getSuspiciousTxn=(val,callback)=>{
  Axios.get(`/admin/getsuspiciousTxn?dates=${new URLSearchParams(val).toString()}`).then(res1=>{ 
   if(res1.status==200 && res1.data) callback({res:res1.data.txn});
  }).
  catch((e:AxiosError)=>callback({err:e.response?e.response.data:e.toString()}))
   
}

export const getAdminTransactions=(val)=>{
   return new Promise((res, rej)=>{
      Axios.get(`/admin/getAdminTransactions?${new URLSearchParams(val).toString()}`).then(res1 => { 
       if(res1.data&&res1.status==200){console.log(res.data);res(res1.data.transactions)}
      })
      .catch((err:AxiosError) =>{ 
        rej(err.response?err.response.data:err.toString())
      }) 
   })
}
export const getFlairCardTransactions=(val)=>{
  return new Promise((res, rej)=>{
     Axios.get(`/admin/getFlairCardTransactions?${new URLSearchParams(val).toString()}`).then(res1 => { 
      if(res1.data&&res1.status==200){res(res1.data)}
     })
     .catch((err:AxiosError) =>{ 
       rej(err.response?err.response.data:err.toString())
     }) 
  })
}

export const updateSuspiciousTxn=(id,key,callback)=>{
  Axios.put(`/admin/updateSuspiciousTxn?key=${key}&&id=${id}`).then(res1=>{ 
   if(res1.status==200 && res1.data) callback({res:{success:true}});
  }).
  catch((e:AxiosError)=>callback({err:e.response?e.response.data:e.toString()}))
   
}

export const getAllCurrency=(callback)=>{
     Axios.get(`/admin/getAllCurrency`).then(res1=>{ 
      if(res1.status==200 && res1.data) callback({res:res1.data.currencies});
     }).
     catch((e:AxiosError)=>callback({err:e.response?e.response.data:e.toString()}))
      
}

export const getAdminExchangeRate=(callback)=>{
  Axios.get(`/admin/getExchangeRate`).then(res1=>{ 
   if(res1.status==200 && res1.data) callback({res:res1.data.exchange_rate});
  }).
  catch((e:AxiosError)=>callback({err:e.response?e.response.data:e.toString()}))
   
}
export const updateAdminExchangeRate=(exchange_rate,callback)=>{
  Axios.put(`/admin/updateExchangeRate`,{exchange_rate}).then(res1=>{ 
   if(res1.status==200 && res1.data) callback({success:true});
  }).
  catch((e:AxiosError)=>callback({err:e.response?e.response.data:e.toString()}))
   
}

export const deleteAdminCurrency=(id,callback)=>{
  Axios.delete(`/admin/deleteAdminCurrency?id=${id}`).then(res1=>{ 
   if(res1.status==200 && res1.data) callback({res:res1.data.success});
  }).
  catch((e:AxiosError)=>callback({err:e.response?e.response.data:e.toString()}))
   
}



export const addAdminCurrency=(value)=>{ 
  return new Promise ((resolve,rej)=>{
  Axios.post(`/admin/addAdminCurrency`,value).then(res=>{ 
      if(res.status==200 && res.data) resolve(res.data.success);
    })
    .catch((e: AxiosError)=>rej(e.response?e.response.data:e.toString()))
  });
}

export const getGiftsCards=(id)=>{ 
  return new Promise ((resolve,rej)=>{
  Axios.get(`/admin/getClientCards?id=${id}`).then(res=>{ 
      if(res.status==200 && res.data) resolve(res.data);
    })
    .catch((e: AxiosError)=>rej(e.response?e.response.data:e.toString()))
  });
}

export const saveClientCards=(value)=>{ 
  return new Promise ((resolve,rej)=>{
    Axios.post(`/admin/saveClientCards`,value).then(res=>{ 
        if(res.status==200 && res.data) resolve(res.data);
      })
      .catch((e: AxiosError)=>rej(e.response?e.response.data:e.toString()))
    });
}



export const updateClientCards=(id,value)=>{ 
  return new Promise ((resolve,rej)=>{
    Axios.put(`/admin/updateClientCard?id=${id}`,value).then(res=>{ 
        if(res.status==200 && res.data) resolve(res.data);
      })
      .catch((e: AxiosError)=>rej(e.response?e.response.data:e.toString()))
    });
}

export const saveCardCategory=(value)=>{ 
  return new Promise ((resolve,rej)=>{
    Axios.post(`/admin/addCardCategory`,value).then(res=>{ 
        if(res.status==200 && res.data) resolve(res.data);
      })
      .catch((e: AxiosError)=>rej(e.response?e.response.data:e.toString()))
    });
}



export const updateCardCategory =(id,value)=>{ 
  return new Promise ((resolve,rej)=>{
    Axios.put(`/admin/updateCardCategory?id=${id}`,value).then(res=>{ 
        if(res.status==200 && res.data) resolve(res.data);
      })
      .catch((e: AxiosError)=>rej(e.response?e.response.data:e.toString()))
    });
}

export const saveCardDiscount=(value)=>{ 
  return new Promise ((resolve,rej)=>{
    Axios.post(`/admin/addCardDiscount`,value).then(res=>{ 
        if(res.status==200 && res.data) resolve(res.data);
      })
      .catch((e: AxiosError)=>rej(e.response?e.response.data:e.toString()))
    });
}



export const updateCardDiscount =(id,value)=>{ 
  return new Promise ((resolve,rej)=>{
    Axios.put(`/admin/updateCardDiscount?id=${id}`,value).then(res=>{ 
        if(res.status==200 && res.data) resolve(res.data);
      })
      .catch((e: AxiosError)=>rej(e.response?e.response.data:e.toString()))
    });
}




export const changeTransfer=(id,params)=>{ 
  return new Promise((resolve, reject) => {
    Axios.post(`/admin/setTransferToggle?merchant_id=${id}`,params)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e: AxiosError) => {
        if (e.response) reject(e.response.data);
        else reject(e.toString());
      });
  });
};

export const getAllNotifications=(params, callback)=>{ 
  Axios.post(`/admin/getAllNotifications`, params).then(res=>{ 
    if(res.data&&res.status==200) callback({res:res.data})
  }).
  catch((e: AxiosError)=>{
    callback({err:e.response?e.response.data:e.toString()});
  })
}

export const addUser=(params,callback)=>{ 
  Axios.post(`/admin/addAdminBackofficeUser`, {user:params}).then(res=>{ 
    if(res.data&&res.status==200) callback({res:res.data.users})
  }).
  catch((e: AxiosError)=>{
    callback({err:e.response?e.response.data:e.toString()});
  })
  
}


export const updateUser=(id,params,callback)=>{ 
  Axios.put(`/admin/updateAdminBackofficeUser?id=${id}`, params).then(res=>{ 
    if(res.data&&res.status==200) callback({res:res.data})
  }).
  catch((e: AxiosError)=>{
    callback({err:e.response?e.response.data:e.toString()});
  })
  
}

export const getUser=(callback)=>{ 
  Axios.get(`/admin/getAllAdminUser`).then(res=>{ 
    if(res.data&&res.status==200) callback({res:res.data.users})
  }).
  catch((e: AxiosError)=>{
    callback({err:e.response?e.response.data:e.toString()});
  })
  
}


export const addSubscription=(params,callback)=>{ 
  Axios.post(`/admin/addSubcription`, params)
  .then((res) => {
    if ((res.status = 200 && res.data)) callback({res:res.data});
  })
  .catch((e: AxiosError) => {
    if (e.response) callback&&callback({ err: e.response.data });
    else callback&&callback({ err: e.toString() });
  });

}

export const getSubscription =(params,callback)=>{ 
  Axios.post(`/admin/getSubscription`, params)
  .then((res) => {
    if ((res.status = 200 && res.data)) callback({res:res.data});
  })
  .catch((e: AxiosError) => {
    if (e.response) callback({ err: e.response.data });
    else callback({ err: e.toString() });
  });
}

export const addClient =(params,callback)=>{ 
 
  Axios.post(`/admin/addClient`, params)
  .then((res) => {
    if ((res.status = 200 && res.data)) callback({res:res.data});
  })
  .catch((e: AxiosError) => {
    if (e.response) callback&&callback({ err: e.response.data });
    else callback&&callback({ err: e.toString() });
  });

}

export const getClient =(callback)=>{ 
 Axios.get(`/admin/getClient`)
  .then((res) => {
    if ((res.status = 200 && res.data)) callback({res:res.data});
  })
  .catch((e: AxiosError) => {
    if (e.response) callback({ err: e.response.data });
    else callback({ err: e.toString() });
  });
}

export const getType =(callback)=>{ 
  Axios.get(`/admin/getType`)
   .then((res) => {
     if ((res.status = 200 && res.data)) callback({res:res.data});
   })
   .catch((e: AxiosError) => {
     if (e.response) callback({ err: e.response.data });
     else callback({ err: e.toString() });
   });
 }
 export const getSubType =(callback)=>{ 
  Axios.get(`/admin/getSubType`)
   .then((res) => {
     if ((res.status = 200 && res.data)) callback({res:res.data});
   })
   .catch((e: AxiosError) => {
     if (e.response) callback({ err: e.response.data });
     else callback({ err: e.toString() });
   });
 }

export const addBeneficiary=(id,params,callback)=>{ 
  Axios.post(`/admin/addMerchantBeneficiary?id=${id}`, params)
    .then((res) => {
      if ((res.status = 200 && res.data)) callback({res:res.data});
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
      else callback({ err: e.toString() });
    });
}
export const getExchangeRate=(params,callback)=>{ 
  console.log(params);
  Axios.post(`/admin/getAllExchange`, params)
    .then((res1) => {
      if ((res1.status = 200 && res1.data)) callback({res:res1.data.exchanges});
    })
    .catch((e: AxiosError) => {
     callback&&callback({ err: e.response? e.response.data:e.toString() });
     
    });
}

export const toggleParterStatus = (params, callback) => {
  Axios.put("/admin/togglePartnerStatus", params)
    .then((res) => {
      if ((res.status = 200 && res.data)) callback({});
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
      else callback({ err: e.toString() });
    });
};

export const toggleMerchantStatus = (params, callback) => {
  Axios.put("/admin/toggleMerchantStatus", params)
    .then((res) => {
      if ((res.status = 200 && res.data)) callback({});
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
      else callback({ err: e.toString() });
    });
};

export const createCardForLedger = (params) => {
  return new Promise((resolve, reject) => {
    Axios.post("/admin/issue-debit-card", { ...params })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e: AxiosError) => {
        reject(e.response?e.response.data.err:e.toString());
      });
  });
};

export const getMerchantAccount = (callback) => {
  Axios.get("/admin/getMerchantRealAccount")
    .then((res) => {
      if (res.status == 200 && res.data) callback({ res: res.data });
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
      else callback({ err: e.toString() });
    });
};

export const getAllBeneficiary = (callback) => {
  Axios.get("/admin/getBeneficiaries")
    .then((res) => {
      if (res.status == 200 && res.data) callback({ res: res.data });
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
      else callback({ err: e.toString() });
    });
};

export const getAllRegDevice = (id) => {
  return new Promise((resolve, reject) => {
    Axios.get(`/admin/getAllRegDevice?merchant_id=${id}`)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e: AxiosError) => {
        if (e.response) reject(e.response.data);
        else reject(e.toString());
      });
  });
};



export  const getHanpointKey=(id)=>{ 
    return new Promise((r,rej)=>{
    Axios.get(`/admin/getHandpointKey?merchant_id=${id}`)
      .then((res) => {
        if (res.status == "200" && res.data) {
          r(res.data);
        }
       
      })
      .catch((e: AxiosError) => {
        if (e.response) rej(e.response.data)
        else rej(e.toString());
      });
    });


}


export  const saveIFXCredentials =(data)=>{ 
  return new Promise((r,rej)=>{
  Axios.post(`/admin/saveIFxCredentials`,data)
    .then((res) => {
      if (res.status == "200" && res.data) {
        r(res.data);
      }
     
    })
    .catch((e: AxiosError) => {
      if (e.response) rej(e.response.data)
      else rej(e.toString());
    });
  });


}


export  const updateIFXCredentials =(data)=>{ 
  return new Promise((r,rej)=>{
  Axios.put(`/admin/updateIFxCredentials`,data)
    .then((res) => {
      if (res.status == "200" && res.data) {
        r(res.data);
      }
     
    })
    .catch((e: AxiosError) => {
      if (e.response) rej(e.response.data)
      else rej(e.toString());
    });
  });


}

export  const updateHanpointKey=(params)=>{ 
  return new Promise((resolve, reject) => {
    Axios.put(`/admin/updateHandpointKey`,params)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e: AxiosError) => {
        if (e.response) reject(e.response.data);
        else reject(e.toString());
      });
  });


}

export const changeEcomStatus=(params)=>{ 
  return new Promise((resolve, reject) => {
    Axios.put(`/admin/updateEcomStatus`, params)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e: AxiosError) => {
        if (e.response) reject(e.response.data);
        else reject(e.toString());
      });
  });

}


export const setTestingMerchant = (params) => {
  return new Promise((resolve, reject) => {
    Axios.post(`/admin/setTesting`, params)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e: AxiosError) => {
        if (e.response) reject(e.response.data);
        else reject(e.toString());
      });
  });
};

export const makeRefundTxn = (params) => {
  return new Promise((resolve, reject) => {
    Axios.post(`/admin/refundTxn`, params)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e: AxiosError) => {
        if (e.response) reject(e.response.data);
        else reject(e.toString());
      });
  });
};



export const getAllDevice = () => {
  return new Promise((resolve, reject) => {
    Axios.get("/admin/getAllDevice")
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const getRole = (callback) => {
  Axios.get("/admin/getRole")
    .then((res) => {
      if (res.status == "200" && res.data) {
        callback({ res: res.data.roles });
      }
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
      else callback({ err: e.toString() });
    });
};
export const addRole = (params, callback) => {
  Axios.post("/admin/addRole", params)
    .then((res) => {
      if (res.status == "200" && res.data) {
        callback({ res: res.data.success });
      }
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
      else callback({ err: e.toString() });
    });
};

export const deleteRole = (id, callback) => {
  Axios.delete(`/admin/deleteRole?id=${id}`)
    .then((res) => {
      if (res.status == "200" && res.data) {
        callback({ res: res.data.success });
      }
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
      else callback({ err: e.toString() });
    });
};

export const updateCurrencyAccount = (params) => {
  return new Promise((resolve, reject) => {
    Axios.put("/admin/updateCurrencyAccount", { ...params })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const deletCurrencyAccount = (id) => {
  return new Promise((resolve, reject) => {
    Axios.delete(`/admin/deleteCurrencyAccount?id=${id}`)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const getMerchantCredentials = (id) => {
  return new Promise((resolve, reject) => {
    Axios.get(`/admin/getCredentials?merchant_id=${id}`)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const addOrUpdateMerchantCredentials = (params) => {
  return new Promise((resolve, reject) => {
    Axios.post(`/admin/addEcomKey`, { ...params })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const UpdateMerchantCredentials = (params) => {
  return new Promise((resolve, reject) => {
    Axios.put(`/admin/updateEcomKey`, { ...params })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e:AxiosError) => {
        reject(e.response?e.response.data:e.toString());
      });
  });
};


export const deleteMerchantCredentials = (id) => {
  return new Promise((resolve, reject) => {
    Axios.delete(`/admin/deleteEcomKey?id=${id}`, )
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e:AxiosError) => {
        reject(e.response?e.response.data.toString():e.toString());
      });
  });
};

export const getAllCards = (id) => {
  return new Promise((resolve, reject) => {
    Axios.get(`/admin/getAllCards?merchant_id=${id}`)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};



export const getAllMerchant = (params) => {
  return new Promise((resolve, reject) => {
    Axios.get("/admin/getMerchantAll")
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e:AxiosError) => {
        reject(e.response?e.response.data:e.toString());
      });
  });
};

export const activateRailsBank = (id) => {
  return new Promise((resolve, reject) => {
    Axios.get(`/admin/activateRailsBank?merchant_id=${id}`)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        
      })
      .catch((e:AxiosError) => {
        reject(e.response?e.response.data:e.toString());
      });
  });
};

export const getAllLedgerAccount = (params) => {
  return new Promise((resolve, reject) => {
    Axios.post(`/admin/getAllSyncLedgers`, { ...params })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const getMerchant = (id) => {
  return new Promise((resolve, reject) => {
    Axios.get(`/admin/getMerchant?merchant_id=${id}`)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const createAccountForLedger = (params) => {
  return new Promise((resolve, reject) => {
    Axios.post(`/admin/create-account`, { ...params })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const addMerchant = (params) => {
  return new Promise((resolve, reject) => {
    Axios.post("/admin/add-merchant", params,{ headers: { 'Content-Type':'application/json','dataType':'json'}})
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e: AxiosError) => {
        if (e.response&&e.response.status == 400) {
          reject(e.response.data);
        }
      });
  });
};

export const updateMerchant = (id, params) => {
  return new Promise((resolve, reject) => {
    Axios.put(`/admin/updateMerchant?merchant_id=${id}`, {
      ...params,
    })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e:AxiosError) => {
        reject(e.response?e.response.data:e.toString());
      });
  });
};

export const updateMerchantBussiness = (id, params) => {
  return new Promise((resolve, reject) => {
    Axios.put(`/admin/updateMerchantBussiness?merchant_id=${id}`, {
      ...params,
    })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};
export const updateMerchantBank = (id, params) => {
  return new Promise((resolve, reject) => {
    Axios.put(`/admin/updateMerchantBank?merchant_id=${id}`, {
      ...params,
    })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const sendTxnOtp = (params) => {
  return new Promise((resolve, reject) => {
    Axios.post(`/admin/txnOtp`,params)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
       else reject(res.statusText);
      })
      .catch((e) => {
        reject(e.response?.data??e.toString());
      });
  });

};

export const tranferTxn = (params, callback) => {
  Axios.post("/admin/transferTxn", params)
    .then((res) => {
      if (res.status == 200 && res.data) callback({ res: true });
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
      else callback({ err: e.toString() });
    });
};

export const getCountries = () => {
 
    return new Promise((resolve, reject) => {
      Axios.get(`/admin/countries`)
        .then((res) => {
          if (res.status == "200" && res.data) {
            resolve(res.data);
          }
         else reject(res.statusText);
        })
        .catch((e) => {
          reject(e.toString());
        });});
}



export const deleteMerchant = (id) => {
  return new Promise((resolve, reject) => {
    Axios.delete(`/admin/deleteMerchant?merchant_id=${id}`)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e: AxiosError) => {
        reject(e.response?e.response.data:e.toString());
      });
  });
};

export const updateMerchantPayment = (params) => {
  return new Promise((resolve, reject) => {
    Axios.put("/admin/updateMerchantPayment", {
      params: params,
    })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const addRegDevice = (params) => {
  return new Promise((resolve, reject) => {
    Axios.post("/admin/addDeviceReg", {
      ...params,
    })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e:AxiosError) => {
        reject(e.response?e.response.data.err.toString():e.toString());
      });
  });
};

export const upateRegDevice = (params) => {
  return new Promise((resolve, reject) => {
    Axios.put("/admin/updateDeviceReg", {
      ...params,
    })
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const deleteRegDevice = (id) => {
  return new Promise((resolve, reject) => {
    Axios.delete(`/admin/deleteDeviceReg?device_id=${id}`)
      .then((res) => {
        if (res.status == "200" && res.data) {
          resolve(res.data);
        }
        reject(res.statusText);
      })
      .catch((e) => {
        reject(e.toString());
      });
  });
};

export const getAllPartner = (callback) => {
  Axios.get(`admin/getAdminPartner`)
    .then((res) => {
      if (res.status == 200 && res.data) callback({ res: res.data.partners });
      else callback({});
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
      else callback({ err: e.toString() });
    });
};

export const sendEmail = (email, callback) => {
  axios({ url: `${baseURL}/admin/sendOtp`, method: "post", data: { email } })
    .then((res) => {
      if (res.status == 200 && res.data.success) {
        callback({ res: true });
      }
    })
    .catch((e: AxiosError) => {
      if (e.response.data) callback({ err: e.response.data });
      else callback({ err: e.response.statusText });
    });
};

export const forgotPassword = (data1, callback) => {
  axios({
    url: `${baseURL}/admin/forgotPassword`,
    method: "post",
    data: { ...data1 },
  })
    .then((res) => {
      if (res.status == 200 && res.data.success) {
        callback({ res: true });
      }
    })
    .catch((e: AxiosError) => {
      if (e.response.data) callback({ err: e.response.data });
      else callback({ err: e.response.statusText });
    });
};

export const getAllTransactions = (params, callback) => {
  Axios.post("/admin/getAllTransactions", {
    ...params,
  })
    .then((res) => {
      if (res.status == "200" && res.data) {
        callback({ res: res.data });
      } else callback({ err: { message: res.statusText } });
    })
    .catch((e:AxiosError) => {
      callback({ err: { message:e.response?e.response.data: e.toString() } });
    });
};

export const updateAdmin = (params, callback) => {
  Axios.put(`admin/updateAdmin`, { ...params })
    .then((res) => {
      if (res.status == 200 && res.data) callback({ res: res.data });
      else callback({ err: { message: res.data.message } });
    })
    .catch((e: AxiosError) => callback({ err: e.response.data }));
};

export const uploadImage = (body, callback) => {
  Axios.put(`admin/upload`, body)
    .then((res) => {
      if (res.status == 200 && res.data) callback({ res: res.data });
    })
    .catch((e) => callback({ err: { message: getErrorString(e) } }));
};

export const getAdminDetails = (id,callback) => {
  Axios.get(`admin/getAdminDetails?id=${id}`)
    .then((res1) => {
      if (res1.status == 200 && res1.data) {
        callback({ res: { adminDetails: res1.data.adminDetails } });
      } else {
        callback({});
      }
    })
    .catch((e: AxiosError) => {
      console.log(e);
      callback&&callback({ err: { message: e.response?e.response.data:e.toString() } });
    });
};

export const getPaymentGateway = (callback) => {
  Axios.get("admin/getPaymentGateway")
    .then((res) => {
      if (res.status == 200 && res.data) callback({ res: res.data.gateway });
    })
    .catch((e: AxiosError) => {
      if (e.response) {
        callback({
          err: e.response.data ? e.response.data : e.response.status,
        });
        console.log(e.response);
      }
    });
};
export const getTransactionFee = (id, callback) => {
  Axios.get(`admin/getTransactionFee?merchant_id=${id}`)
    .then((res1) => {
      if (res1.status == 200 && res1.data)
        callback({ res: res1.data.transactionFee });
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
    });
};
export const getTransferFee = (callback) => {
  Axios.get(`admin/getTransferFee`)
    .then((res) => {
      if (res.status == 200 && res.data)
        callback({ res: res.data.transferFee });
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
    });
};

export const addOrUpdateTransactionFee = (params, callback) => {
  Axios.post(`admin/mangeTransactionFee`, params)
    .then((res) => {
      if (res.status == 200 && res.data) callback({ res: res.data.success });
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
    });
};

export const addTransferFee = (params, callback) => {
  Axios.post(`admin/addTransferFee`, params)
    .then((res) => {
      if (res.status == 200 && res.data) callback({ res: res.data.success });
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
    });
};
export const deleteTransactionfee = (id, callback) => {
  Axios.delete(`admin/deleteTxnFee?id=${id}`)
    .then((res) => {
      if (res.status == 200 && res.data) callback({ res: res.data.success });
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
    });
};

export const deleteTransferfee = (id, callback) => {
  Axios.delete(`admin/deleteTransferFee?id=${id}`)
    .then((res) => {
      if (res.status == 200 && res.data) callback({ res: res.data.success });
    })
    .catch((e: AxiosError) => {
      if (e.response) callback({ err: e.response.data });
    });
};

export const addPartner = (params) => {
  return new Promise((resolve, reject) => {
    Axios.post(`admin/addAdminPartner`, params)
      .then((res) => {
        if (res.status == 200 && res.data) resolve(res.data);
      })
      .catch((e: AxiosError) => {
        if (e.response) reject(e.response.data);
        else reject(e);
      });
  });
};

export const updatePartner = (params) => {
  return new Promise((resolve, reject) => {
    Axios.put(`admin/updateAdminPartner`, params)
      .then((res) => {
        if (res.status == 200 && res.data) resolve(res.data);
      })
      .catch((e: AxiosError) => {
        if (e.response) reject(e.response.data);
        else reject(e);
      });
  });
};

export const updateCardStatus=(id,params,callback)=>{ 
  Axios.put(`admin/updateCard?id=${id}`,params).then(res=>{ 
    if(res.status==200 && res.data)callback({res:true});
  }).catch((e:AxiosError)=>{ callback({err: e.response?e.response.data:e.toString()}) })
}

export const udpateTransferFee=(value,callback)=>{ 
   Axios.put(`admin/updateTransferFee`,value).then(res=>{ 
     if(res.status==200 && res.data)callback({res:true});
   }).catch((e:AxiosError)=>{ callback({err: e.response?e.response.data:e.toString()}) })
}

//currency  fees_detail 