import { gql } from "@apollo/client";

export const LOGIN_MERCHANT = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      temp_token
      token
    }
  }
`;

export const VERIFY_MERCHANT = gql`
  mutation verify($otp: String!, $temp_token: String!) {
    verify(otp: $otp, temp_token: $temp_token) {
      token
    }
  }
`;

export const UPDATE_MERCHANT = gql`
  mutation updateMerchant($merchant: updateMerchantInput) {
    merchant: updateMerchant(merchant: $merchant) {
      merchant_id
    }
  }
`;

export const UPDATE_MERCHANT_BANK = gql`
  mutation update($bank: updateMerchantBankInput) {
    merchant_bank: updateMerchantBank(merchant_bank: $bank) {
      bank_name
    }
  }
`;

export const UPDATE_MERCHANT_BUSINESS = gql`
  mutation updateMerchantBusiness($business: updateMerchantBusinessInput) {
    merchant_business: updateMerchantBusiness(merchant_business: $business) {
      country
    }
  }
`;

export const UPLOAD_LOGO = gql`
  mutation uploadLogo($file: Upload!) {
    is_uploaded: uploadLogo(file: $file)
  }
`;

export const GET_QRCODE = gql`
  mutation {
    qrcode_data: enableTwoFactorAuth {
      qrcode
    }
  }
`;

export const ENABLE_GOOGLE_AUTHENTICATOR = gql`
  mutation verify($otp: String) {
    merchant: verify(otp: $otp) {
      merchant {
        google_oauth_login
      }
    }
  }
`;

export const DISABLE_GOOGLE_AUTHENTICATOR = gql`
  mutation {
    merchant: disableTwoFactorAuth {
      google_oauth_login
    }
  }
`;

export const CHANGE_PASSWORD = gql`
  mutation updatePassword($oldPassword: String!, $newPassword: String!) {
    passwordUpdated: updatePassword(old_password: $oldPassword, new_password: $newPassword)
  }
`;

export const ADD_INVOICE = gql`
  mutation addInvoice($invoice_details: invoiceDetailsInput!, $item_details: [invoiceItemsInput]!) {
    invoice: addInvoice(invoice_details: $invoice_details, item_details: $item_details) {
      id
    }
  }
`;

export const UPDATE_INVOICE = gql`
  mutation updateInvoice($invoice_details: invoiceDetailsInput, $item_details: [invoiceItemsInput]) {
    invoice: updateInvoice(invoice_details: $invoice_details, item_details: $item_details) {
      id
    }
  }
`;

export const DELETE_INVOICE = gql`
  mutation deleteInvoice($id: String!) {
    deleted: deleteInvoice(id: $id)
  }
`;

export const ADD_CLIENT = gql`
  mutation addClient($client: addClientInput!) {
    client: addClient(client: $client) {
      id
      client_name
    }
  }
`;
