import { gql } from "@apollo/client";

export const GET_MERCHANT = gql`
  query getMerchant {
    merchant: getMerchant {
      merchant_id
      zipcode
      currency
      wallet_id
      name
      phone
      phone_code
      address
      email
      google_oauth_login
      country
      city
      timezone
    }
  }
`;

export const GET_MERCHANT_BANK = gql`
  query {
    merchant_bank: getMerchantBank {
      bank_name
      acct_holder_name
      bic_swift
      iban
      acct_no
      regno_sortcode
    }
  }
`;

export const GET_MERCHANT_BUSINESS = gql`
  query {
    merchant_business: getMerchantBusiness {
      comp_name
      comp_no
      tax_no
      reg_date
      phone_code
      phone_no
      country
      reg_address
      busns_city
      busns_zipcode
      website
    }
  }
`;
export const GET_TRANSACTIONS = gql`
  query getAdminTransactions($start_date: String,  $end_date: String,$merchant_id:Int) {
    transactions: getAdminTransactions(start_date: $start_date,  end_date:$end_date,merchant_id:$merchant_id) {
      id
      merchant_id
      status
      device_seriel
      currency_account_id
      date_of_transaction
      order_id
      paid_type
      pay_method
      payment_mode
      amount
      txn_no
      gateway_id
      refund_status
      refund_type
      pay_type
      txn_type
      txn_ref
      message
      currency
      endtoendid
      refunded_amount
      transfer_type
      base_currency
      paid_status
      transfer_fee
      transfer_amount
      exchange_rate
      margin_amount
      benef_id
      
    }
  }
`;
export const GET_DEVICE_TRANSACTIONS = gql`
  query getTransactions($startDate: String!, $endDate: String!) {
    device_transactions: getTransactions(start_date: $startDate, end_date: $endDate, payment_mode: 2) {
      id
      status
      currency_account_id
      device_seriel
      date_of_transaction
      order_id
      paid_type
      pay_method
      payment_mode
      amount
      txn_no
      refund_status
      refund_type
      pay_type
      message
      currency
    }
  }
`;

export const GET_ACCOUNTS = gql`
  query getAccounts {
    accounts: getAccounts {
      id
      currencycode
      current_balance
      account_name
      account_id
    }
  }
`;

export const GET_DEVICES = gql`
  query getDevices {
    devices: getDevices {
      id
      device_name
      serial
      device_currency
      status
      type
    }
  }
`;

export const GET_INVOICES = gql`
  query getInvoices($startDate: String!, $endDate: String!) {
    invoices: getInvoices(start_date: $startDate, end_date: $endDate) {
      id
      notes
      invoice_number
      invoice_date
      client_uniqid
      base_amount
      total_amount
      currency
      tax
      discount
      is_paid
      due_date
      redirect_type
      client_name
    }
  }
`;

export const GET_CLIENTS = gql`
  query getClients {
    clients: getClients {
      id
      client_name
    }
  }
`;

export const GET_CLIENT_DETAILS = gql`
  query getClientDetails($clientId: Int) {
    client_details: getClients(id: $clientId) {
      id
      client_name
      client_email
      address
      city
      country
      zip_code
      phone_code
      phone
    }
  }
`;

export const GET_INVOICE_ITEMS = gql`
  query getInvoiceItems($invoiceId: String!) {
    invoice_items: getInvoiceItems(invoice_id: $invoiceId) {
      id
      item
      description
      rate
      quantity
    }
  }
`;

export const GET_CURRENCIES = gql`
  query getCurrencies {
    currencies: getAccounts {
      currencycode
    }
  }
`;
