import React,{ Fragment } from "react";
import Avatar from "@components/avatar";
import { toast } from "react-toastify";
import { X, Check, Bell } from "react-feather";

const Toast = ({ title, message, type, ...props }) => {
  if (type == "error") type = "danger";
  return (
    <Fragment>
      <div className="toastify-header">
        <div className="title-wrapper">
          <Avatar
            size="sm"
            color={type}
            icon={type == "success" ? <Check size={12} /> : type == "danger" ? <X size={12} /> : <Bell size={12} />}
          />
          <h6 className="toast-title">{title}</h6>
        </div>
      </div>
      <div className="toastify-body">
        <span role="img" aria-label="toast-text">
          {message}
        </span>
      </div>
    </Fragment>
  );
};

const notification = ({ title, message, type }) => {
  return toast[type](<Toast title={title} message={message} type={type} />, { hideProgressBar: true });
};

export default notification;
