import React, { useContext, Fragment } from "react";
import { Page, Text, View, Document, StyleSheet, BlobProvider, Font, Image } from "@react-pdf/renderer";
import fontFile from "../../assets/fonts/Montserrat-Regular.ttf";
import { GET_MERCHANT } from "@graphql/queries";
import printDoc from "print-js";
import { downloadDoc } from "@utils";
import { useQuery, useReactiveVar } from "@apollo/client";
import { TransactionsTableContext } from ".";
import moment from "moment";
import { graphqlServerUrl } from "../../App";


Font.register({
  family: "Montserrat",
  src: fontFile,
});

const _ = StyleSheet.create({
  h1: { fontSize: "18pt", fontWeight: "bold" },
  h6: { fontSize: "12pt" },
  h5: { fontSize: "16pt" },
  p: { fontSize: "11pt", color: "#626262", marginVertical: "5pt" },
  br1: { marginVertical: "10pt" },
  br2: { marginVertical: "5pt" },
  page: {
    flexDirection: "column",
    backgroundColor: "#fff",
    paddingVertical: 50,
    paddingHorizontal: 50,
    fontFamily: "Montserrat",
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headerRight: {
    flexDirection: "column",
    alignItems: "flex-end",
  },
  details: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  detailsRight: {
    alignItems: "flex-end",
  },
  address: {
    textOverflow: "wrap",
    width: "120pt",
  },
  addressRight: {
    textOverflow: "wrap",
    width: "120pt",
    textAlign: "right",
  },
  table: {
    border: "1pt solid #DCDCDC",
    padding: "10pt",
  },
  tr: {
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  td: {
    width: "200pt",
  },
  tableDesc: {
    width: "260pt",
    color: "red",
  },
});

const { H1, H5, H6, P, BR1, BR2, TR, TD } = {
  H1: ({ children }) => <Text style={_.h1}>{children}</Text>,
  H6: ({ children }) => <Text style={_.h6}>{children}</Text>,
  H5: ({ children }) => <Text style={_.h5}>{children}</Text>,
  P: ({ children }) => <Text style={_.p}>{children}</Text>,
  BR1: ({ children }) => <View style={_.br1}>{children}</View>,
  BR2: ({ children }) => <View style={_.br2}>{children}</View>,
  TR: ({ children }) => <View style={_.tr}>{children}</View>,
  TD: ({ children }) => <View style={_.td}>{children}</View>,
};

const PDF = ({ user, transaction }) => {
  const logo = `${graphqlServerUrl}/images/merchant/${user.merchant_id}.png}`;
  return (
    <Document>
      <Page size="A4" style={_.page}>
        <View style={_.header}>
          <View>
            <Image style={{ width: "105pt" }} src={logo} />
          </View>

          <View style={_.headerRight}>
            <H1>Transaction Details</H1>
            <BR1 />
            <H6>ORDER ID</H6>
            <P>{transaction.order_id}</P>
            <BR2 />
            <H6>ORDER DATE</H6>
            <P>{moment(transaction.date_of_transaction).format("YYYY-MM-DD")}</P>
          </View>
        </View>
        <BR1 />
        <BR1 />
        <View style={_.details}>
          <View>
            <H6>Recipient</H6>
            <BR1 />
            <P>{user.name}</P>
            <View style={_.address}>
              <P>{user.address}</P>
            </View>
            <BR2 />
            <P>{user.email}</P>
            <P>
              {user.phone_code} {user.phone}
            </P>
          </View>

          <View style={_.detailsRight}>
            <H6>Zotto LTD</H6>
            <BR1 />
            <View style={_.addressRight}>
              <P>Staines Upon Thames, TW18 3BA, UK</P>
            </View>
            <BR2 />
            <P>info@zotto.io</P>
            <P>+44 808 169 7040</P>
          </View>
        </View>
        <BR1 />
        <BR1 />
        <View style={_.table}>
          <TR>
            <TD>
              <P>Transaction ID</P>
            </TD>
            <P>{transaction.txn_no}</P>
          </TR>
          <BR2 />
          <TR>
            <TD>
              <P>Status</P>
            </TD>
            <P>{transaction.status == 0 ? "Failed" : transaction.status == 1 ? "Success" : "Pending"}</P>
          </TR>
          <BR2 />
          <TR>
            <TD>
              <P>Amount</P>
            </TD>
            <P>
              {transaction.amount} {transaction.currency}
            </P>
          </TR>
          <BR2 />
          <TR>
            <TD>
              <P>Transaction Type</P>
            </TD>
            <P>{transaction.paid_type == "cr" ? "Credit" : "Debit"}</P>
          </TR>
          <BR2 />
          <TR>
            <TD>
              <P>Fees</P>
            </TD>
            <P>{transaction.fees}</P>
          </TR>
          <BR2 />
          <TR>
            <TD>
              <P>Fixed Fees</P>
            </TD>
            <P>{transaction.fixedfee ? transaction.fixedfee : "0.00"}</P>
          </TR>
          <BR2 />
          <TR>
            <TD>
              <P>Description</P>
            </TD>
            <View style={_.tableDesc}>
              <P>{transaction.message}</P>
            </View>
          </TR>
        </View>
        <BR1 />
        <BR1 />
        <H6>Feel free to reach us at support@zotto.io, if you have any questions.</H6>
        <P>
          {
            "Zotto LTD (Company No. 09548832) is registered by the Financial Conduct Authority to conduct payment activities."
          }
        </P>
      </Page>
    </Document>
  );
};

export default function TransactionPDF() {
  const { actionQueue, setActionQueue, users } = useContext(TransactionsTableContext);
 


  const deleteResolvedAction = () => {
    let temp = JSON.parse(JSON.stringify(actionQueue));
    temp.shift();
    setActionQueue(temp);
  };

  const handleAction = (data, action) => {
    if (!data) return;
    if (action == "print") {
      printDoc(data);
      deleteResolvedAction();
    } else if (action == "download") downloadDoc(data, () => deleteResolvedAction());
  };

  if ( !users) return null;
  return (
    <div style={{ display: "none" }}>
      {actionQueue.length > 0 ? (
        <BlobProvider document={<PDF user={users} transaction={actionQueue[0].transaction} />}>
          {({ url, loading }) => {
            return loading ? null : (
              <Fragment>{setTimeout(() => handleAction(url, actionQueue[0].action), 0) ? null : null}</Fragment>
            );
          }}
        </BlobProvider>
      ) : null}
    </div>
  );
}
