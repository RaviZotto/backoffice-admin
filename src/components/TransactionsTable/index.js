import React,{ Fragment, useState, createContext, useMemo, useEffect } from "react";
import SideBar from "./Sidebar";
import RefundModal from "./RefundModal";
// ** Third Party Components
import { ChevronDown, Eye, DownloadCloud, Printer } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge, Spinner, Button } from "reactstrap";
import moment from "moment";


// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import TransactionPDF from "./TransactionPDF";
import CSV from "./CSV";

export const TransactionsTableContext = createContext();

const Table = ({
  transactions,
  dates,
  setDates,
  selectedDevice = {},
  selectedAccount = {},
  noHeader = false,
  rowsCount = false,
  users,
  type,
}) => {
  const [searchQuery, setSearchQuery] = useState();
  const [statusQuery, setStatusQuery] = useState();
  const [actionQueue, setActionQueue] = useState([]);
  const [selectedTransaction, setSelectedTransaction] = useState({});
  const [statementCSV, setStatementCSV] = useState({});
  const [refundModal,setRefundModal] = useState(false);
  const columns = [
    {
      name: "Date",
      selector: "date_of_transaction",
      sortable: true,
      minWidth: "110px",
      cell: (row) => {
        let date = moment(row.date_of_transaction);
        return (
          <Fragment>
            <div className="mt-1">
              <h6 className="font-small-3">{date.format("YYYY-MM-DD")}</h6>
              <h6 className="text-secondary font-small-2">{date.format("HH:mm:ss")}</h6>
            </div>
          </Fragment>
        );
      },
    },
    {
      name: "Status",
      selector: "status",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
          <div
            className={row.status == 0 ? "bg-danger" : row.status == 1 ? "bg-success" : "bg-primary"}
            style={{
              height: "10px",
              width: "10px",
              borderRadius: "50%",
              display: "inline-block",
              marginRight: "5px",
            }}
          />
          <span>{row.status == 0 ? "Failed" : row.status == 1 ? "Success" : "Pending"}</span>
        </Fragment>
      ),
    },
    {
      name: "Transaction ID",
      selector: "txn_no",
      sortable: false,
      minWidth: "230px",

      cell: (row) => {
        return <div className="font-small-3 text-break">{row.txn_no}</div>;
      },
    },
    // {
    //   name: "Order ID",
    //   selector: "order_id",
    //   sortable: false,
    //   minWidth: "130px",
    //   cell: (row) => {
    //     return <div className="font-small-3">{row.order_id}</div>;
    //   },
    // },
    {
      name: "Payment Type",
      selector: "pay_method",
      sortable: false,
      minWidth: "200px",
      cell: (row) => <Badge color="primary">{row.pay_method}</Badge>,
    },
    {
      name: "Amount",
      selector: "amount",
      sortable: false,
      cell: (row) => (
        <h6 className={`${row.paid_type == "cr" ? "text-success" : "text-danger"} font-weight-bolderer`}>
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: row.currency,
          })
            .format(row.amount)
            .replace(/^(\D+)/, "$1 ")}
        </h6>
      ),
    },
    {
      name: "Action",
      minWidth: "110px",
      selector: "",
      sortable: true,
      onclick:(e)=>alert('hello'),
      cell: (row) => {
        let insideQueue = actionQueue.find((el) => el.transaction.id == row.id);
        let action = insideQueue ? insideQueue.action : false;
        return (
          <div className="column-action d-flex align-items-center">
            <Eye size={17} className="cursor-pointer" onClick={() => setSelectedTransaction(row)} />
            { type=='dashboard'?<Fragment/>:(
              <>
            {action == "print" ? (
              <Spinner size="sm" className="mx-1" />
            ) : (
              <Printer
                size={17}
                className="mx-1 cursor-pointer"
                onClick={() => setActionQueue([...actionQueue, { transaction: row, action: "print" }])}
              />
            )}
            {action == "download" ? (
              <Spinner size="sm" />
            ) : (
              <DownloadCloud
                size={17}
                className="cursor-pointer"
                onClick={() => setActionQueue([...actionQueue, { transaction: row, action: "download" }])}
              />
            )}
            </>)
             }
          </div>
        );
      },
    },
  ];

  // Data filtering function according to filters present in table header.
  const dataToRender = () => {
    let tempData = transactions;
    if (selectedAccount.id) tempData = tempData.filter((el) => el.currency_account_id == selectedAccount.id);
    if (selectedDevice.id) tempData = tempData.filter((el) => el.device_seriel == selectedDevice.serial);
    if (searchQuery)
      tempData = tempData.filter((el) => {
        let temp = JSON.parse(JSON.stringify(el));
        let sq = searchQuery.toLowerCase();
        temp.my_txn_type = el.paid_type == "cr" ? "credit" : "debit";
        temp.my_status = el.status == "1" ? "success" : el.status == "0" ? "failed" : "pending";
        temp.pay_method = `${temp.pay_method}`.toLowerCase();
        let searchFields = ["my_status", "txn_no", "order_id", "pay_method", "amount", "my_txn_type"];
        for (let field of searchFields)
          if (`${temp[field]}`.includes(sq) || `${temp[field]}`.startsWith(sq)) return true;
        return false;
      });
    if (statusQuery) tempData = tempData.filter((el) => el.status == statusQuery);
    return tempData;
  };

  const providerValue = useMemo(
    () => ({
      dates,
      setDates,
      searchQuery,
      setSearchQuery,
      statusQuery,
      setStatusQuery,
      transactions,
      users,
      selectedAccount,
      selectedTransaction,
      setSelectedTransaction,
      actionQueue,
      setActionQueue,
      statementCSV,
      setStatementCSV,
      refundModal,
      setRefundModal
     
    }),
    [dates,refundModal,statementCSV, selectedAccount, actionQueue, searchQuery, statusQuery, selectedTransaction]
  );

  return (
    <TransactionsTableContext.Provider value={providerValue}>
      <div>
        <div className="invoice-list-wrapper">
          <Card>
         
            <div className="invoice-list-dataTable">
              <DataTable
                className="react-dataTable"
                noHeader={type=='dashbaord'?false  :true}
                header={<div><Button.Ripple >View All Transactions </Button.Ripple> </div>}
                pagination
                columns={columns}
                paginationPerPage={rowsCount || 10}
                responsive={true}
                sortIcon={<ChevronDown />}
                data={dataToRender()}
              />
            </div>
          </Card>
        </div>
      </div>
      {statementCSV.loader&&<CSV />}
      <SideBar />
      <TransactionPDF />
     { selectedTransaction.id &&<RefundModal />}
    </TransactionsTableContext.Provider>
  );
};

export default Table;
