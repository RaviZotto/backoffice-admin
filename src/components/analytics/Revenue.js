import React,{ useEffect, useState } from 'react'
import axios from 'axios'
import Chart from 'react-apexcharts'
import { Settings } from 'react-feather'
import { Card, CardHeader, CardTitle, CardBody, CardText } from 'reactstrap'
import moment from 'moment'

const Revenue = props => {
  const {transactions,dates,currency} = props;
  const [credit, setCredit] = useState([0, 0, 0, 0, 0, 0, 0]);
  const [debit, setDebit] = useState([0, 0, 0, 0, 0, 0, 0]);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    // let dayOfWeek = {
    //   0: "Sun",
    //   1: "Mon",
    //   2: "Tue",
    //   3: "Wed",
    //   4: "Thu",
    //   5: "Fri",
    //   6: "Sat",
    // };
    let obj = {},
      sortedDatesArr = [],
      today = moment(dates.start_date).format("YYYY-MM-DD"),
      newEarningsData = [],
      newExpenseData = [],
      newDaysArr = [];


      transactions.forEach((ele) => {
      let d = moment(ele.date_of_transaction).format("YYYY-MM-DD");
      if (!obj[d]) {
        obj[d] = {
          debit: ele.paid_type == "dr" ? ele.amount : 0,
          credit: ele.paid_type == "cr" ? ele.amount : 0,
        };
        sortedDatesArr.unshift(d);
      } else {
        
          if (ele.paid_type=='cr') obj[d].credit  += ele.amount;
          else obj[d].debit += ele.amount;
      }
    });

    if(sortedDatesArr.length>7)sortedDatesArr=sortedDatesArr.slice(0,7);
    sortedDatesArr.forEach((ele,i) => {
      
      newEarningsData.push(
        Math.round((parseFloat(obj[ele].credit) + Number.EPSILON) * 100) / 100
      );
      newExpenseData.push(
        0 -
          Math.round((parseFloat(obj[ele].debit) + Number.EPSILON) * 100) / 100
      );
     
    });
    setCredit(newEarningsData);
    setDebit(newExpenseData);
    setCategories(sortedDatesArr);
  }, [transactions]);


  const options = {
      chart: {
        toolbar: { show: true },
        zoom: { enabled: false },
        type: 'line',
        offsetX: -10
      },
      stroke: {
        curve: 'smooth',
    
        width: [4, 3]
      },
      legend: {
        show: false
      },
      colors: [props.danger, props.success],
      fill: {
        type: 'gradient',
        gradient: {
          shade: 'dark',
          inverseColors: false,
          gradientToColors: [props.danger, props.success],
          shadeIntensity: 1,
          type: 'horizontal',
          opacityFrom: 1,
          opacityTo: 1,
          stops: [0, 100, 100, 100]
        }
      },
      markers: {
        size: 0,
        hover: {
          size: 5
        }
      },
      xaxis: {
        labels: {
          style: {
            colors: '#b9b9c3',
            fontSize: '1rem'
          }
        },
        axisTicks: {
          show: false
        },
        categories:categories,
        axisBorder: {
          show: false
        },
        tickPlacement: 'on'
      },
      yaxis: {
        tickAmount: 5,
        labels: {
          style: {
            colors: '#b9b9c3',
            fontSize: '1rem'
          },
          formatter(val) {
            return val > 999 ? `${(val / 1000).toFixed(0)}k` : val
          }
        }
      },
      grid: {

        borderColor: '#e7eef7',
        padding: {
          top: -20,
          bottom: -10,
          left: 20
        }
      },
      tooltip: {
        x: { show: true }
      }
    },
    series = [
      {
        name: 'Debit',
        data: debit
      },
      {
        name: 'Credit',
        data: credit
      }
    ]
  return (
    <Card>
      <CardHeader>
        <CardTitle tag='h4'>Sales</CardTitle>
        <Settings size={18} className='text-muted cursor-pointer' />
      </CardHeader>
      <CardBody>
        <div className='d-flex justify-content-start mb-2'>
          <div className='mr-1'>
            <CardText className='mb-50'>Credit</CardText>
            <h3 className='font-weight-bolder'>
              <span className='text-success'>{ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(credit.reduce((acc,curr)=>acc+curr,0))
        .replace(/^(\D+)/, "$1 ")}</span>
            </h3>
          </div>
          <div>
            <CardText className='mb-50'>Debit</CardText>
            <h3 className='font-weight-bolder'>
             <span className='text-danger'>{ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(debit.reduce((acc,curr)=>acc+curr,0))
        .replace(/^(\D+)/, "$1 ")}</span>
            </h3>
          </div>
        </div>
        <Chart options={options} series={series} type='line' height={190} />
      </CardBody>
    </Card>
  ) 
}
export default Revenue
