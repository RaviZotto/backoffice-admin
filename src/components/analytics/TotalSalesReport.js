import React, { useState,useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'

// ** Third Party Components
import { Chart } from 'react-chartjs-2'
import RevenueReport from './RevenueReport'
import Loading from "@core/components/spinner/Loading-spinner";
import { ThemeColors } from '@src/utility/context/ThemeColors'
import { useContext } from 'react'
import { Row, Col} from 'reactstrap';
import '@styles/react/libs/charts/apex-charts.scss'
import '@styles/base/pages/dashboard-ecommerce.scss'
import TransactionTable from './TransactionTable';
import CardTransactions from '../ui-elements/cards/advance/CardTransactions';
import StatsCard from '../ui-elements/cards/statistics/StatsCard'
import SessionByDevice from '../ui-elements/cards/analytics/SessionBySales'
import SessionByDevice1 from '../ui-elements/cards/analytics/SessionByDevice1'
import SessionByDevice2 from '../ui-elements/cards/analytics/SessionByDevice2'
import SessionByDevice3 from '../ui-elements/cards/analytics/SessionByDevice3'
import Styled from 'styled-components'
import CSV from './CSV';
import { TotalSalesContext } from '../../views/SalesReport/TotalSalesReport'
import moment from 'moment';
function SalesReport() {
  const {transactions,currency,merchantId,report,dates}=useContext(TotalSalesContext)
  const [statementCSV, setStatementCSV]=useState({loader:false,data:{}});
  const [txn,setTxn]=useState([]);
  useEffect(()=>{
    let txnArray= [...transactions]
       txnArray=txnArray.filter(e=>e.merchant_id==merchantId.value);
       txnArray=txnArray.filter(e=>e.currency==currency);
     setTxn(txnArray)
      
  },[transactions,currency,merchantId])

  if(!Object.keys(report).length) return <Loading/>
  const { colors } = useContext(ThemeColors),
 
  tooltipShadow = 'rgba(0, 0, 0, 0.25)',
  gridLineColor = 'rgba(200, 200, 200, 0.2)',
  lineChartPrimary = '#666ee8',
  lineChartDanger = '#ff4961',
  warningColorShade = '#ffe802',
  warningLightColor = '#FDAC34',
  successColorShade = '#28dac6';

    return (
        <div className='mt-1'>
       <StatsCard cols={{ xl: '3', sm: '6' }} reports={report} 
           currency={currency}
           setStatementCSV={(el)=>setStatementCSV(el)}
           />
           
        <Row>
          <Col md='4' sm='12'>
           <SessionByDevice key='1' reports={report} 
           currency={currency}
           setStatementCSV={(el)=>setStatementCSV(el)}
            tooltipShadow={tooltipShadow}
            successColorShade={successColorShade}
            warningLightColor={warningLightColor}
            primary={colors.primary.main}
            danger ={ colors.danger.main}
           /> 
          </Col>
          <Col md='4' sm='12'>
           <SessionByDevice1 key='1'
            setStatementCSV={(el)=>setStatementCSV(el)}
           reports={report} 
           currency={currency}
            tooltipShadow={tooltipShadow}
            successColorShade={successColorShade}
            warningLightColor={warningLightColor}
            primary={colors.primary.main}
            danger ={ colors.danger.main}
           /> 
          </Col>
          <Col md='4' sm='12'>
           <SessionByDevice2 key='1'
            setStatementCSV={(el)=>setStatementCSV(el)}
           reports={report} 
           currency={currency}
            tooltipShadow={tooltipShadow}
            successColorShade={successColorShade}
            warningLightColor={warningLightColor}
            primary={colors.primary.main}
            danger ={ colors.danger.main}
           /> 
          </Col>
        </Row>
        <Row>
        <Col md='8' sm='12'>
        <RevenueReport  transactions={transactions || []}
        merchantId={merchantId.value}
         setStatementCSV={(el)=>setStatementCSV(el)}
           currency={currency} primary={colors.primary.main} warning={colors.warning.main}   dates={dates} />
          
        </Col>
        <Col md='4' sm='12'>
        <SessionByDevice3 
         setStatementCSV={(el)=>setStatementCSV(el)}
        key='1'
        reports={report} 
        currency={currency}
          tooltipShadow={tooltipShadow}
          successColorShade={successColorShade}
          warningLightColor={warningLightColor}
          primary={colors.primary.main}
          danger ={ colors.danger.main}
         success={successColorShade} 
        />
        </Col>
        </Row>
         <CSV statementCSV={statementCSV} setStatementCSV={setStatementCSV} />
         </div>
    )
}

export default SalesReport;

const CardTransactionsStyled = Styled(CardTransactions)`
margin-bottom:1em;
`
