import React, { useEffect, useState } from "react";
import RevenueReport from "./RevenueReport";
import { ThemeColors } from "@src/utility/context/ThemeColors";
import { useContext } from "react";
import { Row, Col } from "reactstrap";
import "@styles/react/libs/charts/apex-charts.scss";
import "@styles/base/pages/dashboard-ecommerce.scss";
import TransactionTable from "@src/components/TransactionsTable";
import CardTransactions from "../ui-elements/cards/advance/CardTransactions";
import Styled from "styled-components";
import PerfectScrollBar from "react-perfect-scrollbar";
import { useSkin } from "@hooks/useSkin";
import { Device } from "@src/views/Merchant/MerchantSection/Device/Device";
import DeviceSession from '@src/components/ui-elements/cards/analytics/DeviceSession'
import StatsCard from "../ui-elements/cards/statistics/StatsCard";
import { DeviceContext } from "../../views/SalesReport/DevicesReport";
import CSV from './CSV';
function SalesReport() {
  const {Devices,showView,selectedDevice,setSelectedDevice,report, merchants,dates, transactions}=useContext(DeviceContext)
  const { colors } = useContext(ThemeColors),
    [skin, setSkin] = useSkin(),
    labelColor = skin === "dark" ? "#b4b7bd" : "#6e6b7b",
    tooltipShadow = "rgba(0, 0, 0, 0.25)",
    gridLineColor = "rgba(200, 200, 200, 0.2)",
    lineChartPrimary = "#666ee8",
    lineChartDanger = "#ff4961",
    warningColorShade = "#ffe802",
    warningLightColor = "#FDAC34",
    successColorShade = "#28dac6",
    [txn,setTxn]= useState([]),
    [statementCSV, setStatementCSV]=useState({loader:false,data:{}}),
    [deviceCategory,setDeviceCategory]= useState({}),
    [deviceTxn,setDeviceTxn]=useState([]);
    

  useEffect(() => {
     let txnArray=[] ;
    
    if (transactions && !txn.length || Devices&&Devices.length) {
       let deviceCatObj={A920:0,A60:0,S300:0,iPP350:0};
       txnArray = transactions || [];
      txnArray= txnArray.filter(e=>e.merchant_id==showView.merchant_id)||[];
      let deviceSet = new Set();
      for(let obj of txnArray) {
           deviceSet.add(obj.device_seriel);
          let deviceObj =  Devices.find(e=>e.serial===obj.device_seriel);
           
           if(deviceObj)deviceCatObj[deviceObj['type']]+=1;
      }
      setDeviceTxn(Array.from((deviceSet)));
      setDeviceCategory(deviceCatObj);
    }
     if(selectedDevice.serial)txnArray = txnArray.filter((e) => e.device_seriel == selectedDevice.serial);
      setTxn(txnArray||[]);
      
    
  }, [selectedDevice,Devices,transactions]);

  const handleDevice = (el) => {
    setSelectedDevice(el);
  };

  return (
    <Col className="mt-1">
      {selectedDevice.id?
       <StatsCard 
          display='partial'
       key='4'
       setStatementCSV={setStatementCSV}
       key={4}
       currency={selectedDevice.device_currency}
       reports={report}
       cols={{ xl: '3', sm: '6' }} />:null}
      <PerfectScrollBar>
        <Row className="p-2 flex-nowrap">
          {Devices.filter(e=>deviceTxn.includes(e.serial)).map((res) => (
            <div className="col" style={{ flexGrow: 0 }}>
              <Device
                key={res.id}
                selectedDevice={selectedDevice}
                handleDevice={handleDevice}
                data={res}
                setSelected={(el) => setSelectedDevice(el)}
              />
            </div>
          ))}
        </Row>
      </PerfectScrollBar>
      <Row>
      <Col md={8}>
          <RevenueReport 
           key='4'
           transactions={txn||[]}
           dates={dates}
           setStatementCSV={setStatementCSV}
          primary={colors.primary.main} warning={colors.warning.main} />
          </Col>   
          <Col md={4}>
           <DeviceSession 
               key='4'
               setStatementCSV={setStatementCSV}
                deviceCategory={deviceCategory}
               tooltipShadow={tooltipShadow}
               successColorShade={colors.success.main}
               warningLightColor={warningLightColor}
               primary={colors.primary.main}
               danger ={ colors.danger.main}
              success={successColorShade} 
           /> 
           
           
          </Col>
      </Row>
      <Col>
         <CSV key='4' setStatementCSV={setStatementCSV} statementCSV={statementCSV}  />
        <TransactionTable  users={merchants.find(e=>e.merchant_id==showView.merchant_id)}  transactions={txn} />
      </Col>
    </Col>
  );
}

export default SalesReport;

const CardTransactionsStyled = Styled(CardTransactions)`
margin-bottom:1em;
`;
