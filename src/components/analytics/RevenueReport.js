import React, { useEffect, useState } from "react";
import axios from "axios";
import { currencyCodes, selectThemeColors } from "@utils";
import { Card, CardTitle } from "reactstrap";
import Chart from "react-apexcharts";
import { DownloadCloud } from "react-feather";
import moment from "moment";

const RevenueReport = ({
  currency,
  merchantId,
  dates,
  setStatementCSV,
  primary,
  warning,
  transactions,
}) => {
  const [credit, setCredit] = useState([0, 0, 0, 0, 0, 0, 0]);
  const [debit, setDebit] = useState([0, 0, 0, 0, 0, 0, 0]);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    //console.log(transactions);
    // let dayOfWeek = {
    //   0: "Sun",
    //   1: "Mon",
    //   2: "Tue",
    //   3: "Wed",
    //   4: "Thu",
    //   5: "Fri",
    //   6: "Sat",
    // };
    let transactionsArray = [...transactions];
    if(merchantId)transactionsArray= transactionsArray.filter(e=>e.merchant_id== merchantId);
    console.log(transactionsArray,343);
    if(currency)transactionsArray= transactionsArray.filter(e=>e.currency==currency);
    let obj = {},
      sortedDatesArr = [],
      today = moment(dates.start_date).format("YYYY-MM-DD"),
      newEarningsData = [],
      newExpenseData = [],
      newDaysArr = [];
    
    transactionsArray.forEach((ele) => {
      let d = moment(ele.date_of_transaction).format("YYYY-MM-DD");
      if (!obj[d]) {
        obj[d] = {
          debit: ele.paid_type == "dr" ? ele.amount : 0,
          credit: ele.paid_type == "cr" ? ele.amount : 0,
        };
        sortedDatesArr.unshift(d);
      } else {
        if (ele.paid_type === "cr")obj[d].credit = obj[d].credit + ele.amount;
        else obj[d].debit = obj[d].debit + ele.amount;
      }
    });
    if(sortedDatesArr.length>7)sortedDatesArr=sortedDatesArr.slice(0,7);
    //console.log(sortedDatesArr,obj);
    sortedDatesArr.forEach((ele,i) => {
      
      newEarningsData.push(
        Math.round((parseFloat(obj[ele].credit) + Number.EPSILON) * 100) / 100
      );
      newExpenseData.push(
        0 -
          Math.round((parseFloat(obj[ele].debit) + Number.EPSILON) * 100) / 100
      );
     
    });
    setCredit(newEarningsData);
    setDebit(newExpenseData);
    setCategories(sortedDatesArr);
  }, [transactions, currency,merchantId]);

  // const getTotalTransactionAmount = () => {
  //   let total = 0;
  //   credit.forEach((ele) => {
  //     total += ele;
  //   });
  //   debit.forEach((ele) => {
  //     total += 0 - ele;
  //   });
  //   return total;
  // };

  const revenueOptions = {
      chart: {
        stacked: true,
        type: "line",
        toolbar: { show: true },
      },
      grid: {
        padding: {
          top: -20,
          bottom: -10,
        },
        yaxis: {
          lines: { show: false },
        },
      },
      xaxis: {
        categories:categories ,
        labels: {
          style: {
            colors: "#b9b9c3",
            fontSize: "0.86rem",
          },
        },
        axisTicks: {
          show: false,
        },
        axisBorder: {
          show: false,
        },
      },
      legend: {
        show: true,
      },
      dataLabels: {
        enabled: true,
      },
      colors: [primary, warning],
      plotOptions: {
        bar: {
          columnWidth: "17%",
          endingShape: "rounded",
        },
        distributed: true,
      },
      yaxis: {
        labels: {
          style: {
            colors: "#b9b9c3",
            fontSize: "0.86rem",
          },
        },
      },
    },
    revenueSeries = [
      {
        name: "Credit",
        data: credit,
      },
      {
        name: "Debit",
        data: debit,
      },
    ];
  return (
    <Card className="card-revenue-budget px-1 py-1">
      <div className="d-sm-flex justify-content-between align-items-center mb-3">
        <CardTitle className="mb-50 mb-sm-0">Total sales reports</CardTitle>
        <div className="d-flex align-items-center">
          <div className="mr-1">
            <DownloadCloud onClick={(e) => setStatementCSV({})} />
          </div>
          <div className="d-flex align-items-center mr-2">
            <span className="bullet bullet-primary mr-50 cursor-pointer"></span>
            <span>Credit</span>
          </div>
          <div className="d-flex align-items-center">
            <span className="bullet bullet-warning mr-50 cursor-pointer"></span>
            <span>Debit</span>
          </div>
        </div>
      </div>
      <Chart
        id="revenue-report-chart"
        type="bar"
        height="230"
        options={revenueOptions}
        series={revenueSeries}
      />
    </Card>
  );
};

export default RevenueReport;
