 import React, { useContext, useEffect, useState,Fragment } from 'react'
import Loading from "@core/components/spinner/Loading-spinner";
import { ChevronDown, Eye, Edit,Trash, EyeOff, DownloadCloud } from "react-feather";
import DataTable from "react-data-table-component";
import moment from 'moment';
import Badge from 'reactstrap/lib/Badge';
import { useDispatch, useSelector } from 'react-redux';
import { GET_PARTNER, UPDATE_MERCHANT } from '../../redux/actions/storeMerchant.';



export default function index({transactions,count}) {  
const dispatch = useDispatch()
const {partners,merchants}=useSelector(state=>state.merchant);
const [loading,setLoading]=useState(true);




useEffect(() => {
 if(loading){   
 if(!partners.length)dispatch(GET_PARTNER());
 if(!merchants.length)dispatch(UPDATE_MERCHANT());
 setLoading(false);
 }
})





const columns =[
   
    {
        name: "Date",
        selector: "date_of_transaction",
        sortable: true,
        minWidth: "110px",
        cell: (row) => {
          let date = moment(row.date_of_transaction);
          return (
            <Fragment>
              <div className="mt-1">
                <h6 className="font-small-3">{date.format("YYYY-MM-DD")}</h6>
                <h6 className="text-secondary font-small-2">
                  {date.format("HH:mm:ss")}
                </h6>
              </div>
            </Fragment>
          );
        },
      },
      {
        name: "Status",
        selector: "status",
        minWidth: "110px",
        sortable: true,
        cell: (row) => (
          <Fragment>
            <div
              className={
                row.status == 0
                  ? "bg-danger"
                  : row.status == 1
                  ? "bg-success"
                  : "bg-primary"
              }
              style={{
                height: "10px",
                width: "10px",
                borderRadius: "50%",
                display: "inline-block",
                marginRight: "5px",
              }}
            />
            <span>
              {row.status == 0
                ? "Failed"
                : row.status == 1
                ? "Success"
                : "Pending"}
            </span>
          </Fragment>
        ),
      },
      {
        name: "Transaction ID",
        selector: "txn_no",
        sortable: false,
        minWidth: "200px",
        cell: (row) => {
          return <div className="font-small-3">{row.txn_no}</div>;
        },
      },
      {
        name: "Order ID",
        selector: "order_id",
        sortable: false,
        minWidth: "130px",
        cell: (row) => {
          return <div className="font-small-3">{row.order_id}</div>;
        },
      },
      {
        name: "Payment Type",
        selector: "pay_method",
        sortable: false,
        minWidth: "150px",
        cell: (row) => <Badge color="primary">{row.pay_method}</Badge>,
      },
      {
        name: "Amount",
        selector: "amount",
        sortable: false,
        cell: (row) => (
          <h6
            className={`${
              row.paid_type == "cr" ? "text-success" : "text-danger"
            } font-weight-bolderer`}
          >
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: row.currency,
            })
              .format(row.amount)
              .replace(/^(\D+)/, "$1 ")}
          </h6>
        ),
      },
      {
        name: "Action",
        selector: "Action",
        sortable: false,
        minWidth: "150px",
        cell: (row) => (
          <Fragment>
            <Eye size={17} className='mr-1' />
            <DownloadCloud size={17} className='mr-1'/>
            
          </Fragment>
        )
      },
        
    
    ];

const dataToRender = () => {
    let tempData=  transactions || [];
    
    tempData.forEach(el => {
         
       let temp = JSON.parse(JSON.stringify(el));

       temp.my_txn_type = el.paid_type == "cr" ? "credit" : "debit";
       temp.my_status =
         el.status == "1"
           ? "success"
           : el.status == "0"
           ? "failed"
           : "pending";
       temp.pay_method = `${temp.pay_method}`.toLowerCase();
     })
    
   return tempData;
}


if(loading)return <Loading />

    return (
        <div>
         <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            pageCount={count || 10}
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
        
        </div>
    )
}
