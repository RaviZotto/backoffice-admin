import React ,{useEffect, useState} from 'react'
import RevenueReport from './RevenueReport'
import { ThemeColors } from '@src/utility/context/ThemeColors'
import { useContext } from 'react'
import { Row, Col} from 'reactstrap';
import '@styles/react/libs/charts/apex-charts.scss'
import '@styles/base/pages/dashboard-ecommerce.scss'
import TransactionTable from '@src/components/TransactionsTable';
import CardTransactions from '../ui-elements/cards/advance/CardTransactions';
import Styled from 'styled-components'
import { useSkin } from '@hooks/useSkin'
import DeviceStatus from '../ui-elements/cards/analytics/StatusSession';
import TransferStatsCard from '../ui-elements/cards/statistics/TransferStatsCard';
import { TransferContext } from '../../views/SalesReport/TransferReport';
import RadialTransferStats from '../ui-elements/cards/analytics/RadialTransferStats';
import CSV from './CSV';
function SalesReport() {
  const { colors } = useContext(ThemeColors),
  [skin, setSkin] = useSkin(),
  labelColor = skin === 'dark' ? '#b4b7bd' : '#6e6b7b',
  tooltipShadow = 'rgba(0, 0, 0, 0.25)',
  gridLineColor = 'rgba(200, 200, 200, 0.2)',
  lineChartPrimary = '#666ee8',
  lineChartDanger = '#ff4961',
  warningColorShade = '#ffe802',
  warningLightColor = '#FDAC34',
  successColorShade = '#28dac6',
  primaryColorShade = '#836AF9',
  infoColorShade = '#299AFF',
  yellowColor = '#ffe800',
  greyColor = '#4F5D70',
  blueColor = '#2c9aff',
  blueLightColor = '#84D0FF',
  greyLightColor = '#EDF1F4', 
    trackBgColor = '#e9ecef',
   paymentMode={
      1:'sepa_transfer',
      2:'swift_tranfer',
      3:'internal_transfer',
      4:'express_transfer',
      5:'incoming_transfer'
    },
    [txn,setTxn] = useState([]),
    [statementCSV, setStatementCSV]=useState({loader:false,data:{}});
  const{showView, report:reports,currency,dates,transactions,merchants,transfer}=useContext(TransferContext)
    useEffect(()=>{ 
      console.log(showView);
     let txnArr=[...transactions];
      txnArr=txnArr.filter(e=>e.merchant_id==showView.merchant_id);
      txnArr=txnArr.filter(e=>e.currency==(currency||txnArr[0].currency));
     
     setTxn(txnArr);
    },[showView,transactions,currency,transfer])
   

    return (
        <Col className='mt-1'>
           <TransferStatsCard
           setStatementCSV={setStatementCSV}
            reports={reports}
            currency={currency}
            display='partial'
           cols={{ xl: '3', sm: '6' }} />
        <Row>
          <Col md={8}>
            <RevenueReport 
             key='3' 
             dates={dates}
             setStatementCSV={setStatementCSV}
             reports={reports}
             currency={currency}
             transactions={txn||[]}
            primary={colors.primary.main} warning={colors.warning.main} />
          </Col>
          <Col md={4}>
         <DeviceStatus 
            
             setStatementCSV={setStatementCSV}
             report={reports}
             currency={currency}
            tooltipShadow={tooltipShadow}
            successColorShade={colors.success.main}
            warningLightColor={warningLightColor}
            primary={colors.primary.main}
            danger ={ colors.danger.main}
           success={successColorShade} 
         />
          </Col>
        </Row>
        <Row>
        <Col md='8'>
        <TransactionTable  noHeader={false} transactions={txn} users={merchants.find(e=>e.merchant_id==showView.merchant_id)} />
        </Col>
        <Col md='4'>
        <RadialTransferStats primary ={colors.primary.main} danger={colors.danger.main} warning={colors.warning.main}     txn={txn} />
        </Col>
        </Row>
            <CSV statementCSV={statementCSV} setStatementCSV={setStatementCSV}/>
        </Col>
    )
}

export default SalesReport;

const CardTransactionsStyled = Styled(CardTransactions)`
margin-bottom:1em;
`
