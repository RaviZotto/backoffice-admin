import React, { useState,useEffect } from 'react'
// ** Third Party Components
import RevenueReport from './RevenueReport'
import { ThemeColors } from '@src/utility/context/ThemeColors'
import { useContext } from 'react'
import { Row, Col} from 'reactstrap';
import '@styles/react/libs/charts/apex-charts.scss'
import '@styles/base/pages/dashboard-ecommerce.scss'
import TransactionTable from './TransactionTable';
import CardTransactions from '../ui-elements/cards/advance/CardTransactions';
import StatsCard from '../ui-elements/cards/statistics/StatsCard'
import SessionByDevice from '../ui-elements/cards/analytics/SessionBySales'
import SessionByDevice1 from '../ui-elements/cards/analytics/SessionByDevice1'
import SessionByDevice2 from '../ui-elements/cards/analytics/SessionByDevice2'
import SessionByDevice3 from '../ui-elements/cards/analytics/SessionByDevice3'
import CSV from './CSV';
import Styled from 'styled-components'
import { useSkin } from '@hooks/useSkin'
import { MerchantContext } from '../../views/SalesReport/MerchantSalesReport ';
function SalesReport() {


  const [txn,setTxn]=useState([]);

  const [loading,setLoading] =  useState(true);
  const {report,dates,transactions,currency,showView} = useContext(MerchantContext)
  useEffect(()=>{ 
    let txnArray = [...transactions];
    txnArray=txnArray.filter(e=>e.merchant_id==showView.merchant_id);
    txnArray=txnArray.filter(e=>e.currency==currency);
    setTxn(txnArray);

  },[transactions,currency,showView])
  

  const { colors } = useContext(ThemeColors),
  [skin, setSkin] = useSkin(),
  labelColor = skin === 'dark' ? '#b4b7bd' : '#6e6b7b',
  tooltipShadow = 'rgba(0, 0, 0, 0.25)',
  gridLineColor = 'rgba(200, 200, 200, 0.2)',
  lineChartPrimary = '#666ee8',
  lineChartDanger = '#ff4961',
  warningColorShade = '#ffe802',
  warningLightColor = '#FDAC34',
  successColorShade = '#28dac6',
  primaryColorShade = '#836AF9',
  infoColorShade = '#299AFF',
  yellowColor = '#ffe800',
  greyColor = '#4F5D70',
  blueColor = '#2c9aff',
  blueLightColor = '#84D0FF',
  greyLightColor = '#EDF1F4', 
    trackBgColor = '#e9ecef',
  [statementCSV, setStatementCSV]=useState({loader:false,data:{}});
    return  ! Object.keys(showView).length?null:  (
        <div className='mt-1'>
       <StatsCard cols={{ xl: '3', sm: '6' }}   key={2}
            reports={report}
            currency={currency}
            setStatementCSV={setStatementCSV}
            />
        <Row>
          <Col md='4' sm='12'>
           <SessionByDevice 
             setStatementCSV={setStatementCSV}
            key={2}
            reports={report}
            currency={currency}
            tooltipShadow={tooltipShadow}
            successColorShade={successColorShade}
            warningLightColor={warningLightColor}
            primary={colors.primary.main}
            danger ={ colors.danger.main}
           /> 
          </Col>
          <Col md='4' sm='12'>
           <SessionByDevice1
             setStatementCSV={setStatementCSV}
               key={2}
               reports={report}
               currency={currency}
            tooltipShadow={tooltipShadow}
            successColorShade={successColorShade}
            warningLightColor={warningLightColor}
            primary={colors.primary.main}
            danger ={ colors.danger.main}
           /> 
          </Col>
          <Col md='4' sm='12'>
           <SessionByDevice2
             setStatementCSV={setStatementCSV}
               key={2}
               reports={report}
               currency={currency}
            tooltipShadow={tooltipShadow}
            successColorShade={successColorShade}
            warningLightColor={warningLightColor}
            primary={colors.primary.main}
            danger ={ colors.danger.main}
           /> 
          </Col>
        </Row>
        <Row>
        <Col md='8' sm='12'>
        <RevenueReport 
          setStatementCSV={setStatementCSV}
           key={2}
           transactions={transactions}
            merchantId={showView.merchant_id}
            dates={dates}
           currency={currency}
        primary={colors.primary.main} warning={colors.warning.main}  /> 
       
        </Col>
        <Col md='4' sm='12'>
        <SessionByDevice3 
          setStatementCSV={setStatementCSV} 
           key={2}
           reports={report}
           currency={currency}
          tooltipShadow={tooltipShadow}
          successColorShade={successColorShade}
          warningLightColor={warningLightColor}
          primary={colors.primary.main}
          danger ={ colors.danger.main}
         success={successColorShade} 
        />
        </Col>
        <CSV statementCSV={statementCSV} setStatementCSV={setStatementCSV} />
        </Row>
         </div>
    )
}

export default SalesReport;

const CardTransactionsStyled = Styled(CardTransactions)`
margin-bottom:1em;
`
