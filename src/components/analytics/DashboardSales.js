import React, { useState,useEffect } from 'react'


// ** Third Party Components
import {Card,CardHeader,Button} from 'reactstrap';
import { ThemeColors } from '@src/utility/context/ThemeColors'
import { useContext } from 'react'
import { Row, Col} from 'reactstrap';
import '@styles/react/libs/charts/apex-charts.scss'
import '@styles/base/pages/dashboard-ecommerce.scss'
import TransactionTable from '@src/components/TransactionsTable';
import CardTransactions from '../ui-elements/cards/advance/CardTransactions';
import StatsCard from '../ui-elements/cards/statistics/StatsCard'
import OrdersBarChart from '@src/components/ui-elements/cards/statistics/OrdersBarChart'
import ProfitLineChart from '@src/components/ui-elements/cards/statistics/ProfitLineChart'
import Styled from 'styled-components'
import CSV from './CSV';
import { DashboardContext } from '../../views/Dashboard'
import moment from 'moment';
import Revenue from './Revenue';
import {useHistory} from 'react-router'
import Earnings from '@src/components/ui-elements/cards/analytics/Earnings';
import ProductOrders from '../ui-elements/cards/analytics/ProductOrders';
function SalesReport() {
  const {currency,transactions,dates,devices}=useContext(DashboardContext)
  const [loading,setLoading] =  useState(true);
  const [statementCSV, setStatementCSV]=useState({loader:false,data:{}});
  const [txn,setTxn]=useState([]);
  const [report,setReport] = useState({});
  const history = useHistory()
  useEffect(()=>{
    let txnArray= [...transactions]
      
       txnArray=txnArray.filter(e=>e.currency==currency);
        let reportObj = {
         internal: 0,
         sepa: 0,
         total:0,
         failure:0,
         pending:0,
         device:0,
         swift: 0,
         bank: 0,
         express: 0,
         credit: 0,
         debit: 0,
         refund: 0,
         success:0,
         online:0,
         incoming:0,
         A920:0,
         A60:0,
         payment_mode:0,
         payment_link:0,
         S300:0,
         iPP350:0,
         failedAmount:0,
         total_amount:0,
       };
      txnArray= txnArray.filter(e=>e.currency==currency);
      //console.log(txnArray);
       for(let obj of txnArray ){ 
         reportObj.total+=1;
       obj.paid_type=='cr'?reportObj.credit+=obj.amount:reportObj.debit+=obj.amount;
       obj.status==0?(reportObj.failure+=1):(obj.status==1?reportObj.success+=1:reportObj.pending+=1)
         if(obj.status==0)reportObj.failedAmount+=obj.amount;
           if(obj.payment_mode=="1") { reportObj.online+=obj.amount;}
           else if(obj.payment_mode=="2"){
            reportObj.device+=obj.amount; 
            let deviceObj =  devices.find(e=>e.serial===obj.device_seriel);
               if(deviceObj)reportObj[deviceObj['type']]+=1;
           }
           else if(obj.payment_mode=="3"){reportObj.payment_link+=obj.amount;}
           else if(obj.payment_mode=="4"){reportObj.bank+=obj.amount;
                 if(obj.transfer_type==1)reportObj.sepa+=obj.amount;
                 else if(obj.transfer_type==2)reportObj.swift+=obj.amount;
                 else if (obj.transfer_type==3)reportObj.internal+=obj.amount;
                 else if(obj.transfer_type==4)reportObj.express+=obj.amount;
                 else if(obj.transfer_type==5 || !obj.transfer_type){reportObj.incoming+=obj.amount;}
               reportObj.total_amount+=obj.amount;
           }                                               
            
         }
         
    setReport({...reportObj});
   setTxn(txnArray);
  },[transactions,currency])

//   if(!Object.keys(report).length) return <Loading/>
  const { colors } = useContext(ThemeColors)
 


    return (
        <div className='mt-1'>
       <StatsCard cols={{ xl: '3', sm: '6' }} 
           currency={currency}
           reports={report}
           setStatementCSV={(el)=>setStatementCSV(el)}
           />
           
          <Row className='match-height'>
          <Col lg='4' md='12'>
          <Row className='match-height'>
            <Col lg='6' md='3' xs='6'>
              <OrdersBarChart  reports={report} currency={currency} warning={colors.warning.main} />
            </Col>
            <Col lg='6' md='3' xs='6'>
              <ProfitLineChart info={colors.info.main} reports={report} currency={currency}  />
            </Col>
            <Col lg='12' md='6' xs='12'>
              <Earnings success={colors.success.main}  transactions={txn} reports={report}  danger={colors.danger.main} primary={colors.primary.main} currency={currency} />
            </Col>
          </Row>
        </Col>
        <Col lg='8' md='12'>
              <Revenue transactions={txn} dates={dates} currency={currency}   danger={colors.danger.main} success={colors.success.main} />
            </Col>
           </Row>
         <Row>
           <Col lg='8' md='12'>
             <Card>
               <CardHeader className='row justify-content-end'>
                 <Button.Ripple color='primary'  onClick={e=>history.push('/sales-report-txn')} >View All Transactions</Button.Ripple>
               </CardHeader>
         <TransactionTable  type='dashboard' noHeader={false} transactions={txn} rowsCount={7}  />
         </Card>
         </Col>
         <Col lg='4' md='12'>
           <ProductOrders  secondary={colors.secondary.main}   success={colors.success.main} primary ={colors.primary.main} danger={colors.danger.main} warning={colors.warning.main} reports={report} currency={currency}  />
           </Col>
         </Row>

         
         </div>
    )
}

export default SalesReport;

const CardTransactionsStyled = Styled(CardTransactions)`
margin-bottom:1em;
`
