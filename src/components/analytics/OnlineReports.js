import React ,{useEffect, useState} from 'react'
import RevenueReport from './RevenueReport'
import { ThemeColors } from '@src/utility/context/ThemeColors'
import { useContext } from 'react'
import { Row, Col} from 'reactstrap';
import '@styles/react/libs/charts/apex-charts.scss'
import '@styles/base/pages/dashboard-ecommerce.scss'
import TransactionTable from '@src/components/TransactionsTable';
import CardTransactions from '../ui-elements/cards/advance/CardTransactions';
import Styled from 'styled-components'
import { useSkin } from '@hooks/useSkin'
import DeviceStatus from '../ui-elements/cards/analytics/OnlineSession';
import StatsCard from '../ui-elements/cards/statistics/StatsCard';
import { OnlineContext } from '../../views/SalesReport/OnlineReport';
import CSV from './CSV';
function SalesReport() {
  const { colors } = useContext(ThemeColors),
  [skin, setSkin] = useSkin(),
  labelColor = skin === 'dark' ? '#b4b7bd' : '#6e6b7b',
  tooltipShadow = 'rgba(0, 0, 0, 0.25)',
 
  warningLightColor = '#FDAC34',
  successColorShade = '#28dac6',

    [txn,setTxn] = useState([]),
    [statementCSV, setStatementCSV]=useState({loader:false,data:{}});
  const{showView, report:reports,currency,dates,transactions,merchants,Gateway}=useContext(OnlineContext)
    useEffect(()=>{ 
      console.log(showView);
     let txnArr=[...transactions];
      txnArr=txnArr.filter(e=>e.merchant_id==showView.merchant_id);
      if(currency)txnArr=txnArr.filter(e=>e.currency==currency);
      if(Gateway.id)txnArr=txnArr.filter(e=>e.gateway_id == Gateway.id|| e.gateway.toLowerCase()==Gateway.name);
     setTxn(txnArr);
    },[showView,transactions,currency,Gateway])
   

    return (
        <Col className='mt-1'>
           <StatsCard key='3' 
           setStatementCSV={setStatementCSV}
            reports={reports}
            currency={currency}
            display='partial'
           cols={{ xl: '3', sm: '6' }} />
        <Row>
          <Col md={8}>
            <RevenueReport 
             key='3' 
             dates={dates}
             setStatementCSV={setStatementCSV}
             reports={reports}
             currency={currency}
             transactions={txn||[]}
            primary={colors.primary.main} warning={colors.warning.main} />
          </Col>
          <Col md={4}>
         <DeviceStatus 
             key='3' 
             setStatementCSV={setStatementCSV}
             report={reports}
             currency={currency}
            tooltipShadow={tooltipShadow}
            successColorShade={colors.success.main}
            warningLightColor={warningLightColor}
            primary={colors.primary.main}
            danger ={ colors.danger.main}
           success={successColorShade} 
         />
          </Col>
        </Row>
        
        <Col>
        <TransactionTable  noHeader={false} transactions={txn} users={merchants.find(e=>e.merchant_id==showView.merchant_id)} />
        </Col>
            <CSV statementCSV={statementCSV} setStatementCSV={setStatementCSV}/>
        </Col>
    )
}

export default SalesReport;

const CardTransactionsStyled = Styled(CardTransactions)`
margin-bottom:1em;
`
