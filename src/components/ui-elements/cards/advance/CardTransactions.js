import React from 'react';
import Avatar from '@components/avatar'
import * as Icon from 'react-feather'
import { Card, CardHeader, CardTitle, CardBody, Media } from 'reactstrap'

const CardTransactions = () => {
  const transactionsArr = [
    {
      title: 'Bank',
      color: 'light-primary',
      
      amount: '- $74',
      Icon: Icon['Pocket'],
      down: true
    },
    {
      title: 'Online',
      color: 'light-success',
 
      amount: '+ $480',
      Icon: Icon['Check']
    },
    {
      title: 'Devices',
      color: 'light-danger',
    
      amount: '+ $590',
      Icon: Icon['DollarSign']
    },
    {
      title: 'paylinks',
      color: 'light-warning',
    
      amount: '- $12',
      Icon: Icon['CreditCard'],
      down: true
    },
    {
      title: 'Refund',
      color: 'light-info',
    
      amount: '+ $98',
      Icon: Icon['TrendingUp']
    }
  ]

  const renderTransactions = () => {
    return transactionsArr.map(item => {
      return (
        <div key={item.title} className='transaction-item'>
          <Media>
            <Avatar className='rounded' color={item.color} icon={<item.Icon size={18} />} />
            <Media body>
              <h6 className='transaction-title'>{item.title}</h6>
              <small>{item.subtitle}</small>
            </Media>
          </Media>
          <div className={`font-weight-bolder ${item.down ? 'text-danger' : 'text-success'}`}>{item.amount}</div>
        </div>
      )
    })
  }

  return (
    <div className="mb-1">
    <Card className='card-transaction '>
      <CardHeader>
        <CardTitle tag='h4'>Transactions</CardTitle>
        <Icon.MoreVertical size={18} className='cursor-pointer' />
      </CardHeader>
      <CardBody>{renderTransactions()}</CardBody>
    </Card>
    </div>
  )
}

export default CardTransactions
