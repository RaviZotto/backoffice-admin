import React,{ useEffect, useState } from 'react'
import axios from 'axios'
import TinyChartStats from '@components/widgets/stats/TinyChartStats'

const OrdersBarChart = ({ warning,reports,currency }) => {
  const {online}=reports;
  const data={ title: 'Orders',
  statistics:  new Intl.NumberFormat("en-US", {
    style: "currency",
    currency:currency||'GBP',
  })
    .format(online)
    .replace(/^(\D+)/, "$1 "),
  series: [
    {
      name: '2020',
      data: [45, 85, 65, 45, 65]
    }
  ]}

  // useEffect(() => {
  //   axios.get('/card/card-statistics/orders-bar-chart').then(res => setData(res.data))
  // }, [])

  const options = {
    chart: {
      stacked: false,
      toolbar: {
        show: false
      }
    },
    grid: {
      show: false,
      padding: {
        left: 0,
        right: 0,
        top: -15,
        bottom: -15
      }
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '20%',
        startingShape: 'rounded',
        colors: {
          backgroundBarColors: ['#f3f3f3', '#f3f3f3', '#f3f3f3', '#f3f3f3', '#f3f3f3'],
          backgroundBarRadius: 5
        }
      }
    },
    legend: {
      show: true
    },
    dataLabels: {
      enabled: false
    },
    colors: [warning],
    xaxis: {
      labels: {
        show: false
      },
      axisBorder: {
        show:false
      },
      axisTicks: {
        show: false
      }
    },
    yaxis: {
      show: false
    },
    tooltip: {
      x: {
          show: true
      },
      y:{ show: true}
    }
  }

  return (
    <TinyChartStats
      height={70}
      type='bar'
      options={options}
      title={'Online Sales'}
      stats={data.statistics}
      series={data.series}
    />
  )
}

export default OrdersBarChart
