import React, { useContext } from 'react';
import classnames from 'classnames'
import Avatar from '@components/avatar'
import { TrendingUp, User, Box, DollarSign, TrendingDown, DownloadCloud } from 'react-feather'
import { Card, CardHeader, CardTitle, CardBody, CardText, Row, Col, Media } from 'reactstrap'
import { TransactionsTableContext } from '../../../TransactionsTable';

const StatsCard = ({ cols,reports,currency,setStatementCSV,display='all' }) => {
 
  const { internal,swift,sepa,express,incoming, credit:total_credited_amount,debit:total_debited_amount,refund:total_refund} = reports
  const External=swift+sepa+express+incoming;
  const data = [
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(reports.credit)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Total Credited Amount',
      color: 'light-success',
      icon: <TrendingUp size={24} />
    },
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(reports.debit)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Total Debited Amount',
      color: 'light-danger',
      icon: <TrendingDown size={24} />
    },
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(reports.credit+reports.debit)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Total Captured Amount',
      color: 'light-primary',
      icon: <DollarSign size={24} />
    },
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(reports.refund)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Total Refunded Amount',
      color: 'light-primary',
      icon: <DollarSign size={24} />
    },
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(internal)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Total Internal Transfer Amount',
      color: 'light-primary',
      icon: <DollarSign size={24} />
    },
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(External)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Total External Transfer Amount',
      color: 'light-primary',
      icon: <DollarSign size={24} />
    },
  ]

  const renderData = () => {
    return data.map((item, index) => {
      const margin = Object.keys(cols)
      if( display=='partial' && (index==4||index==5))return null;
      return  (
        <Col
          key={index}
          
          
          
          className={classnames({
            [`mb-2 mb-${margin[0]}-0`]: index !== data.length - 1
          })}
        >
          <Media>
            <Avatar color={item.color} icon={item.icon} className='mr-2' />
            <Media className='my-auto' body>
              <h4 className='font-weight-bolder mb-0'>{item.title}</h4>
              <CardText className='font-small-2 mb-0'>{item.subtitle}</CardText>
            </Media>
          </Media>
        </Col>
      )
    })
  }

  return (
    <Card className='card-statistics mt-2 '>
      <CardHeader>
        <CardTitle tag='h4'> Statistics</CardTitle>
        <CardText className='card-text font-small-2 mr-25 mb-0 cursor-pointer'> <DownloadCloud size={17} className='mr-1' onClick={e=>setStatementCSV({loader:true,data:{total_credited_amount,total_debited_amount,total_refund,total_internal:internal,total_external:External}})}  /> </CardText>
      </CardHeader>
      <CardBody className='statistics-body'>
        <Row className='flex-md-nowrap'>{renderData()}</Row>
      </CardBody>
    </Card>
  )
}

export default StatsCard

