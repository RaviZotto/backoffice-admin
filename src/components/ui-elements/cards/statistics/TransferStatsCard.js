import React, { useContext } from 'react';
import classnames from 'classnames'
import Avatar from '@components/avatar'
import { TrendingUp, User, Box, DollarSign, TrendingDown, DownloadCloud } from 'react-feather'
import { Card, CardHeader, CardTitle, CardBody, CardText, Row, Col, Media } from 'reactstrap'
import { TransactionsTableContext } from '../../../TransactionsTable';

const StatsCard = ({ cols,reports,currency,setStatementCSV }) => {
 
  const { fx_swift_transfer,fx_internal_transfer, internal_transfer,swift_transfer,sepa_transfer,express_transfer, margin_amount,fees,incoming_transfer} = reports

  const data = [
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(sepa_transfer)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Sepa Amount',
      color: 'light-success',
      icon: <TrendingUp size={15} />
    },
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(swift_transfer+fx_swift_transfer)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Swift Amount',
      color: 'light-danger',
      icon: <TrendingDown size={15} />
    },
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(express_transfer)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Express Amount',
      color: 'light-primary',
      icon: <DollarSign size={15} />
    },
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(incoming_transfer)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Incoming Amount',
      color: 'light-primary',
      icon: <DollarSign size={15} />
    },
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(fees)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Fees Amount',
      color: 'light-primary',
      icon: <DollarSign size={15} />
    },
    {
      title: `${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(margin_amount)
        .replace(/^(\D+)/, "$1 ")}`,
      subtitle: 'Margin Amount',
      color: 'light-primary',
      icon: <DollarSign size={15} />
    },
  ]

  const renderData = () => {
    return data.map((item, index) => {
      const margin = Object.keys(cols)
     
      return  (
        <Col
          key={index}
          
          
          
          className={classnames({
            [`mb-2 mb-${margin[0]}-0`]: index !== data.length - 1
          })}
        >
          <Media>
            <Avatar color={item.color} icon={item.icon} className='mr-1' />
            <Media  body>
              <h6 className='font-weight-bolder mb-0'>{item.title}</h6>
              <CardText className='font-small-2 mb-0'>{item.subtitle}</CardText>
            </Media>
          </Media>
        </Col>
      )
    })
  }

  return (
    <Card className='card-statistics mt-2 '>
      <CardHeader>
        <CardTitle tag='h4'> Statistics</CardTitle>
        <CardText className='card-text font-small-2 mr-25 mb-0 cursor-pointer'> <DownloadCloud size={17} className='mr-1' onClick={e=>setStatementCSV({loader:true,data:{Swift:fx_swift_transfer+swift_transfer,Internal:internal_transfer+fx_internal_transfer,fees,express:express_transfer,Margin:margin_amount,currency}})}  /> </CardText>
      </CardHeader>
      <CardBody className='statistics-body'>
        <Row className='flex-md-nowrap'>{renderData()}</Row>
      </CardBody>
    </Card>
  )
}

export default StatsCard

