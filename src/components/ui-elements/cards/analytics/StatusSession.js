import React from 'react';
import { Doughnut } from 'react-chartjs-2'
import { DownloadCloud, Monitor, Tablet } from 'react-feather'
import { Card, CardHeader, CardTitle, CardBody,Row,Col, CardText } from 'reactstrap'
const calPercent=(total,val)=>Math.floor(val/total*100)||0;
const ChartjsRadarChart = ({ setStatementCSV,report,currency, tooltipShadow, successColorShade, warningLightColor, primary,danger }) => { 
  const {failure,pending,credit,debit}=report;
  const total=credit+debit;
  let obj1={
     Credit:credit,
     Debit:debit
  }
  const options = {
      responsive: true,
      maintainAspectRatio: false,
      responsiveAnimationDuration: 500,
      cutoutPercentage: 60,
      legend: { display: false },
      tooltips: {
        callbacks: {
          label(tooltipItem, data) {
            const label = data.datasets[0].labels[tooltipItem.index] || '',
              value = data.datasets[0].data[tooltipItem.index]
            const output = ` ${label} : ${value} %`
            return output
          }
        },
        // Updated default tooltip UI
        shadowOffsetX: 1,
        shadowOffsetY: 1,
        shadowBlur: 8,
        shadowColor: tooltipShadow,
        backgroundColor: '#fff',
        titleFontColor: '#000',
        bodyFontColor: '#000'
      }
    },
    data = {
      datasets: [
        {
          labels: ['Credit', 'Debit'],
          data: [calPercent(total,credit),calPercent(total,debit)],
          backgroundColor: [successColorShade, danger ],
          borderWidth: 0,
          pointStyle: 'rectRounded'
        }
      ]
    }

  return (
    <Card>
      <CardHeader className='d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column'>
        <CardTitle tag='h4'>Sales Status</CardTitle>
        <CardText className='cursor-pointer'><DownloadCloud onClick={e=> setStatementCSV({loader:true,data:obj1})} size={17} className='mr-1'/></CardText>
      </CardHeader>
      <CardBody className=' pt-md-0 pl-0 ml-0 '>
        <Col className='mt-md-0'  >
        <div style={{ height: '150px' }}>
          <Doughnut data={data} options={options} height={150 } />
        </div>
        </Col>
        <Row className='justify-content-between p-2'>
        <div className='d-flex justify-content-between  mb-1'>
          <div className='d-flex align-items-center'>
            <Monitor size={17} className='text-success' />
            <span className='font-weight-bold ml-75 mr-25'>Credit  </span>
            <span>-{`${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(credit)
        .replace(/^(\D+)/, "$1 ")}`}</span>
          </div>
       
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Tablet size={17} className='text-danger' />
            <span className='font-weight-bold ml-75 mr-25  text-nowrap'>Debit </span>
            <span>-{`${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(debit)
        .replace(/^(\D+)/, "$1 ")}`}</span>
          </div>
       
        </div>
        <div className='d-flex justify-content-between   '>
          <div className='d-flex align-items-center'>
            <Tablet size={17} className='text-primary' />
            <span className='font-weight-bold ml-75 mr-25  text-nowrap'>Captured </span>
            <span>- {`${ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(debit+credit)
        .replace(/^(\D+)/, "$1 ")}`}</span>
          </div>
       
        </div>
  
        </Row>
      </CardBody>
    </Card>
  )
}

export default ChartjsRadarChart
