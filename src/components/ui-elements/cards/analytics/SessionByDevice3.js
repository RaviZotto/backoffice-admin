import React from 'react';
import { Doughnut } from 'react-chartjs-2'
import { DownloadCloud, Monitor, Tablet } from 'react-feather'
import { Card, CardHeader, CardTitle, CardBody,Row,Col, CardText } from 'reactstrap'
const calPercent=(total,val)=>Math.floor(val/total*100)||0;

const ChartjsRadarChart = ({setStatementCSV,reports,currency, tooltipShadow, successColorShade, warningLightColor, primary,danger }) => {
  const {success,failure,pending,total} = reports;
  let obj1={ 
    Success: success,
    Failure:failure,
    Pending:pending
  }
  const options = {
      responsive: true,
      maintainAspectRatio: false,
      responsiveAnimationDuration: 500,
      cutoutPercentage: 60,
      legend: { display: false },
      tooltips: {
        callbacks: {
          label(tooltipItem, data) {
            const label = data.datasets[0].labels[tooltipItem.index] || '',
              value = data.datasets[0].data[tooltipItem.index]
            const output = ` ${label} : ${value} %`
            return output
          }
        },
        // Updated default tooltip UI
        shadowOffsetX: 1,
        shadowOffsetY: 1,
        shadowBlur: 8,
        shadowColor: tooltipShadow,
        backgroundColor: '#fff',
        titleFontColor: '#000',
        bodyFontColor: '#000'
      }
    },
    data = {
      datasets: [
        {
          labels: ['Success', 'Failure','Pending'],
          data: [ calPercent(total,success),calPercent(total,failure),calPercent(total,pending)],
          backgroundColor: [successColorShade, danger,primary ],
          borderWidth: 0,
          pointStyle: 'rectRounded'
        }
      ]
    }

  return (
    <Card>
      <CardHeader className='d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column'>
        <CardTitle tag='h4'>Status</CardTitle>
        <CardText className='cursor-pointer'><DownloadCloud  onClick={e=> setStatementCSV({loader:true,data:obj1})} size={17} className='mr-1'/></CardText>
      </CardHeader>
      <CardBody className=' pl-0 ml-0 '>
        <Col  >
        <div style={{ height: '175px' }}>
          <Doughnut data={data} options={options} height={175} />
        </div>
        </Col>
        <Row className='justify-content-between p-2'>
        <div className='d-flex justify-content-between  mb-1'>
          <div className='d-flex align-items-center'>
            <Monitor size={17} className='text-success' />
            <span className='font-weight-bold ml-75 mr-25'>Success </span>
            <span>- { success}</span>
          </div>
       
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Tablet size={17} className='text-danger' />
            <span className='font-weight-bold ml-75 mr-25  text-nowrap'>Failure</span>
            <span>- { failure}</span>
          </div>
       
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Tablet size={17} className='text-primary' />
            <span className='font-weight-bold ml-75 mr-25  text-nowrap'>Pending</span>
            <span>-{ pending}</span>
          </div>
       
        </div>
  
        </Row>
      </CardBody>
    </Card>
  )
}

export default ChartjsRadarChart
