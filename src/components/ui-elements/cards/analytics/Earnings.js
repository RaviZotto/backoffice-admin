import Chart from "react-apexcharts";
import { Card, CardBody, CardText, CardTitle, Col, Row } from "reactstrap";
import React,{ useEffect, useState, useContext, useMemo } from "react";
import { formatMoney } from "@utils";

const Earnings = ({ success,currency,transactions }) => {
  
  const [inputData, setInputData] = useState({
    failedAmount: 0,
    successfulTransaction: 0,
    failedTransaction: 0,
    pendingTransaction: 0,
    changed: false,
  });

  useEffect(() => {
    if (transactions.length > 0) {
        //console.log(transactions);
      let success = 0,
        fail = 0,
        pending = 0,
        failedAmount = 0,
        total = 0;
      transactions.forEach((ele) => {
        if (ele.status == "1") success += 1;
        else if (ele.status == "2") pending += 1;
        else {
          fail += 1;
          failedAmount += ele.amount;
        }
      });
      total = success + fail + pending;
      //console.log(success,fail)
      setInputData({
        failedAmount: failedAmount,
        successfulTransaction: (success * 100) / total,
        failedTransaction: (fail * 100) / total,
        pendingTransaction: (pending * 100) / total,
        changed: true,
      });
    }else {
      setInputData({  failedAmount: 0,
        successfulTransaction: 0,
        failedTransaction: 0,
        pendingTransaction: 0,
        changed: false})
    }
  }, [transactions]);

  if (inputData.changed) {
    const options = {
      chart: {
        toolbar: {
          show: false,
        },
      },
      dataLabels: {
        enabled: false,
      },
      legend: { show: false },
      labels: ["Successful", "Pending", "Failed"],
      stroke: { width: 0 },
      colors: ["#28c76f66", "#28c76f33", success],
      grid: {
        padding: {
          right: -20,
          bottom: -8,
          left: -20,
        },
      },
      plotOptions: {
        pie: {
          donut: {
            labels: {
              show: true,
              name: {
                offsetY: 15,
              },
              value: {
                offsetY: -15,
                formatter(val) {
                  return `${parseInt(val)} %`;
                },
              },
              total: {
                show: true,
                offsetY: 15,
                label: "Successful",
                formatter(w) {
                  return `${Number(inputData.successfulTransaction).toFixed(2)} %`;
                },
              },
            },
          },
        },
      },
      responsive: [
        {
          breakpoint: 1325,
          options: {
            chart: {
              height: 100,
            },
          },
        },
        {
          breakpoint: 1200,
          options: {
            chart: {
              height: 120,
            },
          },
        },
        {
          breakpoint: 1065,
          options: {
            chart: {
              height: 100,
            },
          },
        },
        {
          breakpoint: 992,
          options: {
            chart: {
              height: 120,
            },
          },
        },
      ],
    };

    const getSeriesData = () => {
      return [inputData.successfulTransaction, inputData.pendingTransaction, inputData.failedTransaction];
    };

    return (
      <Card className="earnings-card">
        <CardBody>
          <Row>
            <Col xs="6">
              <CardTitle className="mb-1">Success Rate</CardTitle>
              <div className="font-small-2">Total amount failed this week.</div>
              <h5 className="mb-1">{formatMoney(inputData.failedAmount, currency)}</h5>

              <CardText className="text-muted font-small-2">
                <span className="font-weight-bolder">{Number(inputData.successfulTransaction).toFixed(2)} %</span>
                <span> sucessfull transactions.</span>
              </CardText>
            </Col>
            <Col xs="6">
              <Chart options={options} series={getSeriesData()} type="donut" height={120} />
            </Col>
          </Row>
        </CardBody>
      </Card>
    );
  } else return null;
};

export default Earnings;
