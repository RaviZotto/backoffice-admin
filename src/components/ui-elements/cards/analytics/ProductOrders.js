import React,{ useCallback, useEffect, useRef, useState } from 'react'
import axios from 'axios'
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,

} from 'reactstrap'
import Chart from 'react-apexcharts'
import { Circle } from 'react-feather'
const calPercent=(total,val)=>Math.ceil(val/total*100)||0;
const ProductOrders = props => {
const {currency}=props;
const [radial,setRadial]=useState({internal:0,sepa:0,express:0,total:0,total_amount:0, swift:0,incoming:0});

useEffect(() => {
  const {reports} = props;

const {internal, sepa,swift,express,incoming,total_amount}=reports;


setRadial({internal,sepa,swift,express,total_amount,incoming});
},[props.reports])

  let options = {
      labels: ['Internal', 'Swift', 'Sepa','Express','Incoming'],
      plotOptions: {
        radialBar: {
          size: 150,
          hollow: {
            size: '20%'
          },
          track: {
            strokeWidth: '100%',
            margin: 15
          },
          dataLabels: {
            value: {
              fontSize: '1rem',
              colors: '#5e5873',
              fontWeight: '500',
              offsetY: 5
            },
            total: {
              show: false,
              label: 'Total',
              fontSize: '1.286rem',
              colors: '#5e5873',
              fontWeight: '500',

               formater : useCallback(
                () => 
                radial.total_amount
                ,
                [radial],
              )
            }
          }
        }
      },
      colors: [props.primary, props.warning, props.danger,props.secondary, props.success],
      stroke: {
        lineCap: 'round'
      },
      chart: {
        height: 355,
        dropShadow: {
          enabled: true,
          blur: 3,
          left: 1,
          top: 1,
          opacity: 0.1
        }
      }
    },
    series = [calPercent(radial.total_amount,radial.internal),calPercent(radial.total_amount,radial.swift),calPercent(radial.total_amount,radial.sepa),calPercent(radial.total_amount,radial.express),calPercent(radial.total_amount,radial.incoming)]
   
  return props.reports !== null ? (
    <Card>
      <CardHeader>
        <CardTitle tag='h4'>Transfer Sales</CardTitle>
        
      </CardHeader>
      <CardBody>
        <Chart options={options} series={series} type='radialBar' height={285} />
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-primary' />
            <span className='font-weight-bold ml-75'>Internal</span>
          </div>
          <span>{ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(radial.internal)
        .replace(/^(\D+)/, "$1 ")}</span>
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-warning' />
            <span className='font-weight-bold ml-75'>Swift</span>
          </div>
          <span>{ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(radial.swift)
        .replace(/^(\D+)/, "$1 ")}</span>
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-danger' />
            <span className='font-weight-bold ml-75'>Sepa</span>
          </div>
          <span>{ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(radial.sepa)
        .replace(/^(\D+)/, "$1 ")}</span>
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-muted' />
            <span className='font-weight-bold ml-75'>Express</span>
          </div>
          <span>{ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(radial.express)
        .replace(/^(\D+)/, "$1 ")}</span>
        </div>
        <div className='d-flex justify-content-between'>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-success' />
            <span className='font-weight-bold ml-75'>Incoming</span>
          </div>
          <span>{ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(radial.incoming)
        .replace(/^(\D+)/, "$1 ")}</span>
        </div>
      </CardBody>
    </Card>
  ) : null
}
export default ProductOrders
