import React from 'react';
import { Doughnut } from 'react-chartjs-2'
import { DownloadCloud, Monitor, Tablet } from 'react-feather'
import { Card, CardHeader, CardTitle, CardBody,Row,Col, CardText } from 'reactstrap'
const calPercent=(total,val)=>Math.floor(val/total*100)||0;
const ChartjsRadarChart = ({setStatementCSV, deviceCategory,tooltipShadow, successColorShade, warningLightColor, primary,danger }) => {
  const{A920,A60,S300,iPP350}=deviceCategory;
  let obj1={A920,A60,S300,iPP350};
  const total=A920+A60+S300+iPP350;
  const options = {
      responsive: true,
      maintainAspectRatio: false,
      responsiveAnimationDuration: 500,
      cutoutPercentage: 60,
      legend: { display: false },
      tooltips: {
        callbacks: {
          label(tooltipItem, data) {
            const label = data.datasets[0].labels[tooltipItem.index] || '',
              value = data.datasets[0].data[tooltipItem.index]
            const output = ` ${label} : ${value} %`
            return output
          }
        },
        // Updated default tooltip UI
        shadowOffsetX: 1,
        shadowOffsetY: 1,
        shadowBlur: 8,
        shadowColor: tooltipShadow,
        backgroundColor: '#fff',
        titleFontColor: '#000',
        bodyFontColor: '#000'
      }
    },
    data = {
      datasets: [
        {
          labels: ['A920', 'A60','S300','iPP350'],
          data: [ calPercent(total,A920), calPercent(total,A60),calPercent(total,S300),calPercent(total,iPP350)],
          backgroundColor: [successColorShade, danger,primary,warningLightColor ],
          borderWidth: 0,
          pointStyle: 'rectRounded'
        }
      ]
    }

  return (
    <Card>
      <CardHeader className='d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column'>
        <CardTitle tag='h4'>Device Category</CardTitle>
        <CardText className='cursor-pointer'><DownloadCloud onClick={e=> setStatementCSV({loader:true,data:obj1})} size={17} className='mr-1'/></CardText>
      </CardHeader>
      <CardBody className=' pl-0 ml-0 d-flex flex-md-row flex-column py-2'>
        <Col md={6} className=' py-1'  >
        <div style={{ height: '203px' }}>
          <Doughnut data={data} options={options} height={203} />
        </div>
        </Col>
        <Col md={6} className=' py-3'>
        <div className='d-flex justify-content-between  mb-1'>
          <div className='d-flex align-items-center'>
            <Monitor size={17} className='text-success' />
            <span className='font-weight-bold ml-75 mr-25'>A920 </span>
            <span>- {A920}</span>
          </div>
       
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Tablet size={17} className='text-danger' />
            <span className='font-weight-bold ml-75 mr-25  text-nowrap'>A60</span>
            <span>- {A60}</span>
          </div>
       
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Tablet size={17} className='text-primary' />
            <span className='font-weight-bold ml-75 mr-25  text-nowrap'>S300</span>
            <span>- {S300}</span>
          </div>
       
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Tablet size={17} className='text-warning' />
            <span className='font-weight-bold ml-75 mr-25  text-nowrap'>iPP350</span>
            <span>- {iPP350}</span>
          </div>
       
        </div>
  
        </Col>
      </CardBody>
    </Card>
  )
}

export default ChartjsRadarChart
