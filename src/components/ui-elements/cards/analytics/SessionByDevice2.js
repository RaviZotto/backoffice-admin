import React from 'react';
import {CardText, Col} from 'reactstrap'
import { Doughnut } from 'react-chartjs-2'
import { DownloadCloud, Monitor, Tablet } from 'react-feather'
import { Card, CardHeader, CardTitle, CardBody } from 'reactstrap'
const calPercent=(total,val)=>Math.floor(val/total*100)||0;
const ChartjsRadarChart = ({ setStatementCSV,tooltipShadow,reports,currency, successColorShade, warningLightColor, primary,danger }) => {
 const{debit_card_spend,flair_card_topup}=reports;
 const total=debit_card_spend+flair_card_topup;
 let obj1={ 
   Debit:debit_card_spend,
   Topup:flair_card_topup
 }
  const options = {
      responsive: true,
      maintainAspectRatio: false,
      responsiveAnimationDuration: 500,
      cutoutPercentage: 60,
      legend: { display: false },
      tooltips: {
        callbacks: {
          label(tooltipItem, data) {
            const label = data.datasets[0].labels[tooltipItem.index] || '',
              value = data.datasets[0].data[tooltipItem.index]
            const output = ` ${label} : ${value} %`
            return output
          }
        },
        // Updated default tooltip UI
        shadowOffsetX: 1,
        shadowOffsetY: 1,
        shadowBlur: 8,
        shadowColor: tooltipShadow,
        backgroundColor: '#fff',
        titleFontColor: '#000',
        bodyFontColor: '#000'
      }
    },
    data = {
      datasets: [
        {
          labels: ['Card Topup', 'Card Spending'],
          data: [ calPercent(total,flair_card_topup), calPercent(total,debit_card_spend)],
          backgroundColor: [successColorShade, danger, ],
          borderWidth: 0,
          pointStyle: 'rectRounded'
        }
      ]
    }

  return (
    <Card>
      <CardHeader className='d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column'>
        <CardTitle tag='h4'>Card</CardTitle>
        <CardText className='cursor-pointer'><DownloadCloud  onClick={e=> setStatementCSV({loader:true,data:obj1})} size={17} className='mr-1'/></CardText>
      </CardHeader>
      <CardBody className='d-flex flex-md-row flex-column pl-0 ml-0 '>
        <Col className='pl-1 mr-0 ' md={6}>
        <div style={{ height: '175px' }}>
          <Doughnut data={data} options={options} height={175} />
        </div>
        </Col>
        <Col className=' ml-0'md={6}  >
        <div className='d-flex justify-content-between mt-3 mb-1'>
          <div className='d-flex align-items-center'>
            <Monitor size={17} className='text-primary' />
            <span className='font-weight-bold ml-75 mr-25'>Topup</span>
            <span>-{ new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(flair_card_topup)
        .replace(/^(\D+)/, "$1 ")}</span>
          </div>
       
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Tablet size={17} className='text-warning' />
            <span className='font-weight-bold ml-75 mr-25  text-nowrap'>Debit</span>
            <span>- { new Intl.NumberFormat("en-US", {
        style: "currency",
        currency:currency||'GBP',
      })
        .format(debit_card_spend)
        .replace(/^(\D+)/, "$1 ")}</span>
          </div>
       
        </div>
  
        </Col>
      </CardBody>
    </Card>
  )
}

export default ChartjsRadarChart
