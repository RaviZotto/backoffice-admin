import React,{ useEffect, useMemo, useState } from 'react'
import axios from 'axios'
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,

} from 'reactstrap'
import Chart from 'react-apexcharts'
import { Circle } from 'react-feather';
const paymentMode={
  1:'sepa_transfer',
  2:'swift_transfer',
  3:'internal_transfer',
  4:'express_transfer',
  5:'incoming_transfer',
};
const calPercent=(total,val)=>Math.ceil((val/total)*100)||0;
const ProductOrders = props => {

const [radial,setRadial]=useState({internal:0,sepa:0,express:0,total:0,swift:0,incoming:0,credit:0,debit:0});
useEffect(() => {
  let  objNum={
    sepa_transfer:0,
    swift_transfer:0,
    express_transfer:0,
    fx_internal_transfer:0,
    fx_swift_transfer:0,
    internal_transfer:0,
    incoming_transfer:0,
    credit:0,
    debit:0,
    total:0


  }
if(props.txn.length){  
  let txnArray=props.txn;
 console.log(txnArray);
  for(let obj of txnArray){

   objNum.total+=1;
  
  if(!obj['transfer_type'])objNum.incoming_transfer+=1;
  else objNum[paymentMode[obj['transfer_type']]]+=1;

  obj.paid_type=="cr"?objNum.credit+=1:objNum.debit+=1;
 }

 console.log(objNum);
const {sepa_transfer,express_transfer,fx_internal_transfer,internal_transfer,incoming_transfer,credit,debit,total,fx_swift_transfer,swift_transfer}=objNum;
setRadial({credit,debit, total, internal:internal_transfer,sepa:sepa_transfer,swift:swift_transfer,express:express_transfer,incoming:incoming_transfer});
}
},[props.txn])

  const options = useMemo(() =>({
      labels: ['Incoming', 'Swift', 'Sepa','Express'],
      plotOptions: {
        radialBar: {
          size: 150,
          hollow: {
            size: '20%'
          },
          track: {
            strokeWidth: '100%',
            margin: 15
          },
          dataLabels: {
            value: {
              fontSize: '1rem',
              colors: '#5e5873',
              fontWeight: '500',
              offsetY: 5
            },
         
          }
        }
      },
      colors: [props.primary, props.warning, props.danger,props.success],
      stroke: {
        lineCap: 'round'
      },
      chart: {
        height: 355,
        dropShadow: {
          enabled: true,
          blur: 3,
          left: 1,
          top: 1,
          opacity: 0.1
        }
      }
    }),[radial]) ;
    const series = [calPercent(radial.total,radial.incoming),calPercent(radial.total,radial.swift),calPercent(radial.total,radial.sepa),calPercent(radial.total,radial.express)]

  return props.reports !== null ? (
    <Card>
      <CardHeader>
        <CardTitle tag='h4'>Transfer Status</CardTitle>
        
      </CardHeader>
      <CardBody>
       <div className='d-flex  justify-content-center'> 
      <Chart options={options} series={series} type='radialBar' height={285} />
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-primary' />
            <span className='font-weight-bold ml-75'>Internal</span>
          </div>
          <span>{ radial.internal}</span>
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-warning' />
            <span className='font-weight-bold ml-75'>Swift</span>
          </div>
          <span>{radial.swift}</span>
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-danger' />
            <span className='font-weight-bold ml-75'>Sepa</span>
          </div>
          <span>{ radial.sepa}</span>
        </div>
        <div className='d-flex justify-content-between mb-1'>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-danger' />
            <span className='font-weight-bold ml-75'>Express</span>
          </div>
          <span>{ radial.express}</span>
        </div>
        <div className='d-flex justify-content-between mb-1 '>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-danger' />
            <span className='font-weight-bold ml-75'>Incoming</span>
          </div>
          <span>{radial.incoming}</span>
        </div>
        <div className='d-flex justify-content-between mb-1 '>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-danger' />
            <span className='font-weight-bold ml-75'>Credit</span>
          </div>
          <span>{radial.credit}</span>
        </div>
        <div className='d-flex justify-content-between '>
          <div className='d-flex align-items-center'>
            <Circle size={15} className='text-danger' />
            <span className='font-weight-bold ml-75'>Debit</span>
          </div>
          <span>{radial.debit}</span>
        </div>
      </CardBody>
    </Card>
  ) : null
}
export default ProductOrders
