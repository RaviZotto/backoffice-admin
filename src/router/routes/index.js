import { lazy } from "react";

// ** Document title
const TemplateTitle = "%s - Vuexy React Admin Template";

// ** Default Route
const DefaultRoute = "/Dashboard";

// ** Merge Routes
const Routes = [
  { 
    path:"/dashboard", 
    component: lazy(() =>import("../../views/Dashboard")),
    access:"dashboard"
  },

  {
    path: "/merchant",
    component: lazy(() => import("../../views/Merchant")),
    access:'merchant'
  },
  {
    path: "/merchant-manage",
    component: lazy(() => import("../../views/Merchant/AddMerchant/index")),
    access:'merchant'
  },
  {
    path: "/merchant-section",
    component: lazy(() => import("../../views/Merchant/MerchantSection/index")),
    access:'merchant'
  },
  // {
  //   path: "/second-page",
  //   component: lazy(() => import("../../views/SecondPage")),
  //   access:'merchant'
  // },
  {
    path: "/edit-merchant",
    component: lazy(() => import("../../views/Merchant/EditMerchant")),
    access:'merchant'
  },

  {
    path: "/sales-report-txn",
    component: lazy(() => import("../../views/SalesReport/TxnReport")),
    access:'salesreport'
  },

  {
    path: "/sales-account-bal",
    component: lazy(() => import("../../views/SalesReport/AccountReport")),
      access:'salesreport'
  },
  {
    path: "/sales-device-report",
    component: lazy(() => import("../../views/SalesReport/DevicesReport")),
      access:'salesreport'
  },
  {
    path: "/sales-online-report",
    component: lazy(() => import("../../views/SalesReport/OnlineReport")),
      access:'salesreport'
  },
  {
    path: "/sales-transfer-report",
    component: lazy(() => import("../../views/SalesReport/TransferReport")),
      access:'salesreport'
  },
  {
    path: "/sales-total-report",
    component: lazy(() => import("../../views/SalesReport/TotalSalesReport")),
      access:'salesreport'
  },
  {
    path: "/sales-merchant-report",
    component: lazy(() => import("../../views/SalesReport/MerchantSalesReport ")),
      access:'salesreport'
  },
  {
    path: "/sales-flaircard-report",
    component: lazy(() => import("../../views/SalesReport/FlairCardReport")),
      access:'salesreport'
  },
  {
    path: "/suspicious-txn-report",
    component: lazy(() => import("../../views/SalesReport/SuspiciousTxnReport")),
      access:'salesreport'
  },
  {
    path: "/transfers",
    component: lazy(() => import("../../views/Transfer")),
    access:'transfer'
  },
  {
    path: "/settlement-txn",
    component: lazy(() => import("../../views/Transfer/settlement")),
    access:'transfer'
  },

  {
    path: "/fees/transfers-Fee",
    component: lazy(() => import("../../views/Fees/TransferTxn")),
      access:'fees'
  },
  {
    path: "/fees/transaction-fee",
    component: lazy(() => import("../../views/Fees")),
      access:'fees'
  },
  { 
    path:"/fees/transactionfeelist",
    component: lazy(() => import("../../views/Fees/TransactionFeeList")),
    access:'fees'
  },

  {
    path: "/settings",
    component: lazy(() => import("../../views/Settings")),
    access:'settings'
  },
  {
    path: "/users",
    component: lazy(() => import("../../views/Settings/Users")),
    access:'role'
  },
 
  {
    path: "/merchant-fee-manage",
    component: lazy(() => import("../../views/merchantFee/Fees")),
    access:'merchant'
  },
  {
    path: "/merchant-partner",
    component: lazy(() => import("../../views/Partner")),
    access:'partner'
  },
  {
    path: "/add-beneficiary",
    component: lazy(() => import("../../views/Transfer/beneficiary")),
    access:'transfer'
  },
  {
    path: "/transfer-history",
    component: lazy(() => import("../../views/Transfer/TransferHistory/index")),
    access:'transfer'
  },
  { 
     path:'/currency',
     component: lazy(() => import("../../views/Settings/Currency")),
     access:'settings'
  },

  {
    path: "/transfer-settings",
    component: lazy(() => import("../../views/Transfer/transfersetting")),
    access:'transfer'
  },
  {
    path: "/subscriptions",
    component: lazy(() => import("../../views/Subscriptions")),
    access:'subscription'
  },
 {
   path:'/clients',
   component: lazy(() => import("../../views/client")),
   access:'client'
 },
 {
  path:'/giftcards-discounts',
  component: lazy(() => import("../../views/giftcards-discountsection")),
  access:'flaircard'
},
{
  path:'/giftcards',
  component: lazy(() => import("../../views/giftcards")),
  access:'flaircard'
},
{
  path:'/notification',
  component: lazy(() => import("../../views/notification")),
  access:'notification'
},
  // {
  //   path: "/accounts-statement",
  //   component: lazy(() => import("../../views/accounts/Statement")),
  // },
  // {
  //   path: "/manage-invoice",
  //   component: lazy(() => import("../../views/Invoices/ManageInvoice")),
  // },
  // {
  //   path: "/invoices",
  //   component: lazy(() => import("../../views/Invoices")),
  // },
  {
    path: "/login",
    component: lazy(() => import("../../views/authentication/Login")),
    layout: "BlankLayout",
    meta: {
      publicRoute: true,
    },
  },
  {
    path: "/verify",
    component: lazy(() => import("../../views/authentication/Verify")),
    layout: "BlankLayout",
    meta: {
      publicRoute: true,
    },
  },
  {
    path: "/forgot-password",
    component: lazy(() => import("../../views/authentication/ForgotPassword")),
    layout: "BlankLayout",
    meta: {
      publicRoute: true,
    },
  },
  {
    path: "/error",
    component: lazy(() => import("../../views/misc/Error")),
    layout: "BlankLayout",
    meta: {
      publicRoute: true,
    },
  },
];

export { DefaultRoute, TemplateTitle, Routes };
