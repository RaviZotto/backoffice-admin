import moment from "moment";
import produce from 'immer';
// ** Initial State
const initialState = {
  merchants: [],
  transaction: [],
  error: "",
  loading: true,
  partners: [],
  partnerObj: {},
  roles: [],
  devices: [],
  types:[],
  subtypes:[],
  users:[],
  notifications:[]
};

const merchantReducer = (state = initialState, action) => {
  switch (action.type) {
    case "UPDATE_MERCHANT_TYPE":
      const{id,params}=action.payload;
      //console.log(id,params)
      
      
      //console.log(obj2, action.payload, merchantclone);
      // let idx2 = merchantclone1.indexOf(obj2);
      
      return {...state,merchants:state.merchants.map(el=>el.merchant_id==id?{...el,...params}:el)};
    case "UPDATE_DEVICES":
      return {...state,devices:action.payload}
    case "UPDATE_PARTNER":
      {
      let obj = state.partners.find(el=>el.id== action.payload.id);
       obj={...obj,...action.payload};
       const partnerArr=state.partners.filter(el=>el.id!= action.payload.id);
       
       return { ...state, parnters: [...partnerArr,obj] };
                  
      }
    
    case "GET_ROLE":
      return { ...state, roles: action.payload };
    case "GET_TYPE":
      return { ...state, types: action.payload };
    case "GET_SUBTYPE":
      return { ...state, subtypes: action.payload };
    case "GET_USER":
       return {...state,users:action.payload};
    case "UPDATE_USER":
      return {...state,users:produce(state.users, (x) => {
                  let i = x.findIndex((el) => el.id == action.payload.id);
                  x[i]={...x[i],...action.payload.params}
                  //console.log(x[i]) 
                })}
   
    case "UPDATE_ROLE":
      return {
        ...state,
        roles: state.roles.push({
          ...action.payload.data,
          id: state.roles.length + 1,
          create: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
        }),
      };
    case "GET_NOTIFICATION":
      return {...state,notifications:action.payload}  
    case "DELETE_ROLE":
      return {
        ...state,
        roles: state.roles.filter((el) => el.id != action.payload),
      };
    case "UPDATE_MERCHANT":
      return { ...state, merchants: action.payload };
    case "DEL_MERCHANT":
      return {...state,merchants:state.merchants.filter(el=>el.merchant_id!=action.payload)}
    case "GET_MERCHANT":
      return { ...state };
    case "ERROR_HANDLER":
      return { ...state, error: action, loading: false };

    case "GET_PARTNER":
      return { ...state, partners: action.payload };
    case "STORE_PARTNER_OBJ":
      let partnerObj = state.partners.find((res) => res.id == action.payload);
      return { ...state, partnerObj };
    case "CLR_PARTNER_OBJ":
      return { ...state, partnerObj: {} };
    case "TOGGLE_MERCHANT":
      let merchantclone = [...state.merchants];
      let obj = merchantclone.find((el) => el.merchant_id == action.payload.id);
      //console.log(obj, action.payload, merchantclone);
      let idx = merchantclone.indexOf(obj);
      merchantclone[idx]["status"] = action.payload.status ? 0 : 1;
      
      return { ...state, merchants: merchantclone };

    case "TOGGLE_PARTNER":
      let partnerclone = [...state.partners];
      let obj1 = partnerclone.find((el) => el.id == action.payload);
      //console.log(obj1);
      let idx1 = partnerclone.indexOf(obj1);
      partnerclone[idx1]["status"] ^= 1;

      return { ...state, partners: [...partnerclone] };
    default:
      return state;
  }
};

export default merchantReducer;
