import { getMerchantAccount,getAllBeneficiary,addBeneficiary, addClient,getClient,addSubscription,getSubscription, getTransferFee, udpateTransferFee, deleteTransferfee, addTransferFee, getExchangeRate, getPaymentGateway, getAllCurrency, deleteAdminCurrency, getAdminExchangeRate, updateAdminExchangeRate, getTransactionFee, updateCardStatus, getCountries } from "../../../Api";
import notification from "../../../components/notification";

export const STORE_MERCHANTACCOUNTS=(callback)=>dispatch=>{ 
  

  getMerchantAccount(({err,res})=>{ 
      if(err)notification({title:'Error',message:err,type:'error'});
      else dispatch({type:'STORE_MERCHANTACCOUNTS',payload:res})
      callback&&callback()
  });
}


export const ADD_BENEF=(Id,value,callback)=>dispatch=>{
  addBeneficiary(Id,value,({err,res})=>{ 
     if(err){return callback({err}),notification({title:"Error",type:'error',message:err.err})}
     else dispatch({type:"ADD_BENEF",payload:res.benefObj})
     notification({title:'Beneficiary',message:'successfully added',type:'success'})
     callback&&callback({})

  })
}

export const GET_COUNTRIES = (callback) =>async(dispatch)=>{
  try{
   const {countries} = await getCountries();
    dispatch({type:'GET_COUNTRIES',payload:countries});
    callback&&callback({});
}catch(e){
  callback&&callback({err:e});
  notification({title:"Error",type:'error',message:e.toString()})
  }
}


export const STORE_EXCHANGE=(callback)=>dispatch=>{ //
  getExchangeRate(({err,res})=>{ 
    if(err){notification({title:"Error",type:'error',message:err})}
    else {dispatch({type:"STORE_EXCHANGE",payload:res})}
    callback&&callback()
  })

}

export const STORE_BENEFICIARIES=(callback)=>dispatch=>{ 
     getAllBeneficiary(({err,res})=>{ 
        if(err)notification({title:'Error',message:err,type:'error'});
        else dispatch({type:"STORE_BENEFICIARIES",payload:res})
        callback&&callback()
    });
  }




export const STORE_TRANSFERFEE=(callback)=>dispatch=>{ 
  getTransferFee(({err,res})=>{ 
    //console.log(res);
     if(err)notification({title:'Error',message:err,type:'error'});
     else dispatch({type:"STORE_TRANSFERFEE",payload:res})
     callback&&callback()
 });
}

export const UPDATE_TRANSFERFEE=(value,callback)=>dispatch=>{ 
  udpateTransferFee(value,({err,res})=>{ 
    //console.log(res);
     if(err)notification({title:'Error',message:err,type:'error'});
     else dispatch({type:"UPDATE_TRANSFERFEE",payload:value})
     callback&&callback()
 });
}

export const DEL_TRANSFERFEE=(id,callback)=>dispatch=>{ 
  deleteTransferfee(id,({err,res})=>{ 
    //console.log(res);
     if(err)notification({title:'Error',message:err,type:'error'});
     else dispatch({type:"DEL_TRASFERFEE",payload:id})
     callback&&callback()
 });
}

export const ADD_TRANSFERFEE=(value,callback)=>dispatch=>{ 
  addTransferFee(value,({err,res})=>{ 
    //console.log(res);
     if(err)notification({title:'Error',message:err,type:'error'});
     else dispatch({type:"ADD_TRASFERFEE",payload:value})
     callback&&callback()
 });
}

export const GET_SUBSCRIPTION=(value,callback)=>dispatch=>{ 
   getSubscription(value,({err,res})=>{
      if(err)notification({title:"Error",message:err,type:"error"})
      else dispatch({type:'GET_SUBS',payload:res.subscriptions})
      callback&&callback();
    })
}
export const GET_CLIENTS=(callback)=>dispatch=>{ 
  getClient(({err,res})=>{
     if(err)notification({title:"Error",message:err,type:"error"})
     else dispatch({type:'GET_CLIENTS',payload:res.clients})
     callback&&callback();
   })
}

export const ADD_CLIENTSOBJ=(value,callback)=>dispatch=>{ 
  addClient(value,({err,res})=>{
     if(err)callback({err})
     else {dispatch({type:'ADD_CLIENTOBJ',payload:res.clientObj}); callback({})}
     
   })
}

export const ADD_SUBS=(value,callback)=>dispatch=>{ 
  addSubscription(value,({err,res})=>{
    
     if(err)callback&&callback({err:err})
     else dispatch({type:'ADD_SUBS',payload:res.subsObj||{}});callback({})
  
   })
}

export const GET_GATEWAY=(callback)=> dispatch=>{ 
  getPaymentGateway(({err,res})=>{ 
    if(err)callback&&callback({err:err})
    else dispatch({type:'GET_GATEWAY',payload:res})
  })

}
export const GET_CURRENCY=(callback)=> dispatch=>{ 

  getAllCurrency(({err,res})=>{ 
    //console.log(err,res);
    if(err)notification({title:"Error",message:err,type:"error"})
    else dispatch({type:"GET_CURRENCY",payload:res||[{currency:'GBP'},{currency:'EUR'},{currency:'DKK'},{currency:'USD'}]})
    callback&&callback()
  })

}

export const DELETE_CURRENCY=(id,callback)=> dispatch=>{ 

  deleteAdminCurrency(id,({err,res})=>{ 
    //console.log(err,res);
    if(err)notification({title:"Error",message:err,type:"error"})
    // else dispatch({type:"GET_CURRENCY",payload:res||[{currency:'GBP'},{currency:'EUR'},{currency:'DKK'},{currency:'USD'}]})
    callback&&callback()
  })

}

export const GET_EXCHANGE_RATE=(callback)=>dispatch=>{ 
      getAdminExchangeRate(({err,res})=>{
        if(err)notification({title:"Error",message:err,type:"error"});
        else dispatch({type:'SAVE_EXCHANGE_RATE',payload:res})
        callback&&callback()
      })
}

export const UPDATE_EXCHANGE_RATE =(value,callback)=>dispatch=>{
       updateAdminExchangeRate(value,({err,res})=>{
       if(err)notification({title:"Error",message:err,type:"error"});
       dispatch({type:'SAVE_EXCHANGE_RATE',payload:value})
       callback&&callback()
      })
}

export const SAVE_TRANSACTION_FEE=(callback)=>dispatch=>{
    getTransactionFee(undefined,({err,res})=>{
      if(err)notification({title:"Error",message:err,type:"error"});
      dispatch({type:'SAVE_TRANSACTION_FEE',payload:res});
      callback();
    })
}


export const MODIFY_CARD_STATUS=(id,action,callback)=>dispatch=>{
   updateCardStatus(id,action,({err,res})=>{
    if(err)notification({title:"Error",message:err,type:"error"});
    callback()
   })
}