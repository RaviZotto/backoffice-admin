import { getAdminDetails } from "../../../Api"
import notification from "../../../components/notification"

// ** Handle User Login
export const handleLogin = data => {
  return dispatch => {
    dispatch({ type: 'LOGIN', data })

    // ** Add to user to localStorage
    localStorage.setItem('userData', JSON.stringify(data))
   
  }
}

// ** Handle User Logout
export const handleLogout = () => {
  return dispatch => {
    dispatch({ type: 'LOGOUT' })

    // ** Remove user from localStorage
    localStorage.clear()
  }
}
export const ADMIN_IMAGE=(value)=> dispatch=>dispatch({ type: 'IMAGE',payload:value})

export const USER_DETAIL=(id,callback)=>{ return dispatch=>{ 
  getAdminDetails(id,({err,res})=>{
    if(err)notification({title:'Error',message:err.message,type:'error'})
    else { dispatch({type:'GET_USERDETAIL',payload:res.adminDetails}); callback&&callback();}
    
  })
 }
}
 

export const STORE_ADMINDETAIL = (value,callback)=>dispatch=>{dispatch({type:`GET_USERDETAIL`,payload:value});callback&&callback()}