import { addRole, deleteRole,addBeneficiary,updateMerchant, getAllDevice, getAllMerchant,getSubType,getType, getAllPartner, getAllRegDevice, getRole, toggleMerchantStatus, toggleParterStatus, changeTransfer, addUser, getUser, updateUser, getAllNotifications, updateSuspiciousTxn, updatePartner} from "../../../Api"
import notification from "../../../components/notification";
import AddRole from "../../../views/Role/AddRole";
export const UPDATE_MERCHANT = (callback) => {
    return dispatch => {
       getAllMerchant().then(res=>{
         //console.log(res,'12');
           if(res.merchant){dispatch({type:'UPDATE_MERCHANT',payload:res.merchant.filter(el=>!el.is_test)}); }
           callback&&callback();
          }).catch((e)=>{
            notification({title:'Error',message:e.toString(),type:'error'});
            callback&&callback()})
      
    }
  }

export const GET_MERCHANT = value => dispatch => dispatch({ type: 'GET_MERCHANT', value })

export const GET_PARTNER = (callback)=>{
  return dispatch=>{
    getAllPartner(({err,res})=>{
      if(err)notification({title:'Error',type:'error',message:err});
      else dispatch({type:'GET_PARTNER',payload:res});//console.log(res);
      if(callback)callback()
    })

  }
}

export const UPDATE_USER=(id,params,callback)=>{ 
  return dispatch=>{   
    updateUser(id,params,({err,res})=>{ 
      if(err)notification({title:'Error',type:'error',message:err});
      else dispatch({type:'UPDATE_USER',payload:{id,params}});
      callback&&callback();
    })
  }
}

export const ADD_USER=(params,callback)=>{ 
  return dispatch=>{   
    addUser(params,({err,res})=>{ 
      if(err)notification({title:'Error',type:'error',message:err});
      callback&&callback();
    })
  }
}
export const GET_USER=(callback)=>{ 
  return dispatch=>{   
    getUser(({err,res})=>{ 
      if(err)notification({title:'Error',type:'error',message:err});
      else dispatch({type:'GET_USER',payload:res});
      callback&&callback();
    })
  }
}

export const UPDATE_PARTNER=(params,callback)=>{
  return async (dispatch) => {
      try{
      await updatePartner(params);
      callback&&callback()
      notification({title:'Partner',type:'success',message:"saved successfully"});
        const id = params.partner_id;
        delete params.partner_id;
      dispatch({type:"UPDATE_PARTNER",payload:{...params.general,...params.bank,id}});
      }catch(err){
        notification({title:'Error',type:'error',message:err})
      }
   
  }
} 

export const UPDATE_MERCHANT_TYPE=(id,params,callback)=>{ 
  return dispatch=>{ 
    changeTransfer(id,{merchant:params}).then(res=>{ 
       if(res)dispatch({type:"UPDATE_MERCHANT_TYPE",payload:{id,params}})
    })
    .catch(e=>{ 
       //console.log(e)
         if(e)notification({title:'Error',type:'error',message:e});
    })
  }
}


export const GET_SUBTYPE= (callback)=>{
  return dispatch=>{
    getSubType(({err,res})=>{
      if(err)notification({title:'Error',type:'error',message:err});
      else dispatch({type:'GET_SUBTYPE',payload:res.subtypes});//console.log(res);
      if(callback)callback()
    })
  }
}


export const GET_TYPE= (callback)=>{
  return dispatch=>{
    getType(({err,res})=>{
      if(err)notification({title:'Error',type:'error',message:err});
      else dispatch({type:'GET_TYPE',payload:res.types});//console.log(res);
      if(callback)callback()
    })
  }
}


export const STORE_PARTNER_OBJ=id=>dispatch=>dispatch({type:'STORE_PARTNER_OBJ',payload:id})
export const CLR_PARTNER_OBJ=()=>dispatch=>dispatch({type:'CLR_PARTNER_OBJ'});

export const TOGGLE_PARTNER =(id)=>dispatch=>{
  toggleParterStatus({partner_id:id},({err,res})=>{
    if(err)notification({type:'error',message:err,title:'Error'});
    else dispatch({type:"TOGGLE_PARTNER",payload:id});
  })
}

export const TOGGLE_MERCHANT =(id,status)=>dispatch=>{
  toggleMerchantStatus({merchant_id:id,status},({err,res})=>{
    if(err)notification({type:'error',message:err,title:'Error'});
    else dispatch({type:"TOGGLE_MERCHANT",payload:{id,status}});
  })
}


export const GET_ROLE=(callback)=>dispatch=>{
 
    getRole(({err,res})=>{
      if(res){dispatch({type:'GET_ROLE',payload:res});}
      else notification({title:'Error',message:err,type:'error'})
     if(callback)callback();
    })

}


export const UPDATE_ROLE=(params,callback)=>dispatch=>{
   addRole(params,(({err,res})=>{
     if(err)notification({title:'Error',message:err,type:'error'})
     else dispatch({type:'UPDATE_ROLE',payload:params})
     callback()
   })) 
}


export const DELETE_ROLE =(id,callback)=>dispatch=>{
   deleteRole(id,({err,res})=>{
    if(err)notification({title:'Error',message:err,type:'error'})
    else dispatch({type:'DELETE_ROLE',payload:id})
    callback&&callback()
  })
}

export const UPDATE_DEVICES=(callback)=>dispatch=>{ 
  getAllRegDevice().then(res=>{dispatch({type:'UPDATE_DEVICES',payload:res.device})}).catch(err=>notification({title:'Error',message:err.toString(),type:'error'}))
 callback&&callback()
}

export const GET_NOTIFICATION =(value)=>dispatch=>{
  getAllNotifications(value,({err,res})=>{ 
     if(err)notification({title:'Error',message:err, type:'error'})
     else dispatch({type:'GET_NOTIFICATION',payload:res.notifications})
  })
}

export const UPDATE_SUSPICIOUS_TXN =(id,key,callback)=>dispatch=>{
      updateSuspiciousTxn(id,key,({err,res})=>{ 
     if(err)notification({title:'Error',message:err, type:'error'})
     else dispatch({type:'GET_NOTIFICATION',payload:res.notifications})
     callback&&callback();
  })
}