import React from "react";
import {
  Mail,
  ShoppingBag,

  Settings,

  CreditCard,
  AlertTriangle,
  FileText,
  User,
  Users,
   CloudRain,
  Gift,
  Airplay,
  BarChart2,
  Briefcase,
  Bell,
  UserCheck,
  PlayCircle,
  UserPlus,
} from "react-feather";

export default [
  { 
   id:"dashboard",
   title:"Dashboard",
   icon:<Airplay size={20} />,
   navLink:"/dashboard"
  },

  {
    id: "merchant",
    title: "Merchant",
    icon: <ShoppingBag size={20} />,
    navLink: "/merchant",
  },

  {
    id: "partner",
    title: "Partner",
    icon: <UserCheck size={20} />,
    navLink: "/merchant-partner",
  },
 
  {
    id: "subscription",
    title: "Subscription",
    icon: <PlayCircle size={20} />,
    navLink: "/Subscriptions",
  },

  {
    id: "transfer",
    icon: <Mail size={20} />,
    title: "Transfer",
    children: [
      {
        icon: <CreditCard size={20} />,
        navLink: "/transfers",
        title: "TransferTxn",
      },
      {
        icon: <CreditCard size={20}/>,
        navLink: "/settlement-txn",
        title:"Credit/Debit"
      },
      {
        icon: <CreditCard size={20} />,
        navLink: "/transfer-settings",
        title: "Transfer Settings",
      },
   

      {
        icon: <User size={20} />,
        navLink: "/add-beneficiary",
        title: "Beneficiary",
      },
      {
        icon: <CloudRain size={20} />,
        navLink: "/transfer-history",
        title: "Transfer history",
      },
    ],
  },
  {
    id: "fees",
    title: "Fees",
    icon: <Briefcase size={20} />,
    children: [
      {
        icon: <CreditCard size={20} />,
        navLink: "/fees/transfers-fee",
        title: "Transfer Fee",
      },
      {
        icon:<CreditCard size={20} />,
        navLink:'/fees/transaction-fee',
        title:'Transaction Fee',
      },
      { 
         icon:<CreditCard size={20} /> ,
         navLink:'/fees/transactionfeelist',
         title :'Transaction Fee List'
      }
     ]
  },
 
  {
    id: "salesreport",
    title: "SalesReport",
    icon: <BarChart2 size={20} />,
    children: [ 
      { 
        id:"transaction report",
        title:"Transaction Report",
        icon: <FileText size={20} />,
        navLink:'/sales-report-txn',
      },
      { 
        id:"account report",
        title:"Account Balance Report",
        icon: <FileText size={20} />,
        navLink:'/sales-account-bal'
      },
      { 
        id:"devices report",
        title:"Devices Report",
        icon: <FileText size={20} />,
        navLink:'/sales-device-report'
      },
      { 
        id:"online report",
        title:"Online Report",
        icon: <FileText size={20} />,
        navLink:'/sales-online-report'
      },
      { 
        id:"transfer report",
        title:"Transfer Report",
        icon: <FileText size={20} />,
        navLink:'/sales-transfer-report'
      },
      { 
        id:"merchant report",
        title:"Merchant sales report",
        icon: <FileText size={20} />,
        navLink:'/sales-merchant-report'
      },
      { 
        id:"total sales report",
        title:"Total sales report",
        icon: <FileText size={20} />,
        navLink:'/sales-total-report'
      },
      { 
        id:"flaircard report",
        title:"FlairCard Report",
        icon: <FileText size={20} />,
         navLink:'/sales-flaircard-report'
       },
      { 
        id:"suspicious transaction report",
        title:"Suspicious Transaction Report",
        icon: <FileText size={20} />,
         navLink:'/suspicious-txn-report'
       }
    ]
  },
    {
    id: "role",
    title: "Users",
    icon: <UserPlus size={20} />,
    navLink: "/users",
  },
  {
    id: "client",
    title: "Clients",
    icon: <Users size={20} />,
    navLink: "/clients",
  },
 
  {
    id:"flaircard",
    title:'Gift Cards',
    icon: <Gift size={20}/>,
    children: [
      {
    
        title: "Cards",
        icon: <Gift size={20} />,
        navLink: "/giftcards",
      },
      {
      
        title: "Discounts",
        icon: <Gift size={20} />,
        navLink: "/giftcards-discounts",
      },
    ]
  },
 
 
  {
    id: "notification",
    title: "Notification",
    icon: <Bell size={20} />,
    navLink: "/notification",
  },
    
  {
    id: "settings",
    title: "Settings",
    icon: <Settings size={20} />,
    navLink: "/settings",
  }

];
