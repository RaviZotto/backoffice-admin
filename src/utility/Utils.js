// ** Checks if an object is empty (returns boolean)
import countries from "./extras/countries.json";
import phoneCodes from "./extras/phoneCodes.json";
import currencyCodes from "./extras/currencyCodes.json";
import transferCountries from './extras/transferCountries.json';
import  currencyCountryCodes from './extras/currencyCodesWithCountry.json';
import { Routes } from "../router/routes";
import {AdminDetails} from '@src/App';

export var userDetails=JSON.parse(localStorage.getItem('userDetails'))||{};

export { countries, phoneCodes, currencyCodes ,transferCountries,currencyCountryCodes};
export const isObjEmpty = (obj) => Object.keys(obj).length === 0;

// ** Returns K format from a number
export const kFormatter = (num) => (num > 999 ? `${(num / 1000).toFixed(1)}k` : num);

export const getHomeRouteForLoggedInUser = () => {
  const userDetails= JSON.parse(localStorage.getItem('userDetails'));
  //console.log(userDetails);
  let permissionsObj=['dashboard','merchant','transfer','settings','partner','salesreport','subscription','notification','flaircard','client','fees'];
  let permissions = userDetails;
  //console.log(AdminDetails(),userDetails)
  if ( permissions?.role_type=='admin'  ) return "/dashboard";
  for (let key of permissionsObj) {
     //console.log(key,permissions,11);
    if (permissions&&permissions[key]?.includes("R")) { 
      let routes= Routes.filter(e=>e.access==key);
      routes=routes[0].path;
      //console.log(routes);
      return `${routes}`;}

      
  
  }
  

};


export const calculateTotalFee = (amount, fees) => {
  if (!amount || !fees) return "0.00";
  let totalFee = (Number(amount) * Number(fees.fee || fees.fx_fee)) / 100 + Number(fees.fixed_fee);
  return Number.parseFloat(totalFee).toFixed(2);
};

// ** Converts HTML to string
export const htmlToString = (html) => html.replace(/<\/?[^>]+(>|$)/g, "");

// ** Checks if the passed date is today
const isToday = (date) => {
  const today = new Date();
  return (
    /* eslint-disable operator-linebreak */
    date.getDate() === today.getDate() &&
    date.getMonth() === today.getMonth() &&
    date.getFullYear() === today.getFullYear()
    /* eslint-enable */
  );
};

export const monthOptions = [
  { value: "January", label: "January" },
  { value: "February", label: "February" },
  { value: "March", label: "March" },
  { value: "April", label: "April" },
  { value: "May", label: "May" },
  { value: "June", label: "June" },
  { value: "July", label: "July" },
  { value: "August", label: "August" },
  { value: "September", label: "September" },
  { value: "October", label: "October" },
  { value: "November", label: "November" },
  { value: "December", label: "December" },
];

export const yearOptions = [
  { value: "2025", label: "2025" },
  { value: "2024", label: "2024" },
  { value: "2023", label: "2023" },
  { value: "2022", label: "2022" },
  { value: "2021", label: "2021" },
  { value: "2020", label: "2020" },
  { value: "2019", label: "2019" },
  { value: "2018", label: "2018" },
];

export const downloadDoc = (data, callback) => {
  let a = document.createElement("a");
  a.style = "display: none";
  a.href = data;
  a.download = data.split("/").pop();
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
  if (callback) callback();
};

export const previewDoc = (data, toolbar = false) => {
  return window.open(
    `${data}${!toolbar ? "#toolbar=0" : ""}`,
    "Invoice Preview",
    "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no"
  );
};


/**
 ** Format and return date in Humanize format
 ** Intl docs: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/format
 ** Intl Constructor: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/DateTimeFormat
 * @param {String} value date to format
 * @param {Object} formatting Intl object to format with
 */
export const formatDate = (value, formatting = { month: "short", day: "numeric", year: "numeric" }) => {
  if (!value) return value;
  return new Intl.DateTimeFormat("en-US", formatting).format(new Date(value));
};

// ** Returns short month of passed date
export const formatDateToMonthShort = (value, toTimeForCurrentDay = true) => {
  const date = new Date(value);
  let formatting = { month: "short", day: "numeric" };

  if (toTimeForCurrentDay && isToday(date)) {
    formatting = { hour: "numeric", minute: "numeric" };
  }

  return new Intl.DateTimeFormat("en-US", formatting).format(new Date(value));
};

/**
 ** Return if user is logged in
 ** This is completely up to you and how you want to store the token in your frontend application
 *  ? e.g. If you are using cookies to store the application please update this function
 */
export const isUserLoggedIn = () =>{
  let token_item =localStorage.getItem("token_item");
  token_item=JSON.parse(token_item);
  if(token_item){
  const {token,access_expire_date:expire_date,refresh_expire_token,refresh_token}=token_item;
  console.log(token&&refresh_token&&(parseInt(expire_date)>Math.ceil(Date.now()/1000)&&parseInt(refresh_expire_token)>Math.ceil(Date.now()/1000) ));
  return token&&refresh_token&&(parseInt(expire_date)>Math.ceil(Date.now()/1000)&&parseInt(refresh_expire_token)>Math.ceil(Date.now()/1000) );
  }
  return false;
}
export const getUserData = () => JSON.parse(localStorage.getItem("userData"));

/**
 ** This function is used for demo purpose route navigation
 ** In real app you won't need this function because your app will navigate to same route for each users regardless of ability
 ** Please note role field is just for showing purpose it's not used by anything in frontend
 ** We are checking role just for ease
 * ? NOTE: If you have different pages to navigate based on user ability then this function can be useful. However, you need to update it.
 * @param {String} userRole Role of user
 */
// export const getHomeRouteForLoggedInUser = (userRole) => {
//   if (userRole === "admin") return "/";
//   if (userRole === "client") return "/access-control";
//   return "/login";
// };

// ** React Select Theme Colors
export const selectThemeColors = (theme) => ({
  ...theme,
  colors: {
    ...theme.colors,
    primary25: "#7367f01a", // for option hover bg-color
    primary: "#7367f0", // for selected option bg-color
    neutral10: "#7367f0", // for tags bg-color
    neutral20: "#ededed", // for input border-color
    neutral30: "#ededed", // for input hover border-color
  },
});

export const getBase64FromUrl = async (url) => {
  const data = await fetch(url);
  const blob = await data.blob();
  return new Promise((resolve,rej) => {
    const reader = new FileReader();
    reader.readAsDataURL(blob); 
    reader.onloadend = () => {
      const base64data = reader.result;
      if(!base64data.includes('image/png'))rej('not an img')   
      resolve(base64data);
    }
  });
}
   
export const capitalFirstLetter = (data = "") => data.slice(0, 1).toUpperCase() + data.slice(1).toLowerCase();

export const defaultQrCode =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAAAklEQVR4AewaftIAAAeRSURBVO3BQY4kR5IAQVVH/f/Lun0bOwUQyKwm6Wsi9gdrXeKw1kUOa13ksNZFDmtd5LDWRQ5rXeSw1kUOa13ksNZFDmtd5LDWRQ5rXeSw1kUOa13ksNZFfviQyt9UMan8popPqDypeKLyRsUTlaniicrfVPGJw1oXOax1kcNaF/nhyyq+SeVJxaQyVUwqU8Wk8kRlqphUpoonKk8qJpWp4onKN1V8k8o3Hda6yGGtixzWusgPv0zljYrfVPGk4g2VqeKJyt9UMal8k8obFb/psNZFDmtd5LDWRX74j1N5ovJGxZOKSWVSmSreqHhSMams/zmsdZHDWhc5rHWRH/6fqZhUJpWpYlJ5UjGpTBVPVKaKSWWqmFTeUJkq/ssOa13ksNZFDmtd5IdfVvFfUjGpPKl4Q2WqeKPijYonKp+o+Dc5rHWRw1oXOax1kR++TOWfVDGpfFPFpDJVPKmYVKaKSWWqmFSmikllqviEyr/ZYa2LHNa6yGGti/zwoYp/s4pJZaqYVKaKSWWqmFSeqEwV36TyRsWTiv+Sw1oXOax1kcNaF/nhQypTxRsqU8Wk8m9S8aRiUnlD5YnKVDGpTBWTylQxqXxTxROVqeITh7UucljrIoe1LmJ/8AGVJxVPVJ5UTCpTxROVT1Q8UZkqnqg8qXhDZaqYVN6oeKIyVbyhMlV802GtixzWushhrYvYH/wilTcqJpU3Kp6oTBWTypOKSWWqeEPlScWkMlVMKp+omFSmijdUporfdFjrIoe1LnJY6yI//LKKT1RMKlPFpDJVTBXfVDGpPKmYKiaVN1SmikllqphU3lCZKiaVqWJSmSq+6bDWRQ5rXeSw1kV++DKVqWJSmSomlScVTyomlaliUnlDZap4Q+UNlScVTyqeVPyTVKaKTxzWushhrYsc1rqI/cEHVKaKSWWqeKIyVUwqU8Wk8qTiDZVvqphUpoonKk8qnqhMFW+oPKl4Q2Wq+MRhrYsc1rrIYa2L2B98QOVJxaTyiYpJZar4JpWpYlKZKiaVf1LFpPKkYlKZKiaVT1R802GtixzWushhrYv88MtUnlT8l6hMFU8q3lB5UjGpPFF5Q+UTFZPKVDGpTBWfOKx1kcNaFzmsdZEfPlQxqTypmFTeqHii8kbFpPKkYlKZKp6oPKl4ovKkYlKZKiaVT1T8mxzWushhrYsc1rqI/cEHVJ5UTCpPKn6TypOKSWWqeEPlScUbKp+oeKIyVUwqU8WkMlVMKlPFNx3WushhrYsc1rrIDx+qmFTeqJhU3qh4o+KJyhOVqeI3qUwVb6hMKk8qJpVvqvhNh7UucljrIoe1LvLDL1P5RMUbKk8qvknlEypPKp6oTBVTxT9JZaqYVKaKTxzWushhrYsc1rrID19WMalMFU8qJpUnFVPFN1W8oTJVPFF5ojJVfJPKGxVvVEwqU8U3Hda6yGGtixzWusgPH1L5hMqTijdUnlRMKk9UpopJZaqYVKaKT6hMFW+ovFExqTyp+Ccd1rrIYa2LHNa6yA9fVjGpPKmYVCaVJxVPKiaVJxWfUJkq3qh4ojKpTBWfqHhS8YbK33RY6yKHtS5yWOsiP3yZylQxqUwVTyomlUllqphUpopJZVKZKj6hMlW8ofKk4hMVk8qTikllqnhS8ZsOa13ksNZFDmtdxP7gAypTxTepPKn4JpWpYlKZKp6oTBVvqHyi4ptUPlExqUwVnzisdZHDWhc5rHWRH75M5UnFE5WpYlJ5ovJGxROVqWJSmSqmiknlScVUMalMFZPKGypvVEwqU8UbFd90WOsih7UucljrIj98qOKJyhOVqWJSeaLypGJSmVSmim9SmSomlScqb1RMKm9UfJPKVPGbDmtd5LDWRQ5rXcT+4C9SeaPiDZUnFZPKVDGpTBVPVKaKN1TeqHhDZaqYVKaKT6hMFZPKVPGJw1oXOax1kcNaF7E/+IDKGxWTyhsVn1D5popJZaqYVKaKSeUTFU9U/qaKSWWq+KbDWhc5rHWRw1oX+eFDFZ+o+ITKVDGpPKmYVKaKJypvVEwqU8UTlaliUvlExRsqn1CZKj5xWOsih7UucljrIj98SOVvqvg3q5hUpoqpYlL5RMWkMlVMKk9UpoonFZPKVDGpfNNhrYsc1rrIYa2L/PBlFd+k8qRiUpkqnqg8UXlS8aTiicqTiicqU8U3VXyTym86rHWRw1oXOax1kR9+mcobFW+oTBVPVKaKSeVJxaQyVTxReVLxROWNijdUPqHyTzqsdZHDWhc5rHWRH/7jKj6hMlVMKk8qnqg8qXiiMlW8ofKbKp6o/E2HtS5yWOsih7Uu8sNlVKaKJxWTyhOVqWJSmSomlScqU8WkMlVMKk8qnqj8TRXfdFjrIoe1LnJY6yI//LKKv6liUpkq3qiYVCaVqeINlW+qmFSeqDypeENlqphUftNhrYsc1rrIYa2L2B98QOVvqphUnlRMKlPFpPKkYlJ5o+KJypOKSWWqmFSmijdUpopJ5ZsqPnFY6yKHtS5yWOsi9gdrXeKw1kUOa13ksNZFDmtd5LDWRQ5rXeSw1kUOa13ksNZFDmtd5LDWRQ5rXeSw1kUOa13ksNZF/g+GyvZ2lV9zKQAAAABJRU5ErkJggg==";

export const formatMoney = (amount, currency) => {
  return new Intl.NumberFormat(
    "en-UK",
    currency
      ? {
          style: "currency",
          currency,
        }
      : {}
  )
    .format(Math.round((amount || 0) * 100) / 100)
    .replace(/^(\D+)/, "$1 ");
};
