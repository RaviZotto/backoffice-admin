import {useState} from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getAdminDetails } from '../../Api';
 const userPersmission = () => {
    const dispatch = useDispatch();
    const {adminDetails}=useSelector(state=>state.auth);
    const userDetails = JSON.parse(localStorage.getItem('userDetails'));
    if(userDetails?.id){  return {userDetails};}
    const token_item = JSON.parse(localStorage.getItem('token_item'));
    if(token_item?.id){
         getAdminDetails(token_item.id,({err,res})=>{ 
            if(res){
                localStorage.setItem('userDetails',JSON.stringify(res.adminDetails))
                dispatch({type:'GET_USERDETAIL',payload:res.adminDetails});
            }
                       
         })
        }
    return {userDetails:adminDetails}        
     
}

export default userPersmission;

