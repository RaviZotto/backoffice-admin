import defaultAvatar from "@src/assets/images/icons/user_image.png";

export const useGetImage =async (url) => {
    const data = await fetch(url);
    const blob = await data.blob();
    return new Promise((resolve,rej) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob); 
      reader.onloadend = () => {
        const base64data = reader.result;
        if(!base64data.includes('image/png'))resolve(defaultAvatar);   
        resolve(base64data);
      }
    });
}


