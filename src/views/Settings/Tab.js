import React from 'react';
import { Nav, NavItem, NavLink } from "reactstrap";
import { User, Settings, Key, DollarSign } from "react-feather";

const Tabs = ({ activeTab, toggleTab }) => {
  return (
    <Nav className="nav-left" pills vertical>
      <NavItem>
        <NavLink active={activeTab === "1"} onClick={() => toggleTab("1")}>
          <User size={18} className="mr-1" />
          <span className="font-weight-bold">General</span>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink active={activeTab === "2"} onClick={() => toggleTab("2")}>
          <Settings size={18} className="mr-1" />
          <span className="font-weight-bold">Security</span>
        </NavLink>
      </NavItem> 
      <NavItem>
        <NavLink active={activeTab === "3"} onClick={() => toggleTab("3")}>
          <Key size={18} className="mr-1" />
          <span className="font-weight-bold">Keys</span>
        </NavLink>
      </NavItem> 
      <NavItem>
        <NavLink active={activeTab === "4"} onClick={() => toggleTab("4")}>
          <DollarSign size={18} className="mr-1" />
          <span className="font-weight-bold">Currency</span>
        </NavLink>
      </NavItem> 
    </Nav>
  );
};

export default Tabs;

// ALTER TABLE `tbl_merchant` CHANGE `permission_id` `permission_id` INT(11) NULL DEFAULT '0';