import { AvForm, AvInput, AvField } from "availity-reactstrap-validation-safe";
import { selectThemeColors } from "@utils";
import Select from "react-select";
import { Card, Button, CardBody, Row,Input, Col, FormGroup } from "reactstrap";
import React, { useEffect, useState } from "react";
import notification from "../../components/notification";
import { AdminDetails } from ".";
import CardFooter from "reactstrap/lib/CardFooter";
import { getAllMerchant, updateAdmin, setTestingMerchant } from "../../Api";

import { useDispatch, useSelector } from "react-redux";
import {
  GET_EXCHANGE_RATE,
  UPDATE_EXCHANGE_RATE,
} from "../../redux/actions/transfer";
import { userDetails } from "../../utility/Utils";

function index() {
  const dispatch = useDispatch();
  const { role_type, settings } = userDetails || {};
  const { exchange_rate } = useSelector((state) => state.transfer);
  const [merchant, setMerchant] = useState([]);
  const [exchangeRate, setExchangeRate] = useState(exchange_rate || 0);
  const [merchantTest, setMerchantTest] = useState([{}]);
  const [merchantReal, setMerchantReal] = useState([{}]);
  const [loading, setLoading] = useState(true);
  const [state, setState] = useState([]);
  const [refetch, setRefetch] = useState(false);
  useEffect(()=>{
    //console.log(exchange_rate);
    setExchangeRate(exchange_rate);     
},[exchange_rate])
  useEffect(() => {
    if (!exchange_rate) dispatch(GET_EXCHANGE_RATE());
    if (loading || refetch) {
      getAllMerchant()
        .then((res) => {
          if (res) setMerchant(res.merchant);
        })
        .catch((err) => {
          notification({ title: "Error", message: err, type: "error" });
        });
      setLoading(false);
      setRefetch(false);
    }
  }, [refetch]);

  const setTestingAccount = async (dataArray, action) => {
    try {
      let formatData = dataArray.map((el) => ({ merchant_id: el.merchant_id }));
      setLoading(true);
      await setTestingMerchant({ merchantArray: formatData, action });
      setLoading(false);
      setRefetch(true);
      setMerchantReal([]);
      setMerchantTest([]);
      notification({ type: "success", message: "Successfully updated" });
    } catch (e) {
      notification({ title: "Error", message: e, type: "error" });
      setLoading(false);
    }
  };

  const onSubmit = async (event, error, values) => {
    event.preventDefault();
    let key;
    //console.log(values.old_password=="",typeof values.old_password)
    if (values.old_password.trim() != "" && values.password.trim() != "") {
      setState([...state, "old_password", "password"]);
    }
    if (values.twofactor_auth != AdminDetails().twofactor_auth) {
      setState([...state, "twofactor_auth"]);
    }
    if (state.length) {
      let Obj = state.reduce((obj, key) => {
        return {
          ...obj,
          [key]: values[key],
        };
      }, {});

      setLoading(true);
      updateAdmin(
        { data: Obj, admin_id: AdminDetails().id },
        ({ err, res }) => {
          if (err)
            notification({ title: "Error", message: err.err, type: "error" });
          else
            notification({
              title: "Success",
              type: "success",
              message: "Successfully updated",
            });
          setLoading(false);
          setState([]);
        }
      );
    }
  };
  return (
    <div>
      <Card>
        <CardBody>
          <AvForm onSubmit={onSubmit}>
            <Row>
              <FormGroup tag={Col} sm="12" md="6">
                <AvField
                  label={"OldPassword"}
                  name="old_password"
                  type="text"
                />
              </FormGroup>
              <FormGroup tag={Col}>
                <AvField label={"New Password"} name="password" type="text" />
              </FormGroup>
            </Row>

            <FormGroup>
              <AvField
                label="Auth factor"
                type="select"
                name="twofactor_auth"
                value={AdminDetails().twofactor_auth}
              >
                <option value={1} label={"Enable"} />
                <option value={0} label={"Disable"} />
              </AvField>
            </FormGroup>
            <label>Exclude Merchant</label>
            <FormGroup tag={Row}>
              <Col md={8} sm={8}>
                <Select
                  isClearable={true}
                  theme={selectThemeColors}
                  isMulti
                  name="colors"
                  options={merchant
                    .filter((el) => !el.is_test)
                    .map((e) => ({
                      label: e.comp_name,
                      value: e.merchant_id,
                      merchant_id: e.merchant_id,
                    }))}
                  onChange={(e) => {
                    //console.log(e);
                    if (e) {
                      setMerchantReal([...e]);
                    } else {
                      setMerchantReal([]);
                    }
                  }}
                  className="react-select"
                  classNamePrefix="select"
                />
              </Col>
              <Col md={4} sm={4}>
                <Button
                  color="primary"
                  disabled={
                    !merchantReal.length ||
                    loading ||
                    (role_type &&
                      role_type != "admin" &&
                      !userDetails["settings"].includes("U"))
                  }
                  onClick={(el) => setTestingAccount(merchantReal, "1")}
                >
                  Update
                </Button>
              </Col>
            </FormGroup>
            <label>Include Merchant</label>
            <FormGroup tag={Row}>
              <Col md={8} sm={8}>
                <Select
                  isClearable={true}
                  theme={selectThemeColors}
                  isMulti
                  name="colors"
                  options={merchant
                    .filter((el) => el.is_test == 1)
                    .map((e) => ({
                      label: e.comp_name,
                      value: e.merchant_id,
                      merchant_id: e.merchant_id,
                    }))}
                  onChange={(e) => {
                    //console.log(e);
                    if (e) {
                      setMerchantTest([...e]);
                    } else {
                      setMerchantTest([]);
                    }
                  }}
                  className="react-select"
                  classNamePrefix="select"
                />
              </Col>
              <Col md={4} sm={4}>
                <Button
                  color="primary"
                  disabled={
                    !merchantTest.length ||
                    loading ||
                    (role_type &&
                      role_type != "admin" &&
                      !userDetails["settings"].includes("U"))
                  }
                  onClick={(el) => setTestingAccount(merchantTest, "0")}
                >
                  Update
                </Button>
              </Col>
            </FormGroup>

            {/* <FormGroup tag={Row} className='d-flex align-items-center'>
              <Col md={8} sm={8}>
                <label>Exchange Rate</label>
                <Input
                  type="number"
                  onChange={(e) => setExchangeRate(e.target.value)}
                  value={exchangeRate}
                  name="exchange_rate"
                  className=" form-control "
                />
              </Col>
              <Col md={4} sm={4}>
                <Button 
                  className=" mt-1"
                  color="primary"
                  disabled={exchange_rate == exchangeRate}
                  onClick={(e) =>
                    dispatch(
                      UPDATE_EXCHANGE_RATE(exchangeRate, () =>
                        dispatch(GET_EXCHANGE_RATE())
                      )
                    )
                  }
                >
                  Update
                </Button>
              </Col>
            </FormGroup> */}

            <CardFooter className="mb-0 pb-0" />
            <Row className="justify-content-end p-1 ">
              <Button.Ripple
                color="primary"
                disabled={
                  loading ||
                  (role_type &&
                    role_type != "admin" &&
                    !userDetails["settings"].includes("U"))
                }
              >
                {!loading ? "Update" : "updating"}
              </Button.Ripple>
            </Row>
          </AvForm>
        </CardBody>
      </Card>
    </div>
  );
}

export default index;
