import React, { useEffect, useState } from "react";
import { Row, Col, TabContent, TabPane, Card, CardBody } from "reactstrap";
import Tabs from "./Tab";
import General from "./General";
import Loading from "@core/components/spinner/Loading-spinner";
import Security from "./Security";
import { makeVar } from "@apollo/client";
import notification from "../../components/notification";
import { getAdminDetails } from "../../Api";
import {useSelector} from 'react-redux';
import Currency from './Currency';
import Keys from "./Keys";
export const AdminDetails = makeVar({});

const Settings = (props) => {
  const userDetails= JSON.parse(localStorage.getItem('userDetails'))
  const [activeTab, setActiveTab] = useState("1");
  const toggleTab = (el) => setActiveTab(el);
  const [loading, setLoading] = useState(true);
  const [refetch, setRefetch] = useState(false);
  
  useEffect(() => {
    if (loading || refetch) {

      getAdminDetails(userDetails.id,({err, res}) => {

        //console.log(err,res);
        if (err) {notification({ title: "Error", message: err.message, type: "error" });}
        else {localStorage.setItem('userDetails', JSON.stringify(res.adminDetails));AdminDetails(res.adminDetails);}
        setLoading(false);
        setRefetch(false);
      });
    }
  }, [refetch]);
  if (loading) return <Loading />;
  return (
    <Row>
      <Col className="mb-2 mb-md-0" md="3">
        <Tabs activeTab={activeTab} toggleTab={toggleTab} />
      </Col>
      <Col md="9">
        <Card>
          <CardBody>
            <TabContent activeTab={activeTab}>
              <TabPane tabId="1">
                <General />
              </TabPane>
              <TabPane tabId="2">
                <Security />
              </TabPane>
              <TabPane tabId="3">
                <Keys />
              </TabPane>
              <TabPane tabId="4">
                <Currency />
              </TabPane>
            </TabContent>
          </CardBody>
        </Card>
      </Col>
    </Row>
  );
};
export default Settings;
