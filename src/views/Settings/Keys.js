import React,{useState,useEffect, useRef} from 'react';
import {Row, Col,FormGroup,Button, CardTitle ,Label} from 'reactstrap';
import { selectThemeColors } from '@utils'
import { AvForm, AvInput, AvField} from 'availity-reactstrap-validation-safe';
import { useDispatch, useSelector } from 'react-redux';
import { GET_PARTNER, UPDATE_MERCHANT } from '../../redux/actions/storeMerchant.';
import { Axios } from '../../Api';
import { graphqlServerUrl } from '../../App';
import Select from 'react-select'
import notification from '../../components/notification';
import { AxiosError } from 'axios';
export default function Keys() {
  const multiRef=useRef();
  const[ecomKey,setEcomKeys]=useState({})
  const [loading,setLoading] = useState({l1:false,l2:false,l3:false});
    const dispatch = useDispatch()
    const[type,setType]=useState('all');
    const[currencyOpt,setCurrencyOpt]=useState([]);
    // EUR_CVVON, EUR_3D, EUR_CVVOFF,
    const [ecomOptions,setEcomOptions]=useState([])
    var currencyOptions=[
        { value: 'USD', label: 'USD', color: '#00B8D9', isFixed: true },
        { value: 'GBP', label: 'GBP', color: '#0052CC', isFixed: true },
        { value: 'DKK', label: 'DKK', color: '#5243AA', isFixed: true },
        { value: 'EUR', label: 'EUR', color: '#FF5630', isFixed: false },
        { value: 'NOK', label: 'NOK', color: '#FF8B00', isFixed: false },
        { value: 'SEK', label: 'SEK', color: '#FFC400', isFixed: false }
    ]

 
    const {partners,merchants} = useSelector(state=>state.merchant);
     useEffect(() => {
         if(!partners.length)dispatch(GET_PARTNER())
         if(!merchants.length)dispatch(UPDATE_MERCHANT())
         if(!currencyOpt.length)setCurrencyOpt([...currencyOptions])
    })

    const onSubmitUser = (event, error, values)=>{ 
        event.preventDefault();
        if(error.length)return;
        setLoading({...loading,l1:true});
         Axios.post(`${graphqlServerUrl}/admin/saveEcomUser`,values)
         .then(res=>{if(res.status==200&&res.data)notification({title:'Ecom user',type:'success',message:"Saved successfully"});setLoading({...loading,l1:false}) })   
         .catch((e:AxiosError)=>{
           notification({title:'Ecom user',message:e.response?e.response.data:e.toString(),type:'error'});
           setLoading({...loading,l1:false}) })
         
        }   
    
   const onSumbitKeys = (event,error,values)=>{
     event.preventDefault();
    if(error.length)return;
    setLoading({...loading,l2:true}) 
    Axios.post(`${graphqlServerUrl}/admin/saveEcomKey`,{...values,ecomKey})
    .then(res=>{if(res.status==200&&res.data)notification({title:'Ecom key',type:'success',message:"Saved successfully"});setLoading({...loading,l2:false});setEcomOptions([]);  })   
    .catch((e:AxiosError)=>{
      notification({title:'Ecom key',message:e.response?e.response.data:e.toString(),type:'error'});
      setLoading({...loading,l2:false}) 
    })

   }



    return (
        <div>
            <hr/>
            <CardTitle> Ecom User Credentials</CardTitle>
           <AvForm key='0' onSubmit={onSubmitUser}>
           <FormGroup tag={Col}>
             <AvField 
             required
             label='Ecom User Name'
             name='ecom_username'
             type='text'
             />
             </FormGroup>
              <FormGroup tag={Col}>
             <AvField
             required 
             label='Ecom password'
             name='ecom_password'
             type='text'
             />
             </FormGroup>
              <FormGroup tag={Col}>
                 <AvField
                 required
                 label='Type'
                 name='type'
                 type='select'
                 onChange={e=>setType(e.target.value)}
                 >
                    <option value={null} label={'select'} />
                  <option  label="All" value={'all'} />
                  <option  label="partner" value={'partner'} />
                  <option  label="merchant" value={'merchant'} />
                </AvField>  
               </FormGroup>
               <FormGroup tag={Col}>
                {type!='all'?
                 <AvField
                 required
                 label={type.toUpperCase()}
                 name={type+'_id'}
                 type='select'
                 >
                  <option value={null} label={'select'} />
                  {type=='partner'? partners.map(e=><option label={e.comp_name} value={e.id} />):merchants.map(e=><option label={e.comp_name} value={e.merchant_id} />)}
                </AvField>: null    
            }
              </FormGroup>
               <hr/>
               <div className=' d-flex col-12 justify-content-end'>
                <Button color='primary' disabled={loading.l1}  type='submit'>{loading.l1?"Saving...":"Save"}</Button>
               </div>
           </AvForm>
           <hr/>
           <CardTitle>Ecom Key</CardTitle>
           <AvForm key='1' onSubmit={onSumbitKeys}>
           <FormGroup tag={Col}>
                 <AvField
                 requied
                 label='Type'
                 name='type'
                 type='select'
                 onChange={e=>setType(e.target.value)}
                 >
                  <option value ={null} label={'select'} />
                  <option  label="All" value={'all'} />
                  <option  label="partner" value={'partner'} />
                  <option  label="merchant" value={'merchant'} />
                </AvField>  
               </FormGroup>
               <FormGroup tag={Col}>
                {type!='all'?
                 <AvField
                 required
                 label={type.toUpperCase()}
                 name={type+'_id'}
                 type='select'
                 >
                  <option value={null} label={'select'} />
                  {type=='partner'? partners.map(e=><option label={e.comp_name} value={e.id} />):merchants.map(e=><option label={e.comp_name} value={e.merchant_id} />)}
                </AvField>: null    
            }
              </FormGroup>
              
               <Col>
            <Label>Currency</Label>
            <Select
              isClearable={true}
              ref={multiRef}
              theme={selectThemeColors}
              isMulti
              name='colors'
              menuPlacement="top"
              menuPosition="relative"
              options={currencyOpt}
              onChange={e=>{//console.log(e); 
                if(e){setEcomOptions([...e])}else{setEcomOptions([])}  }}
              className='react-select'
              classNamePrefix='select'
            />
            
            
             
           { ecomOptions.length?ecomOptions.map((res,i)=>
           <Row>
               
            <Col key={i} sm='4' >
            <Label className='form-label text-nowrap' for='linkedin'>
              {res.value} CVVOFF
            </Label>
            <AvInput
              required
              type='text'
              id={`linkedin`}
              onChange={el=>{let val={};val[res.value]={...ecomKey[res.value],cvv_off:el.target.value};   setEcomKeys({...ecomKey,...val})}}
              name={`ecom_${res.value.toLowerCase()}_cvvoff`}
            />
              </Col>  
            <Col key={i} sm='4'>
            <Label className='form-label text-nowrap' for='linkedin'>
              {res.value} CVVON
            </Label>
            <AvInput
              required
              type='text'
              onChange={el=>{let val={};val[res.value]={...ecomKey[res.value],cvv_on:el.target.value};   setEcomKeys({...ecomKey,...val})}}
              id={`linkedin`}
              name={`ecom_${res.value.toLowerCase()}_cvvon`}
            />
            </Col>
            <Col key={i}>
            <Label className='form-label text-nowrap' for='linkedin'>
             {res.value} 3D
            </Label>
            <AvInput
              required
              onChange={el=>{let val={};val[res.value]={...ecomKey[res.value],'3d':el.target.value};   setEcomKeys({...ecomKey,...val})}}
              type='text'
              id={`linkedin`}
              name={`ecom_${res.value.toLowerCase()}_3d`}
            />
            </Col>
            
            
           </Row>
           
           ):null}
            <hr/>
           </Col>
               <div className=' mt-1 d-flex col-12 justify-content-end'>
               <Button color='primary' disabled={loading.l2||!ecomOptions.length}  type='submit'>{loading.l2?"Saving...":"Save"}</Button>
               </div>
           </AvForm>
          
        </div>
    )
}
