import { AvForm, AvInput, AvField } from "availity-reactstrap-validation-safe";
import {
  Card,
  Spinner,
  Button,
  Media,
  CardBody,
  CardHeader,
  Row,
  Col,
  FormGroup,
  Label,
} from "reactstrap";
import React, { useState, useEffect, useRef } from "react";
import notification from "../../components/notification";
import { getBase64FromUrl, phoneCodes, userDetails } from "../../utility/Utils";
import { AdminDetails } from ".";
import CardFooter from "reactstrap/lib/CardFooter";
import { updateAdmin, uploadImage } from "../../Api";
import { graphqlServerUrl } from "../../App";
import { useDispatch } from "react-redux";
import { USER_DETAIL } from "../../redux/actions/auth";
import defaultImage from '@src/assets/images/icons/user_image.png';
const defaultAvatar =localStorage.getItem("image")|| defaultImage;

function index() {
  const [url, setUrl] = useState(
    defaultAvatar
  );
  
  useEffect(() =>{
  
      (async()=>{
        try{
       const res =  await getBase64FromUrl(`${graphqlServerUrl}/images/admin/${AdminDetails().id}.png?${Date.now()}`);
       setUrl(res?res:defaultImage);
        }
        catch(e){console.log(e); setUrl(defaultImage);}
      })()
  })

  const {role_type,settings}=userDetails||{}
  const [logoUploading, setLogoUploading] = useState(false);
  const logoRef = useRef();
  const refreshImage = () => {
    getBase64FromUrl( `${graphqlServerUrl}/images/admin/${AdminDetails().id}.png?${Date.now()}`).then(res=>{
      console.log(res);
    logoRef.current &&
      (logoRef.current.src = `${graphqlServerUrl}/images/admin/${AdminDetails().id}.png?${Date.now()}`);
      setUrl(`${graphqlServerUrl}/images/admin/${AdminDetails().id}.png?${Date.now()}`);
    }).catch(e=>setUrl(defaultImage));
    const myEvent = new CustomEvent("image", {
      detail: {},
      bubbles: true,
      cancelable: true,
      composed: false,
    });
    window.dispatchEvent(myEvent);
  };
  const dispatch=useDispatch();
  const [state, setState] = useState({
    phone_code: AdminDetails().phone_code,
    phone: AdminDetails().phone,
    name: AdminDetails().name,
  });
 
  const [update, setUpdated] = useState({ updateInfo: {}, res: true });
  useEffect(() => {
    let temp = { ...AdminDetails() };
    let dupState = {};
    if (Object.keys(AdminDetails()).length > 0) {
      for (let key of Object.keys(state)) {
        if (state[key] !== temp[key]) dupState[key] = state[key];
      }
      if (Object.keys(dupState).length)
        setUpdated({ updateInfo: { ...dupState }, res: false });
    }
  }, [state]);

  const handleLogoUpload = ({
    target: {
      validity,
      files: [file],
    },
  }) => {
    if (file && file.type != "image/png")
      return notification({ type: "error", message: "Not a valid file" });
    if (validity.valid) {
      const formData = new FormData();
      //console.log(file);
      formData.append("logo", file);

      for (var key of formData.entries()) {
        //console.log(key[0] + ", " + key[1]);
      }
      setLogoUploading(true);
      formData.append("path", "admin");
      formData.append("data", JSON.stringify({ path: "admin", id: AdminDetails().id }));
      uploadImage(formData, ({ err, res }) => {
        if (err) {
          //console.log(err.message);
        } else {
          refreshImage();
                      
          notification({
            title: "Image",
            message: "successfully uploaded",
            type: "success",
          });
          localStorage.setItem(
            "image",
            `${graphqlServerUrl}/images/admin/${AdminDetails().id}.png?${new Date().getTime()}`
          );
        }
        setLogoUploading(false);
      });
    }
  };

  const handleSubmit = (event, error, values) => {
    event.preventDefault();
    updateAdmin(
      { data: update.updateInfo, admin_id: AdminDetails().id },
      ({ err, res }) => {
        if (err){
          notification({ title: "Error", message: err.err, type: "error" });
       
        }
        else

       dispatch(USER_DETAIL(AdminDetails().id));
        setUpdated({...update,res:true});
          notification({
            title: "Success",
            message: "successfully Updated",
            type: "success",
          });
      }
    );

    //console.log(values);
  };

  return (
    <div>
      <Card>
        <CardBody>
          <div className="mx-auto  ">
            <img
              className="rounded mr-50"
              src={url}
              ref={logoRef}
              alt="Generic placeholder image"
              height="80"
              width="80"
            />
            <Media className="mt-75 ml-1" body>
              <Button.Ripple
                tag={Label}
                className="mr-75"
                size="sm"
                color="primary"
                disabled={ role_type&&role_type!='admin' && !userDetails['settings'].includes('U')}
              >
                {logoUploading ? <Spinner size="sm" className="mr-1" /> : null}
                {logoUploading ? "Updating" : "Update"}
                <input
                  type="file"
                  onChange={handleLogoUpload}
                  hidden
                  accept="image/x-png"
                />
              </Button.Ripple>
              <p>Only PNG files allowed.</p>
            </Media>
          </div>
          <AvForm onSubmit={handleSubmit}>
            <Row>
              <FormGroup tag={Col} sm="12" md="6">
                <AvField
                  label={"Name"}
                  name="name"
                  type="text"
                  value={AdminDetails().name}
                  onChange={(e) => setState({ ...state, name: e.target.value })}
                />
                <AvField
                  label={"Email"}
                  name="email"
                  type="email"
                  value={AdminDetails().email}
                  disabled
                />
              </FormGroup>
              <FormGroup tag={Col}>
                <AvField
                  label={"Phone Code"}
                  name="phone_code"
                  type="select"
                  value={AdminDetails().phone_code}
                  onChange={(e) =>
                    setState({ ...state, phone_code: e.target.value })
                  }
                >
                  {phoneCodes.map((res) => (
                    <option label={res.label} value={res.value} />
                  ))}
                </AvField>
                <AvField
                  label={"Phone"}
                  name="phone"
                  type="text"
                  value={AdminDetails().phone}
                  onChange={(e) =>
                    setState({ ...state, phone: e.target.value })
                  }
                />
              </FormGroup>
            </Row>
            <CardFooter className="mb-0 pb-0" />
            <Row className="justify-content-end p-1 ">
              <Button.Ripple
                color="primary"
                type="submit"
                disabled={update.res ||role_type&&role_type!='admin' && !userDetails['settings'].includes('U')}   
              >
                Update
              </Button.Ripple>
            </Row>
          </AvForm>
        </CardBody>
      </Card>
    </div>
  );
}

export default index;
