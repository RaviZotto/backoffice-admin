import React,{ createContext, useEffect, useMemo, useState } from "react";
import { SlideDown } from "react-slidedown";
import "react-slidedown/lib/slidedown.css";
import ManageUser from "./ManageUser";
import UsersTable from "./Table";

export const UsersContext = createContext(null);

const Users = () => {
  const [selectedUser, setSelectedUser] = useState({});
  const [userForm, setUserForm] = useState(false);

  const providerValue = useMemo(
    () => ({
      selectedUser,
      setSelectedUser,
      userForm,
      setUserForm,
    }),
    [selectedUser, userForm]
  );

  return (
    <UsersContext.Provider value={providerValue}>
      {userForm ? (
        <SlideDown>
          <ManageUser />
        </SlideDown>
      ) : null}
      <UsersTable />
    </UsersContext.Provider>
  );
};

export default Users;
