// ** React Imports
// ** Custom Components
import Avatar from "@components/avatar";
import React,{ useState, useContext, useEffect,Fragment } from "react";
import {useDispatch} from "react-redux";
// ** Third Party Components
import { Edit, Lock, Trash2 } from "react-feather";
import {
  Button,
  Col,
  CustomInput,
  FormGroup,
  Label,
  Row,
  Table,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Spinner,
} from "reactstrap";
import Card from "reactstrap/lib/Card";
import CardBody from "reactstrap/lib/CardBody";
import * as Yup from "yup";
import { Formik, Field, Form } from "formik";
// // import { UPDATE_USER, ADD_USER, DELETE_USER } from "@graphql/mutations";
// import { GET_USERS } from "@graphql/queries";
import { selectThemeColors, countries as countryOptions } from "@utils";
import Select from "react-select";
import { UsersContext } from ".";
// import { useMutation } from "@apollo/client";
import notification from "../../../components/notification";
import produce from "immer";
import { updateUser } from "../../../Api";
import { ADD_USER, GET_USER, UPDATE_USER } from "../../../redux/actions/storeMerchant.";

const formSchema = Yup.object().shape({
  name: Yup.string().required(),
  email: Yup.string().email().required("Required"),
  zipcode: Yup.number().required("Required"),
  phone: Yup.number().required(),

  
  confirmpassword:  Yup.string().required(),
  password: Yup.string().required(),
  phone_code: Yup.string(),
  country: Yup.string(),
});
const initialFormValues = {
  name: "",
  email: "",
  phone: "",
  phone_code: "+44",
  country: "GB",

};

const initialPermissions = {
   merchant: "",
     transfer: "",
    settings: "",
     partner: "",
     subscription: "",
     role:"",
     salesreport:"",
     dashboard: "",
     fees:"",
     client:"",
     notification:"",
     flaircard:""
  
};

const ManageUser = () => {
  const [phoneCode, setPhoneCode] = useState("+44");
  const dispatch = useDispatch();
  // const [updateUser, { loading: updatingUser }] = useMutation(UPDATE_USER);
  // const [deleteUser, { loading: deletingUser }] = useMutation(DELETE_USER);
  // const [addUser, { loading: addingUser }] = useMutation(ADD_USER);
  const[loading,setLoading]=useState(false);
  const [initialUser, setInitialUser] = useState(initialFormValues);
  const [permissions, setPermissions] = useState(initialPermissions);
  const { setUserForm, selectedUser, setSelectedUser } = useContext(UsersContext);

  const formSchema = Yup.object().shape({
    name: Yup.string().required(),
    email: Yup.string().email().required("Required"),
    phone: Yup.number().required(),
    confirmpassword: !selectedUser.id?Yup.string().required():undefined,
    password: !selectedUser.id?Yup.string().required():undefined,
    phone_code: Yup.string(),
    resetpassword:Yup.string(),
    country: Yup.string(),
  });




  const fieldsToUpdate = (userData) => {
    let user = {};
    let newPermissions = {};
    for (let key of Object.keys(userData)) if (userData[key] != selectedUser[key]) user[key] = userData[key];

    for (let key of Object.keys(permissions))
      if (permissions[key] != selectedUser[key]) newPermissions[key] = permissions[key];

    let userLen = Object.keys(user).length;
    let permLen = Object.keys(newPermissions).length;
    if (userLen == 0 && permLen == 0) {
      notification({ title: "Nothing changed!", type: "error" });
      return false;
    }

    let dataToReturn = { user, id: userData.id };
    if (permLen) dataToReturn.permissions = newPermissions;
    return dataToReturn;
  };

  const handleFormSubmit = (formData) => {
    if (formData.id) {
      // update user
      let dataToUpdate = fieldsToUpdate(formData);
    
      //console.log(dataToUpdate,'2');
      if (!dataToUpdate) return;
      setLoading(true);
     let {id,user,permissions}=dataToUpdate;
      dispatch(UPDATE_USER(id,{...user,...permissions},()=>setLoading(false)))
    } else {
      // add user
  
       
       let {confirmpassword,password}=formData;
       if(confirmpassword.trim()!=password.trim()){notification({ message:"confirmpassword is not matched with password",type:"error"});return;}
        delete formData.confirmpassword;
       let dataToAdd = { ...formData, ...permissions,role_type:'tester' };
       setLoading(true);
      dispatch(ADD_USER(dataToAdd,()=>dispatch(GET_USER(()=>setLoading(false)))));
    }
  };

  const handlePermissionsChange = (e) => {
    let [field, permCode] = e.target.id.split("-");
    let newValue = permissions[field];
    if (newValue.includes(permCode)) newValue = newValue.replace(permCode, "");
    else newValue += permCode;
    newValue = newValue.split("").sort().join("");
    let newPermissions = { ...permissions };
    newPermissions[field] = newValue;
    setPermissions(newPermissions);
  };

  const handleDeleteUser = () => {
    // let { id, permission_id } = selectedUser;
    // let mutationVar = {
    //   variables: { id, permission_id },
    //   update: (cache, { data }) => {
    //     if (data.deleted_user) {
    //       const currData = cache.readQuery({ query: GET_USERS });
    //       cache.writeQuery({
    //         query: GET_USERS,
    //         data: produce(currData, (x) => {
    //           let i = x.users.findIndex((el) => el.id == id);
    //           x["users"].splice(i, 1);
    //         }),
    //       });
    //     }
    //     setSelectedUser(false);
    //     setUserForm(false);
    //   },
    // };
    // deleteUser(mutationVar);
  };

  useEffect(() => {
    if (selectedUser) {
      let user = JSON.parse(JSON.stringify(selectedUser));
      console.log(user);
      // let permissions = user.permissions;
      // delete permissions.__typename;
      let permissionObj={};
      Object.keys(permissions).forEach((el) => {(permissionObj[el] = user[el] ? user[el] : ""); delete user[el];});
      setInitialUser(user);
      setPermissions(permissionObj);
        
    } else {
      setInitialUser(initialFormValues);
      setPermissions(initialPermissions);
    }
  }, [selectedUser]);

  return (
    <Card>
      <CardBody>
        <Row>
          <Col sm="12">
            <Formik
              initialValues={initialUser}
              enableReinitialize
              onSubmit={handleFormSubmit}
              validationSchema={formSchema}
            >
              {({ errors, touched, values, initialValues }) => (
                <Form>
                  <Row>
                    <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="name" className="form-label">
                          Name
                        </Label>
                        <Field
                          name="name"
                          id="name"
                          placeholder="John Doe"
                          className={`form-control ${errors.name && touched.name && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="name" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="email" className="form-label">
                          Email
                        </Label>
                        <Field
                          name="email"
                          type="email"
                          id="email"
                          placeholder="example@domain.com"
                          className={`form-control ${errors.email && touched.email && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="email" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="country" className="form-label">
                          Country
                        </Label>
                        <Select
                          theme={selectThemeColors}
                          className="react-select"
                          classNamePrefix="select"
                          value={countryOptions.find((el) => el.value == values.country) || countryOptions[277]}
                          options={countryOptions}
                          isClearable={false}
                          onChange={(obj) => {
                            values.phone_code = obj.phone_code;
                            values.country = obj.value;
                            setPhoneCode(obj.phone_code);
                          }}
                        />
                      </FormGroup>
                    </Col>
                   
                    <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="phone" className="form-label">
                          Contact
                        </Label>
                        <InputGroup className="input-group-merge">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText style={{ borderColor: errors.phone && touched.phone ? "red" : "" }}>
                              {countryOptions.find((el) => el.value == values.country)?countryOptions.find((el) => el.value == values.country).phone_code : "+44"}
                            </InputGroupText>
                          </InputGroupAddon>
                          <Field
                            name="phone"
                            id="phone"
                            placeholder="763-242-9206"
                            className={`form-control ${errors.phone && touched.phone && "is-invalid"}`}
                          />
                        </InputGroup>
                        {/* <ErrorMessage name="phone" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                    </Col>
                  
                
                  
                  <Col md="4" sm="12">
                      <FormGroup>
                        <Label for='password' className="form-label" >
                          Password
                        </Label>
                        <Field
                         name='password'
                         id='password'
                         type='password'
                         className={`form-control ${errors.password && touched.password && "is-invalid"}`}
                       />
                     </FormGroup>
                    </Col>
                    { !selectedUser.id && (
                      <Fragment>
                    <Col md="4" sm="12">
                      <FormGroup>
                        <Label for='confirmpassword' className="form-label" >
                         Confirm Password
                        </Label>
                        <Field
                         name='confirmpassword'
                         id='confirmpassword'
                         type='text'
                         className={`form-control ${errors.confirmpassword && touched.confirmpassword && "is-invalid"}`}
                       />
                     </FormGroup>
                    </Col>
                    </Fragment>
                    )}
                    <Col sm="12">
                      <div className="permissions border mt-1">
                        <h6 className="py-1 mx-1 mb-0 font-medium-2">
                          <Lock size={18} className="mr-25" />
                          <span className="align-middle">Permissions</span>
                        </h6>
                        <Table borderless striped responsive>
                          <thead className="thead-light">
                            <tr>
                              <th>Module</th>
                              <th>Read</th>
                              <th>Create</th>
                              <th>Update</th>
                              <th>Delete</th>
                            </tr>
                          </thead>
                          <tbody>
                            {Object.keys(permissions).map((el, i) => (
                              <tr id={`${el}`} key={i}>
                                <td id={`${el}-0`}>{el[0].toUpperCase() + el.slice(1)}</td>
                                <td>
                                  <CustomInput
                                    type="checkbox"
                                    id={`${el}-R`}
                                    onChange={handlePermissionsChange}
                                    checked={permissions[el].includes("R")}
                                  />
                                </td>
                                <td>
                                  <CustomInput
                                    onChange={handlePermissionsChange}
                                    type="checkbox"
                                    id={`${el}-C`}
                                    checked={permissions[el].includes("C")}
                                  />
                                </td>
                                <td>
                                  <CustomInput
                                    onChange={handlePermissionsChange}
                                    type="checkbox"
                                    id={`${el}-U`}
                                    checked={permissions[el].includes("U")}
                                  />
                                </td>
                                <td>
                                  <CustomInput
                                    onChange={handlePermissionsChange}
                                    type="checkbox"
                                    id={`${el}-D`}
                                    checked={permissions[el].includes("D")}
                                  />
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </Table>
                      </div>
                    </Col>
                    <Col className="d-flex flex-sm-row flex-column mt-2" sm="12">
                      <Button className="mb-1 mb-sm-0 mr-0 mr-sm-1" type="submit" color="primary">
                        {loading ? <Spinner size="sm" color="white" className="mr-1" /> : null}
                        {selectedUser.id ? "Update user" : "Add user"}
                      </Button>
                 
                      <Button color="secondary" outline onClick={() => setUserForm(false)}>
                        Close
                      </Button>
                    </Col>
                  </Row>
                </Form>
              )}
            </Formik>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};
export default ManageUser;
