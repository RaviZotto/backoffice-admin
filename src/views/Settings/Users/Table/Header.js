import "@styles/react/libs/flatpickr/flatpickr.scss";
import React,{ useContext } from "react";
import { Button, Col, Input, Row } from "reactstrap";
import { UsersContext } from "../index";

const CustomHeader = () => {
  const { setUserForm, userForm, setSelectedUser } = useContext(UsersContext);

  return (
    <div className="invoice-list-table-header w-100 mr-1 mt-2 mb-75">
      <Row>
        <Col className="d-flex flex-sm-row flex-column justify-content-between" sm="12">
          <Button
            className="mb-1 mb-sm-0 mr-0 mr-sm-1 px-md-5"
            type="submit"
            color="primary"
            onClick={() => {
              setSelectedUser(false);
              setUserForm(true);
            }}
          >
            Add User
          </Button>
          {/* <div>
            <Input placeholder="search" />
          </div> */}
        </Col>
      </Row>
    </div>
  );
};

export default CustomHeader;
