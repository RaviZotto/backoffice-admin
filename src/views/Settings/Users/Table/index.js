// ** Styles
import Avatar from "@components/avatar";
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import React,{ useContext, useEffect, useMemo, useState } from "react";
import DataTable from "react-data-table-component";
import { ChevronDown } from "react-feather";
import {useDispatch,useSelector} from "react-redux";
import userPermission from '@hooks/userPersmission';
// ** Third Party Components
import { Card, CustomInput, Button } from "reactstrap";
import { UsersContext } from "../index";
import Header from "./Header";
import { countries } from "@utils";
import produce from "immer";
import { GET_USER, UPDATE_USER } from "../../../../redux/actions/storeMerchant.";
import Loading from "@core/components/spinner/Loading-spinner";
import notification from "../../../../components/notification";
const renderClient = (row, i) => {
  const stateNum = i % 6,
    states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
    color = states[stateNum];

  return <Avatar color={color || "primary"} className="mr-1" content={row.name || "John Doe"} initials />;
};

const Table = () => {
  // const { data } = useQuery(GET_USERS);
  const { setSelectedUser, setUserForm } = useContext(UsersContext);
  // const [updateUser] = useMutation(UPDATE_USER);
  const [loading,setLoading]=useState(true);
  const [user, setUser] = useState({});
  const {users} = useSelector(state=>state.merchant);
  const dispatch = useDispatch();
 
  const {userDetails} = userPermission();


  useEffect(() => {
    console.log
   if(!users.length){
      dispatch(GET_USER(()=>setLoading(false)))
   }else{
     setLoading(false);
   }
   
  });

  const columns =[
      {
        name: "User",
        selector: "name",
        sortable: true,
        minWidth: "180px",
        cell: (row, i) => {
          return (
            <div
              className="d-flex justify-content-left align-items-center cursor-pointer"
              onClick={() => {
                setSelectedUser(row);
                setUserForm(true);
              }}
            >
              {renderClient(row, i)}
              <div className="d-flex flex-column">
                <span className="font-weight-bold">{row.name}</span>
                <small className="text-truncate text-muted mb-0">{row.role_type}</small>
              </div>
            </div>
          );
        },
      },
      {
        name: "Email",
        selector: "email",
        sortable: true,
        minWidth: "180px",
        cell: (row) => row.email,
      },

      {
        name: "Phone",
        selector: "phone",
        sortable: true,
        minWidth: "180px",
        cell: (row) => `${row.phone_code} ${row.phone}`,
      },
      {
        name: "Country",
        selector: "country",
        sortable: true,
        minWidth: "180px",
        cell: (row) => {
          let country = countries.find((el) => el.value == row.country);
          return country ? country.label : "";
        },
      },
      {
        name: "Status",
        minWidth: "110px",
        selector: "status",
        sortable: true,
        cell: (row) => (
          <CustomInput
            type="switch"
            className={row.id ? "custom-control-success " : ""}
            id={`status-${row.id}`}
            disabled={userDetails?.role_type!="admin" && !userDetails?.user?.includes("U")}
           defaultChecked={row.status==1 ? true: false}
           onChange={e=>  dispatch(UPDATE_USER(row.id,{status:!row.status},()=>notification({title:'Status ',message:`User is set to ${!row.status?"Active":"Inactive"} `,type:'success' })))}
           name="customSwitch"
             
            inline
          />
        ),
      },
     
  ]
    
  
  if(loading) return <Loading />
  
  return (
    <div className="invoice-list-wrapper">
      <Card>
        <div className="px-2 pb-1">
          <Header />
        </div>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={users?.filter(user => user.role_type!='admin')}
          />
        </div>
      </Card>
    </div>
  );
};

export default Table;
