import React,{ useContext, Fragment, useEffect, useState } from "react";
import Avatar from "@components/avatar";
import moment from 'moment'
import { selectThemeColors, countries as countryOptions } from "@utils";
// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash, Trash2, PlusCircle } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge,CardBody,FormGroup,Button, Input } from "reactstrap";
import Loading from "@core/components/spinner/Loading-spinner";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import { useDispatch, useSelector } from "react-redux";
import { ADD_TRANSFERFEE, DELETE_CURRENCY, DEL_TRANSFERFEE, GET_CURRENCY, STORE_TRANSFERFEE, UPDATE_TRANSFERFEE } from "../../redux/actions/transfer";
import Select from "react-select";
import { Formik ,Field,Form, ErrorMessage} from "formik";
import SlideDown from "react-slidedown";
import Row from "reactstrap/lib/Row";
import  currencyCodes  from "../../utility/extras/currencyCodes.json";
import Col from "reactstrap/lib/Col";
import Spinner from "reactstrap/lib/Spinner";
import * as Yup from "yup";
import { userDetails } from "../Merchant";
import { addAdminCurrency } from "../../Api";
import notification from "../../components/notification";
const TransferFee = () => {
    const dispatch=useDispatch();
    const [loading,setLoading]=useState(true);
    const {currencies} =useSelector(state=>state.transfer);
    const [Form,setForm]=useState(false);
    const [rowId,setRowId]=useState(0);
    useEffect(()=>{
   
    if(!currencies.length)dispatch(GET_CURRENCY(()=>setLoading(false)));
    else setLoading(false);    
},[currencies])
  
  if(loading) return <Loading/>

  const renderClient = (row, i) => {
    const stateNum = i % 6,
      states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
      color = states[stateNum];
  
    return <Avatar color={color || "primary"} className="mr-1" content={row.currency|| "John Doe"} initials />;
  };

  const columns = [
    {
      name: "Sr. no",
      selector: "id",
      sortable: true,
      minWidth: "110px",
      
      cell: (row,i) => {
        // let date = moment(row.date_of_transaction);
        return (
          <Fragment>
             {renderClient(row, i)}
         <span className='text-nowrap' >{row.id}</span>
          </Fragment>
        );
      },
    },
    {
      name: "Currency",
      selector: "currency",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.currency}</span>
        </Fragment>
      ),
    },
    {
        name: "Symbol",
        selector: "symbol",
        minWidth: "110px",
        sortable: true,
        cell: (row) => (
          <Fragment>
           
            <span > {currencyCodes[row.currency]} </span>
          </Fragment>
        ),
      },

      {
        name: "Created",
        selector: "created",
        sortable: true,
        minWidth: "110px",
        cell: (row) => {
          let date = moment(row.created);
          return (
            <Fragment>
              <div className="mt-1">
                <h6 className="font-small-3">{date.format("YYYY-MM-DD")}</h6>
                <h6 className="text-secondary font-small-2">{date.format("HH:mm:ss")}</h6>
              </div>
            </Fragment>
          );
        },
      },
      {
        name: "Action",
        selector: "action",
        sortable: true,
        minWidth: "110px",
        cell: (row) => {
         
          return (
            <Fragment>
              <div className="mt-1">
               <Trash size={17}  className='cursor-pointer' onClick={e=>dispatch(DELETE_CURRENCY(row.id,()=>dispatch(GET_CURRENCY())))} />
              </div>
            </Fragment>
          );
        },
      },


    
    
  ];

  const dataToRender = () => {
    let tempData=currencies;
   
    return tempData;
  };

  return (
    <>
         {Form ? (
        <SlideDown>
          <CustomForm  setForm={setForm} id={rowId} />
        </SlideDown>
      ) : null}
     <Button.Ripple className='mb-1'         color='primary' onClick={el=>{setForm(true);}}>
      <PlusCircle size={15} className="mr-1" />
       Add Currency
       </Button.Ripple>  
    <div className="invoice-list-wrapper">
     <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>
    </div>
    </>
  );
};

export default TransferFee;

const CustomForm =({setForm,row})=>{ 
  const dispatch =useDispatch()
const [loading,setLoading]=useState(false); 
const [symbol,setSymbol]= useState('');

const initialValues=  { 
   currency:'',
   symbol:''


}
const schema = Yup.object().shape({
  currency:Yup.string().required(),
  symbol:Yup.string().required()
})

const handleSubmit=async(value) => {
   setLoading(true);
   try{
   await addAdminCurrency(value);
    dispatch(GET_CURRENCY(()=>setLoading(false)));
   }catch(e){ 
     notification({title:"Error",message:e.toString(),type:'error'});
     setLoading(false);
   }
  }

return ( 
  <Card  >
    <CardBody >
      <Formik
      onSubmit={handleSubmit}
      initialValues={initialValues}
      validationSchema={schema}
      >
        { ({values,errors,touched})=>( 
           <Form>
             <Row>
             <FormGroup tag={Col} md='6' sm='12'>
            <label>Currency</label>
             <Select
              theme={selectThemeColors}
              className="react-select"
              classNamePrefix="select"
             defaultValue={{label:values.currency,value:values.currency}}
              options={Object.keys(currencyCodes).map(el=>({label:el,value:el,symbol:currencyCodes[el]}))}
              isClearable={false}
              onChange={el=>{values.currency=el.value;values.symbol=el.symbol;setSymbol(el.symbol)}}
              />
             </FormGroup>
             <FormGroup tag={Col} md='6' sm='12'>
               <label>Symbol</label>
               <Field
                name={'symbol'}
                id={'symbol'}
                className={`form-control ${errors.symbol && touched.symbol && "is-invalid"}`}
                value={symbol}
                />
                <ErrorMessage name="symbol" component="div" className="field-error text-danger" />

             </FormGroup>
            
             
             </Row>
             <Row>
            <Col md={4} sm={12} >
             <Button.Ripple       disabled={loading }    type="submit" className='mr-1' color="primary" outline >
            {loading ? <Spinner size="sm" color="primary" className="mr-1" /> : null}
            Add</Button.Ripple>
            <Button.Ripple onClick={e=>{e.preventDefault(); setForm(false);} } className='mr-1' color="secondary" outline disabled={loading}>Close</Button.Ripple>
            </Col>
             </Row>
           </Form>

        )
          
        }
      </Formik>
    </CardBody>
  </Card>


)


}