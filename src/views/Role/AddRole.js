import React, { useState } from 'react'
import * as Yup from "yup";
import { Formik, Field, Form, ErrorMessage } from "formik";
import{Button,Row,Card,FormGroup,Input,CardBody,Spinner} from 'reactstrap'
import { selectThemeColors, countries as countryOptions } from "@utils";
import Select from "react-select";
import Col from 'reactstrap/lib/Col';
import { useDispatch } from 'react-redux';
import { UPDATE_ROLE } from '../../redux/actions/storeMerchant.';
import { userDetails } from '../../utility/Utils';

export default function AddRole({setForm}) {
    const dispatch =useDispatch()
    const initialRole ={
        role_type:1,
        identifier:"",
        role_name:"",
    }
  const handleFormSubmit=(values)=>{
      //console.log(values);
      setLoading(true);
      dispatch(UPDATE_ROLE({data:values},()=>setLoading(false)));

  }
  const [loading,setLoading]=useState(false);
  const formSchema = Yup.object().shape({
 
   role_name:Yup.string().required("Role name is  Required"),
   identifier:Yup.string().required(" Identifier is Required"),
  })
    return (
     <Card className='mb-1'>
        <CardBody>
        <Row>
          <Col sm="12">
            <Formik
              initialValues={initialRole}
              enableReinitialize
              onSubmit={handleFormSubmit}
              validationSchema={formSchema}
            >
              {({ errors, touched, values, initialValues }) => (
                <Form>
                  <Row>
                    <Col md="4" sm="12">
                      <FormGroup>
                        <label for="role_name" className="form-label">
                          Name
                        </label>
                        <Field
                          name="role_name"
                          id="role_name"
                          placeholder="Name"
                          className={`form-control ${errors.role_name && touched.role_name && "is-invalid"}`}
                        />
                        <ErrorMessage name="role_name" component="div" className="field-error text-danger" />
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="12">
                      <FormGroup>
                      <label for="identifier" className="form-label">
                          Identifier
                        </label>
                        <Field
                         placeholder="Identifier"
                          name="identifier"
                           id="identifier"
                        
                          className={`form-control ${errors.identifier && touched.identifier && "is-invalid"}`}
                        />
                        <ErrorMessage name="identifier" component="div" className="field-error text-danger" />
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="12">
                      <FormGroup>
                      <label for="role_type" className="form-label">
                          Role Type
                        </label>
                        <Select
                          theme={selectThemeColors}
                          className="react-select"
                          classNamePrefix="select"
                          defaultValue={{label:"Parnter",value:1}}
                          options={[{label:"Parnter",value:1},{label:"Merchant",value:2}]}
                          isClearable={false}
                          onChange={(obj) => {
                            values.role_type = obj.value;
                          
                          }}
                        />
                      </FormGroup>
                    </Col>
                   </Row>
                   
                   <Col md={4} sm={12} >
                       <Button.Ripple type="submit" className='mr-1' color="primary" outline disabled={loading ||userDetails.role_type&& userDetails.role_type!='admin' && !userDetails['role'].includes('C')} >
                       {loading ? <Spinner size="sm" color="primary" className="mr-1" /> : null}
                           Add</Button.Ripple>
                       <Button.Ripple onClick={e=>setForm(false)} className='mr-1' color="secondary" outline disabled={loading}>Close</Button.Ripple>
                   </Col>
                 </Form>
               )}
            </Formik>
         </Col>
        </Row>
      </CardBody>      
     </Card>
    )
}
