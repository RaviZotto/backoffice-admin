import React, { createContext, useMemo, useState } from 'react'
import SlideDown from 'react-slidedown'
import AddRole from './AddRole'
import Table from './Table';
import CustomHeader from './Table/Header';
export const FormContext=createContext();
export default function index() {
const[Form,setForm]=useState(false);
const [active,setActive]=useState(1);
const form = useMemo(()=>({Form,setForm,active,setActive}),[form,active]);
    return (
        <FormContext.Provider value={form}>
            
             {Form ? (
        <SlideDown>
          <AddRole setForm={setForm}  />
        </SlideDown>
      ) : null}
          
          <CustomHeader/> 
          <Table />
        </FormContext.Provider>
    )
}
