import React, { useContext } from "react";
import { PlusCircle, Search } from "react-feather";
import { Row, Col, Button, Input, Card } from "reactstrap";
import CardBody from "reactstrap/lib/CardBody";
import CustomInput from "reactstrap/lib/CustomInput";
import Label from "reactstrap/lib/Label";
import { FormContext } from "..";
import { userDetails } from "../../Merchant";
const CustomHeader = () => {
 const {Form,setForm,setActive}=useContext(FormContext);
  return (
  
     <Card className='pt-1 mt-3'>
      <CardBody className='p-0'>
        <div  className="d-flex  flex-md-row  flex-column justify-content-between  align-item-center"> 
          <Col md={4} sm={12}>
        <Button.Ripple
        color="primary"
        className="pl-1 mb-2 "
        onClick={(el) => setForm(true)}
        disabled={userDetails.role_type&& userDetails.role_type!='admin' && !userDetails['role'].includes('C')} 
      >
        <PlusCircle size={15} className="mr-1" />
        Add Role
      </Button.Ripple> 
      </Col> 
       <Col md={4} sm={12} className='mb-1'>
         <Input
         type="select"
          name='toggle'
          defaultValue={1}
          onChange={el=>setActive(el.target.value)}
         >
           <option value={1} label="Partner"/>
           <option value={2} label="Merchant"/>
         
         </Input>
       </Col>
       </div>
        
        </CardBody>
        </Card>  
  );
};

export default CustomHeader;
