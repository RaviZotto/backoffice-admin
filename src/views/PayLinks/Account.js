import { useContext } from "react";
import { Card, CardBody } from "reactstrap";
import { TransactionsContext } from ".";

export default function Account({ data }) {
  const { selectedAccount, handleAccountFilter } = useContext(TransactionsContext);

  const isSelected = selectedAccount.id == data.id;

  return (
    <Card
      className="cursor-pointer"
      color={isSelected ? "primary" : ""}
      style={{ color: isSelected ? "white" : "" }}
      onClick={() => handleAccountFilter(data)}
    >
      <CardBody style={{ marginBottom: "-1.5rem" }}>
        <div className="d-flex justify-content-between">
          <div className="font-small-2 text-bold-600 mr-2 text-uppercase">{data.account_name}</div>
          <div className="font-small-2  text-bold-600 ml-2 text-uppercase" style={{ letterSpacing: "0.05rem" }}>
            {data.account_id}
          </div>
        </div>
      </CardBody>
      <hr style={{ borderColor: isSelected ? "#d0d2d6" : "" }} />
      <CardBody style={{ marginTop: "-1.3rem" }}>
        <div className="font-small-1">Balance</div>
        <h3 style={{ marginTop: "0.2rem", color: isSelected ? "white" : "" }}>
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: data.currencycode,
          })
            .format(data.current_balance)
            .replace(/^(\D+)/, "$1 ")}
        </h3>
      </CardBody>
    </Card>
  );
}
