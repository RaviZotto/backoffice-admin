import { useState, createContext, useMemo } from "react";
import { useQuery } from "@apollo/client";
import { GET_TRANSACTIONS, GET_ACCOUNTS } from "@graphql/queries";
import moment from "moment";
import PerfectScrollBar from "react-perfect-scrollbar";
import { Row, Col } from "reactstrap";
import Loading from "@core/components/spinner/Loading-spinner";
import Account from "./Account";
import TransactionsTable from "./Table";
import TransactionsSidebar from "./Sidebar";

export const TransactionsContext = createContext(null);

const Transactions = () => {
  // Making one week time interval for transactions query
  let temp = "2021-01-17"; // date offset for testing
  const d = new Date(temp);
  d.setDate(d.getDate() - 6);

  const [dates, setDates] = useState({
    startDate: moment(d).format("YYYY-MM-DD"),
    endDate: moment(temp).format("YYYY-MM-DD"),
  });

  // Getting all the transactions
  const { loading: loading1, data: data1 } = useQuery(GET_TRANSACTIONS, { variables: dates });

  // Getting all the currency accounts
  const { loading: loading2, data: data2 } = useQuery(GET_ACCOUNTS);

  // component logic starts here
  const [selectedAccount, setSelectedAccount] = useState({});
  const [searchQuery, setSearchQuery] = useState("");
  const [statusQuery, setStatusQuery] = useState("");
  const [selectedTransaction, setSelectedTransaction] = useState({});

  const handleAccountFilter = (el) => {
    if (el.id == selectedAccount.id) setSelectedAccount({});
    else setSelectedAccount(el);
  };

  // setting provider value for all child components
  const providerValue = useMemo(
    () => ({
      dates,
      setDates,
      searchQuery,
      setStatusQuery,
      statusQuery,
      setSearchQuery,
      transactions: data1 ? data1.transactions : [],
      accounts: data2 ? data2.accounts : [],
      selectedAccount,
      handleAccountFilter,
      selectedTransaction,
      setSelectedTransaction,
    }),
    [dates, data1, data2, selectedAccount, searchQuery, statusQuery, selectedTransaction]
  );

  if (loading1 || loading2) return <Loading />;

  return (
    <TransactionsContext.Provider value={providerValue}>
      <PerfectScrollBar>
        {/* give each component a key */}
        <Row className="row flex-row flex-nowrap">
          {data2.accounts.map((el) => {
            return (
              <Col key={el.id} className="col-auto">
                <Account data={el} />
              </Col>
            );
          })}
        </Row>
      </PerfectScrollBar>
      <TransactionsTable />
      <TransactionsSidebar />
    </TransactionsContext.Provider>
  );
};

export default Transactions;
