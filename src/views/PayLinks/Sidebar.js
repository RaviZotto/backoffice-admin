// ** Third Party Components
import { useContext } from "react";
import { Table, Button } from "reactstrap";
import Sidebar from "@components/sidebar";
import { TransactionsContext } from ".";
import moment from "moment";
// import "@styles/base/pages/app-invoice.scss";

const TransactionSidebar = () => {
  const { selectedTransaction: transaction, setSelectedTransaction, accounts } = useContext(TransactionsContext);

  const closeSidebar = () => setSelectedTransaction({});

  return (
    <Sidebar
      size="lg"
      open={transaction.id ? true : false}
      title="Transaction Details"
      headerClassName="mb-2"
      contentClassName="p-0"
      toggleSidebar={closeSidebar}
    >
      <Table style={{ tableLayout: "fixed" }} borderless={true} striped={true}>
        {transaction.id ? (
          <tbody>
            <tr>
              <td>Date</td>
              <td>{moment(transaction.date_of_transaction).format("YYYY-MM-DD")}</td>
            </tr>
            <tr>
              <td>Time</td>
              <td>{moment(transaction.date_of_transaction).format("HH:mm:ss")}</td>
            </tr>
            <tr>
              <td>Status</td>
              <td>{transaction.status == 1 ? "Success" : "Failed"}</td>
            </tr>
            <tr>
              <td>Account</td>
              <td>{accounts.find((el) => el.id == transaction.currency_account_id).account_name}</td>
            </tr>
            <tr>
              <td>Transaction ID</td>
              <td>
                <div style={{ wordWrap: "break-word" }}>{transaction.txn_no}</div>
              </td>
            </tr>
            <tr>
              <td>Order ID</td>
              <td>{transaction.order_id}</td>
            </tr>
            <tr>
              <td>Amount</td>
              <td>
                {transaction.amount} {transaction.currency}
              </td>
            </tr>
            <tr>
              <td>Payment Type</td>
              <td>{transaction.paid_type == "cr" ? "Credit" : "Debit"}</td>
            </tr>
            <tr>
              <td>Payment Method</td>
              <td>{transaction.pay_method}</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>
                <div className="font-small-3">{transaction.message}</div>
              </td>
            </tr>
            <tr>
              <td>Refunded</td>
              <td>{transaction.refund_type !== 0 ? "Yes" : "No"}</td>
            </tr>
            <tr>
              <td>Refundable</td>
              <td>{transaction.refund_status == 1 ? "Yes" : "No"}</td>
            </tr>
          </tbody>
        ) : (
          <tbody></tbody>
        )}
      </Table>
      <div className="text-center mt-2">
        <Button.Ripple color="danger">TODO</Button.Ripple>
      </div>
    </Sidebar>
  );
};

export default TransactionSidebar;
