import React, { useState } from 'react';
import {useDispatch,useSelector} from 'react-redux';
import { Link, useHistory } from "react-router-dom";
import InputPasswordToggle from "@components/input-password-toggle";
import { Card, CardBody, CardTitle, CardText, FormGroup, Label, Button, Spinner } from "reactstrap";
import "@styles/base/pages/page-auth.scss";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import logoZotto from "../../assets/images/logo/Zottologo.png";
import axios from 'axios';
import notification from '../../components/notification';
import {STORE_ADMINDETAIL} from '../../redux/actions/auth'
import { graphqlServerUrl } from '../../App';
import styled from 'styled-components'
import { AdminDetails} from '@src/App';

const formSchema = Yup.object().shape({
  email: Yup.string().email().required("Required"),
  password: Yup.string().required("Required"),

});

const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  // const [login, { data, loading }] = useMutation(LOGIN_MERCHANT);
  
 const [loader,setLoading]=useState(false);
  const handleLogin = (data) =>{
      setLoading(true);
      axios({url:`${graphqlServerUrl}/admin/login`,method:'post',data:{...data}}).then(
        res=>{
             const {token_temp}=res.data;
            if(res.data && res.status==200){
              

            localStorage.setItem('temp_token',token_temp)
            history.push('/verify');
         
          }
          setLoading(false);
        }
      ).catch((e:AxiosError)=>{
        //console.log(e)
        localStorage.removeItem('temp_token');
        history.push('/login')
        notification({title:'Error',message:e.response?e.response.data:e.toString(),type:'error'});
        setLoading(false);
      })
  
    }



  return (
    <div className="auth-wrapper auth-v1 px-2">
      <div className="auth-inner py-2">
        <Card className="mb-0">
          <CardBody>
            <Link  onClick={(e) => e.preventDefault()}>
            <IMG
                    
                    src={logoZotto}
                  
                  />
            </Link>
            <CardTitle tag="h4" className="mb-1">
              Welcome to Zotto Admin! 👋
            </CardTitle>
            <CardText className="mb-2">Please sign-in to your account and start the adventure</CardText>
            <Formik
              initialValues={{
                email: "",
                password: "",
         
              }}
              onSubmit={handleLogin}
              validationSchema={formSchema}
            >
              {({ errors, touched }) => (
                <Form className="auth-login-form mt-2">
                  <FormGroup>
                    <Label className="form-label" for="email">
                      Email
                    </Label>
                    <Field
                      className={`form-control ${errors.email && touched.email && "is-invalid"}`}
                      type="email"
                      name="email"
                      placeholder="john@example.com"
                      autoFocus
                    />
                    <ErrorMessage name="email" component="div" className="field-error text-danger" />
                  </FormGroup>
                  <FormGroup>
                    <div className="d-flex justify-content-between">
                      <Label className="form-label" for="password">
                        Password
                      </Label>
                      <Link to="/forgot-password">
                        <small>Forgot Password?</small>
                      </Link>
                    </div>
                    <Field className="input-group-merge" name="password" component={InputPasswordToggle} />
                    <ErrorMessage name="password" component="div" className="field-error text-danger" />
                  </FormGroup>
                 
                  <Button.Ripple color="primary" block type="submit" disabled={loader} >
                    {loader ? <Spinner color="white" size="sm mr-1" /> : null}
                    Sign in
                  </Button.Ripple>
                </Form>
              )}
            </Formik>
          </CardBody>
        </Card>
      </div>
    </div>
  );
};

export default Login;
const IMG = styled.img`
width: 100%;
margin-left:5em;
max-width: 200px;
height: auto;
   
   `


