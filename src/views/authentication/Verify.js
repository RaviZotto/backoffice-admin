import React from 'react';
import { Link, useHistory } from "react-router-dom";
import { ChevronLeft } from "react-feather";
import { Card, CardBody, CardTitle, CardText, FormGroup, Label, Button, Spinner } from "reactstrap";
import "@styles/base/pages/page-auth.scss";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import styled from 'styled-components'
import notification from "../../components/notification";
import { useEffect,useState } from "react";
import axios from 'axios'
import { graphqlServerUrl } from '../../App';
import logoZotto from "../../assets/images/logo/Zottologo.png";
import { STORE_ADMINDETAIL } from '../../redux/actions/auth';
import { useDispatch } from 'react-redux';
const formSchema = Yup.object().shape({
  otp: Yup.string()
    .test("len", "Must be exactly 6 digits", (val) => (val ? val.length === 6 : true))
    .required("Required"),
});

const ResetPasswordV1 = () => {
  const dispatch = useDispatch()
  const history = useHistory();
  const[loading,setLoading]=useState(false);
  const[loader,setLoader]=useState(false);

 const resetGoogleAuth=()=>{
  setLoader(true);
  let temp_token = localStorage.getItem("temp_token");
  if (!temp_token)
    notification({
      type: "error",
      title: "Error",
      message: "Please Login First!",
    });


  axios.put(`${graphqlServerUrl}/admin/resetGoogleAuth`,{temp_token},{headers: {'Content-Type': 'application/json'}}).then(res=>{
    if(res.data&&res.status === 200){
      setLoader(false);
      notification({ title: "Reset google auth", message:"new google auth qr code is sent to your email id" ,type:'success'});
      
    }
  }).catch((e:AxiosError)=>{
    setLoader(false);
    notification({type: 'error', message:e.response?e.response.data:e.toString(),title:'Error'});
    if(e.response&&e.response.data.includes('jwt expired')){
      history.push('/')
      localStorage.clear();
  
    }
  });

 }


  const handleVerification = (data) => {
    let temp_token = localStorage.getItem("temp_token");
    if (!temp_token)
      notification({
        type: "error",
        title: "Error",
        message: "Please Login First!",
      });
    else {
      setLoading(true);
      axios.post(`${graphqlServerUrl}/admin/verify`,{data,temp_token},{headers: {'Content-Type': 'application/json'}}).then(res=>{
        if(res.data&&res.status === 200){
          setLoading(false);
          const {token,user,userDetails,access_expire_date,refresh_token,refresh_expire_token} = res.data;
          localStorage.setItem('token_item',JSON.stringify({token,user,access_expire_date,refresh_token,refresh_expire_token}));
          localStorage.setItem('userDetails',JSON.stringify(userDetails));
          dispatch(STORE_ADMINDETAIL(userDetails,()=>{
          window.dispatchEvent(new Event('refreshtoken'));
          localStorage.setItem('image',`${graphqlServerUrl}/images/admin/${userDetails.id}.png`);
          history.push('/'); 
          }));
        }
      }).catch((e:AxiosError)=>{
         setLoading(false);
         notification({type: 'error', message:e.response?e.response.data:e.toString(),title:'Error'});
         if(e.response&&e.response.data.includes('jwt expired')){
          localStorage.clear();
          history.push('/')
        }
      });

     
    }
  };


  return (
    <div className="auth-wrapper auth-v1 px-2">
      <div className="auth-inner py-2">
        <Card className="mb-0">
          <CardBody>
          <Link  onClick={(e) => e.preventDefault()}>
            <IMG
                    
                    src={logoZotto}
                  
                  />
            </Link>
             <CardTitle>
              Verify it's you 🔒
            </CardTitle>
            <CardText className="mb-2">Please enter the 6 digit OTP shown in the google authenticator app.</CardText>
            <Formik
              initialValues={{
                otp: "",
              }}
              onSubmit={handleVerification}
              validationSchema={formSchema}
            >
              {({ errors, touched }) => (
                <Form className="auth-reset-password-form mt-2">
                  <FormGroup>
                    <Label className="form-label" for="otp">
                      OTP
                    </Label>
                    <Field
                      className={`form-control ${errors.otp && touched.otp && "is-invalid"}`}
                      name="otp"
                      autoFocus
                    />
                    <ErrorMessage name="otp" component="div" className="field-error text-danger" />
                  </FormGroup>
                  <Link  onClick={e=>resetGoogleAuth()}  disabled={loader} className='d-flex justify-content-end' >
                
                <span >{ loader?'sending....':'Reset google auth'}</span>
              </Link>
                  <Button.Ripple color="primary" block className='mt-1' type="submit" disabled={loading}>
                    {loading ? <Spinner color="white" size="sm mr-1" /> : null}
                    Verify
                  </Button.Ripple>
                </Form>
              )}
            </Formik>
            
            <p className="text-center mt-2">
              <Link to="/login">
                <ChevronLeft className="mr-25" size={14} />
                <span className="align-middle">Back to login</span>
              </Link>
            </p>
          </CardBody>
        </Card>
      </div>
    </div>
  );
};

export default ResetPasswordV1;
const IMG = styled.img`
width: 100%;
margin-left:5em;
max-width: 200px;
height: auto;
   
   `