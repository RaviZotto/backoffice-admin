import { Link } from "react-router-dom";
import { ChevronLeft } from "react-feather";
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import "@styles/base/pages/page-auth.scss";
import React,{ useEffect, useState } from "react";
import ResetPage from "./resetPassword";
import logoZotto from "../../assets/images/logo/Zottologo.png";
import { validateEmail } from "../../utility/extras";
import notification from "../../components/notification";
import Spinner from "reactstrap/lib/Spinner";
import axios from "axios";
import { sendEmail } from "../../Api";
import { Fragment } from "react";
const ForgotPassword = () => {
  const [reset, setReset] = useState(false);
  const [email, setEmail] = useState("");
  const [loading,setLoading]=useState(false);





  const handleEmail = () => {
    if (!validateEmail(email))
      return notification({
        title: "Error",
        message: "Invalid email",
        type: "Error",
      });
      setLoading(true);
     sendEmail(email,({err,res})=>{
       if(err)notification({type:'error',message:err.message,title:'Error'});
       else setReset(true);
       setLoading(false);
      })

  };

  return (
    <Fragment>
      {!reset ? (
        <div className="auth-wrapper auth-v1 px-2">
          <div className="auth-inner py-2">
            <Card className="mb-0">
              <CardBody>
                <Link
                  className="brand-logo"
                  to="/"
                  onClick={(e) => e.preventDefault()}
                >
                  <img
                    className="Login"
                    src={logoZotto}
                    style={{ width: 200 }}
                  />
                </Link>
                <CardTitle tag="h4" className="mb-1">
                  Forgot Password? 🔒
                </CardTitle>
                <CardText className="mb-2">
                  Enter your email and we'll send you instructions to reset your
                  password
                </CardText>
                <Form
                  className="auth-forgot-password-form mt-2"
                  onSubmit={(e) => e.preventDefault()}
                >
                  <FormGroup>
                    <Label className="form-label" for="login-email">
                      Email
                    </Label>
                    <Input
                      type="email"
                      id="login-email"
                      value={email}
                      placeholder="john@example.com"
                      autoFocus
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </FormGroup>
                  <Button.Ripple
                    disabled={!validateEmail(email) || loading}
                    color="primary"
                    block
                    onClick={() => handleEmail()}
                  >
                    {loading ? <Spinner size="sm" className="mr-1" /> : null}{" "}
                    Send reset link
                  </Button.Ripple>
                </Form>
                <p className="text-center mt-2">
                  <Link to="/login">
                    <ChevronLeft className="mr-25" size={14} />
                    <span className="align-middle">Back to login</span>
                  </Link>
                </p>
              </CardBody>
            </Card>
          </div>
        </div>
      ) : (
        <ResetPage email={email} />
      )}
    </Fragment>
  );
};

export default ForgotPassword;
