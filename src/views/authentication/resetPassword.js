import React, { useCallback, useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import "@styles/base/pages/page-auth.scss";
import { Link } from "react-router-dom";
import { ChevronLeft } from "react-feather";
import logoZotto from "../../assets/images/logo/Zottologo.png";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import InputPasswordToggle from "@components/input-password-toggle";
import notifier from "../../components/notification";
import { useHistory } from "react-router-dom";
import Spinner from "reactstrap/lib/Spinner";
import { forgotPassword, sendEmail } from "../../Api";
import notification from "../../components/notification";


const formSchema = Yup.object().shape({
  otp: Yup.string().required("Required"),
  password: Yup.string().required("Required"),
  confirmpassword: Yup.string().required("Required"),
});

const ResetPage = ({ email }) => {
 
  const[loading,setLoading]=useState(false);
  const [timer, setTimer] = useState(180);
  const history = useHistory();
  useEffect(() => {
    timer > 0 && setTimeout(() => setTimer(timer - 1), 1000);
  }, [timer]);


  // const Email=process.env.EMAIL;
  //console.log(email);

  const handleEmail = () => {
    // if(!validateEmail(email))return notification({title:'Error',message:"Invalid email",type:'Error'});
    sendEmail(email,({err,res})=>{
      if(err){notification({type:'error',message:err.message,title:'Error'});}
      else setTimer(180);
      })

  };

  const handleReset = (data) => {
    const { password, otp } = data;
    //console.log(data);
    //  if(password!=confirmpassword)return notifier({title:'Error', type:'error',message:'confirm password is not matched'})
     forgotPassword({otp,password,email},({err})=>{
      if(err){notification({type:'error',message:err.message,title:'Error'});}
      else{history.push('/login');notification({type:'success',message:'Succesfully updated',title:'Forgot password'})}
     })
  };


  return (
    <div className="auth-wrapper auth-v1 px-2">
      <div className="auth-inner py-2">
        <Card className="mb-0">
          <CardBody>
            <Link
              className="brand-logo"
              to="/"
              onClick={(e) => e.preventDefault()}
            >
              <img className="Login" src={logoZotto} style={{ width: 200 }} />
            </Link>
            {timer ? (
              <CardTitle tag="dispaly-6" className="mb-1">
                Please enter the otp , which is sent to your email, otp will
                expire withing{" "}
                <span style={{ marginRight: "0.3em" }} className="text-danger">
                  {timer}
                </span>{" "}
                seconds
              </CardTitle>
            ) : (
              <div
                disabled={loading}
                className="d-flex justify-content-center text-success"
              >
                <span>
                  Click{" "}
                  <a
                    onClick={() => handleEmail()}
                    className="border-bottom-primary"
                    href="#"
                  >
                    here
                  </a>{" "}
                  to resend otp
                </span>
              </div>
            )}
            <CardText className="mb-2"></CardText>
            <Formik
              className="w-75"
              initialValues={{
                otp: "",
                password: "",
                confirmpassword: "",
              }}
              validate={(values) => {
                const errors = {};
                if (values.confirmpassword != values.password) {
                  errors.confirmpassword =
                    "confirm password is not matched with new password";
                }
                return errors;
              }}
              onSubmit={handleReset}
              validationSchema={formSchema}
            >
              {({ errors, touched, values }) => (
                <Form className="auth-login-form mt-2">
                  <FormGroup>
                    <Label className="form-label" for="email">
                      Otp
                    </Label>
                    <Field
                      className={`form-control ${
                        errors.otp && touched.otp && "is-invalid"
                      }`}
                      required
                      type="text"
                      name="otp"
                      placeholder="Please enter the otp"
                      autoFocus
                    />
                    <ErrorMessage
                      name="otp"
                      component="div"
                      className="field-error text-danger"
                    />
                  </FormGroup>
                  <FormGroup>
                    <div className="d-flex justify-content-between">
                      <Label className="form-label" for="password">
                        Password
                      </Label>
                    </div>
                    <Field
                      className="input-group-merge"
                      name="password"
                      component={InputPasswordToggle}
                    />
                    <ErrorMessage
                      name="password"
                      component="div"
                      className="field-error text-danger"
                    />
                  </FormGroup>
                  <FormGroup>
                    <div className="d-flex justify-content-between">
                      <Label className="form-label" for="password">
                        Confirm Password
                      </Label>
                    </div>
                    <Field
                      className={`form-control ${
                        errors.confirmpassword &&
                        touched.confirmpassword &&
                        "is-invalid"
                      }`}
                      name="confirmpassword"
                      
                    />
                    <ErrorMessage
                      name="confirmpassword"
                      component="div"
                      className="field-error text-danger"
                    />
                  </FormGroup>
                  <Button.Ripple
                    color="primary"
                    block
                    type="submit"
                    disabled={
                      !values.otp ||
                      values.confirmpassword != values.password ||
                      loading
                    }
                  >
                    {loading ? <Spinner color="white" size="sm mr-1" /> : null}
                    Reset passowrd
                  </Button.Ripple>
                </Form>
              )}
            </Formik>

            <p className="text-center mt-2">
              <Link to="/login">
                <ChevronLeft className="mr-25" size={14} />
                <span className="align-middle">Back to login</span>
              </Link>
            </p>
          </CardBody>
        </Card>
      </div>
    </div>
  );
};
export default ResetPage;
