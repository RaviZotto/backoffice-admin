import React,{useMemo,Fragment, useContext} from 'react';
import DataTable from "react-data-table-component";
import Avatar from "@components/avatar";
import { ChevronDown } from 'react-feather';
import {Card} from 'reactstrap'
import { AddClientContext } from '.';
function Table({query}) {
    const {merchantId,clients} = useContext(AddClientContext)
   
    const renderClient = (row, i) => {
        const stateNum = i % 6,
          states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
          color = states[stateNum];
      
        return <Avatar color={color || "primary"} className="mr-1" content={row.comp_name || "John Doe"} initials />;
      };
       const columns = useMemo(()=>
    
      ( [
        {
          name: "Name", 
          selector: "name",
          sortable: true,
          minWidth: "150px",
          
          cell: (row,i) => {
            // let date = moment(row.date_of_transaction);
            return (
              <Fragment>
                 {renderClient(row, i)}
             <span className='text-wrap'>{row.client_name}</span>
              </Fragment>
            );
          },
        },
        {
          name: "Phone",
          selector: "phone",
          minWidth: "110px",
          sortable: true,
          cell: (row) => (
            <Fragment>
             
              <span>{row.phone_code+row.phone}</span>
            </Fragment>
          ),
        },
        {
          name: "Email",
          selector: "email",
          minWidth: "170px",
          sortable: true,
          cell: (row) => (
            <Fragment>
             
              <span className='text-wrap '>{row.client_email}</span>
            </Fragment>
          ),
        },
        {
            name: "Country",
            selector: "country",
            minWidth: "170px",
            sortable: true,
            cell: (row) => (
              <Fragment>
               
                <span className='text-wrap '>{row.country}</span>
              </Fragment>
            ),
          }
       
        
        
      ]),[clients])
    
      const dataToRender = () => {
        let tempData=clients;
         if(merchantId!=0)tempData=tempData.filter(el=>el.merchant_id==merchantId);
         if(query!="" || query!=undefined){
           tempData = tempData.filter(el=>{
           
               if(el.client_name.startsWith(query)|| el.client_name.includes(query)||el.client_email.includes(query)) return true;
               else return false;
           })
         }
        return tempData;
      };
    
      return (
        <div className="invoice-list-wrapper">
      {/* <CustomHeader setMessage={setMessage} history={history} setSelectedPartner={setSelectedPartner} setSearchQuery={setSearchQuery} /> */}


          <Card>
            <div className="invoice-list-dataTable">
              <DataTable
                className="react-dataTable"
                noHeader
                pagination
                columns={columns}
                responsive={true}
                sortIcon={<ChevronDown />}
                data={dataToRender()}
              />
            </div>
          </Card>
        </div>
      );
}


export default Table
