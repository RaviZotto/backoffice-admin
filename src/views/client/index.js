import React, { useEffect, useState, createContext, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GET_CLIENTS } from "../../redux/actions/transfer";
import Loading from "@core/components/spinner/Loading-spinner";
import Table from "./Table";
import { Button, Input, Row,Card, Col } from "reactstrap";
import { Plus, Search } from "react-feather";
import { UPDATE_MERCHANT } from "../../redux/actions/storeMerchant.";
import AddClientSidebar from "./ClientModal";
import userPersmission from "@hooks/userPersmission";
export const AddClientContext = createContext(null);
function index() {
  const {userDetails} = userPersmission()
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [clientModal, setClientModal] = useState(false);
  const [merchantId, setMerchantId] = useState(0);
  const [query, setQuery] = useState("");
  const { clients } = useSelector((state) => state.transfer);
  const { merchants } = useSelector((state) => state.merchant);
  useEffect(() => {

    if (loading) {
      if (!clients.length) dispatch(GET_CLIENTS());
      if (!merchants.length) dispatch(UPDATE_MERCHANT(() => setLoading(false)));
    }
  },[]);




  const CustomHeader = () => {
    return (
      <Card className='p-1 mt-1'>
        <Row>
          <Col md={6} sm={12}>  
           
          <Input
            id="search-invoice"
            type="text"
            className='form-control w-100'
            value={query}
            onChange={(el) =>{ el.preventDefault();setQuery(el.target.value)}}
            placeholder="search by name,email.."
          />
          </Col>
          <Col md={6} sm={12}>
          
            <Input
            type="select"
            name="merchant"
            value={merchantId}
            onChange={(e)=>setMerchantId(e.target.value)}
            >
                <option value={0} label="select merchant--"  />
           {  merchants.map(el=> <option value={el.merchant_id} label={el.comp_name} />)}
            </Input>  
          </Col>
        </Row>
      </Card>
    );
  };

  const clientMemo = useMemo(
    () => ({
      clients,
      merchants,
      clientModal,
      setClientModal,
      merchantId,
      query,
      userDetails,
      setQuery,
      setMerchantId,
    }),
    [clients,userDetails,  merchantId, merchants, clientModal, setClientModal]
  );
  if (!clients.length) return <Loading />;
  return (
    <AddClientContext.Provider value={clientMemo}>
      <AddClientSidebar />
      <Button color="primary" disabled={!userDetails?.client?.includes('C')}  onClick={(e) => setClientModal(true)}>
        <Plus size={14} /> Add clients
      </Button>
      <CustomHeader />
      <Table query={query} />
    </AddClientContext.Provider>
  );
}

export default index;
