import Sidebar from "@components/sidebar";
import React, { useContext, useEffect, useState } from "react";
import { InputGroup, InputGroupAddon, InputGroupText, FormGroup, Label, Button } from "reactstrap";
import Select from "react-select";
import { selectThemeColors, countries as countryOptions } from "@utils";
import { AddClientContext} from ".";
import Spinner from "reactstrap/lib/Spinner";
import produce from "immer";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { ADD_CLIENTSOBJ } from "../../redux/actions/transfer";
import { useDispatch } from "react-redux";
import notification from "../../components/notification";

const formSchema = Yup.object().shape({
  client_name: Yup.string().required("Required"),
  client_email: Yup.string().email("Invalid email").required("Required"),
  phone: Yup.number("Contact must be number").required("Required"),
  zip_code: Yup.number("Zip code must be number").required("Required"),
  address: Yup.string().required("Required"),
  company_name: Yup.string().required("Required"),
  city: Yup.string().required("Required"),
  phone_code: Yup.string(),
  country: Yup.string(),
  company_number: Yup.string(),
  tax_number: Yup.string(),
});

export default function AddClientSidebar() {
  const { clientModal:clientSidebarOpen, setClientModal:setClientSidebarOpen, merchantId,setMerchantId,merchants } =
    useContext(AddClientContext);
    const dispatch = useDispatch()
  const [loading,setLoading] = useState(false);
  const [phoneCode, setPhoneCode] = useState("+44");
  
  useEffect(() => setPhoneCode("+44"), [clientSidebarOpen]);

  const handleAddClient = (fieldData) => {
    let submitData = { ...fieldData ,merchant_id:merchantId};
    setLoading(true);
    dispatch(ADD_CLIENTSOBJ(submitData,({err})=>{setLoading(false)
    if(err)notification({title:"Error",message:err,type:"error"})
    else setClientSidebarOpen(false);
    }));
       

  };

  return (
    <Sidebar
      size="lg"
      open={clientSidebarOpen}
      title="Add Clients"
      headerClassName="mb-1"
      contentClassName="p-0"
      toggleSidebar={() => setClientSidebarOpen(!clientSidebarOpen)}
    >
      <Formik
        initialValues={{
          client_name: "",
          client_email: "",
          phone: "",
          zip_code: "",
          address: "",
          company_name: "",
          city: "",
          phone_code: "+44",
          country: "GB",
          company_number: "",
          tax_number: "",
        }}
        onSubmit={handleAddClient}
        validationSchema={formSchema}
      >
        {({ errors, touched, values }) => (
          <Form>
            <FormGroup>
            <Label for="client_name" className="form-label">
                Merchants
              </Label>
            <Select
                      className="react-select"
                      classNamePrefix="select"
                      id="merchants"
                      name="merchants"
                      onChange={(e) =>setMerchantId(e.value)}
                      options={merchants.map(el=>({label:el.comp_name,value:el.merchant_id}))}
                      theme={selectThemeColors}
                    
                    />
            </FormGroup>
            <FormGroup>
              <Label for="client_name" className="form-label">
                Client Name
              </Label>
              <Field
                name="client_name"
                id="client_name"
                placeholder="John Doe"
                className={`form-control ${errors.client_name && touched.client_name && "is-invalid"}`}
              />
              <ErrorMessage name="client_name" component="div" className="field-error text-danger" />
            </FormGroup>
            <FormGroup>
              <Label for="client_email" className="form-label">
                Client Email
              </Label>
              <Field
                name="client_email"
                type="email"
                id="client_email"
                placeholder="example@domain.com"
                className={`form-control ${errors.client_email && touched.client_email && "is-invalid"}`}
              />
              <ErrorMessage name="client_email" component="div" className="field-error text-danger" />
            </FormGroup>
            <FormGroup>
              <Label for="company_name" className="form-label">
                Company Name
              </Label>
              <Field
                name="company_name"
                id="company_name"
                placeholder="Zotto"
                className={`form-control ${errors.company_name && touched.company_name && "is-invalid"}`}
              />
              <ErrorMessage name="company_name" component="div" className="field-error text-danger" />
            </FormGroup>
            <FormGroup>
              <Label for="company_number" className="form-label">
                Company Number
              </Label>
              <Field
                name="company_number"
                id="company_number"
                placeholder="123456"
                className={`form-control ${errors.company_number && touched.company_number && "is-invalid"}`}
              />
              <ErrorMessage name="company_number" component="div" className="field-error text-danger" />
            </FormGroup>
            <FormGroup>
              <Label for="tax_number" className="form-label">
                Tax Number
              </Label>
              <Field
                name="tax_number"
                id="tax_number"
                placeholder="123456"
                className={`form-control ${errors.tax_number && touched.tax_number && "is-invalid"}`}
              />
              <ErrorMessage name="tax_number" component="div" className="field-error text-danger" />
            </FormGroup>
            <FormGroup>
              <Label for="address" className="form-label">
                Address
              </Label>
              <Field
                type="textarea"
                cols="2"
                rows="2"
                name="address"
                id="address"
                placeholder="1307 Lady Bug Drive New York"
                className={`form-control ${errors.address && touched.address && "is-invalid"}`}
              />
              <ErrorMessage name="address" component="div" className="field-error text-danger" />
            </FormGroup>
            <FormGroup>
              <Label for="city" className="form-label">
                City
              </Label>
              <Field
                name="city"
                id="city"
                placeholder="london"
                className={`form-control ${errors.city && touched.city && "is-invalid"}`}
              />
              <ErrorMessage name="city" component="div" className="field-error text-danger" />
            </FormGroup>
            <FormGroup>
              <Label for="country" className="form-label">
                Country
              </Label>
              <Select
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
                defaultValue={countryOptions[227]}
                options={countryOptions}
                isClearable={false}
                onChange={(obj) => {
                  setPhoneCode(obj.phone_code);
                  values.country = obj.value;
                  values.phone_code = obj.phone_code;
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label for="zip_code" className="form-label">
                Zip Code
              </Label>
              <Field
                name="zip_code"
                id="zip_code"
                placeholder="345653"
                className={`form-control ${errors.zip_code && touched.zip_code && "is-invalid"}`}
              />
              <ErrorMessage name="zip_code" component="div" className="field-error text-danger" />
            </FormGroup>
            <FormGroup>
              <Label for="phone" className="form-label">
                Contact
              </Label>
              <InputGroup className="input-group-merge">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText style={{ borderColor: errors.phone && touched.phone ? "red" : "" }}>
                    {phoneCode}
                  </InputGroupText>
                </InputGroupAddon>
                <Field
                  name="phone"
                  id="phone"
                  placeholder="763-242-9206"
                  className={`form-control ${errors.phone && touched.phone && "is-invalid"}`}
                />
              </InputGroup>
              <ErrorMessage name="phone" component="div" className="field-error text-danger" />
            </FormGroup>
            <FormGroup className="d-flex flex-wrap mt-2">
              <Button.Ripple disabled={loading} className="mr-1" color="primary" type="submit">
                {loading ? <Spinner size="sm" className="mr-1" /> : null}
                {loading ? "Adding" : "Add"}
              </Button.Ripple>
              <Button.Ripple
                disabled={loading}
                color="secondary"
                onClick={() => setClientSidebarOpen(false)}
                outline
              >
                Cancel
              </Button.Ripple>
            </FormGroup>
          </Form>
        )}
      </Formik>
    </Sidebar>
  );
}
