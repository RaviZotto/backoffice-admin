import React,{Fragment,useMemo, useState,useEffect} from 'react';
import {Card,Modal,ModalHeader,ModalFooter,ModalBody,Row} from 'reactstrap';
import DataTable from 'react-data-table-component';
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import { ChevronDown ,Eye, X} from 'react-feather';
import { useSelector } from 'react-redux';
import Avatar from "@components/avatar";
import moment from 'moment';
import { graphqlServerUrl } from '../../App';


const  ViewModal =({viewPopup,setViewPopup,merchants,partners})=>{
 
  const [read,setRead]=useState([]);
  useEffect(() => {
    //console.log(read,viewPopup);
   
    if(!read.length && viewPopup.mark_status){
      viewPopup.mark_status.replace('0','')
      setRead(viewPopup.mark_status.split(','));
    }
     if(!viewPopup.mark_status){setRead([])}

  },[viewPopup])
    

  const CloseBtn = (
    <X
      className="cursor-pointer"
      size={15}
      onClick={(e) => setViewPopup({})}
    />
  );
  return (
    <Modal
    isOpen={viewPopup.id}
    toggle={(e) => setViewPopup({})}
    //   className='middle-lg'
    //   modalClassName='modal-slide-in'
    contentClassName=" middle-sm pt-0"
   
    >
    <ModalHeader
       className=""
       toggle={(e) => setViewPopup({})}
       close={CloseBtn}
       tag="div"
    >
      Read 
    </ModalHeader>
    <ModalBody>
     <Row>
      {
        read.length>1?read.map(res=>{
          console.log(res)  
          if(res==0)return null;
          let merchantObj=merchants.find(el=>el.merchant_id==Number(res));
    
         return ( <div className='p-1 col-2'  >
            
          <Avatar img={`${graphqlServerUrl}/images/merchant/${Number(res)||1}.png`} />
          <small>{merchantObj&&merchantObj.comp_name}</small>
           </div>)
          }  ):null
      }
      </Row>
    </ModalBody>
    </Modal>
  )
}


function Table({query}) {
    const {notifications,merchants,partners} = useSelector(state=>state.merchant)
    const [viewPopup,setViewPopup]=useState({});
    const renderClient = (row, i) => {
        const stateNum = i % 6,
          states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
          color = states[stateNum];
      
        return <Avatar color={color || "primary"} className="mr-1" content={row?row.comp_name : "John Doe"} initials />;
      };

    const columns = useMemo(()=>

    ( [
      {
        name: "Name", 
        selector: "name",
        sortable: true,
        minWidth: "150px",
        
        cell: (row,i) => {
          // let date = moment(row.date_of_transaction);
          let obj;
          if(row.type=="merchant") obj = merchants.find(el=>row.merchant_id==el.merchant_id);
          if(row.type=="partner") obj = partners.find(el=>row.merchant_id==el.merchant_id);
           
          return (
            <Fragment>
               {renderClient(obj, i)}
            <span className='text-wrap'>{obj?obj.comp_name:null}</span>
            </Fragment>
          );
        },
      },
      {
        name: "title",
        selector: "title",
        minWidth: "110px",
        sortable: true,
        cell: (row) => (
          <Fragment>
           
            <span>{row.title}</span>
          </Fragment>
        ),
      },
      {
        name: "message",
        selector: "message",
        minWidth: "170px",
        sortable: true,
        cell: (row) => (
          <Fragment>
           
            <span className='text-wrap '>{row.message}</span>
          </Fragment>
        ),
      },
      {
        name: "type",
        selector: "type",
        minWidth: "170px",
        sortable: true,
        cell: (row) => (
          <Fragment>
           
            <span className='text-wrap '>{row.type}</span>
          </Fragment>
        ),
      },
      {
        name: "Created",
        selector: "created",
        minWidth: "170px",
        sortable: true,
        cell: (row) => (
          <Fragment>
           
            <span className='text-wrap '>{ moment(row.created).format('YYYY-MM-DD-HH:mm:ss') }</span>
          </Fragment>
        ),
      },
  
      {
        name: "Action",
        maxWidth: "50px",
        selector: "",
        sortable: true,
        cell: (row) => (
          <Fragment>
          <div className="d-flex " style={{padding:'1em'}}>
            <Eye size={15} className="cursor-pointer"  onClick={e=>{  setViewPopup(row)} } />
           
            {/* {rowId==row.merchant_id&&Delete?<Spinner size="sm" />:<Trash size={15} className=" cursor-pointer" onClick={e=>{setRowId(row.merchant_id);setDelete(true)}} />} */}
          </div>
          </Fragment>
        ),
      },

    ]),[notifications])


 const dataToRender = () => { 
     
     let tempData = notifications || [];
   
     if (query)
       tempData = tempData.filter((el) => {
         let temp = JSON.parse(JSON.stringify(el));
         let sq = query.trim().toLowerCase();
         
         let searchFields = ["title","message","type"];
         for (let field of searchFields)
           if (`${temp[field]}`.includes(sq) || `${temp[field]}`.startsWith(sq)) return true;
         return false;
       });
 
     return tempData;
 }
 
 
 
    return (
        <div className="invoice-list-wrapper">
          <ViewModal viewPopup={viewPopup}   partners={partners} setViewPopup={setViewPopup} merchants={merchants} />
        <Card>
            <div className="invoice-list-dataTable">
              <DataTable
                className="react-dataTable"
                noHeader
                pagination
                columns={columns}
                responsive={true}
                sortIcon={<ChevronDown />}
                data={dataToRender()}
              />
            </div>
          </Card>
        </div>
    )
}

export default Table
