import React, { useState, useEffect ,useRef} from "react";
import Styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { selectThemeColors, userDetails } from "../../utility/Utils";
import Select from 'react-select';
import {
  Modal,
  ModalBody,
  ModalHeader,
  Label,
  ModalFooter,
  Spinner,
  Button,
  FormGroup,
  Col,
} from "reactstrap";
import { AvForm, AvField, AvInput } from "availity-reactstrap-validation-safe";
import { X } from "react-feather";
import { AxiosError } from "axios";
import {
  GET_PARTNER,
  UPDATE_MERCHANT,
} from "../../redux/actions/storeMerchant.";
import { Axios } from "../../Api";
import { graphqlServerUrl } from "../../App";
import notification from "../../components/notification";

export default function Notification({ message, setMessage ,refetch}) {
  const CloseBtn = (
    <X
      className="cursor-pointer"
      size={15}
      onClick={(e) => setMessage(false)}
    />
  );
  const [user, setUser] = useState({});
  const [loader, setLoader] = useState(false);
  const [type, setType] = useState("all");
  const [Id,setId] = useState([]);
  const dispatch = useDispatch();
  const multiRef = useRef();
  const { merchants, partners } = useSelector((state) => state.merchant);
  const onSubmit = (e, error, value) => {
    e.preventDefault();
    if (error.length) return;
    value.sender_id = user.id;
    value.sender_name = user.name;
    // setId(Id.map(e=>e.value));
    setLoader(true);
    Axios.post(`${graphqlServerUrl}/admin/sendNotification`, {
      notification: {...value,Id},
    })
      .then((res) => {
        if (res.status == 200 && res.data.success) {
          setLoader(false);
          setMessage(false);
          notification({ title: "notification send", type: "success" });
          refetch();
        }
      })
      .catch((e: AxiosError) => {
        notification({
          title: "Error",
          message: e.response ? e.response.data : e.toString(),
          type: "error",
        });
        setLoader(false);
      });
  };
  const [merchant, setMerchant] = useState([]);
  const [partner, setPartner] = useState([]);
  useEffect(() => {
    if (!merchants.length) dispatch(UPDATE_MERCHANT());
    if (!partners.length) dispatch(GET_PARTNER());
    if (merchants)
      setMerchant([
        merchant,
        ...merchants.map((e) => (
         ({  value:e.merchant_id, label:e.comp_name })
        )),
      ]);
    if (partners)
      setPartner([
        partner,
        ...partners.map((e) =>  ({  value:e.partner_id, label:e.comp_name })),
      ]);
    if (typeof userDetails == "object" && Object.keys(userDetails).length)
      setUser(userDetails);
  }, [merchants, partners, userDetails]);
  useEffect(()=>setId([]),[type]);
  return (
    <ModalNew
      isOpen={message}
      toggle={(e) => setMessage(false)}
      //   className='middle-lg'
      //   modalClassName='modal-slide-in'
      contentClassName="pt-0"
      className="modal-dialog-top"
    >
      <ModalHeader
        className=""
        toggle={(e) => setMessage(false)}
        close={CloseBtn}
        tag="div"
      >
        Send Notification
      </ModalHeader>
      <AvForm onSubmit={onSubmit}>
        <ModalBody>
          <Col>
            <FormGroup>
              <Label>Reciver Type</Label>
              <AvField
                type="select"
                required
                name="type"
                onChange={(e) => setType(e.target.value)}
                defaultValue={"all"}
              >
                <option value={"merchant"} label="merchant" />
                <option value={"partner"} label="partner" />
                <option value={"all"} label="All" />
              </AvField>
            </FormGroup>
            {type != "all" ? (
            <>
            <Label>{type.toUpperCase()}</Label>
             <Select
             isClearable={true}
             label={type.toUpperCase()}
             ref={multiRef}
             theme={selectThemeColors}
             isMulti
             name='colors'
             
             menuPosition="relative"
             options={type=='merchant'?merchant:partner}
             onChange={e=>{//console.log(e); 
              if(e){setId([...e.map(e=>e.value)])}else{setId([])}  }}
             className='react-select'
             classNamePrefix='select'
           />
           </>
            ) : null}
            <FormGroup>
              <AvField name="title" required label="Title" type="text" />
            </FormGroup>
            <FormGroup>
              <AvField
                label="Description"
                name="message"
                required
                type="textarea"
              />
            </FormGroup>
          </Col>
        </ModalBody>
        <ModalFooter>
          <div className="d-flex justify-content-center">
            <Button.Ripple className="mr-2" type="submit" color={"primary"}>
              {loader ? <Spinner size="sm" className="mr-1" /> : null}{" "}
              <span>Send</span>
            </Button.Ripple>
            <Button.Ripple color={"danger"} onClick={(e) => setMessage(false)}>
              Close
            </Button.Ripple>
          </div>
        </ModalFooter>
      </AvForm>
    </ModalNew>
  );
}

const ModalNew = Styled(Modal)`
z-index:9999999999;
`;
