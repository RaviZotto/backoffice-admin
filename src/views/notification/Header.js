import React, { useState } from 'react';
import {Card,Button} from 'reactstrap';
import "@styles/react/libs/flatpickr/flatpickr.scss";
import Flatpickr from "react-flatpickr";
import { AlertCircle, Search } from 'react-feather';
import moment from 'moment'
import { useDispatch, useSelector } from 'react-redux';

function Header({dates,setDates,setQuery,setMessage}) {
  const [state,setState]=useState({...dates});
  const dispatch = useDispatch();

    return (
        <Card className='p-1'>
        <div className="row row-flex-wrap pr-sm-0 justify-content-end">
      <div className="col-md-2 col-6  ">
        <Flatpickr
          className="form-control"
           value={dates.start_date}
          onChange={(e) =>
            setState({
              ...state,
              start_date: moment(e[0]).format("YYYY-MM-DD"),
            })
          }
        />
      </div>
      <div className="  col-md-2 col-6   ">
        <Flatpickr
          className="form-control "
           value={dates.end_date}
          onChange={(e) =>
            setState({ ...state, end_date: moment(e[0]).format("YYYY-MM-DD") })
          }
        />
      </div>
      <div className=" col-12 col-md-1 ">
        <Button
          className="w-100 mb-2 mt-1 mb-md-0 mt-md-0  "
          color="primary"
          disabled={state.start_date==dates.start_date&&state.end_date==dates.end_date}
           onClick={() => {setDates({ ...state })
            
          } }
        >
          <Search size={17} />
        </Button>
      </div>

     <div className="col-12 col-md-4 mx-auto">
     <input 
      className="form-control"
      id='query'
      type='text'
      placeholder='search ...'
      onChange={e=>setQuery(e.target.value)}
     />
     </div>
     <div className=' col-12 col-md-2 pl-0  text-nowrap mr-md-1'>
       <Button 
         className="w-100 mb-2 mt-1 mb-md-0 mt-md-0  "
       color='primary'onClick={e=>setMessage(true)} >
        <AlertCircle size={13} /> send Message     
      </Button>  
     </div>
     
    </div>
        </Card>
    )
}

export default Header
