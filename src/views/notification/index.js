import moment from 'moment';
import React,{useState, useEffect} from 'react';
import CustomHeader  from './Header';
import Table from './Table';
import Notification from './Notifications'
import { useDispatch, useSelector } from 'react-redux';
import { GET_NOTIFICATION, GET_PARTNER, UPDATE_MERCHANT } from '../../redux/actions/storeMerchant.';
function index() {
  const [dates,setDates]=useState({
      start_date:moment(new Date).add('day',-7).format('YYYY-MM-DD'),
      end_date:moment(new Date).format('YYYY-MM-DD')
  })
  const dispatch = useDispatch()
  const [messageBox,setMessageBox]=useState([]);
  const [message,setMessage]=useState(false);
  const [query,setQuery]=useState("");
  const [refetch, setRefetch]=useState(false);
  const {merchants,partners} = useSelector((state) => state.merchant);
  useEffect(() => {
      if(!merchants.length)dispatch(UPDATE_MERCHANT());
      if(!partners.length)dispatch(GET_PARTNER())
      dispatch(GET_NOTIFICATION({...dates},()=>setRefetch(false)))
  },[dates,refetch])
    return (
        <div>
            <Notification message={message}   refetch={()=>setRefetch(true)} setMessage={setMessage}/>
            <CustomHeader setMessage={setMessage}  setQuery={setQuery}  dates={dates} setDates={setDates} />
            <Table  query={query} />
        </div>
    )
}

export default index
