import React,{ useState, Fragment, useEffect, createContext, useMemo } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, FormGroup, Label } from "reactstrap";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import Select, { components } from "react-select";
import { Plus } from "react-feather";
import AddClientSidebar from "./AddClientSidebar";
import Flatpickr from "react-flatpickr";
import { selectThemeColors } from "@utils";
import moment from "moment";
import Spinner from "reactstrap/lib/Spinner";
import {useDispatch, useSelector} from 'react-redux';
import { ADD_SUBS } from "../../redux/actions/transfer";
import notification from "../../components/notification";

const formSchema = Yup.object().shape({
  amount: Yup.number().required("Required"),
});

const billingPeriodOptions = [
  { label: "Daily", value: "daily" },
  { label: "Weekly", value: "weekly" },
  { label: "Monthly", value: "monthly" },
  { label: "Yearly", value: "yearly" },
];

export const AddSubscriptionContext = createContext(null);
const AddSubscription = ({ dates, open, setOpen }) => {
  const{clients}=useSelector(state=>state.transfer)
  const dispatch = useDispatch()
  const{merchants} =useSelector(state=>state.merchant);
  const {currencies}=useSelector(state=>state.transfer);
  const [loading,setLoading] = useState(false);
  const [currencyOption,setCurrencyOption]=useState([]);
  const[merchantId,setMerchantId] = useState(null);
  const [clientsOptions, setClientsOptions] = useState([
    {
      value: "add-new",
      label: "Add New Client",
      type: "button",
      color: "flat-success",
    },
  ]);

  const [clientSidebarOpen, setClientSidebarOpen] = useState(false);
  const [subscriptionData, setSubscriptionData] = useState({
    addedClient: false,
    client_id: "",
    client_email: "",
    merchant_id:"",
    currency: "EUR",
    billing_end_date: moment().format("YYYY-MM-DD"),
    billing_period: "daily",
  });
 
  useEffect(()=>{
      
     
     setCurrencyOption(currencies.map(el=>({label:el.currency,value:el.currency})));
   

  },[])


  useEffect(() => {
    // if (clientsData) {
    //   !subscriptionData.addedClient
    //     ? setSubscriptionData({
    //         ...subscriptionData,
    //         client_id: clientsData.clients[0].id,
    //         client_email: clientsData.clients[0].client_email,
    //       })
    //     : setSubscriptionData({ ...subscriptionData, addedClient: false });
    //   setClientsOptions([
    //     ...clientsOptions,
    //     ...clientsData.clients.map((el) => ({ label: el.client_name, value: el.id, client_email: el.client_email })),
    //   ]);
    })


  const ProviderValue = useMemo(
    () => ({
      clientSidebarOpen,
      setClientSidebarOpen,
      subscriptionData,
      setSubscriptionData,
      merchantId,
      setMerchantId,
      merchants
    }),
    [clientSidebarOpen, subscriptionData,merchantId,merchants]
  );

  const handleFormSubmit = (fieldData) => {
    //console.log(subscriptionData);
       const merchantObj = merchants.find(el=>el.id==merchantId);
     
       if(!merchantObj.timezone)return notification({type: 'error', message:"Plz set your timezone ",title:"Error"})   
       setLoading(true);
        let subscriptionObj={...subscriptionData};
        delete subscriptionObj.addedClient;
        subscriptionObj.amount = fieldData.amount;
        dispatch(ADD_SUBS({...subscriptionObj},({err,res})=>{
          setLoading(false)
          if(err) {notification({title:'Error',message:err.message?err.message:err.toString(),type:'error'}); return;}
          else setOpen(false);
          // setOpen(false);
         
         
        })) ;
         
     };

  const handleChange = (newValue) => setSubscriptionData({ ...subscriptionData, ...newValue });

  const clientsOptionComponent = ({ data, ...props }) => {
    if (data.type === "button") {
      return (
        <Button className="text-left rounded-0" color={data.color} block onClick={() => setClientSidebarOpen(true)}>
          <Plus size={14} /> <span className="align-middle ml-50">{data.label}</span>
        </Button>
      );
    } else {
      return <components.Option {...props}> {data.label} </components.Option>;
    }
  };

 

  return (
    <AddSubscriptionContext.Provider value={ProviderValue}>
      <Modal isOpen={open} toggle={() => setOpen(!open)} className="modal-dialog-centered">
        <ModalHeader toggle={() => setOpen(!open)}>Create Payment Link</ModalHeader>
        <Formik
          initialValues={{
            amount: "",
          }}
          validationSchema={formSchema}
          onSubmit={handleFormSubmit}
        >
          {({ errors, touched, values, handleSubmit }) => (
            <Fragment>
              <ModalBody>
                <Form>
                <FormGroup>
                    <Label for="client">Merchant</Label>
                    <Select
                      className="react-select"
                      classNamePrefix="select"
                      id="merchants"
                      name="merchants"
                      onChange={(e) =>{handleChange({merchant_id:e.value});setMerchantId(e.value)  }}
                      options={merchants.map(el=>({label:el.comp_name,value:el.merchant_id}))}
                      theme={selectThemeColors}
                    
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="client">Client</Label>
                    <Select
                      className="react-select"
                      classNamePrefix="select"
                      id="clients"
                      name="cliens"
                      value={clients.find((el) => subscriptionData.client_id == el.value)}
                      onChange={(e) => handleChange({ client_id: e.value, client_email: e.client_email })}
                      options={[...clientsOptions,  ...clients.filter(el=>merchantId?merchantId==el.merchant_id:true).map(el=>({label:el.client_name,value:el.client_id,client_email:el.client_email}))]}
                      theme={selectThemeColors}
                      components={{ Option: clientsOptionComponent }}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="billing_period" className="form-label">
                      Billing Period
                    </Label>
                    <Select
                      theme={selectThemeColors}
                      className="react-select"
                      classNamePrefix="select"
                      defaultValue={billingPeriodOptions[0]}
                      options={billingPeriodOptions}
                      isClearable={false}
                      onChange={(obj) => handleChange({ billing_period: obj.value })}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label>Currency</Label>
                    <Select
                      className="react-select"
                      options={currencyOption}
                      defaultValue={currencyOption.find(el => el.value == subscriptionData.currency)}
                      onChange={(e) => handleChange({ currency: e.value })}
                      classNamePrefix="select"
                      id="label"
                      theme={selectThemeColors}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="amount" className="form-label">
                      Amount
                    </Label>
                    <Field
                      name="amount"
                      id="amount"
                      className={`form-control ${errors.amount && touched.amount && "is-invalid"}`}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="email" className="form-label">
                      Billing End Date
                    </Label>
                    <Flatpickr
                      options={{
                        dateFormat: "Y-m-d",
                        altInput: true,
                        altFormat: "F j, Y",
                      }}
                      placeholder="Set Billing End Date"
                      value={subscriptionData.billing_end_date || null}
                      onChange={(e) => handleChange({ billing_end_date: moment(e[0]).format("YYYY-MM-DD") })}
                      className="form-control"
                      size={9}
                    />
                  </FormGroup>
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" disabled={loading} type="submit" onClick={handleSubmit}>
                  {loading ? <Spinner size="sm" className="mr-1" /> : null}
                  {loading? "Creating" : "Create"}
                </Button>
                <Button color="secondary" outline onClick={() => setOpen(false)}>
                  Cancel
                </Button>
              </ModalFooter>
            </Fragment>
          )}
        </Formik>
      </Modal>
      <AddClientSidebar />
    </AddSubscriptionContext.Provider>
  );
};

export default AddSubscription;
