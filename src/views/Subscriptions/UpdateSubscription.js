import { useState, Fragment, useEffect} from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, FormGroup, Label,InputGroup, InputGroupAddon} from "reactstrap";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import Select, { components } from "react-select";

import { selectThemeColors } from "@utils";

import {useReactiveVar } from "@apollo/client";
import Spinner from "reactstrap/lib/Spinner";
import produce from "immer";
import notification from "../../components/notification";
import { SelectedSubscription } from ".";
import { Axios } from "../../Api";
import { useDispatch } from "react-redux";
// import { SelectedSubscription } from ".";

const formSchema = Yup.object().shape({
  amount: Yup.string()
    .test((el) => Number(el) > 0)
    .required("Required"),
    billing_period:Yup.string()
});

const billingPeriodOptions = [
  { label: "Daily", value: "daily" },
  { label: "Weekly", value: "weekly" },
  { label: "Monthly", value: "monthly" },
  { label: "Yearly", value: "yearly" },
];


const UpdateSubscription = ({ open, setOpen }) => {
  const subscription = useReactiveVar(SelectedSubscription)

const dispatch = useDispatch()
const [updatingSubscription,setUpdatingSubscription] = useState(false);

  
  const [subscriptionData, setSubscriptionData] = useState({
  
   
    currency: "EUR",
    amount:"",
    billing_period: "daily",
  });

  useEffect(() => {
    if(subscription.id){
        setSubscriptionData({billing_period:subscription.billing_period,amount:subscription.amount,currency:subscription.currency});
    }


  }, [subscription]);


  const handleFormSubmit = async(fieldData) => {
    fieldData.amount = Number(fieldData.amount);
    delete fieldData.currency;
    
    for(let [key,val] of Object.entries(fieldData)) if(fieldData[key]==subscriptionData[key]) delete fieldData[key];
    let submitData = {
      ...fieldData,
        id:subscription.id
    };
     try{ 
       setUpdatingSubscription(true);
       await Axios.put("/admin/updateSubscription",submitData);
       dispatch({type:"UPDATE_SUBS",payload:submitData});
       setUpdatingSubscription(false);
       setOpen(false);
     }catch(e){
      setUpdatingSubscription(false);
        notification({title:"Update Error",message:e.toString(),type:"error"})

     }

    // let mutationVar = {
    //   variables: {  ...submitData },
    //   update: (cache, { data }) => {
    //     if (data.updateSubscription) {
    //       const oldSubscriptionData = cache.readQuery({ query: GET_SUBSCRIPTIONS, variables: dates });
    //       cache.writeQuery({
    //         query: GET_SUBSCRIPTIONS,
    //         variables: dates,
    //         data: produce(oldSubscriptionData, (x) => {
                
    //          const id =  x.subscriptions.findIndex(e=>e.id==subscription.id);
            
    //          x.subscriptions[id]={...x.subscriptions[id],...submitData}
    //         }),
    //       });
    //       setOpen(false);
    //     }
    //   },
    // };
    //  updateSubscription(mutationVar).catch((err) =>{console.log(err); notification({ code: err.message })});
  };



 

  return (

      <Modal isOpen={open} toggle={() => setOpen(!open)} className="modal-dialog-centered">
        <ModalHeader toggle={() => setOpen(!open)}>Update Payment Link</ModalHeader>
        <Formik
          initialValues={subscriptionData}
          validationSchema={formSchema}
          onSubmit={handleFormSubmit}
        >
          {({ errors, touched, values, handleSubmit }) => (
            <Fragment>
              <ModalBody>
                <Form>
     
                  <FormGroup>
                    <Label for="billing_period" className="form-label">
                      Billing Period
                    </Label>
                    <Select
                      theme={selectThemeColors}
                      className="react-select "
                      classNamePrefix="select"
                      defaultValue={billingPeriodOptions.find(e=>e.value==values.billing_period)}
                      options={billingPeriodOptions}
                      isClearable={false}
                      onChange={(obj) => 
                        values.billing_period=obj.value
                    }
                    />
                  </FormGroup>
           
                  
                  <FormGroup>
                      <Label for="amount" className="form-label">
                        Amount
                      </Label>
                      <InputGroup>
                        <Field
                          name="amount"
                          id="amount"
                          placeholder="499.99"
                          type="number"
                          className={`form-control ${errors.amount && touched.amount && "is-invalid"}`}
                        />
                        <InputGroupAddon addonType="append">
                          <Button
                            color={`${(errors.amount && touched.amount && "danger") || "primary"}`}
                            outline
                            onClick={(e) => e.preventDefault()}
                          >
                            { subscription?.currency}
                          </Button>
                        </InputGroupAddon>
                      </InputGroup>
                  </FormGroup>
                
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" disabled={updatingSubscription} type="submit" onClick={handleSubmit}>
                  {updatingSubscription ? <Spinner size="sm" className="mr-1" /> : null}
                  {updatingSubscription ? "updating" : "update"}
                </Button>
                <Button color="secondary" outline onClick={() => setOpen(false)}>
                  Cancel
                </Button>
              </ModalFooter>
            </Fragment>
          )}
        </Formik>
      </Modal>
 
  );
};

export default UpdateSubscription;
