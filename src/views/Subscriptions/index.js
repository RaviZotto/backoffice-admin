import React,{ useState, Fragment, createContext, useMemo,useEffect } from "react";
import { Button } from "reactstrap";
import { PlusCircle } from "react-feather";
import Loading from "@core/components/spinner/Loading-spinner";
import AddSubscription from "./AddSubscription";
import SubscriptionsTable from "./Table";
import moment from "moment";
import {GET_SUBSCRIPTION,GET_CLIENTS, GET_CURRENCY} from '../../redux/actions/transfer'
import {UPDATE_MERCHANT} from '../../redux/actions/storeMerchant.'
import {useDispatch,useSelector} from 'react-redux';
import { userDetails } from "../../utility/Utils";
import UpdateSubscription from "./UpdateSubscription";
import { makeVar } from "@apollo/client";
export const SelectedSubscription = makeVar({});
const PayLinks = () => {
  const {role_type,subscription}=userDetails||{};
  const dispatch = useDispatch()
  const [dates, setDates] = useState({
    startDate: moment().subtract(60, "days").format("YYYY-MM-DD"),
    endDate: moment().format("YYYY-MM-DD"),
  });
  const [openAddSubscription, setOpenAddSubscription] = useState(false);
  const [openUpdateSubscription, setOpenUpdateSubscription] = useState(false);
  const [loading,setLoading] = useState(true);

  useEffect(()=>{
   if(loading){
    dispatch(GET_SUBSCRIPTION({start_date:dates.startDate,end_date:dates.endDate},()=>{
       dispatch(GET_CURRENCY());
       dispatch(UPDATE_MERCHANT(
      dispatch(GET_CLIENTS(()=>setLoading(false))) 
     )) })) }})

  if (loading) return <Loading />;
  return (
    <Fragment>
      <Button color="primary" disabled={role_type&&role_type!='admin' && !userDetails['subscription'].includes('C')}  className="mb-1" onClick={() => setOpenAddSubscription(true)}>
        <PlusCircle size={17} className="mr-1" />
        Create Subscription
      </Button>
      <SubscriptionsTable
        dates={dates}
        setDates={setDates}
        setUpdateOpen={setOpenUpdateSubscription}
      />
      <UpdateSubscription open={openUpdateSubscription} setOpen = {setOpenUpdateSubscription} />
      <AddSubscription dates={dates} open={openAddSubscription} setOpen={setOpenAddSubscription} />
    </Fragment>
  );
};

export default PayLinks;
