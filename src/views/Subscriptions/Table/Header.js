import { Row, Col, Button, Input, Label } from "reactstrap";
import Flatpickr from "react-flatpickr";
import { Search } from "react-feather";
import { LinksTableContext } from ".";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import React,{ useContext, useState } from "react";
import moment from "moment";
import { useSelector } from "react-redux";

const CustomHeader = () => {
  const { dates, setDates, searchQuery, setSearchQuery, statusQuery,setMerchantId, setStatusQuery } = useContext(LinksTableContext);
  const {merchants}=useSelector((state) => state.merchant)
  const [state, setState] = useState({ ...dates });

  return (
    <div className="invoice-list-table-header w-100 mr-1 mt-2 mb-75">
     <div className='mb-1'>
       
       <Input
       className="form-control"
       type='select'
       onChange={el=>setMerchantId(el.target.value)}
       >
         <option value={null} label="select Merchant" />
        {merchants.map(e=>
          <option value={e.merchant_id} label={e.comp_name} />
          )}
       </Input>
     </div>
      <div className="d-flex flex-lg-row flex-column w-100 justify-content-between align-items-start" sm="12">
        
        <div className="d-flex flex-sm-row flex-column w-100 mb-1 mb-lg-0">
          <Flatpickr
            className="form-control mb-1 mb-sm-0"
            value={dates.startDate}
            onChange={(e) => setState({ ...state, startDate: moment(e[0]).format("YYYY-MM-DD") })}
            style={{ minWidth: "150px" }}
          />
          <Flatpickr
            className="form-control mx-0 mx-sm-1 mb-1 mb-sm-0"
            value={new Date()}
            value={dates.endDate}
            onChange={(e) => setState({ ...state, endDate: moment(e[0]).format("YYYY-MM-DD") })}
            style={{ minWidth: "150px" }}
          />
          <Button color="primary" className="w-100" onClick={() => setDates({ ...state })}>
            <Search size={17} />
          </Button>
        </div>
        <div className="w-100"></div>
        <div className="w-100"></div>
        <div className="d-flex flex-sm-row w-100 flex-column">
          <Input
            style={{ minWidth: "250px" }}
            id="search-invoice"
            className="mr-2 w-100 mb-1 mb-sm-0"
            type="text"
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
            placeholder="Search"
          />
          <Input
            className="w-auto pr-4"
            type="select"
            value={statusQuery}
            onChange={(e) => setStatusQuery(`${e.target.value}`)}
          >
            <option value="">Select Status</option>
            <option value="1">Enabled</option>
            <option value="0">Disabled</option>
            <option value="2">Not Initiated</option>
          </Input>
        </div>
      </div>
    </div>
  );
};

export default CustomHeader;
