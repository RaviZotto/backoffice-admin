import React,{ Fragment, useState, createContext, useMemo,useEffect } from "react";
import CustomHeader from "./Header";
import Avatar from "@components/avatar";
import {useDispatch,useSelector} from 'react-redux'
import {GET_SUBSCRIPTION,GET_CLIENTS} from '../../../redux/actions/transfer'
import Loading from "@core/components/spinner/Loading-spinner";
// ** Third Party Components
import { ChevronDown, Edit } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge } from "reactstrap";
import moment from "moment";

// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import { SelectedSubscription } from "..";

export const LinksTableContext = createContext();

const renderClient = (row, i) => {
  const stateNum = i % 6,
    states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
    color = states[stateNum];

  return <Avatar color={color || "primary"} className="mr-1" content={row.client_name || "John Doe"} initials />;
};

const Table = ({  dates, setDates,setUpdateOpen }) => {
  const [merchantId,setMerchantId] =useState(null);
  const dispatch = useDispatch();
  const [searchQuery, setSearchQuery] = useState("");
  const [statusQuery, setStatusQuery] = useState("");
  const {subscriptions}=useSelector(state=>state.transfer);
  const[loading,setLoading]=useState(false);
   
  useEffect(() =>{
   console.log(subscriptions.find(e=>e.id==SelectedSubscription().id));
  },[subscriptions])



  useEffect(()=> {
      
      
       dispatch(GET_SUBSCRIPTION({start_date:dates.startDate,end_date:dates.endDate},()=>setLoading(false)));
    
  },[dates])
  const columns = [
    {
      name: "Client",
      selector: "client_name",
      sortable: true,
      minWidth: "180px",
      cell: (row, i) => {
        return (
          <div
            className="d-flex justify-content-left align-items-center cursor-pointer"
            onClick={() => {
              // setSelectedUser(row);
              // setUserForm(true);
            }}
          >
            {renderClient(row, i)}
            <div className="d-flex flex-column">
              <span className="font-weight-bold">{row.client_name}</span>
              <small className="text-truncate text-muted mb-0">{row.client_email}</small>
            </div>
          </div>
        );
      },
    },
    {
      name: "Amount",
      selector: "amount",
      sortable: false,
      cell: (row) => (
        <h6
          className={`${
            row.status == "1" ? "text-success" : row.status == "2" ? "text-primary" : "text-danger"
          } font-weight-bolderer`}
        >
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: row.currency,
          })
            .format(row.amount)
            .replace(/^(\D+)/, "$1 ")}
        </h6>
      ),
    },
    {
      name: "Start Date",
      selector: "billing_start_date",
      sortable: true,
      minWidth: "130px",
      cell: (row) => {
        if (!row.billing_start_date) return "TBD";
        let date = moment(row.billing_start_date);
        return (
          <Fragment>
            <div className="mt-1">
              <h6 className="font-small-3">{date.format("YYYY-MM-DD")}</h6>
              <h6 className="text-secondary font-small-2">{date.format("HH:mm:ss")}</h6>
            </div>
          </Fragment>
        );
      },
    },
    {
      name: "End Date",
      selector: "email",
      sortable: true,
      minWidth: "130px",
      cell: (row) => {
        if (!row.billing_start_date) return "TBD";
        let date = moment(row.billing_end_date);
        return (
          <Fragment>
            <div className="mt-1">
              <h6 className="font-small-3">{date.format("YYYY-MM-DD")}</h6>
              <h6 className="text-secondary font-small-2">{date.format("HH:mm:ss")}</h6>
            </div>
          </Fragment>
        );
      },
    },
    {
      name: "Status",
      selector: "status",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
          <div
            className={row.status == 0 ? "bg-danger" : row.status == 1 ? "bg-success" : "bg-primary"}
            style={{
              height: "10px",
              width: "10px",
              borderRadius: "50%",
              display: "inline-block",
              marginRight: "5px",
            }}
          />
          <span>{row.status == 0 ? "Disabled" : row.status == 1 ? "Enabled" : "Not Initiated"}</span>
        </Fragment>
      ),
    },
    { 
      name:"Action",
      selector: "action",
      minWidth:"50px",
      sortable: true,
      cell: (row) => (
        <Edit className="ml-1 cursor-pointer"  onClick={e=>{SelectedSubscription(row);setUpdateOpen(true)}} size={15} />
      )
    }
  ];
//  if(loading)return <Loading/>
  // Data filtering function according to filters present in table header.
  const dataToRender = () => {
    let tempData = subscriptions || [];
    if (searchQuery)
      tempData = tempData.filter((el) => {
        let temp = JSON.parse(JSON.stringify(el));
        let sq = searchQuery.toLowerCase();
        temp.my_status = el.status == "1" ? "dnabled" : el.status == "0" ? "disabled" : "not initiated";
        let searchFields = [
          "my_status",
          "client_name",
          "client_email",
          "billing_start_date",
          "billing_end_date",
          "currency",
          "amount",
        ];
        for (let field of searchFields)
          if (`${temp[field]}`.includes(sq) || `${temp[field]}`.startsWith(sq)) return true;
        return false;
      });
    if (statusQuery) tempData = tempData.filter((el) => el.status == statusQuery);
    if(merchantId)tempData = tempData.filter((el) =>el.merchant_id==merchantId)
    return tempData;
  };

  const providerValue = useMemo(
    () => ({
      searchQuery,
      setSearchQuery,
      statusQuery,
      setStatusQuery,
      dates,
      setDates,
      merchantId,
      setMerchantId,
      subscriptions
    }),
    [dates, searchQuery, statusQuery,subscriptions]
  );

  return (
    <LinksTableContext.Provider value={providerValue}>
      <div className="invoice-list-wrapper">
        <Card>
          <div className="px-2 pb-1">
            <CustomHeader loading={loading}  />
          </div>
        { !loading? <div className="invoice-list-dataTable">
            <DataTable
              className="react-dataTable"
              noHeader
              pagination
              columns={columns}
              responsive={true}
              sortIcon={<ChevronDown />}
              data={dataToRender()}
            />
          </div>:<Loading/>}
        </Card>
      </div>
    </LinksTableContext.Provider>
  );
};

export default Table;
