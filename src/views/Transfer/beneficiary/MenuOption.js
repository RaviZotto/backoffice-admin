import React from 'react';
import { MoreVertical } from 'react-feather';
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from 'reactstrap';
import {Axios} from '@src/Api';
import notification from "@src/components/notification";


const MenuOption = ({data,className,style,userDetails,setData,dispatch}) => {
                               
    const deleteBeneficiaryById=async(id)=>{ 
        try{ 
            await Axios.delete(`/admin/deleteBeneficiary?id=${id}`);
            dispatch({type:'DEL_BENEF',payload:id});
            }catch(e){ 
          notification({ title: "Error", message: e.toString(),type: "error"});
        }
      }



    return (
        <UncontrolledDropdown className="chart-dropdown  " >
        <DropdownToggle
          color=""
          className="bg-transparent btn-sm border-0 p-50"
        >
          <MoreVertical size={18} className={`cursor-pointer ${className} `} style={style} />
        </DropdownToggle>
        <DropdownMenu top  className="position-absolute  mr-0  "   >
          <DropdownItem
            className="w-100 d-flex text-center"
             onClick={(e) =>{
            
            setData(data)
           
            }}
          >
            View
          </DropdownItem>
          <DropdownItem
            className="w-100 d-flex text-center"
            disabled={!( !userDetails?.role_type ||userDetails?.role_type=='admin'|| userDetails.role_type&&userDetails['transfer'].includes('D'))}
             onClick={(e) =>{
                
              deleteBeneficiaryById(data.id)
           
            }}
          >
            Delete
          </DropdownItem>
        
         
        </DropdownMenu>
      </UncontrolledDropdown>
    )
}

export default MenuOption
