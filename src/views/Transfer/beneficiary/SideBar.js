// ** Third Party Components
import React from 'react'
import { useContext, useState } from "react";
import { Table, Button, Col } from "reactstrap";
import Sidebar from "@components/sidebar";
import moment from "moment";
// import "@styles/base/pages/app-invoice.scss";

const TransactionSidebar = ({setSelectedBeneficiary,selectedBeneficiary}) => {

  
  const closeSidebar = () => setSelectedBeneficiary({});

  console.log(selectedBeneficiary,'343');
  return (
    <Sidebar
      size="lg"
      open={selectedBeneficiary.id ? true : false}
      title="Transaction Details"
      headerClassName=""
      contentClassName="p-0"
      toggleSidebar={closeSidebar}
    >
      <Table style={{ tableLayout: "fixed" }}  className='mb-0' borderless={true} striped={true}>
        {selectedBeneficiary.id ? (
          <tbody>
                <tr>
              <td>Created</td>
              <td>
                {moment(selectedBeneficiary.created).format("YYYY-MM-DD")}
              </td>
            </tr>
            <tr>
              <td>Merchant Id</td>
              <td>
                {selectedBeneficiary.merchant_id}
              </td>
            </tr>
            <tr>
              <td>Beneficiary Id</td>
              <td>
                {selectedBeneficiary.beneficiary_id}
              </td>
            </tr>
            <tr>
              <td> Name</td>
              <td>
                {selectedBeneficiary.benef_name}
              </td>
            </tr>
            <tr>
              <td > Email</td>
              <td className="text-break">
                {selectedBeneficiary.bef_email}
              </td>
            </tr>
            <tr>
              <td>Phone No:</td>
              <td>
                {selectedBeneficiary.benef_phone}
                </td>
            </tr>
            <tr>
              <td>Bank Address</td>
              <td className="text-break">
                {selectedBeneficiary.bank_address}
                </td>
            </tr>
            <tr>
              <td>City</td>
              <td>
                {selectedBeneficiary.city}
                </td>
            </tr>
            <tr>
              <td>Country</td>
              <td>
                {selectedBeneficiary.country}
                </td>
            </tr>
            <tr>
              <td>State</td>
              <td>
                {selectedBeneficiary.state}
                </td>
            </tr>
            <tr>
              <td>Iban No:</td>
              <td className="text-break">
                {selectedBeneficiary.iban_account_no}
                </td>
            </tr>
            <tr>
              <td>Account No</td>
              <td>
                {selectedBeneficiary.account_number}
                </td>
            </tr>
            <tr>
              <td>Bic Code</td>
              <td>
                {selectedBeneficiary.bic_code}
                </td>
            </tr>
            <tr>
              <td>Gateway</td>
              <td>
                {selectedBeneficiary.gateway}
                </td>
            </tr>
            </tbody>):null
        }
      </Table>
      
    </Sidebar>
  );
};

export default TransactionSidebar;