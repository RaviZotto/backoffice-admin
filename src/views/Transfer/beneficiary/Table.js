import React,{ useContext, Fragment, useEffect, useState } from "react";
import CustomHeader from "./Header";
import Avatar from "@components/avatar";
import Loading from "@core/components/spinner/Loading-spinner";
// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge } from "reactstrap";
import moment from "moment";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import {STORE_BENEFICIARIES,GET_COUNTRIES} from "../../../redux/actions/transfer";
import { useDispatch, useSelector } from "react-redux";
import MenuOption from "./MenuOption";
import { userDetails } from "../../../utility/Utils";

const Table = ({merchantId,setSelectedBeneficiary,search}) => {
  const dispatch =useDispatch()
  const [loading,setLoading]=useState(true);
  const {beneficiares}=useSelector(state=>state.transfer);

  useEffect(()=>{

   !beneficiares.length&&dispatch(STORE_BENEFICIARIES())
  
   if(beneficiares.length)setLoading(false);
     
  },[beneficiares])


  const renderClient = (row, i) => {
    const stateNum = i % 6,
      states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
      color = states[stateNum];
  
    return <Avatar color={color || "primary"} className="mr-1" content={row.benef_name || "John Doe"} initials />;
  };

  const columns = [
    {
      name: "Name",
      selector: "name",
      sortable: true,
      minWidth: "150px",
      
      cell: (row,i) => {
        // let date = moment(row.date_of_transaction);
        return (
          <Fragment>
             {renderClient(row, i)}
         <span className='text-wrap'>{row.benef_name}</span>
          </Fragment>
        );
      },
    },
    {
      name: "Type",
      selector: "benef_type",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.is_sepa?'SEPA':"SWIFT"}</span>
        </Fragment>
      ),
    },
    {
      name: "Email",
      selector: "bef_email",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.bef_email}</span>
        </Fragment>
      ),
    },
    {
      name: "IBAN/Ac no",
      selector: "iban_account_no",
      minWidth: "170px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span className='text-wrap '>{row.iban_account_no||row.account_number}</span>
        </Fragment>
      ),
    },
    {
      name: "BIC",
      selector: "bic_code",
      minWidth: "170px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span className='text-wrap '>{row.bic_code}</span>
        </Fragment>
      ),
    },{

      name: "Action",
      selector: "action",
      minWidth: "170px",
      sortable: true,
      cell: (row) => (
        <Fragment>
          <MenuOption setData={setSelectedBeneficiary} userDetails={userDetails} dispatch={dispatch} data={row}  />
         {/* <Eye className='cursor-pointer' size={15} onClick={e=>setSelectedBeneficiary(row)} /> */}
        </Fragment>
      ),
    }
   

  ];
  if(loading) return <Loading />
  const dataToRender = () => {
    let tempData=[]
    tempData=beneficiares;
    merchantId&&(tempData=tempData.filter(e=>e.merchant_id==merchantId))
    console.log(search)
    if(search){
      let sq = search?.trim().toLowerCase();
      let fields=['iban_account_no','account_number','bank_code','identifier','currency_code'];
       tempData =  tempData.filter(e=>{ 
         for(let field of fields)
            if(e[field]?.includes(sq))return true;
            return false;
       })
            
    }
    return tempData;
  };

  return (
    <div className="invoice-list-wrapper">
      {/* <CustomHeader history={history} setSelectedPartner={setSelectedPartner} setSearchQuery={setSearchQuery} /> */}
      <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>
    </div>
  );
};

export default Table;
