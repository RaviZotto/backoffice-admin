import React, { useContext, useState, useEffect, Fragment, useRef } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  Col,
  Label,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
import { selectThemeColors, transferCountries as transferCountriesOptions, countries as countryOptions } from "@utils";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import {ADD_BENEF, STORE_BENEFICIARIES, STORE_MERCHANTACCOUNTS} from '../../../redux/actions/transfer'
import Select from "react-select";
import Spinner from "reactstrap/lib/Spinner";
import {useDispatch,useSelector} from 'react-redux'
import { GET_PARTNER, UPDATE_MERCHANT } from "../../../redux/actions/storeMerchant.";
import { useHistory } from "react-router";
const formSchema = Yup.object().shape({
  first_name: Yup.string().min(3).required("Required"),
  last_name:Yup.string().min(3).required("Required"),
  benef_phone: Yup.number().required("Required"),
  bef_email: Yup.string().email("Invalid Email").required("Required"),
  benef_phone_code: Yup.string().required("Required"),
  country: Yup.string(),
  city: Yup.string().required("Required"),
  zipcode: Yup.string().required("Required"),
  benef_type: Yup.string().required("Required"),
  account_type: Yup.string().required("Required"),
  bank_country: Yup.string().required("Required"),
  iban_account_no: Yup.string().required("Required"),
  bic_code: Yup.string().required("Required"),
  account_number: Yup.string().required("Required"),
  bank_code: Yup.string().required("Required"),
  bank_code_type: Yup.string().required("Required"),
  currency_code: Yup.string().required("Required"),
});

const  AddBeneficiaryModal=({ openAddBeneficiaryModal, setOpenAddBeneficiaryModal,account_id})=> {



  const [merchantId,setMerchantId]=useState(null);
  const [partnerId,setPartnerId]=useState(null);
  const {partners,merchants}=useSelector(state=>state.merchant);
  const dispatch = useDispatch();
 
  const [inputData, setInputData] = useState({
    selectedBankCountry: {},
    benef_phone_code: "",
  });
  const[loader,setLoader]=useState(false);
  const loaderRef = useRef(false)
  useEffect(()=>{
    loaderRef.current=loader     
    !partners.length&&dispatch(GET_PARTNER())
    !merchants.length&&dispatch(UPDATE_MERCHANT())  
   
  })
  
  

  const beneficiaryTypeOptions = [
    { label: "Person", value: "person" },
    { label: "Company", value: "company" },
  ];
  const accountTypeOptions = [
    { label: "Savings", value: "savings" },
    { label: "Checkings", value: "checkings" },
  ];

  const handleSubmit = (fieldData) => {
    setLoader(true);
    let postData = {};
    console.log(fieldData);
    Object.keys(fieldData).forEach((key) => {
      if (fieldData[key] !== false ) postData[key] = fieldData[key];
    });
     if(postData.bank_country=='GBR')postData.bank_country = 'GB';
     postData.bic_code=postData.bic_code.toUpperCase();
     postData.benef_phone=fieldData.benef_phone_code+fieldData.benef_phone;
     delete postData.benef_phone_code;
   
     dispatch(ADD_BENEF(merchantId,postData,({err,res})=>{
    if(!err){setOpenAddBeneficiaryModal(false);dispatch(STORE_BENEFICIARIES())}

    setLoader(false);
    }
    
    ));
  
  }
  useEffect(()=>{loaderRef.current=loader},[loader])

  useEffect(() => {
    if (openAddBeneficiaryModal === true)
      setInputData({
        ...inputData,
        selectedBankCountry: {
          label: "United Kingdom (GBP)",
          country_code: "GB",
          currency_code: "GBP",
          supports_iban: false,
          bank_code_type:'sort-code',
          bank_code_field_name: "Sort Code"
        
        },
        benef_phone_code: "+44",
      });
  }, [openAddBeneficiaryModal]);

  return (
    <div className="vertically-centered-modal">
      <Modal
        isOpen={openAddBeneficiaryModal}
        toggle={() => setOpenAddBeneficiaryModal(!openAddBeneficiaryModal)}
        className="modal-dialog-centered modal-lg"
      >
        <ModalHeader toggle={() => setOpenAddBeneficiaryModal(!openAddBeneficiaryModal)}>Add Beneficiary</ModalHeader>
        <Formik
          initialValues={{
            first_name: "",
            last_name:"",
            benef_phone: "",
            bef_email: "",
            benef_phone_code: "+44",
            country: "GB",
            city: "",
            zipcode: "",
            benef_type: "person",
            account_type: "savings",
            bank_country: "GB",
            iban_account_no: "",
            bic_code: "",
        
            bank_code: "",
        
            currency_code: "GBP",
          }}
          onSubmit={handleSubmit}
          
          validationSchema={formSchema}
        >
          {({ errors, touched, values }) => (
            <Form>
              <ModalBody>
                
                  <Row>
                  <div className="h5 w-100 ml-1 font-weight-bolderer">Personal Details</div>
                    <Col md="4" sm="6">
                     <Label>Parnter Id</Label>
                     <Select
                         theme={selectThemeColors}
                         className="react-select"
                         classNamePrefix="select"
                         options={partners.map((el) => ({
                           label: el.comp_name,
                           value: el.partner_id,
                         }))}
                         onChange={el=>{setPartnerId(el.value);//console.log(el.value)
                        }}
                     />
                    </Col>
                    <Col md='4' sm='6'>
                     <Label>Merchant</Label>
                     <Select 
                         theme={selectThemeColors}
                         className="react-select"
                         classNamePrefix="select"
                         options={merchants.filter(e=>e.partner_id==partnerId).map(el=>({label:el.comp_name,value:el.merchant_id}))}
                         onChange={el=>setMerchantId(el.value)}
                      /> 
                     </Col>
                    {/* <Col md="4" sm="6">
                     <Label>Account</Label>
                      <Select 
                         theme={selectThemeColors}
                         className="react-select"
                         classNamePrefix="select"
                         
                         options={merchantsAccounts.filter(e=>e.merchant_id==merchantId&&e.gateway).map(el=>({label:`${el.account_name}-${el.gateway}`,value:el.account_id}))}
                         onChange={el=>setAccountId(el.value)}
                      /> 
                      </Col> */}
                    
                    <Col md="4" sm="4">
                      <FormGroup>
                        <Label for="first_name" className="form-label">
                          First Name
                        </Label>
                        <Field
                          name="first_name"
                          id="first_name"
                          placeholder="John"
                          className={`form-control ${errors.first_name && touched.first_name && "is-invalid"}`}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="4">
                      <FormGroup>
                        <Label for="last_name" className="form-label">
                          Last Name
                        </Label>
                        <Field
                          name="last_name"
                          id="last_name"
                          placeholder="Doe"
                          className={`form-control ${errors.last_name && touched.last_name && "is-invalid"}`}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="4">
                      <FormGroup>
                        <Label for="benef_phone" className="form-label">
                          Contact Number
                        </Label>
                        <InputGroup className="input-group-merge">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText
                              id='phone_code'
                             
                              name='phone_code'
                              style={{ borderColor: errors.benef_phone_code && touched.benef_phone_code ? "red" : "" }}
                            >
                              {inputData.benef_phone_code}
                            </InputGroupText>
                          </InputGroupAddon>
                          <Field
                            
                            name="benef_phone"
                            id="benef_phone"
                            placeholder="12345567"
                            className={`form-control ${errors.benef_phone && touched.benef_phone && "is-invalid"}`}
                          />
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="4">
                      <FormGroup>
                        <Label for="bef_email" className="form-label">
                          Email
                        </Label>
                        <Field
                          name="bef_email"
                          id="bef_email"
                          type="email"
                          placeholder="someone@gmail.com"
                          className={`form-control ${errors.bef_email && touched.bef_email && "is-invalid"}`}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="4">
                      <FormGroup>
                        <Label for="country" className="form-label">
                          Country
                        </Label>
                        <Select
                          theme={selectThemeColors}
                          className="react-select"
                          classNamePrefix="select"
                          defaultValue={countryOptions[227]}
                          options={countryOptions}
                          isClearable={false}
                          onChange={(obj) => {
                            setInputData({ ...inputData, benef_phone_code: obj.phone_code });
                            values.country = obj.value;
                            values.benef_phone_code = obj.phone_code;
                          }}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="4">
                      <FormGroup>
                        <Label for="city" className="form-label">
                          City
                        </Label>
                        <Field
                          name="city"
                          id="city"
                          placeholder="london"
                          className={`form-control ${errors.city && touched.city && "is-invalid"}`}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="4">
                      <FormGroup>
                        <Label for="zipcode" className="form-label">
                          Zip Code
                        </Label>
                        <Field
                          name="zipcode"
                          id="zipcode"
                          placeholder="112233"
                          className={`form-control ${errors.zipcode && touched.zipcode && "is-invalid"}`}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="12" sm="12">
                      <br />
                    </Col>
                    <Col md="12" sm="12">
                      <div className="h5 font-weight-bolderer">Bank Details</div>
                    </Col>

                    <Col md="4" sm="4">
                      <FormGroup>
                        <Label for="beneficiary_type" className="form-label">
                          Beneficiary Type
                        </Label>
                        <Select
                          name="benef_type"
                          theme={selectThemeColors}
                          className="react-select"
                          classNamePrefix="select"
                          defaultValue={beneficiaryTypeOptions[0]}
                          options={beneficiaryTypeOptions}
                          isClearable={false}
                          onChange={(obj) => {
                            values.benef_type = obj.value;
                          }}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="4">
                      <FormGroup>
                        <Label for="account_type" className="form-label">
                          Account Type
                        </Label>
                        <Select
                          name="account_type"
                          theme={selectThemeColors}
                          className="react-select"
                          classNamePrefix="select"
                          defaultValue={accountTypeOptions[0]}
                          options={accountTypeOptions}
                          isClearable={false}
                          onChange={(obj) => {
                            values.account_type = obj.value;
                          }}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="4">
                      <FormGroup>
                        <Label for="bank_country" className="form-label">
                          Bank Country
                        </Label>
                        <Select
                          name="bank_country"
                          theme={selectThemeColors}
                          className="react-select"
                          classNamePrefix="select"
                          defaultValue={transferCountriesOptions.find(el=>el.country_code==values.bank_country) || transferCountriesOptions[40]}
                          options={transferCountriesOptions}
                          getOptionValue={(ele) => ele.country_code}
                          isClearable={false}
                          onChange={(obj) => {
                            setInputData({ ...inputData, selectedBankCountry: obj });

                            values.bank_country = obj.country_code;
                            values.currency_code = obj.currency_code;
                            values.bank_code_type = obj.bank_code_type ? obj.bank_code_type : false;
                            values.bank_code = obj.bank_code_type ? "" : false;
                            values.account_number = obj.supports_iban ? false : "";
                            values.iban_account_no = obj.supports_iban ? "" : false;
                          }}
                        />
                      </FormGroup>
                    </Col>
                    {inputData.selectedBankCountry.bank_code_type ? (
                      <Col md="4" sm="4">
                        <FormGroup>
                          <Label for="bank_code" className="form-label">
                            {inputData.selectedBankCountry.bank_code_field_name}
                          </Label>
                          <Field
                            name="bank_code"
                            id="bank_code"
                            className={`form-control ${errors.bank_code && touched.bank_code && "is-invalid"}`}
                          />
                        </FormGroup>
                      </Col>
                    ) : null}

                    {inputData.selectedBankCountry?.supports_iban? (
                      <Col md="4" sm="4">
                        <FormGroup>
                          <Label for="iban_account_no" className="form-label">
                            IBAN Number
                          </Label>
                          <Field
                            name="iban_account_no"
                            id="iban_account_no"
                            className={`form-control ${
                              errors.iban_account_no && touched.iban_account_no && "is-invalid"
                            }`}
                          />
                        </FormGroup>
                      </Col>
                    ) : (
                      <Col md="4" sm="4">
                        <FormGroup>
                          <Label for="account_number" className="form-label">
                            Account Number
                          </Label>
                          <Field
                            name="account_number"
                            id="account_number"
                            className={`form-control ${
                              errors.account_number && touched.account_number && "is-invalid"
                            }`}
                          />
                        </FormGroup>
                      </Col>
                    )}
                    <Col md="4" sm="4">
                      <FormGroup>
                        <Label for="bic_code" className="form-label">
                          BIC/SWIFT Code
                        </Label>
                        <Field
                          name="bic_code"
                          id="bic_code"
                          className={`form-control ${errors.bic_code && touched.bic_code && "is-invalid"}`}
                        />
                      </FormGroup>
                     </Col>
                      {inputData?.selectedBankCountry?.multiple_currency?
                       (
                         <Col md="4" sm="4">
                             <FormGroup>
                        <Label for="currency_code" className="form-label">
                        Currency
                        </Label>
                        <Select
                          name="currency_code"
                          theme={selectThemeColors}
                          className="react-select"
                          classNamePrefix="select"
                          defaultValue={{label:values.country_code,value:values.country_code}}
                          options={inputData?.selectedBankCountry?.multiple_currency?.map((el)=>({label:el,value:el}))}
                          isClearable={false}
                          onChange={(obj) => {
                            values.currency_code = obj.value;
                          }}
                        />
                      </FormGroup>
                         </Col>
                       ) :null 
                    }
                    
                  </Row>
              
              </ModalBody>
              <ModalFooter>
                <Button
                  disabled={loader==true}
                  className="mr-1"
                  color="primary"
                  type="submit"
                 
                >
                  {loader==true ? <Spinner size="sm" className="mr-1" /> : null  }
                   Add
                </Button>
                <Button.Ripple
                
                  color="secondary"
                  onClick={() => setOpenAddBeneficiaryModal(false)}
                  outline
                >
                  Cancel
                </Button.Ripple>
              </ModalFooter>
            </Form>
          )}
        </Formik>
      </Modal>
    </div>
  );
}
export default AddBeneficiaryModal