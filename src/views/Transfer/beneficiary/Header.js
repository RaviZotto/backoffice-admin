import flatpickr from "flatpickr";
import React, { useContext, useEffect, useState } from "react";
import { selectThemeColors } from "@utils";
import Flatpickr from "react-flatpickr";
import { Download, DownloadCloud, File, FilePlus, FileText, Search } from "react-feather";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import { useSelector } from "react-redux";
import Select from "react-select";
import { CardBody, Card, CardText, Button } from "reactstrap";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import Spinner from "reactstrap/lib/Spinner";
import Label from "reactstrap/lib/Label";


export default function ({setMerchantId,setSearch}){
const {merchants}=useSelector(state=>state.merchant);
 

  return (
    <div>
      <Card>
        <CardBody>
          <Row>
            <Col md={6} sm={12} className="mb-sm-1">
                
              <Select
                placeholder='select merchant'
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
                options={merchants.map((el) => ({
                  label: el.comp_name,
                  value: el.merchant_id,
                }))}
                onChange={el=>{setMerchantId(el.value)}}
              />
            </Col>
            <Col md={6} sm={12} className="mb-sm-1">
              
                 <input 
                  className="form-control"
                  placeholder="search beneficiary"
                  type="text"
                  onChange={(e) =>setSearch(e.target.value)}  
                />
              </Col>
           
            </Row>

        </CardBody>
      </Card>
    </div>
  );
}
