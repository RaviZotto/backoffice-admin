import React, { Fragment, useEffect, useState } from "react";
import { Button } from "reactstrap";
import { PlusCircle } from "react-feather";
import AddBeneficiary from "./AddBeneficiary";
import Table from "./Table";
import Header from "./Header";

import { useHistory } from "react-router";
import { userDetails } from "../../../utility/Utils";
import TransactionSidebar from "./SideBar";
const Beneficiary = () => {
  const history = useHistory();
  const {role_type,transfer}=userDetails||{};
  const [openAddBeneficiaryModal, setOpenAddBeneficiaryModal] = useState(false);
  const [selectedBeneficiary,setSelectedBeneficiary]=useState({});
  const [merchantId, setMerchantId] = useState(null);
  const [search,setSearch]=useState(null);
  return (
    <Fragment>
      <TransactionSidebar selectedBeneficiary={selectedBeneficiary} setSelectedBeneficiary={setSelectedBeneficiary} />
      <Button.Ripple
        disabled={
          role_type &&
          role_type != "merchant" &&
          !transfer.includes("C")
        }
        color="primary"
        className="mb-1"
        onClick={(e) => setOpenAddBeneficiaryModal(true)}
      >
        {" "}
        <PlusCircle size={15} className="mr-1" />
        Add Beneficiary
      </Button.Ripple>
      <AddBeneficiary
        openAddBeneficiaryModal={openAddBeneficiaryModal}
        setOpenAddBeneficiaryModal={setOpenAddBeneficiaryModal}
      />
      <Header setMerchantId={setMerchantId} setSearch={setSearch} />
      <Table merchantId={merchantId} search={search}  setSelectedBeneficiary={setSelectedBeneficiary}    />
    </Fragment>
  );
};

export default Beneficiary;
