import React,{ Fragment, useContext, useEffect, useState } from "react";
import { Page, Text, View, Document, StyleSheet, BlobProvider, Font, Image, pdf } from "@react-pdf/renderer";
import logo from "../../../assets/images/logo/Zottologo.png";
import fontFile from "../../../assets/fonts/Montserrat-Regular.ttf";
import moment from "moment";
import { downloadDoc, formatMoney } from "@utils";
import printDoc from "print-js";
import { dataToRender, TxnHistoryContext } from ".";
import { useSelector } from "react-redux";


Font.register({
  family: "Montserrat",
  src: fontFile,
});

const _ = StyleSheet.create({
  h1: { fontSize: "18pt", fontWeight: "bold" },
  h6: { fontSize: "12pt" },
  h5: { fontSize: "12pt" },
  p: { fontSize: "11pt", color: "#626262", marginVertical: "5pt" },
  p2: { fontSize: "9pt", color: "#626262" },
  br1: { marginVertical: "10pt" },
  br2: { marginVertical: "5pt" },
  startPage: {
    flexDirection: "column",
    backgroundColor: "#fff",
    paddingVertical: 50,
    paddingHorizontal: 50,
    fontFamily: "Montserrat",
  },
  statementPage: {
    paddingHorizontal: 50,
    fontFamily: "Montserrat",
    backgroundColor: "#fff",
    paddingTop: 50,
    paddingBottom: 42,
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headerRight: {
    flexDirection: "column",
    alignItems: "flex-end",
  },
  details: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  detailsRight: {
    alignItems: "flex-end",
  },
  address: {
    textOverflow: "wrap",
    width: "120pt",
  },
  addressRight: {
    textOverflow: "wrap",
    width: "120pt",
    textAlign: "right",
  },
  table: {
    border: "1pt solid #DCDCDC",
    padding: "10pt",
  },
  tr: {
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  td: {
    width: "200pt",
  },
  statementTable: {
    border: "1pt solid #DCDCDC",
  },
  sr: {
    flexDirection: "row",
    borderBottom: "1pt solid #dcdcdc",
  },
  sd: {
    padding: "5pt",
    textAlign: "center",
    width: "99pt",
    justifyContent: "center",
  },
  sh: { fontSize: "10pt" },
});

const { H1, H5, H6, P, BR1, BR2, TR, TD, SR, SD, P2, SH } = {
  H1: ({ children }) => <Text style={_.h1}>{children}</Text>,
  H6: ({ children }) => <Text style={_.h6}>{children}</Text>,
  H5: ({ children }) => <Text style={_.h5}>{children}</Text>,
  P: ({ children }) => <Text style={_.p}>{children}</Text>,
  P2: ({ children }) => <Text style={_.p2}>{children}</Text>,
  BR1: ({ children }) => <View style={_.br1}>{children}</View>,
  BR2: ({ children }) => <View style={_.br2}>{children}</View>,
  TR: ({ children }) => <View style={_.tr}>{children}</View>,
  TD: ({ children }) => <View style={_.td}>{children}</View>,
  SR: ({ children }) => <View style={_.sr}>{children}</View>,
  SD: ({ children }) => <View style={_.sd}>{children}</View>,
  SH: ({ children }) => <Text style={_.sh}>{children}</Text>,
};

const PDF = ({ user, statement }) => {
  const { account, period, transactions, totals } = statement;
   
  return (
    <Document>
      <Page size="A4" style={_.startPage}>
        <View style={_.header}>
          <View>
            <Image style={{ width: "105pt" }} src={logo} />
          </View>

          <View style={_.headerRight}>
            <H1>Zotto Statement</H1>
            <BR1 />
            <H6>Statement Date</H6>
            <P>{moment().format("YYYY-MM-DD")}</P>
          </View>
        </View>
        <BR1 />
        <BR1 />
        <View style={_.details}>
         
          <View style={_.detailsRight}>
            <H6>Zotto LTD</H6>
            <BR1 />
            <View style={_.addressRight}>
              <P>Staines Upon Thames, TW18 3BA, UK</P>
            </View>
            <BR2 />
            <P>info@zotto.io</P>
            <P>+44 808 169 7040</P>
          </View>
        </View>
        <BR1 />
        <BR1 />
        <BR1 />
        <View style={_.accDetails}>
          <H6>Account Summary:</H6>
        </View>
        <BR1 />
        <View style={_.table}>
          
          <BR2 />
          <TR>
            <TD>
              <P>Total Transactions</P>
            </TD>
            <P>{totals.length}</P>
          </TR>
          <BR2 />
          <TR>
            <TD>
              <P>Transaction Period</P>
            </TD>
            <P>
              {period[0]} {period[1]}
            </P>
          </TR>
          <BR2 />
       
          <BR2 />
          <TR>
            <TD>
              <P>Total Amount Credited</P>
            </TD>
            <P>{totals.credited}</P>
          </TR>
          <BR2 />
          <TR>
            <TD>
              <P>Total Amount Debited</P>
            </TD>
            <P>{totals.debited}</P>
          </TR>
          <BR2 />
          <TR>
            <TD>
              <P>Total Fees</P>
            </TD>
            <P>{totals.fees}</P>
          </TR>
        </View>
        <BR1 />
        <BR1 />
        <P>Check detailed account statement on next page.</P>
        <H6>Feel free to reach us at support@zotto.io, if you have any questions.</H6>
        <P>
          {
            "Zotto LTD (Company No. 09548832) is registered by the Financial Conduct Authority to conduct payment activities."
          }
        </P>
      </Page>
      <Page size="A4" style={_.statementPage}>
        <View style={_.accDetails}>
          <H6>Account Statement:</H6>
        </View>
        <BR1 />
        <View style={_.statementTable}>
          <SR>
            <SD>
              <SH>Date</SH>
            </SD>
            <SD>
              <SH>Amount</SH>
            </SD>
            <SD>
              <SH>Transaction Type</SH>
            </SD>
            <SD>
              <SH>Payment Mode</SH>
            </SD>
            <SD>
              <SH>Fee</SH>
            </SD>
          </SR>
          {transactions.map((el) => (
            <SR key={el.id}>
              <SD>
                <P2>{moment(el.date_of_transaction).format("YYYY-MM-DD")}</P2>
                <P2>{moment(el.date_of_transaction).format("HH:mm:ss")}</P2>
              </SD>
              <SD>
                <P2>{formatMoney(el.amount, el.currency)}</P2>
              </SD>
              <SD>
                <P2>{el.paid_type == "cr" ? "Credit" : "Debit"}</P2>
              </SD>
              <SD>
                <P2>{el.pay_method}</P2>
              </SD>
              <SD>
                <P2>{formatMoney(el.fees ? el.fees : 0, el.currency)}</P2>
              </SD>
            </SR>
          ))}
        </View>
      </Page>
    </Document>
  );
};

export default function StatementPDF() {
  const {merchants}=useSelector(state=>state.merchant);
  const {dates,setStatement,statement,merchantId,Txn:transactions,searchQuery,beneficiares} = useContext(TxnHistoryContext)
  const user = merchants.length ? merchants.find(e=>e.merchant_id==merchantId) : null;
  // const transactionsObj= transactions.filter(e=>e.currency_account_id== selectedAccount.id);
  const transactionsObj=dataToRender({merchantId,beneficiares,transactions,searchQuery});
  
  const total=transactionsObj.length;
  //console.log({user,statement});
  const deleteResolvedAction = () => {
    setStatement({});
  };
  
   

  const handleAction = (data, action) => {
    if (!data) return;
    if (action == "print") {
      printDoc(data);
      deleteResolvedAction();
    } else if (action == "download") {downloadDoc(data, () => deleteResolvedAction());}
  };
useEffect(() => {
  if(statement&&statement.action&&transactionsObj.length){
(async()=>{  const pdfBlob = await pdf(<PDF user={user} statement={{period:dates,transactions:transactionsObj,totals:total}} />).toBlob();
  const url = URL.createObjectURL(pdfBlob);
  console.log(url);
  handleAction(url,statement.action);})()
  }
},[statement&&statement.action&&transactionsObj.length])
  if ( !statement || !statement.action || !transactionsObj.length){deleteResolvedAction();return null;}
  return (
    <div style={{ display: "none" }}>
      {/* <BlobProvider document={<PDF user={user} statement={{period:dates,transactions:transactionsObj,totals:total}} />}>
        {({ url, loading }) => {
          return loading ? null : (
            <Fragment>{setTimeout(() => handleAction(url, statement.action), 0) ? null : null}</Fragment>
          );
        }}
      </BlobProvider> */}
    </div>
  );
}
