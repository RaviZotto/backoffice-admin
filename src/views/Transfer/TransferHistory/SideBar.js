// ** Third Party Components
import React from 'react'
import { useContext, useState } from "react";
import { Table, Button, Col } from "reactstrap";
import Sidebar from "@components/sidebar";
import moment from "moment";
import { TxnHistoryContext } from '.';
// import "@styles/base/pages/app-invoice.scss";

const TransactionSidebar = () => {
  const {
    selectedTransaction: transaction,
    setSelectedTransaction,
    setRefundTxn,
    accounts,
    beneficiares
  } = useContext(TxnHistoryContext);
  const [refundAmount, setRefundAmount] = useState(null);
  const closeSidebar = () => setSelectedTransaction({});
  const refundAmountLimit = transaction.amount - transaction.refunded_amount;
  let beneObj = beneficiares.find(el=>el.id==transaction.benef_id);
  console.log(transaction,'343');
  return (
    <Sidebar
      size="lg"
      open={transaction.id ? true : false}
      title="Transaction Details"
      headerClassName=""
      contentClassName="p-0"
      toggleSidebar={closeSidebar}
    >
      <Table style={{ tableLayout: "fixed" }}  className='mb-0' borderless={true} striped={true}>
        {transaction.id ? (
          <tbody>
            <tr>
              <td>Date</td>
              <td>
                {moment(transaction.date_of_transaction).format("YYYY-MM-DD")}
              </td>
            </tr>
            <tr>
              <td>Time</td>
              <td>
                {moment(transaction.date_of_transaction).format("HH:mm:ss")}
              </td>
            </tr>
            <tr>
              <td>Merchant Id</td>
              <td>
                {transaction.merchant_id}
              </td>
            </tr>
            <tr>
              <td>Beneficiary Id</td>
              <td>
                {  
                 beneObj?beneObj.id:'---'
                }
              </td>
            </tr>
            <tr>
              <td>Beneficiary Name</td>
              <td>
                {  
                   beneObj?beneObj.benef_name:'------'
                }
              </td>
            </tr>
            <tr>
              <td>Status</td>
              <td>{!transaction.status ? "Failed" : transaction.status=='1'?'Success': "Pending"}</td>
            </tr>
             <tr>
              <td>Account Id</td>
              <td>
             { transaction.currency_account_id}
              </td>
            </tr> 
            <tr>
              <td>Transaction ID</td>
              <td>
                <div style={{ wordWrap: "break-word" }}>
                  {transaction.txn_no}
                </div>
              </td>
            </tr>
            <tr>
              <td>Order ID</td>
              <td>{transaction.order_id}</td>
            </tr>
            <tr>
              <td>Amount</td>
              <td>
                {transaction.amount} {transaction.currency}
              </td>
            </tr>
            <tr>
              <td>Conversion Rate</td>
              <td>
                {transaction.margin_amount}
              </td>
            </tr>
            
            <tr>
              <td>Fees</td>
              <td>
                {Number(transaction.fees)+Number(transaction.fixedfee)}
              </td>
            </tr>
            <tr>
              <td>Payment Type</td>
              <td>{transaction.paid_type == "cr" ? "Credit" : "Debit"}</td>
            </tr>
            <tr>
              <td>Payment Method</td>
              <td>{transaction.pay_method}</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>
                <div className="font-small-3">{transaction.message}</div>
              </td>
            </tr>
            <tr>
            <td>Refunded</td>
            <td>
              
                {transaction.refund_status==1
                  ? (
                    transaction.refunded_amount==0?'Not Refunded':
                    transaction.refunded_amount < transaction.amount
                    ? "Partially refunded"
                    : " Refunded")
                  : "No"}
              </td>
            </tr>
            <tr className="pb-0 mb-0">
              <td>Refundable</td>
              <td>{transaction.refund_status == 1 ? "Yes" : "No"}</td>
            </tr>
            {
            transaction.refund_status==1?(
              <tr>
            <td>Refunded Amount</td>
            <td>
            {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: transaction.currency,
          })
            .format(transaction.refunded_amount)
            .replace(/^(\D+)/, "$1 ")}
              </td>
              </tr>
              ):null
            }
             
            
          </tbody>
        ) : (
          <tbody></tbody>
        )}
      </Table>
      {transaction.refund_status && refundAmountLimit <= transaction.amount ? (
        
          <div className="d-flex pl-2 ml-1 pr-2 pt-0 justify-content-center">
      
            <Button.Ripple
              color="danger"
         
              onClick={(e) =>
                setRefundTxn(
                 transaction
                  )
              }
            >
              Refund
            </Button.Ripple>
          </div>
      ):null}
    </Sidebar>
  );
};

export default TransactionSidebar;