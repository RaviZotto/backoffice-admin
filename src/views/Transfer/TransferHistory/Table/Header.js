import flatpickr from "flatpickr";
import React, { useContext, useEffect, useState } from "react";
import { selectThemeColors } from "@utils";
import Flatpickr from "react-flatpickr";
import { DownloadCloud, FileText, Search } from "react-feather";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import { useSelector } from "react-redux";
import Select from "react-select";
import { CardBody, Card, CardText, Button } from "reactstrap";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import moment from "moment";
import { TxnHistoryContext } from "..";
import Spinner from "reactstrap/lib/Spinner";

export default function () {
  const {dates,setDates,merchantId,setMerchantId,setSearchQuery, statementCSV,setStatement,statement, setStatementCSV}=useContext(TxnHistoryContext)
  const { merchants } = useSelector((state) => state.merchant);
  const [Merchant,setMerchant]=useState([])
 const [partner, setPartner] = useState(null);
 const [state,setState]=useState({...dates})
  useEffect(() => {
      merchants.length&&setMerchant(merchants.map(el=>({label:el.comp_name,value:el.merchant_id,id:el.partner_id})))
      //console.log(merchants)
    },[merchants])

  return (
    <div>
      <Card>
        <CardBody>
          <Row>
            <Col md={2} sm={12} className="mb-sm-1">
                <label>Transfer Type</label>
              <Select
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
                options={['Select','SEPA','SWIFT','INTERNAL','EXPRESS'].map((el,i) => ({
                  label: el,
                  value: i==0?0:i
                })) }
                onChange={el=>{setSearchQuery(el.value)}}
              />
            </Col>
            <Col md={3} sm={12} className="mb-sm-1">
                <label>Merchant</label>
              <Select
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
                options={[ {label:'select',value:0}, ...Merchant]}
                onChange={el=>setMerchantId(el.value)}
              />
            </Col>
            <Col md={7} sm={12} className="mb-sm-1  pt-sm-2">
              <Row>
                <div className="col-md-4 col-6  ">
                  <Flatpickr
                    className="form-control"
                    value={state.start_date}
                    onChange={(e) =>
                      setState({
                        ...state,
                        start_date: moment(e[0]).format("YYYY-MM-DD"),
                      })
                    }
                  />
                </div>
                <div className="  col-md-4 col-6   ">
                  <Flatpickr
                    className="form-control "
                    value={state.end_date}
                    onChange={(e) =>
                      setDates({
                        ...state,
                        end_date: moment(e[0]).format("YYYY-MM-DD"),
                      })
                    }
                  />
                </div>
                <div className=" d-flex justify-content-center col-12 col-md-1 mr-1 ">
                  <Button
                    className="flex-fill mb-2 mt-1 mb-md-0 mt-md-0 "
                    color="primary"
                    disabled={state.start_date==dates.start_date&&state.end_date==dates.end_date}
                    onClick={() => 
                       setDates({...state})
                    }
                  >
                    <Search size={17} />
                  </Button>
                </div>
                <div className=' d-flex justify-content-center col-12 col-md-1 mr-1'>
                  <Button
                     disabled={statementCSV}
                     data-toggle="tooltip" data-placement="top" title="CSV"
                    className="flex-fill mb-2 mt-1 mb-md-0 mt-md-0  "
                    color='primary'
                    onClick={el=>setStatementCSV(true)}
                  >
                    <FileText size={17} />
                  </Button>
                  </div>
                  <div className='d-flex justify-content-center col-12 col-md-1 mr-1'>
                  <Button
                   disabled={statement.loading}
                   data-toggle="tooltip" data-placement="top" title="PDF"
                    className="flex-fill mb-2 mt-1 mb-md-0 mt-md-0  "
                    color='primary'
                    onClick={el=>setStatement({action:'download',loading:true})}
                 >
                   {statement.loading?<Spinner size="sm"/>:<DownloadCloud size={17} /> }
                  </Button>
                  </div>
              </Row>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </div>
  );
}
