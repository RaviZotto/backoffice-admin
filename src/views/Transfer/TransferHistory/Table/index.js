import React, { useContext, useEffect, useState } from 'react'
import Loading from "@core/components/spinner/Loading-spinner";
import { ChevronDown, Eye, Edit,Trash, DownloadCloud } from "react-feather";
import DataTable from "react-data-table-component";
import moment from 'moment';
import { Fragment } from 'react';
import { GET_MERCHANT, GET_PARTNER, UPDATE_MERCHANT} from "../../../../redux/actions/storeMerchant.";
import Badge from 'reactstrap/lib/Badge';
import { useDispatch, useSelector } from 'react-redux';
import notification from '../../../../components/notification';
import { TxnHistoryContext } from '..';
import { GET_COUNTRIES, STORE_BENEFICIARIES } from '../../../../redux/actions/transfer';
import { Spinner } from 'reactstrap';


export default function index() {  
const dispatch = useDispatch()
const {partners,merchants}=useSelector(state=>state.merchant);
const {beneficiares,countries}=useSelector(state=>state.transfer);
const [loading,setLoading]=useState(true);
const {error,Txn:transactions,loading:loading1,searchQuery,merchantId,setTxn,actionQueue,setActionQueue,setSelectedTransaction}=useContext(TxnHistoryContext);
if(error)notification({title:'Transaction details',type:'error',message:error.message})


useEffect(() => {
 if(loading){   
 if(!beneficiares.length)dispatch(STORE_BENEFICIARIES());
 if(!merchants.length)dispatch(UPDATE_MERCHANT());
 if(!countries.length)dispatch(GET_COUNTRIES());
 setLoading(false);
 }
})





const columns =[
   
    {
        name: "Beneficiary Name",
        selector: "benef_name",
        sortable: true,
        minWidth: "110px",
        omit:searchQuery==3,
        cell: (row) => {
          let date = moment(row.benef_name);
          return (
            <Fragment>
              <div className="mt-1">
               {row.benef_name }
                
              </div>
            </Fragment>
          );
        },
      },
      {
        name: "Status",
        selector: "status",
        minWidth: "110px",
        sortable: true,
        cell: (row) => (
          <Fragment>
            <div
              className={
                row.status == 0
                  ? "bg-danger"
                  : row.status == 1
                  ? "bg-success"
                  : "bg-primary"
              }
              style={{
                height: "10px",
                width: "10px",
                borderRadius: "50%",
                display: "inline-block",
                marginRight: "5px",
              }}
            />
            <span>
              {row.status == 0
                ? "Failed"
                : row.status == 1
                ? "Success"
                : "Pending"}
            </span>
          </Fragment>
        ),
      },
      {
        name: "Transaction ID",
        selector: "txn_no",
        sortable: false,
        minWidth: "200px",
        cell: (row) => {
          return <div className="font-small-3">{row.txn_no}</div>;
        },
      },
      {
        name: "Order ID",
        selector: "order_id",
        sortable: false,
        minWidth: "130px",
        cell: (row) => {
          return <div className="font-small-3">{row.order_id}</div>;
        },
      },
      {
        name: "Payment Type",
        selector: "pay_method",
        sortable: false,
        minWidth: "150px",
        cell: (row) => <Badge color="primary">{row.pay_method}</Badge>,
      },
      {
        name: "Amount",
        selector: "amount",
        sortable: false,
        cell: (row) => (
          <h6
            className={`${
              row.paid_type == "cr" ? "text-success" : "text-danger"
            } font-weight-bolderer`}
          >
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: row.currency,
            })
              .format(row.amount)
              .replace(/^(\D+)/, "$1 ")}
          </h6>
        ),
      },
      {
        name:'Action',
        selector:"action",
        sortable: false,
        minWidth:"150px",
        cell: (row) => {
          let insideQueue = actionQueue.find((el) => el.transaction.id == row.id);
          let action = insideQueue ? insideQueue.action : false;
          return (
            <div className="column-action d-flex align-items-center">
              <Eye size={17} className="cursor-pointer mr-1" onClick={() => setSelectedTransaction(row)} />
            
                <>
              {/* {action == "print" ? (
                <Spinner size="sm" className="mx-1" />
              ) : (
                <Printer
                  size={17}
                  className="mx-1 cursor-pointer"
                  onClick={() => setActionQueue([...actionQueue, { transaction: row, action: "print" }])}
                />
              )} */}
              {action == "download" ? (
                <Spinner size="sm" />
              ) : (
                <DownloadCloud
                  size={17}
                  className="cursor-pointer"
                  onClick={() => setActionQueue([...actionQueue, { transaction: row, action: "download" }])}
                />
              )}
              </>
             
            </div>
        )
      }
      }
    ];

const dataToRender = () => {
    let tempData=[];
    if(transactions){
        
    tempData =[...transactions];
     
      tempData=tempData.filter(e=>e.payment_mode=='4');
      let tempDataObj=[];
      for(let temp of tempData){
         
       let tempt = {...temp};
       let beneObj = beneficiares.find(el=>el.id==temp.benef_id);
       //console.log(beneObj);
       tempt.my_txn_type = temp.paid_type == "cr" ? "credit" : "debit";
       tempt.my_status =
         temp.status == "1"
           ? "success"
           : temp.status == "0"
           ? "failed"
           : "pending";
       tempt.pay_method = `${temp.pay_method}`.toLowerCase();
       tempt.benef_name= beneObj?beneObj.benef_name:null;
      tempDataObj.push(tempt);
      }
     //console.log(tempDataObj);
     if(merchantId)tempDataObj=tempDataObj.filter(e=>e.merchant_id==merchantId)
     if(searchQuery)tempDataObj = tempDataObj.filter(el=>el.transfer_type==searchQuery)
 
     return tempDataObj;
    }
   
}


if(loading||loading1)return <Loading />

    return (
        <div>
         <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
        </div>
    )
}
