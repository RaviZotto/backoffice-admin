import React, { createContext, useEffect, useMemo, useState } from "react";
import Header from "./Table/Header";
import Table from "./Table";
import { GET_TRANSACTIONS } from "../../../graphql/queries";
import { useQuery } from "@apollo/client";
import { STORE_BENEFICIARIES, STORE_MERCHANTACCOUNTS } from "../../../redux/actions/transfer";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import PdfDownload from "./pdfDownload"
import CsvPreview from "./csvPreview";
import { getAdminTransactions } from "../../../Api";
import TransactionPDF from "./TransactionPdf";
import notification from "../../../components/notification";
import TransactionSidebar from "./SideBar";
export const TxnHistoryContext = createContext();
export const dataToRender = ({merchantId,beneficiares,transactions,searchQuery}) => {
 
       
    let tempData=[];
    if(transactions){
        
    tempData =[...transactions];
    tempData=tempData.filter(e=>e.payment_mode=='4');
      let tempDataObj=[];
      for(let temp of tempData){
       let tempt = {...temp};
       let beneObj = beneficiares.find(el=>el.beneficiary_id==temp.benef_id);
       //console.log(beneObj);
       tempt.my_txn_type = temp.paid_type == "cr" ? "credit" : "debit";
       tempt.my_status =
         temp.status == "1"
           ? "success"
           : temp.status == "0"
           ? "failed"
           : "pending";
       tempt.pay_method = `${temp.pay_method}`.toLowerCase();
       tempt.benef_name= beneObj?beneObj.benef_name:null;
      tempDataObj.push(tempt);
      }
     
     if(merchantId)tempDataObj=tempDataObj.filter(e=>e.merchant_id==merchantId)
     if(searchQuery)tempDataObj = tempDataObj.filter(el=>el.transfer_type==searchQuery)
   return tempDataObj;
  }
 
}


 function TransferHistory() {
  var temp = new Date();
  temp = temp.setDate(temp.getDate() - 6);

  const [dates, setDates] = useState({
    start_date: moment(temp).format("YYYY-MM-DD"),
    end_date: moment(new Date()).format("YYYY-MM-DD"),
  });
  const [statementCSV,setStatementCSV]=useState(false)
  const dispatch = useDispatch();
  const [selectedTransaction, setSelectedTransaction] = useState({});
  const [merchantId, setMerchantId] = useState(null);
  
const { beneficiares } = useSelector((state) => state.transfer);
  const [statement,setStatement]=useState({});
  const [searchQuery,setSearchQuery]=useState(null)
  const [Txn,setTxn]=useState([]);
  const [loading,setLoading]=useState(true);
  const [actionQueue,setActionQueue]=useState([]);
  
  
  useEffect(() => {
    if (!beneficiares.length) dispatch(STORE_BENEFICIARIES());
  }, [beneficiares]);



  useEffect(() => {
    (async () => {
      try {
        setLoading(true);
        let transaction = await getAdminTransactions({...dates,payment_mode:4});
        //console.log(transaction);
        if (transaction.length) {setTxn(transaction);}
        setLoading(false);
      } catch (e) {
        setLoading(false);
        notification({ title: "Error", message: e, type: "error" });
      }
    })();
  }, [dates]);



  const TxnReportProvider = useMemo(
    () => ({
      Txn,
      setTxn,
      loading,
     setStatement,
      statement,
      setSelectedTransaction,
      selectedTransaction,
      dates,
      setDates,
      searchQuery,
      setSearchQuery,
      merchantId,
      setMerchantId,
      statementCSV,
      beneficiares,
      setStatementCSV,
      actionQueue,
      setActionQueue
    }),
    [beneficiares,
      actionQueue,
      dates,
      Txn,
      statementCSV,
      statement,
     selectedTransaction,
      merchantId, 
      loading,
      searchQuery
    ]
  );

  return (
    <TxnHistoryContext.Provider value={TxnReportProvider}>
      <Header />
      <TransactionPDF />
      {statementCSV&&<CsvPreview/>}
      {statement.loading&&<PdfDownload/>}
      <Table  loading1={loading} data={Txn} />
     {selectedTransaction.id && <TransactionSidebar/>}
    </TxnHistoryContext.Provider>
  
  );
}

export default TransferHistory;
