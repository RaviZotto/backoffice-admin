import React, { useContext, useEffect, useRef, useState } from "react";
import {
  Col, Row, Input, Button, CustomInput, Label, FormGroup, InputGroupAddon,
  InputGroup,
} from "reactstrap";
import Select from "react-select";
import { selectThemeColors } from "@utils";
import Confirmation from './ConfirmationModal';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { formatMoney } from '@utils';
import Styled from 'styled-components'
import { TransferContext } from ".";
import { ArrowRight, Plus } from "react-feather";
import notification from "../../components/notification";
import { useHistory } from "react-router-dom";
import { getExchangeRate } from "../../Api";
import { Spinner } from "reactstrap/lib";
import { calculateTotalFee, userDetails } from "../../utility/Utils";
import { STORE_MERCHANTACCOUNTS } from "../../redux/actions/transfer";
export default function Accounts() {
  const { role_type, transfer } = userDetails || {};
  const history = useHistory()
  const { partners, merchants } = useSelector(state => state.merchant);
  const { merchantsAccounts, beneficiares, transferFee, storeRate } = useSelector(state => state.transfer);
  const [senderAccount, setSenderAccount] = useState({});
  const [receiverAccount, setRecieverAccount] = useState({});
  const [realAccount, setRealAccount] = useState([]);
  const [merchant, setMerchant] = useState([])
  const [currencyAccount, setCurrencyAccount] = useState([]);
  const [partnerId, setPartnerId] = useState(null);
  const [merchantId, setMerchantId] = useState(null);
  const [type, setType] = useState('internal_transfer');
  const [currentId, setCurrentIdInstance] = useState(Date.now());
  const [confirmDetailsModal, setConfirmDetailsModal] = useState(false);
  const [beneficiary, setBeneficiary] = useState([]);
  const dispatch = useDispatch();

  const { setTransferDetailsData, transferDetailsData, sendOtp, setSendOtp } = useContext(TransferContext);
  const [loading, setLoading] = useState(false);
  const fixedSideOptions = [
    { label: "Sender", value: "sender" },
    { label: "Receiver", value: "receiver" },
  ];


  useEffect(() => {

    merchants.length && setMerchant(merchants.map(el => ({ label: el.comp_name, value: el.merchant_id, id: el.partner_id })))
    merchantsAccounts.length && setCurrencyAccount(merchantsAccounts.filter(e => !e.gateway && e.merchant_id == merchantId).map(el => ({ label: el.account_name, value: el.account_id, id: el.merchant_id })))
    merchantsAccounts.length && setRealAccount(merchantsAccounts.filter(e => e.gateway && merchantId == e.merchant_id).map(el => ({ label: `${el.comp_name}-${el.account_name}-${el.currencycode}`, value: el.account_id, id: el.merchant_id })))
    setBeneficiary([...beneficiares.filter(e => e.merchant_id == merchantId).
      map(el => ({ label: `${el.benef_name}-${el.currency_code}-${el.iban_account_no}-${el.gateway}`, value: el.beneficiary_id, gateway: el.gateway }))])
  }, [merchants, merchantsAccounts, transferFee, merchantId, partnerId])

  useEffect(() => {
    if (senderAccount?.gateway && type == 'external_transfer') setBeneficiary(beneficiares.filter(el => el.currency_code == senderAccount.currencycode && merchantId == el.merchant_id).map(el => ({ label: `${el.benef_name}-${el.currency_code}-${el.iban_account_no}-${el.gateway}`, value: el.beneficiary_id, gateway: el.gateway })));
    if (senderAccount?.currencycode && receiverAccount?.currencycode) {

      if (senderAccount.currencycode != (receiverAccount.currencycode)) setTransferDetailsData({ ...transferDetailsData, isIfx: true, currency: senderAccount.currencycode })
      else setTransferDetailsData({ ...transferDetailsData, isIfx: false, currency: senderAccount.currencycode })
    } else setTransferDetailsData({ ...transferDetailsData, currency: senderAccount.currencycode })
  }, [senderAccount, receiverAccount])

  useEffect(() => {
   setRealAccount(merchantsAccounts?.filter(el => el.gateway&& el.merchant_id == merchantId).map(el => ({ label: el.account_name, value: el.account_id, id: el.merchant_id })))
   if (type == 'forex_transfer') {
       
      setCurrencyAccount(merchantsAccounts?.filter(el => el.merchant_id == merchantId).map(el => ({ label: el.account_name, value: el.account_id, id: el.merchant_id })))
      setRealAccount(merchantsAccounts?.filter(el => el.merchant_id == merchantId).map(el => ({ label: el.account_name, value: el.account_id, id: el.merchant_id })))
    }
    else {
      setCurrencyAccount(merchantsAccounts?.filter(el => !el.gateway&& el.merchant_id == merchantId).map(el => ({ label: el.account_name, value: el.account_id, id: el.merchant_id })))
    }
   

  }, [type])



  const schema = Yup.object().shape({
    amount: Yup.string("amount is required")
      .required(),

    message: Yup.string("Description is required").required(),
  });
  // const ref = useRef();




  const handleSubmit = (value) => {



    let dataRequest = { merchant_id: merchantId, 
      amount: Number(value.amount).toFixed(2), 
      payer_currency: senderAccount.currencycode, 
      fixed_side: transferDetailsData.fixed, 
      transfer_type: type, 
      payee_currency: receiverAccount.currencycode ?? receiverAccount.currency_code ,
      from_id: senderAccount?.account_id,
      to_id: receiverAccount?.account_id
    };
                                                               
    if (!merchants.find(e => e.merchant_id == merchantId).timezone) { notification({ title: "Error", message: "timezone is not set for transactions", type: "error" }); return; }

   
    if (type == 'forex_transfer' && senderAccount?.currencycode == receiverAccount?.currencycode) return notification({ title: 'Not Allowed', type: "error", message: "forex transfer cannot between same account" });
    if (type == 'external_transfer') {

      //console.log(senderAccount)
      //  console.log(merchants.find(e=>e.merchant_id==merchantId),senderAccount);
      if (!merchants.find(e => e.merchant_id == merchantId).is_swift && ((senderAccount.currencycode != receiverAccount.currency_code) || (senderAccount.currencycode == 'EUR' && !receiverAccount.is_sepa))) { notification({ title: "Error", message: "Swift payment is not allowed", type: "error" }); return; }
      if (!merchants.find(e => e.merchant_id == merchantId).is_sepa && senderAccount.currencycode == 'EUR' && receiverAccount.is_sepa) { notification({ title: "Error", message: "Sepa payment is not allowed", type: 'error' }); return; }
      if (!merchants.find(e => e.merchant_id == merchantId).is_express && (senderAccount.currencycode == receiverAccount.currency_code == 'GBP')) { notification({ title: "Error", message: "Express payment is not allowed", type: 'error' }); return; }
    }

    //console.log(transferDetailsData,'1',stepper); 
    setLoading(true);
    getExchangeRate(dataRequest, ({ res, err }) => {
      if (err) { notification({ title: 'Error', message: err, type: 'error' }) }
      else {
        console.log(res);
        if (senderAccount.current_balance < (res.payer_amount+calculateTotalFee(res?.payer_amount,res?.fees))) { setLoading(false); notification({ type: 'error', message: 'Insufficient balance' }); return; }
        setTransferDetailsData({
          ...transferDetailsData,
          account_from: senderAccount,
          account_to: type != 'external_transfer' ? receiverAccount : undefined,
          beneficiary: type == 'external_transfer' ? receiverAccount : undefined,
          transferFee: res,
          ...value,
          timezone:merchant.timezone||"Europe/London",
          type

        });
        if(type=='internal_transfer' && (res?.payer_amount+calculateTotalFee(res.payer_amount,res?.fees)>res?.settlement_available)){ setLoading(false); return notification({type:'error',title:"Exceeds more than settlment available amount ",message:`Cannot send more than ${res?.settlement_available}`})}
        
        setConfirmDetailsModal(true);
        // //console.log(transferDetailsData,'23')

      }
      setLoading(false);
    })

  };
  return (

    <Col className="card p-2"  >
      {
        confirmDetailsModal &&
        <Confirmation

          resetTransfer={() => setCurrentIdInstance(Date.now())}
          sendOtp={sendOtp}
          setSendOtp={setSendOtp}

          refetchAccounts={() => dispatch(STORE_MERCHANTACCOUNTS)}
          transferType={type}
          transferDetails={transferDetailsData}

          confirmDetailsModal={confirmDetailsModal}
          setConfirmDetailsModal={setConfirmDetailsModal}
        />
      }
      <div onChange={(e) => {setType(e.target.id); setCurrentIdInstance(Date.now());setSenderAccount({}) }}  >
        <CustomInput
          type="radio"
          className="custom-control-Primary"
          name="transferType"
          id="internal_transfer"
          defaultChecked
          label="Internal Transfer"
          inline
        />
        <CustomInput
          type="radio"
          className="custom-control-Primary"
          name="transferType"
          id="external_transfer"
          label="External Transfer"
          inline
        />
        <CustomInput
          type="radio"
          className="custom-control-Primary"
          name="transferType"
          id="forex_transfer"
          label="Forex Transfer"
          inline
        />
      </div>
      <CardTextDiv>Bal: {senderAccount && formatMoney(senderAccount.current_balance, senderAccount.currencycode)}</CardTextDiv>
      <Row  >
        <Col md={6} sm={12}>
          <label>Parnters</label>
          <Select
            theme={selectThemeColors}
            className="react-select"
            classNamePrefix="select"
            placeholder="Select"
            onChange={el => { setPartnerId(el.value); }}
            options={partners.map(el => ({ label: el.comp_name, value: el.partner_id }))}
            isClearable={false}
          />
        </Col>
        <Col md={6} sm={12}>
          <label>Merchant</label>
          <Select
            theme={selectThemeColors}
            className="react-select"
            classNamePrefix="select"
            placeholder="select"
            options={merchant.filter(e => e.id == partnerId)}
            onChange={el => setMerchantId(el.value)}
            isClearable={false}
          />
        </Col>
        <div style={{flexGrow:1,display:'flex'}} key={currentId} >
        <Col md={6} sm={12}>
          <label>Sender Account</label>
          <Select
            theme={selectThemeColors}
            className="react-select"
            classNamePrefix="select"
            placeholder='select'
            onChange={el => setSenderAccount(merchantsAccounts.find(e => e.account_id == el.value))}
            options={type == 'external_transfer' ?([...realAccount,...currencyAccount]):(currencyAccount)}
            isClearable={false}
          />
        </Col>
        <Col md={6} sm={12}>
          <label>Reciever Account</label>
          <Select
            theme={selectThemeColors}
            className="react-select"
            classNamePrefix="select"
            placeholder='select'
            options={type != 'external_transfer' ? (realAccount) : [{ label: (<span onClick={e1 => history.push('/add-beneficiary')} ><Plus size={14} className='mr-1' />Add Beneficiary</span>), value: 'beneficiary' }, ...beneficiary]}
            onChange={(el, meta) => {
              if (el.value == 'beneficiary') {//console.log(meta);
                return;
              }
              if (type == 'external_transfer') { setRecieverAccount(beneficiares.find(e => el.value == e.beneficiary_id)); }
              else {
                let obj = merchantsAccounts.find(e => e.account_id == el.value);
                console.log(obj, 204)
                setRecieverAccount(obj)
              }
            }}
            isClearable={false}
          />
        </Col>
        </div>
      </Row>
      <Row key={currentId} >
        <Formik
          initialValues={{ amount: 0, message: "" }}
          enableReinitialize
          onSubmit={handleSubmit}
          validationSchema={schema}
    

        >
          {({ errors, touched }) => (
            <Form className="w-100 d-flex flex-wrap">
              {transferDetailsData.isIfx ? (
                <Col md="3" sm="3">

                  <FormGroup>
                    <Label for="description" className="form-label">
                      Fixed Side
                    </Label>
                    <Select
                      className="react-select z-index-2"
                      classNamePrefix="select"
                      options={fixedSideOptions}
                      theme={selectThemeColors}
                      value={fixedSideOptions.find((el) => el.value == transferDetailsData.fixed)}
                      onChange={(e) =>
                        setTransferDetailsData({
                          ...transferDetailsData,
                          fixed: e.value,
                          currency:
                            (e.value == "sender"
                              ? senderAccount?.currencycode
                              : receiverAccount?.currency_code ||
                              receiverAccount?.currencycode) || "EUR",
                        })
                      }
                    />
                  </FormGroup>
                </Col>
              ) : null}
              <Col md={transferDetailsData.isIfx ? 3 : 6} sm={12}>
                <label>Amount</label>

                <InputGroup>
                  <Field
                    name="amount"
                    id="amount"
                    placeholder="499.99"
                    type="number"
                    className={`form-control ${errors.amount && touched.amount && "is-invalid"}`}
                  />
                  <InputGroupAddon addonType="append" style={{zIndex:0}} >
                    <Button
                      color={`${(errors.amount && touched.amount && "danger") || "primary"}`}
                      outline
                      onClick={(e) => e.preventDefault()}
                    >
                      {transferDetailsData?.currency}
                    </Button>
                  </InputGroupAddon>
                </InputGroup>
                <ErrorMessage
                  name="amount"
                  component="div"
                  className="field-error text-danger"
                />
              </Col>
              <Col md={6} sm={12}>
                <label>Description</label>

                <Field
                  className={`form-control ${errors.message && touched.message && "is-invalid"
                    }`}
                  name="message"
                  id="message"
                />
                <ErrorMessage
                  name="message"
                  component="div"
                  className="field-error text-danger"
                />
              </Col>
              <div className="d-flex float-right mt-1 ml-1">
                <Button.Ripple type='submit' color="primary" className="btn-next" disabled={loading || role_type && role_type != 'admin' && !userDetails['transfer'].includes('U')}   >
                  <span className="align-middle ">{loading ? <Spinner size="sm" className="mr-1" /> : null} Next</span>
                  <ArrowRight size={14} className="align-middle ml-sm-25 ml-0" />
                </Button.Ripple>
              </div>
            </Form>
          )}
        </Formik>
      </Row>
    </Col>
  );
}

const CardTextDiv = Styled.div`
text-align-last: center;
font-size:2em;
margin-top:0.5em;
color:green;
`
