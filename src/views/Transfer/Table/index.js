import React,{ useContext, Fragment, useEffect, useState } from "react";
import CustomHeader from "./Header";
import Avatar from "@components/avatar";

// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash, Trash2 } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge } from "reactstrap";
import Loading from "@core/components/spinner/Loading-spinner";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import { useDispatch, useSelector } from "react-redux";
import { DELETE_ROLE, GET_ROLE } from "../../../redux/actions/storeMerchant.";
import { FormContext } from "..";
const Table = () => {
  const {active}=useContext(FormContext)
  const dispatch=useDispatch();
  const{roles}=useSelector(state=>state.merchant);
  const [loading,setLoading]=useState(true);
  const [role,setRoles]=useState([]);
  
  useEffect(()=>{
    //console.log(roles);
    if(!roles.length)dispatch(GET_ROLE(()=>setLoading(false)));
    else setRoles([...roles]);setLoading(false);
  },[roles.length])
  
  if(loading) return <Loading/>

  const renderClient = (row, i) => {
    const stateNum = i % 6,
      states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
      color = states[stateNum];
  
    return <Avatar color={color || "primary"} className="mr-1" content={row.role_name || "John Doe"} initials />;
  };

  const columns = [
    {
      name: "Name",
      selector: "name",
      sortable: true,
      minWidth: "110px",
      
      cell: (row,i) => {
        // let date = moment(row.date_of_transaction);
        return (
          <Fragment>
             {renderClient(row, i)}
         <span className='text-nowrap' >{row.role_name}</span>
          </Fragment>
        );
      },
    },
    {
      name: "Identifier",
      selector: "identifier",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.identifier}</span>
        </Fragment>
      ),
    },
    {
      name: "Created",
      selector: "created",
      minWidth: "170px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span className='text-wrap '>{row.created}</span>
        </Fragment>
      ),
    },

    {
      name: "Action",
      maxWidth: "50px",
      selector: "",
      sortable: true,
      cell: (row) => (
        <Fragment>
        <div className="d-flex " style={{padding:'1em'}}>
          <Trash2 className=" cursor-pointer"  onClick={el=>{el.preventDefault(); dispatch(DELETE_ROLE(row.id))}} size={15} />
          {/* {rowId==row.merchant_id&&Delete?<Spinner size="sm" />:<Trash size={15} className=" cursor-pointer" onClick={e=>{setRowId(row.merchant_id);setDelete(true)}} />} */}
        </div>
        </Fragment>
      ),
    },
    
  ];

  const dataToRender = () => {
    let tempData=role;
    tempData= tempData.filter(el=>el.role_type==active)
    return tempData;
  };

  return (
    <>
    
    <div className="invoice-list-wrapper">
     
      <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>
    </div>
    </>
  );
};

export default Table;
