import React,{useState,useEffect,useRef}  from 'react';
import { Formik ,Field,Form, ErrorMessage} from "formik";
import {Col,FormGroup,Card,Button,Spinner,CardBody,Row,Input} from 'reactstrap';
import * as Yup from "yup";
import {selectThemeColors,currencyCodes} from '@utils';
import Select from "react-select";
import Loading from "@core/components/spinner/Loading-spinner";
import { useDispatch,useSelector } from 'react-redux';
import { UPDATE_MERCHANT } from '../../../redux/actions/storeMerchant.';
import { STORE_MERCHANTACCOUNTS } from '../../../redux/actions/transfer';
import { Axios } from '../../../Api';
import { graphqlServerUrl } from '../../../App';
import notification from '../../../components/notification';



const SettlementTxn =()=>{ 
    const {merchants}=useSelector(state=>state.merchant);
    const {merchantsAccounts}=useSelector(state=>state.transfer);
    const dispatch =useDispatch()
    const formRef = useRef();
  const [loading,setLoading]=useState(false); 
  const [merchantId,setMerchantId]=useState(0);

   useEffect(() => {
       if(!merchants.length) dispatch(UPDATE_MERCHANT());
       if(!merchantsAccounts.length)dispatch(STORE_MERCHANTACCOUNTS())
       
   })


  const initialValues=  { 
    type:'fee_adjustment_credit',
    description:'',
    amount:''
  
  
  }
  const schema = Yup.object().shape({
    type:Yup.string().required(),
    description:Yup.string().required(),
    amount:Yup.string().required(),

  })
  
  const handleSubmit=async(values,{resetForm}) => {
     //console.log(value);
     try{
         setLoading(true);
      await Axios.post(`${graphqlServerUrl}/admin/settlementTxn`,values);
      notification({ title: "Txn ", message:"Txn successfully done ",type:'error'});
      resetForm();
      setLoading(false);

     }catch(e){
        notification({ title: "Error", message: e.toString(),type:'error'});
        setLoading(false);
     }
    }
  
  return ( 
    <Card  className='col-12 col-md-6 mx-auto' >
      <CardBody >
        <Formik
        
        onSubmit={handleSubmit}
        initialValues={initialValues}
        validationSchema={schema}
        >
          { ({values,errors,touched})=>( 
             <Form>
               
              
               <FormGroup tag={Col} md='12'>
                  <label>Merchant</label>
                  <Select
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
               options={merchants.map(el=>({label:el.comp_name,value:el.merchant_id}))}
                isClearable={false}

                onChange={el=>{values.merchant_id=el.value;setMerchantId(el.value)}}
                />
                </FormGroup>
                    
               <FormGroup tag={Col} md='12'>
                  <label>Account</label>
                  <Select
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
               options={merchantsAccounts.filter(e=>e.merchant_id==merchantId&&!e.gateway).map(el=>({label:`${el.account_name}-${currencyCodes[el.currencycode]}${el.current_balance}`,value:el.account_id}))}
                isClearable={false}
                onChange={el=>values.account_id=el.value}
                />
                </FormGroup>
                <FormGroup tag={Col} md='12'>
                  <label>Type</label>
                  <Select
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
               options={['fee_adjustment_credit','fee_adjustment_debit','settlement','charge_back'].map(res=>({label:res.split('_').join(' ').toUpperCase(),value:res}))}
                isClearable={false}
                defaultValue={values.type}
                onChange={el=>values.type=el.value}
                />
                </FormGroup>
               
          
               <FormGroup tag={Col} >
                 <label>Amount</label>
                 <Field
                  name={'amount'}
                  id={'amount'}
                  className={`form-control ${errors.amount && touched.amount&& "is-invalid"}`}
                  />
                  <ErrorMessage name="amount" component="div" className="field-error text-danger" />
  
               </FormGroup>
               <FormGroup tag={Col}>
                 <label>Description</label>
                 <Field
                  name={'description'}
                  type='textarea'
                  id={'description'}
                  className={`form-control ${errors.description && touched.description && "is-invalid"}`}
                 
                 />
                 <ErrorMessage name="description" component="div" className="field-error text-danger" />  
                
                
  
               </FormGroup>
               
               
               
               <Button.Ripple   disabled={loading}  type="submit" className='ml-1' color="primary" outline >
              {loading ? <Spinner size="sm" color="primary" className="mr-1 " /> : null}
              Submit
              </Button.Ripple>
             
         
             </Form>
  
          )
            
          }
        </Formik>
      </CardBody>
    </Card>
  
  
  )
  
  
  }
  export default SettlementTxn;