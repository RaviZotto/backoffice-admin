import React,{ useContext, Fragment, useEffect, useState } from "react";
import Avatar from "@components/avatar";
import moment from 'moment'
import { selectThemeColors, countries as countryOptions } from "@utils";
// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash, Trash2, PlusCircle } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge,CardBody,FormGroup,Button, Input } from "reactstrap";
import Loading from "@core/components/spinner/Loading-spinner";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import { useDispatch, useSelector } from "react-redux";
import { ADD_TRANSFERFEE, DEL_TRANSFERFEE, STORE_TRANSFERFEE, UPDATE_TRANSFERFEE } from "../../redux/actions/transfer";
import Select from "react-select";
import { Formik ,Field,Form, ErrorMessage} from "formik";
import SlideDown from "react-slidedown";
import Row from "reactstrap/lib/Row";
import  currencyCodes  from "../../utility/extras/currencyCodes.json";
import Col from "reactstrap/lib/Col";
import Spinner from "reactstrap/lib/Spinner";
import * as Yup from "yup";
import { makeVar } from "@apollo/client";
import { userDetails } from "../Merchant";
export const rowData=makeVar({});
const TransferFee = () => {
    const dispatch=useDispatch();
    const [loading,setLoading]=useState(true);
    const {transferFee}=useSelector(state=>state.transfer);
    const [Form,setForm]=useState(false);
    const [rowId,setRowId]=useState(0);
    useEffect(()=>{
   
    if(!transferFee.length)dispatch(STORE_TRANSFERFEE(()=>setLoading(false)));
    else setLoading(false);    
},[transferFee])
  
  if(loading) return <Loading/>

  const renderClient = (row, i) => {
    const stateNum = i % 6,
      states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
      color = states[stateNum];
  
    return <Avatar color={color || "primary"} className="mr-1" content={row.currency|| "John Doe"} initials />;
  };

  const columns = [
    {
      name: "Currency",
      selector: "currency",
      sortable: true,
      minWidth: "110px",
      
      cell: (row,i) => {
        // let date = moment(row.date_of_transaction);
        return (
          <Fragment>
             {renderClient(row, i)}
         <span className='text-nowrap' >{row.currency}</span>
          </Fragment>
        );
      },
    },
    {
      name: "fee",
      selector: "fee",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.fee}</span>
        </Fragment>
      ),
    },
    {
        name: "Status",
        selector: "status",
        minWidth: "110px",
        sortable: true,
        cell: (row) => (
          <Fragment>
           
            <span className={`${row.status?"text-success":"text-danger"} fw-5`} >{row.status?"Active":"Inactive"}</span>
          </Fragment>
        ),
      },

      {
        name: "Created",
        selector: "created",
        minWidth: "50px",
        sortable: true,
        cell: (row) => {
            let date = moment(row.created);
         return ( <Fragment>
             <div class='break-word d-block d-flex flex-column'>  
            <span  >{date.format('YYYY-MM-DD')}</span>
            <span  >{date.format('HH:mm:ss')}</span>
            </div>
          </Fragment>)
        },
      },


    {
      name: "Action",
      maxWidth: "50px",
      selector: "",
      sortable: true,
      cell: (row) => (
        <Fragment>
        <div className="d-flex " style={{padding:'1em'}}>
          {/* <Trash2 className=" cursor-pointer mr-1"  onClick={el=>{el.preventDefault();dispatch(DEL_TRANSFERFEE(row.id))   }} size={15} /> */}
          <Edit className=" cursor-pointer" size={15} onClick={el=>{ el.preventDefault(); rowData(row);setForm(true)}} />
          {/* {rowId==row.merchant_id&&Delete?<Spinner size="sm" />:<Trash size={15} className=" cursor-pointer" onClick={e=>{setRowId(row.merchant_id);setDelete(true)}} />} */}
        </div>
        </Fragment>
      ),
    },
    
  ];

  const dataToRender = () => {
    let tempData=transferFee;
   
    return tempData;
  };

  return (
    <>
         {Form ? (
        <SlideDown>
          <CustomForm  setForm={setForm} id={rowId} />
        </SlideDown>
      ) : null}
     <Button.Ripple className='mb-1'      disabled={userDetails.role_type&&userDetails.role_type!='admin' && !userDetails['transfer'].includes('U')}    color='primary' onClick={el=>{setForm(true);rowData({})}}>
      <PlusCircle size={15} className="mr-1" />
       Add Txn Fee
       </Button.Ripple>  
    <div className="invoice-list-wrapper">
     <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>
    </div>
    </>
  );
};

export default TransferFee;

const CustomForm =({setForm,row})=>{ 
  const dispatch =useDispatch()
const [loading,setLoading]=useState(false); 


const initialValues=  { 
  fee:rowData().fee||0, 
  currency: rowData().currency||'GBP',
  fixed_fee:rowData().fixed_fee||0, 
  fee_type:rowData().fee_type||0


}
const schema = Yup.object().shape({
  fee:Yup.number("Number is required").required(),
  fixed_fee:Yup.number("Number is required").required()
})

const handleSubmit=(value) => {
   //console.log(value)
   setLoading(true);
   if(Object.keys(rowData()).length){
    delete value.currency;
    dispatch(UPDATE_TRANSFERFEE({id:rowData().id,...value},()=>setLoading(false)));
   }else{
    dispatch(ADD_TRANSFERFEE(value,()=>setLoading(false)))
   }
  }

return ( 
  <Card  >
    <CardBody >
      <Formik
      onSubmit={handleSubmit}
      initialValues={initialValues}
      validationSchema={schema}
      >
        { ({values,errors,touched})=>( 
           <Form>
             <Row>
             <FormGroup tag={Col} md='6' sm='12'>
            <label>Currency</label>
             <Select
              theme={selectThemeColors}
              className="react-select"
              classNamePrefix="select"
              isDisabled={Object.keys(rowData()).length}
              defaultValue={{label:values.currency,value:values.currency}}
              options={Object.keys(currencyCodes).map(el=>({label:el,value:el}))}
              isClearable={false}
              onChange={el=>values.currency=el.value}
              />
             </FormGroup>
             <FormGroup tag={Col} md='6' sm='12'>
               <label>Fee</label>
               <Field
                name={'fee'}
                id={'fee'}
                className={`form-control ${errors.fee && touched.fee && "is-invalid"}`}
                />
                <ErrorMessage name="fee" component="div" className="field-error text-danger" />

             </FormGroup>
             <FormGroup tag={Col} md='6' sm='12'>
               <label>Fixed Fee</label>
               <Field
                name={'fixed_fee'}
                id={'fixed_fee'}
                className={`form-control ${errors.fixed_fee && touched.fixed_fee && "is-invalid"}`}
                />
                <ErrorMessage name="fixed_fee" component="div" className="field-error text-danger" />

             </FormGroup>
             <FormGroup tag={Col} md='6' sm='12'>
               <label>Fee Type</label>
               <Input
                name={'fee_type'}
                type='select'
                id={'fee_type'}
                defaultValue={values.fee_type}
                onChange={el=>values.fee_type=el.target.value}
                >
                  <option value={0} label="Domestic" />
                  <option value={1} label="SWIFT" />
                </Input>
              
              

             </FormGroup>
             
             </Row>
             <Row>
            <Col md={4} sm={12} >
             <Button.Ripple       disabled={loading|| Object.keys(rowData()).length?userDetails.role_type!='admin' && !userDetails['transfer'].includes('U'):userDetails.role_type!='admin' && !userDetails['transfer'].includes('C') }    type="submit" className='mr-1' color="primary" outline >
            {loading ? <Spinner size="sm" color="primary" className="mr-1" /> : null}
            {Object.keys(rowData()).length? "Update":"Add"}</Button.Ripple>
            <Button.Ripple onClick={e=>{e.preventDefault(); setForm(false);rowData({})} } className='mr-1' color="secondary" outline disabled={loading}>Close</Button.Ripple>
            </Col>
             </Row>
           </Form>

        )
          
        }
      </Formik>
    </CardBody>
  </Card>


)


}