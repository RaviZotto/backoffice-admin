import React, { createContext, useEffect, useMemo, useRef, useState } from "react";
// import Wizard from '@components/wizard'
// import Confirmation from "./Confirmation";
import Accounts from "./Accounts";
// import Loading from "@core/components/spinner/Loading-spinner";
import { useDispatch, useSelector } from "react-redux";
import { GET_PARTNER, GET_ROLE, UPDATE_MERCHANT } from "../../redux/actions/storeMerchant.";
import { GET_COUNTRIES, STORE_BENEFICIARIES, STORE_EXCHANGE, STORE_MERCHANTACCOUNTS, STORE_TRANSFERFEE } from "../../redux/actions/transfer";
export const TransferContext = createContext();
export default function index() {
const ref = useRef(null)


const dispatch =useDispatch()
  const[loading,setLoading]=useState(true);
  const[stepper,setStepper]=useState(null);
  const [transferDetailsData,setTransferDetailsData]=useState({fixed:'sender',isIfx:false});
  const[sendOtp,setSendOtp]=useState(true);
  const {merchantsAccounts,beneficiares,countries}=useSelector(state=>state.transfer);
  const {partners,merchants}=useSelector(state=>state.merchant);

  useEffect(() => {
 
  if(!partners.length)dispatch((GET_PARTNER()));
  if(!merchants.length)dispatch(UPDATE_MERCHANT());
  if(!merchantsAccounts.length)dispatch(STORE_MERCHANTACCOUNTS())
  if(!beneficiares.length)dispatch(STORE_BENEFICIARIES())

  setLoading(false);
  },[partners,merchants,merchantsAccounts,beneficiares])
   
  

 

  // const steps = [
  //   {
  //     id: "account-information",
  //     title: "Account Information",

  //     content:<Accounts stepper={stepper} type="wizard-horizontal" />,
  //   },
  //   // {
  //   //   id: "confirmation",
  //   //   title: "Confirmation",
  //   //   content: <Confirmation stepper={stepper} type="wizard-horizontal" />,
  //   // },
   
  // ];


  return (
    <TransferContext.Provider value={useMemo(()=>({sendOtp,setStepper,stepper, setSendOtp,transferDetailsData,setTransferDetailsData}),[countries,transferDetailsData,sendOtp,stepper])}  >
    
    <Accounts/>
     
    </TransferContext.Provider>
  );
}
