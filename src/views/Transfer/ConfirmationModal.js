import React, { useEffect, useState,useContext} from "react";
import { FormGroup, Button, Spinner, Modal, ModalHeader, ModalBody } from "reactstrap";
import { SlideDown } from "react-slidedown";
import { capitalFirstLetter } from "@utils";
import { Formik, Field, Form } from "formik";
import produce from "immer";
import * as Yup from "yup";
import TransferContext from '.'
import { sendTxnOtp, tranferTxn } from "../../Api";
import notification from "@src/components/notification";
import { calculateTotalFee } from "../../utility/Utils";
import moment from 'moment';

const _sendTxnOtp=async(id,callback)=>{ 
     
    try{
      const val =await  sendTxnOtp({merchant_id:id});
      console.log(val);
       callback({}); 
    }catch(e){ 
    
       notification({title:'Error',message:e,type:'error'});
       callback({err:true})
        
    }  
     
}


const Confirmation = (props) => {
  const { confirmDetailsModal, setConfirmDetailsModal, transferType, transferDetails ,sendOtp,setSendOtp } = props;
  const [loading,setLoading]=useState({l1:false,l2:false});
 
  const {  refetchAccounts, resetTransfer } = props;
  const [transferAccepted, setTransferAccepted] = useState(false);

  const {transferFee,isIfx:isFx} = transferDetails;



  const handleFundTransfer = ({ otp }) => {
    let transferData = {
      account_from_id: String(transferDetails.account_from.account_id),
      amount: transferFee.payer_amount,
      reference: transferDetails.description,
      transfer_type: transferType,
      otp: `${otp}`,
      fast_payment: false,
      settlement_days:transferFee?.settlement_days||"2"
    };
    if (transferType == "external_transfer")
      transferData.beneficiary_identifier = String(transferDetails.beneficiary?.identifier);
    else transferData.account_to_id = String(transferDetails.account_to.account_id);

    if (transferFee.forex_id) transferData.forex_id = transferFee.forex_id;

    setLoading({...loading,l2:true});
   

      tranferTxn({id:transferDetails.account_from.merchant_id,...transferData},({err,res})=>{ 
          if(res){
            setConfirmDetailsModal(false);
          setSendOtp(true);
          resetTransfer();
          refetchAccounts();         
            notification({type:'success',message:"Txn made successfully",title:'Txn'})
          }
          else notification({type:'error',message:err,title:'Error'});
         
          setLoading({...loading,l2:false});
      });
  };

  useEffect(() => !confirmDetailsModal && setTransferAccepted(false), [confirmDetailsModal]);


  return (
    <Modal
      isOpen={confirmDetailsModal}
      toggle={() => setConfirmDetailsModal(!confirmDetailsModal)}
      className="modal-dialog-centered"
      backdrop="static"
    >
      <ModalHeader toggle={() => setConfirmDetailsModal(!confirmDetailsModal)}>Summary</ModalHeader>
      <ModalBody>
        <div className="d-flex justify-content-between">
          <div>Type of transfer</div>
          <div className="font-weight-bold">
            {transferType
              ?.split("_")
              .map((el) => capitalFirstLetter(el))
              .join(" ")}
          </div>
        </div>
        <hr />
        <div className="d-flex justify-content-between">
          <div>From account</div>
          <div className="font-weight-bold">{transferDetails?.account_from.account_name}</div>
        </div>
        {transferType != "internal_transfer" ? (
          <div className="d-flex justify-content-between">
            <div>Account details</div>
            <div className="font-weight-bold">{transferDetails?.account_from.iban || "NA"}</div>
          </div>
        ) : null}
        <hr />
        {transferType == "external_transfer" ? (
          <div className="d-flex justify-content-between">
            <div>Beneficiary</div>
            <div className="font-weight-bold">{transferDetails?.beneficiary.benef_name}</div>
          </div>
        ) : (
          <div className="d-flex justify-content-between">
            <div>To account</div>
            <div className="font-weight-bold">{transferDetails?.account_to.account_name}</div>
          </div>
        )}
        <div className="d-flex justify-content-between">
          <div>Account details</div>
          <div className="font-weight-bold">
            {transferDetails?.beneficiary?.iban_account_no || transferDetails?.account_to?.iban || "NA"}
          </div>
        </div>
        <div className="d-flex justify-content-between">
          <div>Message</div>
          <div className="font-weight-bold">{transferDetails?.message}</div>
        </div>
        <hr />
        <div className="d-flex justify-content-between">
          <div>You are sending</div>
          <div className="font-weight-bold">{`${transferFee?.payer_currency} ${transferFee?.payer_amount}`}</div>
        </div>
        <div className="d-flex justify-content-between">
          <div>Receiver will get</div>
          <div className="font-weight-bold">{`${transferFee?.payee_currency} ${transferFee?.payee_amount}`}</div>
        </div>
        { transferType=='internal_transfer'&&(
          <>
        <div className="d-flex justify-content-between">
          <div>Settlement limit </div>
          <div className="font-weight-bold">{`${transferFee?.payer_currency} ${transferFee?.settlement_available}`}</div>
        </div>    <div className="d-flex justify-content-between">
          <div>Amount will be settled on</div>
          <div className="font-weight-bold">{`${moment(new Date()).add(transferFee?.settlement_days,'days').format("YYYY-MM-DD")} `}</div>
        </div>
        </>
        )
         }
        <div className="d-flex justify-content-between">
          <div>Total Fees</div>
          <div className="font-weight-bold">{`${transferFee?.payer_currency} ${calculateTotalFee(
            transferFee?.payer_amount,
            transferFee?.fees
          )}`}</div>
          
        </div>
        {isFx ? (
          <div className="d-flex justify-content-between">
            <div>Exchange Rate</div>
            <div className="font-weight-bold">{`${transferFee.exchange_rate}`}</div>
          </div>
        ) : null}
        <hr />

        {!transferAccepted ? (
          <Button color="primary" className="mb-1 float-right" onClick={() => {
                setLoading({...loading,l1:true}); _sendTxnOtp(transferDetails.account_from.merchant_id,({err})=>{
                if(err)setSendOtp(true);
                else setTransferAccepted(true);   
                setLoading({...loading,l1:false}) } )
              
            

          }}>
            {loading.l1 ? <Spinner size="sm" className="mr-1" /> : null}
            Accept & Continue
          </Button>
        ) : (
          <Formik
            initialValues={{ otp: "" }}
            validationSchema={Yup.object().shape({
              otp: Yup.string().length(6).required("Required"),
            })}
            onSubmit={handleFundTransfer}
          >
            {({ errors, touched, handleSubmit }) => (
              <Form onSubmit={(e) => e.preventDefault()}>
                <FormGroup>
                  <SlideDown>
                    <Field
                      name="otp"
                      id="otp"
                      type="number"
                      className={`form-control ${errors.otp && touched.otp && "is-invalid"}`}
                      placeholder="Enter 6 digits OTP"
                      autoFocus
                    />
                  </SlideDown>
                  <div className="font-small-1 float-right">
                    <em>* An OTP has been sent to your mobile number and email</em>
                  </div>
                </FormGroup>
                <div className="my-1 float-right">
                  <Button disabled={loading.l1} color="secondary" className="mr-1" onClick={() =>  {  setLoading({...loading,l1:true});
                    _sendTxnOtp(transferDetails.account_from.merchant_id, ()=>setLoading({...loading,l1:false}))}} >
                    {loading.l1 ? <Spinner size="sm" className="mr-1" /> : null}
                    Resend OTP
                  </Button>
                  <Button disabled={loading.l2} color="primary" onClick={handleSubmit}>
                    {loading.l2 ? <Spinner size="sm" className="mr-1" /> : null}
                    Send
                  </Button>
                </div>
              </Form>
            )}
          </Formik>
        )}
      </ModalBody>
    </Modal>
  );
};

export default Confirmation;
