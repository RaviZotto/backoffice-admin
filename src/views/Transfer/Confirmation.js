import React,{ Fragment, useContext, useEffect, useState } from "react";
import {
  Card,
  CardBody,
  CardTitle,
  Row,
  Col,
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  FormGroup,
  Label,
} from "reactstrap";
import { Formik, Field, Form } from "formik";
import { TransferContext } from ".";
import { ArrowLeft, ArrowRight } from "react-feather";
import * as Yup from "yup";
import Spinner from "reactstrap/lib/Spinner";
import "@styles/react/libs/react-select/_react-select.scss";
import { sendTxnOtp, tranferTxn } from "../../Api";
import notification from "../../components/notification";
import { STORE_MERCHANTACCOUNTS } from "../../redux/actions/transfer";
import { useDispatch } from "react-redux";

const _sendTxnOtp=async(id,callback)=>{ 
     
     try{
       const val =await  sendTxnOtp({merchant_id:id});
       console.log(val);
        callback({}); 
     }catch(e){ 
     
        notification({title:'Error',message:'Banking request error',type:'error'});callback({err:true})
         
     }  
      
}


const Confirmation = ({ stepper}) => {
  const { transferDetailsData, setTransferDetailsData, sendOtp, setSendOtp } = useContext(TransferContext);
  const [openOtpModal, setOpenOtpModal] = useState(false);
  //console.log(transferDetailsData,stepper);
  const [loading,setLoading]=useState(false);
  if (!transferDetailsData.fromAccount) return null;

 

  const SenderInfoCardBody = () => {
    return (
      <Fragment>
        <div>Name : {transferDetailsData.fromAccount.account_name}</div>
        {transferDetailsData.fromAccount.iban != "" ? (
          <Fragment>
            <div>IBAN Number : {transferDetailsData.fromAccount.iban}</div>
            <div>BIC/SWIFT Code : {transferDetailsData.fromAccount.bic}</div>
          </Fragment>
        ) : (
          <Fragment>
            <div>IBAN Number : NA</div>
            <div>BIC/SWIFT Code : NA</div>
          </Fragment>
        )}
      </Fragment>
    );
  };

  // const ReceiverInfoCardBody = () => {
  //   return (
  //     <Fragment>
  //       {transferDetailsData.type=="internal_transfer"?                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        Data.type!='external_transfer' ? (
  //         (<Fragment>
  //           <div>Name : {transferDetailsData.toAccount.account_name}</div>
  //           <div>IBAN Number : {transferDetailsData.toAccount.iban}</div>
  //           <div>BIC/SWIFT Code : {transferDetailsData.toAccount.bic}</div>
  //         </Fragment>
  //        ): (
  //         <Fragment>
  //           <div>Name: {transferDetailsData.toAccount.benef_name}</div>
  //           {transferDetailsData.toAccount.account_number && transferDetailsData.toAccount.account_number != "" ? (
  //             <div>Account Number : {transferDetailsData.toAccount.account_number}</div>
  //           ) : (
  //             <div>IBAN Number : {transferDetailsData.toAccount.iban_account_no}</div>
  //           )}
  //           <div>BIC/SWIFT Code : {transferDetailsData.toAccount.bic_code}</div>
  //         </Fragment>
  //       )
  //     </Fragment>
  //   );
  // };

  return (
    <Fragment>
      <Row>
        <Col md="6" sm="6">
          <Card className="bg-transparent border-primary shadow-none">
            <CardBody>
              <CardTitle className="mb-1">Sender's Info</CardTitle>
              <SenderInfoCardBody />
            </CardBody>
          </Card>
        </Col>
        <Col md="6" sm="6">
          <Card className="bg-transparent border-primary shadow-none">
            <CardBody>
              <CardTitle className="mb-1">Receiver's Info</CardTitle>
              <ReceiverInfoCardBody />
            </CardBody>
          </Card>
        </Col>
        <Col md="6" sm="6">
          <Card className="bg-transparent border-primary shadow-none">
            <CardBody>
              <CardTitle className="mb-1">Transfer Info</CardTitle>
              <div>
                <div>Transfer Amount : {transferDetailsData.amount}</div>
                <div>Fees : {transferDetailsData.fee}</div>
                <div>Exchange Rate : {Number(transferDetailsData.rate).toFixed(2)}</div>
                <div>Equivalent Amount : {Number(transferDetailsData.equivalent_amount).toFixed(2)}</div>
                <div>Transfer Description: {transferDetailsData.message}</div>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <div className="d-flex justify-content-between">
        <Button.Ripple color="secondary" className="btn-prev" outline onClick={() => stepper.previous()}>
          <ArrowLeft size={14} className="align-middle mr-sm-25 mr-0"></ArrowLeft>
          <span className="align-middle ">Previous</span>
        </Button.Ripple>
        <Button.Ripple
          type="submit"
          disabled={loading}
          color="primary"
          className="btn-next"
          onClick={el=>{ 
            if(sendOtp){setSendOtp(false);setLoading(true); _sendTxnOtp(transferDetailsData.fromAccount.merchant_id,({err})=>{
              if(!err)setOpenOtpModal(true);
              setLoading(false) } )}
            else{setOpenOtpModal(true);}
          }}
          >
          <span className="align-middle "  >{loading?<Spinner size="sm" className="mr-1"/>:null}Confirm</span>
          <ArrowRight size={14} className="align-middle ml-sm-25 ml-0"></ArrowRight>
        </Button.Ripple>
      </div>
      <OtpModal openOtpModal={openOtpModal} setOpenOtpModal={setOpenOtpModal} />
    </Fragment>
  );
};

const OtpModal = ({ openOtpModal, setOpenOtpModal, resendOtp }) => {
  const dispatch = useDispatch();
  const [waitTime, setWaitTime] = useState(180);
  const { transferDetailsData, sendOtp, reset, setReset,setSendOtp } = useContext(TransferContext);
  const [loading,setLoading]=useState(false);
  const [loading1,setLoading1]=useState(false);
  useEffect(() => {
    !sendOtp && waitTime > 0 && setTimeout(() => setWaitTime(waitTime - 1), 1000);
  }, [waitTime, sendOtp]);

  useEffect(() => {
    // if (fundTransferStatus && fundTransferStatus.fundTransferStatus === true) {
    //   setOpenOtpModal(false);
    //   setReset(!reset);
    // }
  }, []);

  const otpFormSchema = Yup.object().shape({
    otp: Yup.number().required("Required"),
  });

  const transferFunds = (otp) => {
    setLoading(true);
    let transferData = {
      account_from_id: String(transferDetailsData.fromAccount.account_id),
      amount: String(Number(transferDetailsData.amount).toFixed(2)),
      reference: transferDetailsData.message,
      otp: otp,
      fast_payment: false,
      transfer_type: transferDetailsData.isInternalTransfer?0:1
    };
    transferDetailsData.isInternalTransfer
      ? (transferData.account_to_id = String(transferDetailsData.toAccount.account_id))
      : (transferData.beneficiary_id = String(transferDetailsData.toAccount.id));
      tranferTxn({id:transferDetailsData.fromAccount.merchant_id,...transferData},({err,res})=>{ 
          if(res)notification({type:'success',message:"Txn made successfully",title:'Txn'});
          else notification({type:'error',message:err,title:'Error'});
          setWaitTime(0);
          setOpenOtpModal(!openOtpModal);
           dispatch(STORE_MERCHANTACCOUNTS(()=>setLoading(false),setSendOtp(true)))
          
      });
  };

  return (
    <Modal
      isOpen={openOtpModal}
      toggle={() => {
        setOpenOtpModal(!openOtpModal);
      }}
      className="modal-dialog-centered"
    >
      <ModalHeader
        toggle={() => {
          setOpenOtpModal(!openOtpModal);
        }}
      >
        Verify OTP
      </ModalHeader>
      <ModalBody>
        <div>OTP was sent to your registered Phone number and Email.</div>
        <br />
        <Formik
          initialValues={{ otp: "" }}
          onSubmit={(values) => transferFunds(values.otp)}
          validationSchema={otpFormSchema}
        >
          {({ errors, touched }) => (
            <Form>
              <FormGroup>
                <Label for="otp" className="form-label">
                  Enter OTP
                </Label>
                <Field name="otp" id="otp" className={`form-control ${errors.otp && touched.otp && "is-invalid"}`} />
              </FormGroup>
              <div className="d-flex justify-content-between">
                <Button.Ripple type="submit" color="primary" disabled={loading} >
                  {loading? <Spinner size="sm" className="mr-1" /> : null}
                  Submit
                </Button.Ripple>
                <Button.Ripple
                  disabled={waitTime !== 0|| loading1}
                  color="secondary"
                  onClick={() => {
                    setLoading1(true);
                    _sendTxnOtp(transferDetailsData.fromAccount.merchant_id, ()=>setWaitTime(180),setLoading1(false))
                  }}
                >
                  {waitTime === 0 ? (loading1?(<Spinner size="sm" className="mr-1" />):"")+"Resend OTP" : "Resend in " + waitTime}
                </Button.Ripple>
              </div>
              <div></div>
            </Form>
          )}
        </Formik>
      </ModalBody>
    </Modal>
  );
};

export default Confirmation;
