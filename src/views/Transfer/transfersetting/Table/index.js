import React,{ useContext, Fragment, useEffect, useState } from "react";
import CustomHeader from "./Header";
import Avatar from "@components/avatar";

// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge } from "reactstrap";
import moment from "moment";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import Loading from "@core/components/spinner/Loading-spinner";
import Spinner from "reactstrap/lib/Spinner";
import CustomInput from "reactstrap/lib/CustomInput";
import { UPDATE_MERCHANT_TYPE, UPDATE_MERCHANT } from "../../../../redux/actions/storeMerchant.";
import { useDispatch, useSelector } from "react-redux";
import { updateMerchant } from "../../../../Api";
import { userDetails } from "../../../../utility/Utils";


const Table = ({history,refetch}) => {
  const dispatch =useDispatch()

  const [selectedPartner, setSelectedPartner]=useState();
  const [searchQuery,setSearchQuery]=useState();
  const [Delete,setDelete]=useState(false);
  const {merchants}=useSelector(state=>state.merchant);
  const [merchant,setMerchant]=useState([]);
  const [loading,setLoading]=useState(true);
  const {role_type}=userDetails||{};
  useEffect(()=>{
    if(!merchants.length)dispatch(UPDATE_MERCHANT(()=>setLoading(false))) 
    if(merchants.length){
      setMerchant([...merchants]);
      //console.log(merchant);
      setLoading(false)
    }
    
  },[merchants])

 if(loading)return <Loading />

  const renderClient = (row, i) => {
    const stateNum = i % 6,
      states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
      color = states[stateNum];
  
    return <Avatar color={color || "primary"} className="mr-1" content={row.comp_name || "John Doe"} initials />;
  };

  const columns = [
    {
      name: "Name",
      selector: "name",
      sortable: true,
      minWidth: "150px",
      
      cell: (row,i) => {
        // let date = moment(row.date_of_transaction);
        return (
          <Fragment>
             {renderClient(row, i)}
         <span className='text-wrap'>{row.comp_name}</span>
          </Fragment>
        );
      },
    },
    {
      name: "Phone",
      selector: "phone",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.phonecode+row.phone}</span>
        </Fragment>
      ),
    },
    {
      name: "Email",
      selector: "email",
      minWidth: "150px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span className='text-wrap '>{row.email}</span>
        </Fragment>
      ),
    },
    {
      name:"Settlement Percentage",
      maxWidth:"50px",
      selector: "settlment_pct",
      sortable:false,
      cell: (row) => (
        <input 
        disabled={role_type&&role_type!='admin' && !userDetails['transfer']?.includes('U')}   
        className="form-control text-center" type="number" defaultValue={Number(row.settlement_pct)}
        onChange={el=>dispatch(UPDATE_MERCHANT_TYPE(row.merchant_id,{settlement_pct:el.target.value}))}
        />
      )

    },
    {
      name:"Settlement days",
      maxWidth:"50px",
      selector: "settlment_days",
      sortable:false,
      cell: (row) => (
        <input 
         disabled={role_type&&role_type!='admin' && !userDetails['transfer']?.includes('U')}   
        className="form-control text-center" type="number" defaultValue={Number(row?.settlement_days??2)} 
        onChange={el=>dispatch(UPDATE_MERCHANT_TYPE(row.merchant_id,{settlement_days:el.target.value}))}
        />
      )

    },
    {
      name: "Swift",
      maxWidth: "50px",
      selector: "is_swift",
      sortable: true,
      cell: (row) => (
        <CustomInput
          type="switch"
          className={row.id ? "custom-control-success " : ""}
          id={`is_swift-${row.id}`}
         defaultChecked={row.is_swift==1 ? true: false}
         disabled={role_type&&role_type!='admin' && !userDetails['transfer']?.includes('U')}   
          name="customSwitch"
          onChange={el=>dispatch(UPDATE_MERCHANT_TYPE(row.merchant_id,{is_swift:!row.is_swift}))}
          inline
        />
      ),
    },
    {
      name: "SEPA",
      maxWidth: "50px",
      selector: "is_sepa",
      sortable: true,
      cell: (row) => (
        <CustomInput
          type="switch"
          disabled={userDetails?.role_type&&userDetails?.role_type!='admin' && !userDetails['transfer']?.includes('U')}   
          className={row.id ? "custom-control-success " : ""}
          id={`is_sepa-${row.id}`}
          defaultChecked={row.is_sepa==1 ? true: false}
          name="customSwitch"
          onChange={el=>dispatch(UPDATE_MERCHANT_TYPE(row.merchant_id,{is_sepa:!row.is_sepa}))}
          inline
        />
      ),
    },
    {
      name: "Express",
      maxWidth: "50px",
      selector: "is_express",
      sortable: true,
      cell: (row) => (
        <CustomInput
          type="switch"
          disabled={role_type&&role_type!='admin' && !userDetails['transfer']?.includes('U')}   
          className={row.id ? "custom-control-success " : ""}
          id={`is_express-${row.id}`}
          defaultChecked={row.is_express==1 ? true: false}
          name="customSwitch"
          onChange={el=>dispatch(UPDATE_MERCHANT_TYPE(row.merchant_id,{is_express:!row.is_express}))}
          inline
        />
      ),
    },
  ];

  const dataToRender = () => {
    let tempData=merchant;
    //console.log(selectedPartner,tempData)
     if(selectedPartner){
        //console.log(selectedPartner);
        tempData=tempData.filter(el=>el.partner_id==selectedPartner);
     }
     if(searchQuery){
       let searchClone=searchQuery;
       searchClone=searchClone.toLowerCase();
       tempData=tempData.filter(el=>el.comp_name&& el.comp_name.toLowerCase().includes(searchClone)
       || el.email&&el.email.toLowerCase().includes(searchClone)||el.phone&&el.phone_code&&(el.phone_code+el.phone).includes(searchClone)
       );
     }
    return tempData;
  };

  return (
    <div className="invoice-list-wrapper">
      <CustomHeader history={history} setSelectedPartner={setSelectedPartner} setSearchQuery={setSearchQuery} />
      <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>
    </div>
  );
};

export default Table;
