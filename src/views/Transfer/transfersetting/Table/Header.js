import React, { useEffect, useState } from "react";
import { Fragment } from "react";
import { PlusCircle, Search } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import { Row, Col, Button, Input, Card } from "reactstrap";
import { GET_PARTNER } from "../../../../redux/actions/storeMerchant.";
import styled  from 'styled-components'
const CustomHeader = ({ history ,setSelectedPartner,setSearchQuery}) => {
const [partner,setPartner]=useState([{}]);
const dispatch=useDispatch();
const {partners}=useSelector(state=>state.merchant);
  useEffect(() => {
    if(partners&&!partners.length){
       dispatch(GET_PARTNER());
    }else{
      setPartner(partners.map(el=>({label:el.comp_name,value:el.partner_id})));
    }
    

  }, [partners])
  return (
    <Fragment>
   
      <Card className='p-1'>
        <Row className="align-items-center justify-content-between">
        <Col md={4} sm={12} className="align-items-center d-flex" > 
        <Search/>       
        <Input name='serachQuery' style={{border:'none'}} placeholder='search by name'
        onChange={el=>setSearchQuery(el.target.value)}
        />
        </Col>
        <Col md={4} sm={12} >
        
        <Selectnew
         placeholder='select partner'
         id='partner_select'
         style={{"zIndex":99999}}
         menuPlacement="bottom"
         menuPosition="relative"
         options={partner}
         onChange={el=> {setSelectedPartner(el.value)}}
                  
        />
        </Col>
       </Row>
        </Card>
    </Fragment>
  );
};

export default CustomHeader;
const Selectnew = styled(Select)`
   z-index:99999;
`