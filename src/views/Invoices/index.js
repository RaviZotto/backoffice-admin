import { useState, createContext, useMemo, useEffect } from "react";
import { makeVar, useQuery } from "@apollo/client";
import { GET_INVOICES } from "@graphql/queries";
import moment from "moment";
import Loading from "@core/components/spinner/Loading-spinner";
import InvoicesTable from "./Table";
import SecondaryActions from "./SecondaryActions";

export const InvoicesContext = createContext(null);
export const selectedInvoice = makeVar([]);

const Invoices = () => {
  // Making one week time interval for transactions query
  // let temp = "2020-10-08"; // date offset for testing
  const d = new Date();
  d.setDate(d.getDate() - 6);

  const [dates, setDates] = useState({
    startDate: moment(d).format("YYYY-MM-DD"),
    endDate: moment().format("YYYY-MM-DD"),
  });

  // Getting all the invoices
  const { loading, data } = useQuery(GET_INVOICES, { variables: dates });

  // component logic starts here
  const [searchQuery, setSearchQuery] = useState("");
  const [statusQuery, setStatusQuery] = useState("");
  const [invoices, setInvoices] = useState([]);
  const [invoicesQueue, setInvoicesQueue] = useState([]);

  // setting provider value for all child components
  const providerValue = useMemo(
    () => ({
      dates,
      setDates,
      searchQuery,
      setStatusQuery,
      statusQuery,
      setSearchQuery,
      invoicesQueue,
      setInvoicesQueue,
      invoices,
      setInvoices,
    }),
    [dates, searchQuery, statusQuery, invoices, invoicesQueue]
  );

  useEffect(() => {
    if (data) setInvoices(data.invoices);
  }, [data]);

  if (loading) return <Loading />;

  return (
    <InvoicesContext.Provider value={providerValue}>
      <InvoicesTable />
      <SecondaryActions />
    </InvoicesContext.Provider>
  );
};

export default Invoices;
