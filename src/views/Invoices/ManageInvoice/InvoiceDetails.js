import { useEffect, useState, useContext } from "react";
import { useQuery } from "@apollo/client";
import Flatpickr from "react-flatpickr";
import { Plus } from "react-feather";
import Select, { components } from "react-select";
import { Card, CardHeader, CardTitle, CardBody, Row, Col, FormGroup, Label, Button } from "reactstrap";
import { selectThemeColors } from "@utils";
import { GET_CURRENCIES } from "@graphql/queries";
import { GET_CLIENTS } from "@graphql/queries";
import { InvoicesContext } from ".";
import moment from "moment";

import "@styles/react/libs/react-select/_react-select.scss";
import "@styles/react/libs/flatpickr/flatpickr.scss";

const InvoiceDetails = () => {
  const { loading: loading1, data: clientsData } = useQuery(GET_CLIENTS);
  const { loading: loading2, data: currencyData } = useQuery(GET_CURRENCIES);
  const { invoiceInfo, setInvoiceInfo } = useContext(InvoicesContext);

  // select options
  const [clientsOptions, setClientsOptions] = useState([
    {
      value: "add-new",
      label: "Add New Client",
      type: "button",
      color: "flat-success",
    },
  ]);
  const [currencyOptions, setCurrencyOptions] = useState([]);
  const paymentMethodOptions = [
    { label: "Card", value: "1" },
    { label: "Open Banking", value: "2" },
    { label: "Both", value: "3" },
  ];

  const clientsOptionComponent = ({ data, ...props }) => {
    if (data.type === "button") {
      return (
        <Button className="text-left rounded-0" color={data.color} block onClick={() => setClientSidebar(true)}>
          <Plus size={14} /> <span className="align-middle ml-50">{data.label}</span>
        </Button>
      );
    } else {
      return <components.Option {...props}> {data.label} </components.Option>;
    }
  };

  // handle change of any key in invoiceInfo
  const handleChange = (newValue) => setInvoiceInfo({ ...invoiceInfo, ...newValue });

  useEffect(() => {
    if (clientsData) {
      setClientsOptions([
        ...clientsOptions,
        ...clientsData.clients.map((el) => ({ label: el.client_name, value: el.id })),
      ]);
    }
    if (currencyData) {
      let { currencies } = currencyData;
      let arr = new Set();
      currencies.forEach((el) => arr.add(el.currencycode));
      arr = Array.from(arr);

      setCurrencyOptions(arr.map((el) => ({ label: el, value: el })));
    }
  }, [clientsData, currencyData]);

  if (loading1 || loading2) return null;

  return (
    <Card>
      <CardHeader>
        <CardTitle>Invoice Details</CardTitle>
      </CardHeader>
      <CardBody>
        <Row>
          <Col md="3" sm="6">
            <FormGroup>
              <Label>Client</Label>
              <Select
                className="react-select"
                classNamePrefix="select"
                id="label"
                value={clientsOptions.find((el) => invoiceInfo.client_uniqid == el.value)}
                onChange={(e) => handleChange({ client_uniqid: e.value })}
                options={clientsOptions}
                theme={selectThemeColors}
                components={{ Option: clientsOptionComponent }}
              />
            </FormGroup>
          </Col>
          <Col md="3" sm="6">
            <FormGroup>
              <Label>Due Date</Label>
              <Flatpickr
                options={{
                  dateFormat: "Y-m-d",
                  altInput: true,
                  altFormat: "F j, Y",
                }}
                placeholder="Set Due Date"
                value={invoiceInfo.due_date || null}
                onChange={(e) => handleChange({ due_date: moment(e[0]).format("YYYY-MM-DD") })}
                className="form-control"
                size={9}
              />
            </FormGroup>
          </Col>
          <Col md="3" sm="6">
            <FormGroup>
              <Label>Payment Method</Label>
              <Select
                className="react-select"
                options={paymentMethodOptions}
                value={paymentMethodOptions.find((el) => el.value == invoiceInfo.redirect_type)}
                onChange={(e) => handleChange({ redirect_type: e.value })}
                classNamePrefix="select"
                id="label"
                theme={selectThemeColors}
              />
            </FormGroup>
          </Col>
          <Col md="3" sm="6">
            <FormGroup>
              <Label>Currency</Label>
              <Select
                className="react-select"
                options={currencyOptions}
                value={currencyOptions.find((el) => el.value == invoiceInfo.currency)}
                onChange={(e) => handleChange({ currency: e.value })}
                classNamePrefix="select"
                id="label"
                theme={selectThemeColors}
              />
            </FormGroup>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

export default InvoiceDetails;
