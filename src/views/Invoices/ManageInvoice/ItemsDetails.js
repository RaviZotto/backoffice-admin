/*
items
logic to calculate total, subtotal, tax, discount, total
notes
*/
import {
  Card,
  CardHeader,
  CardTitle,
  CardFooter,
  Input,
  Button,
  Table,
  Label,
  Collapse,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
import { Trash2, PlusCircle } from "react-feather";
import { useContext, useEffect, useState } from "react";
import { formatMoney, currencyCodes } from "@utils";
import { InvoicesContext } from ".";
import produce from "immer";

export class Item {
  constructor(data) {
    this.id = data ? data.id + "" : null;
    this.item = data ? data.item : "";
    this.description = data ? data.description : "";
    this.rate = data ? data.rate : "";
    this.quantity = data ? data.quantity : "";
    this.key = Math.random();
  }
  getTotal() {
    return Math.round(this.quantity * this.rate * 100) / 100;
  }
  dataFilled() {
    if (this.getTotal() > 0 && this.item.length > 0) return true;
    return false;
  }
}

export const ItemsDetails = () => {
  const { items, setItems, invoiceInfo, setInvoiceInfo } = useContext(InvoicesContext);

  const handleItemsChange = ({ index, key, value, action }) => {
    setItems(
      produce([...items], (x) => {
        switch (action) {
          case "add":
            x.push(new Item());
            break;
          case "delete":
            x.pop();
            break;
          default:
            x[index][key] = value;
        }
      })
    );
  };

  const handleInvoiceInfoChange = (newValue) => setInvoiceInfo({ ...invoiceInfo, ...newValue });

  // calculate total amount in real time
  useEffect(() => {
    let { discount, tax } = invoiceInfo;
    let base_amount = 0;
    items.forEach((el) => (base_amount += el.getTotal() ? el.getTotal() : 0));
    let total_amount = base_amount + (base_amount * tax) / 100 - (base_amount * discount) / 100;
    total_amount = Math.round(total_amount * 100) / 100;
    handleInvoiceInfoChange({ base_amount, total_amount });
  }, [items, invoiceInfo.discount, invoiceInfo.tax]);

  return (
    <Card>
      <CardHeader>
        <CardTitle>Items Details</CardTitle>
        <div>
          <Button onClick={() => handleItemsChange({ action: "add" })} className="btn-icon" color="flat">
            <PlusCircle size={16} />
          </Button>
          <Button onClick={() => handleItemsChange({ action: "delete" })} className="btn-icon" color="flat">
            <Trash2 size={16} />
          </Button>
        </div>
      </CardHeader>
      <Table responsive style={{ overflow: "hidden" }} borderless={true}>
        <thead>
          <tr>
            <th>Description</th>
            <th>Quantity</th>
            <th>Rate</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {items.map((el, i) => (
            <ItemRow data={el} index={i} handleChange={handleItemsChange} currency={invoiceInfo.currency} />
          ))}

          {/* Sub Total */}
          <tr>
            <td className="w-50"></td>
            <td className="w-10 font-weight-bolder text-uppercase font-small-3">Sub Total</td>
            <td className="w-10"></td>
            <td className="w-30">
              <InputGroup className="input-group-merge disabled">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>{currencyCodes[invoiceInfo.currency]}</InputGroupText>
                </InputGroupAddon>
                <Input disabled value={formatMoney(invoiceInfo.base_amount)} />
              </InputGroup>
            </td>
          </tr>
          {/* Discount */}
          <tr>
            <td className="w-50"></td>
            <td className="w-10 font-weight-bolder text-uppercase font-small-3">Discount</td>
            <td className="w-10">
              <InputGroup className="input-group-merge">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>%</InputGroupText>
                </InputGroupAddon>
                <Input
                  defaultValue={invoiceInfo.discount}
                  onChange={(e) => handleInvoiceInfoChange({ discount: e.target.value })}
                />
              </InputGroup>
            </td>
            <td className="w-30">
              <InputGroup className="input-group-merge disabled">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>{currencyCodes[invoiceInfo.currency]}</InputGroupText>
                </InputGroupAddon>
                <Input disabled value={formatMoney((invoiceInfo.base_amount * invoiceInfo.discount) / 100)} />
              </InputGroup>
            </td>
          </tr>
          {/* Tax */}
          <tr>
            <td className="w-50"></td>
            <td className="w-10 font-weight-bolder text-uppercase font-small-3">Tax</td>
            <td className="w-10">
              <InputGroup className="input-group-merge">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>%</InputGroupText>
                </InputGroupAddon>
                <Input
                  defaultValue={invoiceInfo.tax}
                  onChange={(e) => handleInvoiceInfoChange({ tax: e.target.value })}
                />
              </InputGroup>
            </td>
            <td className="w-30">
              <InputGroup className="input-group-merge disabled">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>{currencyCodes[invoiceInfo.currency]}</InputGroupText>
                </InputGroupAddon>
                <Input disabled value={formatMoney((invoiceInfo.base_amount * invoiceInfo.tax) / 100)} />
              </InputGroup>
            </td>
          </tr>
          {/* Total */}
          <tr>
            <td className="w-50"></td>
            <td className="w-10 font-weight-bolder text-uppercase font-small-3">Total</td>
            <td className="w-10"></td>
            <td className="w-30">
              <InputGroup className="input-group-merge disabled">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>{currencyCodes[invoiceInfo.currency]}</InputGroupText>
                </InputGroupAddon>
                <Input disabled value={formatMoney(invoiceInfo.total_amount)} />
              </InputGroup>
            </td>
          </tr>
        </tbody>
      </Table>
      <CardFooter>
        <div className="form-label-group">
          <Input
            defaultValue={invoiceInfo.notes}
            onBlur={(e) => handleInvoiceInfoChange({ notes: e.target.value })}
            type="textarea"
            placeholder="Invoice Note"
          />
          <Label>Invoice Note</Label>
        </div>
      </CardFooter>
    </Card>
  );
};

function ItemRow({ data, index, handleChange, currency }) {
  const [open, setOpen] = useState(false);

  return (
    <tr>
      <td className="w-50" onFocus={() => setOpen(true)}>
        <div className="lead collapse-title collapsed" id={`desc-${index}`}>
          <Input
            type="text"
            placeholder="Name"
            defaultValue={data.item}
            style={{ minWidth: "200px" }}
            onBlur={(e) => handleChange({ key: "item", value: e.target.value, index })}
          />
        </div>

        <Collapse isOpen={open}>
          <Input
            type="text"
            bsSize="sm"
            placeholder="Description"
            style={{ minWidth: "150px", marginTop: "0.5rem" }}
            defaultValue={data.description}
            onBlur={(e) => handleChange({ key: "description", value: e.target.value, index })}
          />
        </Collapse>
      </td>
      <td className="w-10" style={{ minWidth: "100px" }} onFocus={() => setOpen(false)}>
        <Input
          defaultValue={data.quantity}
          onChange={(e) => handleChange({ key: "quantity", value: e.target.value, index })}
          type="number"
        />
      </td>
      <td className="w-10" style={{ minWidth: "150px" }} onFocus={() => setOpen(false)}>
        <InputGroup className="input-group-merge">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>{currencyCodes[currency]}</InputGroupText>
          </InputGroupAddon>
          <Input
            defaultValue={data.rate}
            onChange={(e) => handleChange({ key: "rate", value: e.target.value, index })}
          />
        </InputGroup>
      </td>
      <td className="w-30" style={{ minWidth: "200px" }}>
        <InputGroup className="input-group-merge disabled">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>{currencyCodes[currency]}</InputGroupText>
          </InputGroupAddon>
          <Input disabled value={formatMoney(data.getTotal() ? data.getTotal() : 0)} />
        </InputGroup>
      </td>
    </tr>
  );
}
