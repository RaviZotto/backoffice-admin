// component for edit & duplicate actions;
import React,{ createContext, useEffect, useMemo, useState } from "react";
import { useLazyQuery, useMutation } from "@apollo/client";
import { GET_INVOICE_ITEMS } from "@graphql/queries";
import { ADD_INVOICE, UPDATE_INVOICE } from "@graphql/mutations";
import InvoiceDetails from "./InvoiceDetails";
import Loading from "@core/components/spinner/Loading-spinner";
import { Item, ItemsDetails } from "./ItemsDetails";
import { Button, Spinner } from "reactstrap";
import { selectedInvoice } from "..";
import produce from "immer";
import moment from "moment";
import { useHistory } from "react-router-dom";
import "@styles/base/pages/app-invoice.scss";

export const InvoicesContext = createContext();
const initialInvoiceInfo = {
  client_uniqid: "",
  currency: "EUR",
  due_date: "",
  redirect_type: "",
  discount: "",
  tax: "",
  base_amount: "",
  total_amount: "",
  notes: "",
};

const ManageInvoice = () => {
  const history = useHistory();
  const [addInvoice, { loading: addingInvoice }] = useMutation(ADD_INVOICE);
  const [updateInvoice, { loading: updatingInvoice }] = useMutation(UPDATE_INVOICE);
  const [getInvoiceItems, { loading: loadingItems, data: itemsData }] = useLazyQuery(GET_INVOICE_ITEMS);
  const [items, setItems] = useState([new Item()]);
  const [invoiceInfo, setInvoiceInfo] = useState(initialInvoiceInfo);

  const handleDiscard = () => {
    setInvoiceInfo(initialInvoiceInfo);
    setItems([new Item()]);
    selectedInvoice([]);
  };

  const handleSubmit = () => {
    let [_, action] = selectedInvoice();
    let finalItems = produce([...items], (n) => {
      n = JSON.parse(JSON.stringify(n));
      n.forEach((item, i) => {
        let originalItem = itemsData ? itemsData.invoice_items[i] : null;
        if (originalItem && action == "edit") item.id = originalItem.id;
        else delete item.id;
        delete item.key;
      });
      return n;
    });

    let invoice = {};
    Object.keys(initialInvoiceInfo).forEach((key) => {
      invoice[key] = `${invoiceInfo[key]}`;
    });

    let mutationVar = { variables: { invoice_details: invoice, item_details: finalItems } };

    if (action == "edit") {
      mutationVar.variables.invoice_details.invoice_number = _.invoice_number;
      updateInvoice(mutationVar);
      history.push("/invoices");
    } else {
      invoice.invoice_date = moment().format("YYYY-MM-DD HH:mm:ss");
      addInvoice(mutationVar);
    }
  };

  const ProviderValue = useMemo(
    () => ({
      items,
      setItems,
      invoiceInfo,
      setInvoiceInfo,
    }),
    [items, invoiceInfo]
  );

  // Get selected invoices from invoices table page
  useEffect(() => {
    let currInvoiceData = selectedInvoice();
    if (currInvoiceData.length > 0) {
      let [invoice, action] = currInvoiceData;
      getInvoiceItems({ variables: { invoiceId: invoice.id } });
      setInvoiceInfo(invoice);
    }
  }, [selectedInvoice()]);

  // set items if selectedInvoice is present
  useEffect(() => {
    if (itemsData) {
      let temp = [];
      itemsData.invoice_items.forEach((el) => {
        temp.push(new Item(el));
      });
      setItems(temp);
    }
  }, [itemsData]);

  if (loadingItems) return <Loading />;

  return (
    <InvoicesContext.Provider value={ProviderValue}>
      <InvoiceDetails />
      <ItemsDetails />
      <div className="d-flex justify-content-start">
        <Button.Ripple disabled={addingInvoice || updatingInvoice} onClick={handleDiscard} color="danger">
          Discard
        </Button.Ripple>
        <Button.Ripple disabled={addingInvoice || updatingInvoice} className="mx-2" color="primary">
          Preview
        </Button.Ripple>
        <Button.Ripple color="primary" outline disabled={addingInvoice || updatingInvoice} onClick={handleSubmit}>
          {addingInvoice || updatingInvoice ? <Spinner size="sm" className="mr-1" /> : null}
          {addingInvoice || updatingInvoice ? "Saving" : "Save"}
        </Button.Ripple>
      </div>
    </InvoicesContext.Provider>
  );
};

export default ManageInvoice;
