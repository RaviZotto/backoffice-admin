import { useContext, Fragment } from "react";
import CustomHeader from "./Header";
import { InvoicesContext } from "../index";
import { formatMoney } from "@utils";
import { selectedInvoice } from "..";

// ** Third Party Components
import { ChevronDown, Eye, Download, MoreVertical, Edit, Trash, Copy, Printer } from "react-feather";
import { Card, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import DataTable from "react-data-table-component";
import { useHistory } from "react-router-dom";
import produce from "immer";
import moment from "moment";

// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";

const Table = () => {
  const history = useHistory();
  const { invoicesQueue, setInvoicesQueue, invoices } = useContext(InvoicesContext);

  const handleAction = (row, action) => {
    if (action == "edit" || action == "create" || action == "duplicate") {
      selectedInvoice([row, action]);
      history.push("/manage-invoice");
    } else {
      let newInvoice = { ...row };
      newInvoice.action = action;
      setInvoicesQueue(
        produce(invoicesQueue, (n) => {
          n.push(row);
        })
      );
    }
  };

  const columns = [
    {
      name: "Issue Date",
      selector: "invoice_date",
      sortable: true,
      minWidth: "110px",
      cell: (row) => {
        return (
          <Fragment>
            <div>
              <div className="text-uppercase font-weight-bold font-small-3">
                {moment(row.invoice_date).format("YYYY-MM-DD")}
              </div>
            </div>
          </Fragment>
        );
      },
    },
    {
      name: "Due Date",
      selector: "due_date",
      sortable: true,
      minWidth: "110px",
      cell: (row) => {
        return (
          <Fragment>
            <div>
              <div className="text-uppercase font-weight-bold font-small-3">
                {moment(row.due_date).format("YYYY-MM-DD")}
              </div>
            </div>
          </Fragment>
        );
      },
    },
    {
      name: "Invoice Number",
      selector: "invoice_number",
      sortable: false,
      minWidth: "130px",
      cell: (row) => {
        return <div className="font-small-3">{row.invoice_number}</div>;
      },
    },
    {
      name: "Bill To",
      sortable: false,
      cell: (row) => {
        return <div className="font-small-2">{row.client_name.toUpperCase()}</div>;
      },
    },
    {
      name: "Status",
      selector: "is_paid",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
          <div
            className={row.is_paid == 0 ? "bg-danger" : row.is_paid == 1 ? "bg-success" : "bg-primary"}
            style={{
              height: "10px",
              width: "10px",
              borderRadius: "50%",
              display: "inline-block",
              marginRight: "5px",
            }}
          />
          <span>{row.is_paid == 0 ? "Unpaid" : row.is_paid == 1 ? "Paid" : "Pending"}</span>
        </Fragment>
      ),
    },
    {
      name: "Amount",
      sortable: false,
      cell: (row) => (
        <div className={`${row.is_paid == 1 ? "text-success" : "text-danger"} font-weight-bold`}>
          {formatMoney(row.total_amount, row.currency)}
        </div>
      ),
    },
    {
      name: "Action",
      minWidth: "110px",
      selector: "",
      sortable: true,
      cell: (row) => (
        <div className="column-action d-flex align-items-center">
          <Eye size={17} onClick={() => handleAction(row, "view")} />
          <Edit size={14} onClick={() => handleAction(row, "edit")} className="mx-1" />
          <UncontrolledDropdown>
            <DropdownToggle tag="span">
              <MoreVertical size={17} className="cursor-pointer" />
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem onClick={() => handleAction(row, "print")} className="w-100">
                <Printer size={14} className="mr-50" />
                <span className="align-middle">Print</span>
              </DropdownItem>
              <DropdownItem onClick={() => handleAction(row, "download")} className="w-100">
                <Download size={14} className="mr-50" />
                <span className="align-middle">Download</span>
              </DropdownItem>
              <DropdownItem onClick={() => handleAction(row, "duplicate")} className="w-100">
                <Copy size={14} className="mr-50" />
                <span className="align-middle">Duplicate</span>
              </DropdownItem>
              <DropdownItem onClick={() => handleAction(row, "delete")} className="w-100">
                <Trash size={14} className="mr-50" />
                <span className="align-middle">Delete</span>
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </div>
      ),
    },
  ];

  // Data filtering function according to filters present in table header.
  const dataToRender = () => {
    let tempData = invoices;
    // if (selectedAccount.id) tempData = tempData.filter((el) => el.currency_account_id == selectedAccount.id);
    // if (searchQuery)
    //   tempData = tempData.filter((el) => {
    //     let temp = JSON.parse(JSON.stringify(el));
    //     let sq = searchQuery.toLowerCase();
    //     temp.my_txn_type = el.paid_type == "cr" ? "credit" : "debit";
    //     temp.my_status = el.status == "1" ? "success" : el.status == "0" ? "failed" : "pending";
    //     temp.pay_method = `${temp.pay_method}`.toLowerCase();
    //     let searchFields = ["my_status", "txn_no", "order_id", "pay_method", "amount", "my_txn_type"];
    //     for (let field of searchFields)
    //       if (`${temp[field]}`.includes(sq) || `${temp[field]}`.startsWith(sq)) return true;
    //     return false;
    //   });
    // if (statusQuery) tempData = tempData.filter((el) => el.status == statusQuery);
    return tempData;
  };

  return (
    <div className="invoice-list-wrapper">
      <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            subHeader={true}
            subHeaderComponent={<CustomHeader />}
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>
    </div>
  );
};

export default Table;
