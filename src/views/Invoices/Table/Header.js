import { Row, Col, Button, Input } from "reactstrap";
import Flatpickr from "react-flatpickr";
import { Search } from "react-feather";
import { InvoicesContext } from "..";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import { selectedInvoice } from "..";
import moment from "moment";

const CustomHeader = () => {
  const { dates, setDates, searchQuery, setSearchQuery, statusQuery, setStatusQuery } = useContext(InvoicesContext);
  const history = useHistory();

  const [state, setState] = useState({ ...dates });

  return (
    <div className="invoice-list-table-header w-100 py-2">
      <Row>
        <Col lg="6" className="d-flex align-items-center justify-content-start px-0 px-lg-1">
          <div className="d-flex align-items-center mr-2">
            <Flatpickr
              className="form-control"
              value={dates.startDate}
              onChange={(e) => setState({ ...state, startDate: moment(e[0]).format("YYYY-MM-DD") })}
              style={{ width: "150px" }}
            />
            <Flatpickr
              className="form-control ml-1"
              value={dates.endDate}
              onChange={(e) => setState({ ...state, endDate: moment(e[0]).format("YYYY-MM-DD") })}
              style={{ width: "150px" }}
            />
          </div>
          <div className='col-12 col-md-2'>
          <Button color="primary" onClick={() => setDates({ ...state })}>
            <Search size={15} />
          </Button>
          </div>
          <Button
            color="primary"
            style={{ minWidth: "150px" }}
            className="ml-2"
            onClick={() => {
              selectedInvoice([]);
              history.push("/manage-invoice");
            }}
          >
            Add Invoice
          </Button>
        </Col>
        <Col
          lg="6"
          className="actions-right d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap mt-lg-0 mt-1 pr-lg-1 p-0"
        >
          <div className="d-flex align-items-center">
            <Input
              id="search-invoice"
              className="mr-2 w-100"
              type="text"
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
              placeholder="Search"
            />
          </div>
          <Input
            className="w-auto pr-4"
            type="select"
            value={statusQuery}
            onChange={(e) => setStatusQuery(`${e.target.value}`)}
          >
            <option value="">Select Status</option>
            <option value="1">Success</option>
            <option value="2">Pending</option>
            <option value="0">Failed</option>
          </Input>
        </Col>
      </Row>
    </div>
  );
};

export default CustomHeader;
