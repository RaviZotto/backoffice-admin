import flatpickr from "flatpickr";
import React, { useContext, useEffect, useState } from "react";
import { selectThemeColors,currencyCodes } from "@utils";
import Flatpickr from "react-flatpickr";
import { DownloadCloud, FileText, Search } from "react-feather";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import { useSelector } from "react-redux";
import Select from "react-select";
import { CardBody, Card, CardText, Button } from "reactstrap";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import moment from "moment";
import Spinner from "reactstrap/lib/Spinner";
import { DashboardContext } from ".";

export default function (){
const {currency,setCurrency,dates,setDates,currencyOption}=useContext(DashboardContext)
const {currencies} =useSelector(state=>state.transfer);
const [state,setState]=useState({...dates})
return (
    <div>
      <Card>
        <CardBody>
          <Row>
            <Col md={7} sm={12} className="mb-1 mb-md-0 ">
            <Row className='mx-auto'>
                <div className="col-md-4 col-6  ">
                  <Flatpickr
                    className="form-control"
                    value={dates.start_date}
                    onChange={(e) =>
                      setState({
                        ...state,
                        start_date: moment(e[0]).format("YYYY-MM-DD"),
                      })
                    }
                  />
                </div>
                <div className="  col-md-4 col-6   ">
                  <Flatpickr
                   
                   
                    className="form-control "
                    value={dates.end_date}
                    onChange={(e) =>
                       setState({...state,end_date:moment(e[0]).format("YYYY-MM-DD")})
                    }
                  />
                </div>
                 
                <div className=" col-12 col-md-1 d-flex justify-content-center   ">
                  <Button
                    className="   flex-fill  mb-1 mt-1   mb-md-0 mt-md-0 "
                    color="primary"
                     disabled={state.start_date==dates.start_date&&state.end_date==dates.end_date}
                    onClick={() =>setDates({...state}) }
                  
                  >
                    <Search size={17} />
                  </Button>
                  </div>    
                  </Row>       
             </Col> 
            <Col md={5} sm={12} className=" d-flex justify-content-end">
            
           
                <Col md={5}  >
               
                <Select 
                placeholder='Currency'
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
                defaultValue={{label:currency, value:currency}}
                onChange={e=>setCurrency(e.value)}
                options={[ ...currencyOption.map((el) => ({
                  label: el,
                  value: el,
                })) ]}  
                />
                </Col>
                  
                
                </Col>
             
          </Row>
        </CardBody>
      </Card>
    </div>
  );
}



//removed suspicious
//forward risk
//block txn 
//put in monitoring 
