import React, { createContext, useEffect, useMemo, useState } from "react";
import Header from "./Header";
import DashboardSales from "@src/components/analytics/DashboardSales";
import Loading from "@core/components/spinner/Loading-spinner";
import { UPDATE_DEVICES, UPDATE_MERCHANT } from "../../redux/actions/storeMerchant.";
import { GET_CURRENCY, GET_GATEWAY } from "../../redux/actions/transfer";
import { GET_DEVICES, GET_TRANSACTIONS } from "../../graphql/queries";
import moment from "moment";
import { useQuery } from "@apollo/client";
import { useDispatch, useSelector } from "react-redux";
import notification from "../../components/notification";
import { getAdminTransactions } from "../../Api";
export const DashboardContext = createContext(null);


function Dashboard() {
  const dispatch = useDispatch();
  const { merchants,devices } = useSelector((state) => state.merchant);
  const { gateway, currencies } = useSelector((state) => state.transfer);
  const [report, setReport] = useState({});
  const [currency, setCurrency] = useState();
  const [loading,setLoading] = useState(true);
  

  const [dates, setDates] = useState({
    start_date: moment(new Date()).format("YYYY-MM-DD"),
    end_date: moment(new Date()).format("YYYY-MM-DD"),
  });
  const [currencyOption,setCurrencyOption]=useState([]);
  // const { data, error, loading } = useQuery(GET_TRANSACTIONS, {
  //   variables: { ...dates },
  // });


  useEffect(()=>{
    if (!merchants.length)dispatch(UPDATE_MERCHANT());
    if (!gateway.length) dispatch(GET_GATEWAY());
 
    if(!devices.length) dispatch(UPDATE_DEVICES())
          setLoading(true);
        
         getAdminTransactions(dates).then(res=>{
          
          setTransactions(res||[]);
          if(res.length){setCurrency(res[0].currency);
          let setT=new Set();
            for(let obj of res)setT.add(obj.currency);
            setCurrencyOption(Array.from(setT))
         
        }
          setLoading(false);
         }).
        catch(e=>{
          setLoading(false);
         notification({title:"Error",message:e.toString(),type:'error'});
        
        })
   
    },[dates])
  
  
  
 


  const [transactions, setTransactions] = useState([]);
  const dashboardProvider = useMemo(
    () => ({
      currency,
      transactions,
      loading,
      setLoading,
      setTransactions,
      dates,
      setDates,
      setCurrency,
      devices,
      currencyOption,
      setCurrencyOption,
     
    }),
    [ currency,currencyOption ,loading,devices, transactions, dates]
  );



  if (loading) return <Loading />;

  return (
    <DashboardContext.Provider value={dashboardProvider}>
      <Header />
      <DashboardSales />
    </DashboardContext.Provider>
  );
}

export default Dashboard;
