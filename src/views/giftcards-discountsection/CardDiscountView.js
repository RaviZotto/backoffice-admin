import React,{Fragment,useContext} from 'react'
import {Card,CardHeader,CardBody, Badge,Button, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';
import Table from 'react-data-table-component'
import { ChevronDown, MoreVertical, PlusCircle } from 'react-feather';
import moment from 'moment';
import { Axios } from '../../Api';
import notification from '../../components/notification';
import { graphqlServerUrl } from '../../App';
import { GiftCardDiscount } from '.';
const CardDiscountView = ({selectedCardCategory,setModalOpen,setModalClose,setDataRow}) => {
 const {cardDiscounts,setCardDiscounts}=useContext(GiftCardDiscount);
    const columns = [
        {
          name: "Date",
          selector: "date_of_transaction",
          sortable: true,
          minWidth: "110px",
          cell: (row) => {
            let start = moment(row.discount_from);
            let end = moment(row.discount_to);
            return (
              <Fragment>
                <div className="mt-1  d-flex">
                  <h6 className="font-small-3 mr-1">{start.format("DD MMM YYYY")}</h6>
                  <span> - </span>
                  <h6 className=" ml-1 font-small-3">{end.format("DD MMM YYYY")}</h6>
                   
                </div>
              </Fragment>
            );
          },
        },
        {
          name: "Discount",
          selector: "discount",
          minWidth: "110px",
          sortable: true,
          cell: (row) => (
            <div className="d-block mt-1">
              <h6 className="font-small-3 mb-0">{row.discount}</h6>
              <p className='text-gray'>{row.discount_type}</p>
            </div>
          ),
        },
        {
            name: "Max Discount",
            selector: "max_discount_amount",
            minWidth: "110px",
            sortable: true,
            cell: (row) => (
              <div className="d-block mt-1">
                <h6 className="font-small-3 mb-0">{row.discount_type=='amount'?row.max_discount_amount:0}</h6>
                
              </div>
            ),
          },
        {
            name: "Validity Type",
            selector: "validity_type",
            minWidth: "110px",
            sortable: true,
            cell: (row) => (
              <Fragment>
                <Badge color="primary">{row.validity_type}</Badge>
              </Fragment>
            ),
          },
        
          {
            name:"Action",
            minWidth: "110px",
            className:"cursor-pointer",
            cell: (row) => (
              <UncontrolledDropdown className="chart-dropdown">
              <DropdownToggle
                color=""
                className="bg-transparent btn-sm border-0 p-50"
              >
                <MoreVertical size={18} className="cursor-pointer" />
              </DropdownToggle>
              <DropdownMenu top>
                <DropdownItem
                  className="w-100 d-flex text-center"
             
                  onClick={(e) =>{
                    setDataRow(row);
                  
                  }}
                >
                 Edit
                </DropdownItem>
             
                <DropdownItem
                  className="w-100 d-flex text-center"
             
                  onClick={async(e) =>{
                  try{  
                  await  Axios.delete(`${graphqlServerUrl}/admin/deleteCardDiscount?id=${row.id}`);
                  setCardDiscounts(cardDiscounts.filter(el=>el.id!=row.id));  
                }catch(e){
                    notification({ title: "Error", message: e.message,type: "error"});
                  }
      
             
                  
                  }}
                >
                 Delete
                </DropdownItem>
                   
                
        
              </DropdownMenu>
            </UncontrolledDropdown>
            )
          },
      ];
   
       
    const DataToRender = ()=>{
       let cardDiscountsArr= cardDiscounts.filter(el=>el.merchant_id==el.merchant_id)||[];
        if(!selectedCardCategory.category_id)return cardDiscountsArr;
       return cardDiscountsArr.filter(e=>e.category_id==selectedCardCategory.category_id);   
   
     }

    return (
        <div className="invoice-list-wrapper">
        <Button.Ripple 
        onClick={e=>setModalOpen(true)}
        color="primary mb-1"> 
        <PlusCircle size={15} className="mr-1"/>
        Add Discount </Button.Ripple>
        <Card>
       
          <div className="invoice-list-dataTable">
            <Table
              className="react-dataTable"
              noHeader={false}
            
              pagination
              columns={columns}
            
              responsive={true}
              sortIcon={<ChevronDown />}
              data={DataToRender()}
            />
          </div>
        </Card>
      </div>
    )
}

export default CardDiscountView
