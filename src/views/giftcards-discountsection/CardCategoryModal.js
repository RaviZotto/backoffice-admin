import AvForm from "availity-reactstrap-validation-safe/lib/AvForm";
import React, { useContext, useEffect, useState } from "react";
import {
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  ModalFooter,
  Row,
  Col,
} from "reactstrap";
import FormGroup from "reactstrap/lib/FormGroup";
import Label from "reactstrap/lib/Label";
import { X } from "react-feather";
import AvInput from "availity-reactstrap-validation-safe/lib/AvInput";
import "@styles/react/libs/react-select/_react-select.scss";
import Spinner from "reactstrap/lib/Spinner";
import notification from "../../components/notification";
import { saveCardCategory, saveClientCards, updateCardCategory, updateClientCards } from "../../Api";
import moment from "moment";
import AvField from "availity-reactstrap-validation-safe/lib/AvField";

import '@availity/reactstrap-validation-date/styles.scss';
import { GiftCardDiscount } from ".";
const CardsModal = ({ modalOpen, handleModal, dataRow, setDataRow }) => {
  const { cardCategories, merchant, merchants, setCardCategories } = useContext(GiftCardDiscount)
  //console.log(modalOpen);
  // alert(modalOpen)
  const [discountType, setDiscountType] = useState(dataRow.category_id ? dataRow.discount_type : 'amount');
  const [validityType, setValdityType] = useState(dataRow.category_id ? dataRow.validity_type : 'onetime');



  const CloseBtn = (
    <X className="cursor-pointer" size={15} onClick={handleModal} />
  );



  const [loader, setLoader] = useState(false);
  const handleSumbit = async (event, errors, value) => {
    if (errors.length) return;
    event.preventDefault();
    if (value.validity_type == 'percent' && value.discount > 100) return notification({ type: 'error', message: 'discount cannot be greater than 100%', title: "Validation Error" });
    if (value.validity_type == 'amount' && value.discount > value.max_discount_amount) return notification({ type: 'error', message: 'discount cannot be greater than 100%', title: "Validation Error" });

    try {

      setLoader(true);
      if (!dataRow.category_id) {

        // let merchantObj = merchants.find(e => e.merchant_id == merchant) || {};
        // //console.log(merchantObj);
        

        const data = await saveCardCategory(value);
        setCardCategories([{ ...data, ...value}, ...cardCategories]);
      } else {
        let updateObj = {};

        for (const [key, val] of Object.entries(value)) {
          if (val == "" || !val) continue;
          if ((key == 'discount_from' || key == 'discount_to') && (moment(dataRow[key]).format('YYYY-MM-DD') != moment(val).format('YYYY-MM-DD'))) { updateObj[key] = val; }
          else if (value[key] != dataRow[key]) updateObj[key] = val;

        }
        console.log(updateObj, moment(dataRow['discount_from']).format('YYYY-MM-DD') == moment(value['discount_from']).format('YYYY-MM-DD'));

        const temp = await updateCardCategory(dataRow.category_id, updateObj);
        console.log(temp);
        setCardCategories([{ ...dataRow, ...updateObj }, ...cardCategories.filter(e => e.category_id != dataRow.category_id)].sort((a, b) => b.category_id - a.category_id));
      }
      setLoader(false);

      handleModal();
    } catch (e) {
      notification({ title: "Error", message: e.toString(), type: "error" });
      setLoader(false);
      //console.log(e);
    }
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={handleModal}
      backdrop='static'
      //   className='middle-lg'
      //   modalClassName='modal-slide-in'
      contentClassName="pt-0"
      className="modal-dialog-centered"
    >
      <ModalHeader className="" toggle={handleModal} close={CloseBtn} tag="div">
        {(!dataRow.category_id ? "Add" : "Update") + "  Category"}
      </ModalHeader>
      <AvForm onSubmit={handleSumbit} model={dataRow} >
        <ModalBody>
          <Col>
            {!dataRow.category_id&&(
              <AvField required type="select" name="merchant_id"  label="Merchant"  >
              {  merchants.map(el=>
                  <option value={el.api_key}  label ={el.comp_name}/>
                )
              }
                </AvField>
            )}
            <AvField required name="category_name" value={dataRow.category_name} type="text" label="Category Name" />
            <FormGroup tag={Row}>
              <Col>
                <AvField name="discount_from" type="date" label="Start" value={moment(dataRow.discount_from).format('YYYY-MM-DD')} />
              </Col>
              <Col>
                <AvField name="discount_to" type="date" label="End" value={moment(dataRow.discount_to).format('YYYY-MM-DD')} />
              </Col>
            </FormGroup>
            <FormGroup tag={Row}>
              <Col>
                <AvField name="discount_type" label='Discount Type'
                  type='select' value={discountType}
                  onChange={e => setDiscountType(e.target.value)}
                >

                  <option value="amount" label="Amount" />
                  <option value="percent" label="Percent" />
                </AvField>
              </Col>
            </FormGroup>
            {
              discountType == 'amount' ?

                <AvField label="Max Discount" value={dataRow.max_discount_amount} name="max_discount_amount" type="number" />
                : null
            }

            <AvField type="number" label={discountType == 'amount' ? 'Amount' : 'Percent'} name="discount" value={dataRow.discount} />
            <AvField name='validity_type' type='select' label="Validity Type" value={validityType} onChange={e => setValdityType(e.target.value)} >
              <option value="timeperiod" label="Time Period" />
              <option value="onetime" label="One Time" />
              <option value="everytime" label="Every Time" />
              <option value="nooftransaction" label="No of Transaction" />
            </AvField>
            {
              validityType == 'nooftransaction' ? <AvField required name='no_of_transaction' type='number' label="No Of Transaction" value={validityType} /> : null
            }
          </Col>
        </ModalBody>

        <ModalFooter>
          <div className="d-flex justify-content-center">
            <Button.Ripple
              className="mr-2"
              type="submit"
              color={"primary"}
            // disabled={loader|| userDetails.role_type!='admin' && !userDetails['merchant'].includes('C')}
            >
              {loader ? <Spinner size="sm" className="mr-1" /> : null}{" "}
              <span>{dataRow.category_id ? 'Update' : 'Add'}</span>
            </Button.Ripple>
            <Button.Ripple color={"danger"} onClick={handleModal}>
              Close
            </Button.Ripple>
          </div>
        </ModalFooter>
      </AvForm>
    </Modal>
  );
};
export default CardsModal;
