import { Row, Card, Col, Button, Input, Label } from "reactstrap";

import "@styles/react/libs/flatpickr/flatpickr.scss";
import React, { useContext, useState } from "react";

import { GiftCardDiscount } from ".";


const CustomHeader = () => {
    const { merchants, merchant, setMerchant, partners } = useContext(GiftCardDiscount);
    const [partner, setPartner] = useState(null);


    return (
        <Card className="p-1 pr-0">
            <Row className=" justify-content-between mb-1 ">
                <Col md={6} sm={12} className='ml-0 mb-1 mb-md-0'>
                    <Label>Partner</Label>
                    <Input
                        onChange={e => setPartner(e.target.value)}
                        type='select'
                        name='parnter'
                        value={partner}
                    >
                        <option value={0} label="select--" />
                        {partners.map(el => <option value={el.id} label={el.comp_name} />)}
                    </Input>
                </Col>
                <Col md={6} sm={12} className='mr-0' >
                    <Label>Merchant</Label>
                    <Input
                        type='select'
                        name='merchant'
                        onChange={e => setMerchant(merchants.find(el=>e.target.value==el.merchant_id))}
                        value={merchant.merchant_id}
                    >
                        <option value={0} label="select--" />
                        {merchants.filter(e => partner == 0 ? e : (e.partner_id == partner)).map(el => <option value={el.merchant_id} label={el.comp_name} />)}
                    </Input>
                </Col>
            </Row>


        </Card>
    );
};

export default CustomHeader;
