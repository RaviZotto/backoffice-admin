import React, { createContext, useEffect, useMemo, useState } from 'react'
import { PlusCircle } from 'react-feather';
import { Button, Row } from 'reactstrap'
import PerfectScrollBar from "react-perfect-scrollbar";

import CardCategoryView from './CardCategoryView';
import CardDiscountView from './CardDiscountView';
import CardCategoryModal from './CardCategoryModal';
import CardDiscountModal from './CardDiscountModal';
import { useDispatch, useSelector } from 'react-redux';
import { Axios } from '../../Api';
import { graphqlServerUrl } from '../../App';
import Header from './Header';
import notification from '../../components/notification';
import { GET_PARTNER, UPDATE_MERCHANT } from '../../redux/actions/storeMerchant.';
import Loading from "@core/components/spinner/Loading-spinner";
export const GiftCardDiscount = createContext(null);
const index = () => {
  const { merchants,partners } = useSelector(state => state.merchant)
  const [cardCategories, setCardCategories] = useState([]);
  const [selectedCardCategory, setSelectedCardCategory] = useState({});
  const [cardDiscounts, setCardDiscounts] = useState([]);
  const [cardCategoryModal, setCardCategoryModal] = useState(false);
  const [cardDiscountModal, setCardDiscountModal] = useState(false);
  const [dataRowCategory, setDataRowCategory] = useState({});
  const [dataRowDiscount, setDataRowDiscount] = useState({});
  const [merchant, setMerchant] = useState({});
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch()
  const handleSelectCategory = (data) => {
    if (selectedCardCategory.category_id) setSelectedCardCategory({})
    else setSelectedCardCategory(data);
  }
   useEffect(() => {console.log(merchant)},[merchant])
  useEffect(() => {
    (async () => {
      try {
        if(!partners.length) dispatch(GET_PARTNER());
        if(!merchants.length)dispatch(UPDATE_MERCHANT());
        const { data:{card_categories} } = await Axios.get(`${graphqlServerUrl}/admin/getCardCategory?id=${undefined}`)
        const { data: { card_discounts } } = await Axios.get(`${graphqlServerUrl}/admin/getCardDiscounts?id=${undefined}`)
        console.log(card_categories,42);
        setCardCategories(card_categories || []);
        setCardDiscounts(card_discounts || []);
        setLoading(false);
      }
      catch (e) {
        notification({ title: "Error", message: e, type: "error" });
        setLoading(false);
      }

    })()
  }, [])


  const giftCardDiscountValue = useMemo(() => ({
    cardCategories,
    setCardCategories,
    cardCategoryModal,
    dataRowCategory,
    cardDiscounts, setCardDiscounts,
    partners,
    merchants,
    merchant,
    setMerchant,
    handleSelectCategory
  }), [merchants, merchant,cardDiscounts, cardCategories, selectedCardCategory, dataRowCategory, dataRowDiscount])

 if(loading)return <Loading />
  return (
    <GiftCardDiscount.Provider value={giftCardDiscountValue}>
      <CardCategoryModal dataRow={dataRowCategory} modalOpen={cardCategoryModal || dataRowCategory.category_id} handleModal={() => dataRowCategory.category_id ? setDataRowCategory({}) : setCardCategoryModal(false)} />
      <CardDiscountModal dataRow={dataRowDiscount} modalOpen={cardDiscountModal || dataRowDiscount.id} handleModal={() => dataRowDiscount.category_id ? setDataRowDiscount({}) : setCardDiscountModal(false)} />
      <Header />
      <Button.Ripple
        color="primary"
        onClick={() => setCardCategoryModal(true)}
        className=" mb-1"
      >
        {" "}
        <PlusCircle size={12} className="mr-2" /> Add Category
      </Button.Ripple>

      <PerfectScrollBar>


        <Row className='p-2 flex-nowrap px-2'>
          {cardCategories.filter(el=>(el.merchant_id==merchant.api_key)||(el.merchant_id==merchant.merchant_id)).map((res) => {
         console.log(res);

            return <CardCategoryView setDataRow={setDataRowCategory} handleSelect={handleSelectCategory} selectedCardCategory={selectedCardCategory} key={res.category_id} data={res} />
          })}
        </Row>

      </PerfectScrollBar>

      <CardDiscountView setDataRow={setDataRowDiscount} setModalOpen={setCardDiscountModal} selectedCardCategory={selectedCardCategory} />

    </GiftCardDiscount.Provider>
  )
}

export default index;
