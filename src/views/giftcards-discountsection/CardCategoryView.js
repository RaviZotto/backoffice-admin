
import React, { useContext, useEffect } from "react";
import {CheckCircle, Edit, MoreVertical} from 'react-feather'
import {Card,CardBody, CardHeader, DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import { Button } from "reactstrap";



import { GiftCardDiscount } from ".";
import { Axios } from "../../Api";
import { graphqlServerUrl } from "../../App";
import notification from "../../components/notification";

const DebitsCards = ({
  data,
  handleSelect,
  selectedCardCategory,
  setDataRow
  
}) => {



  const {cardCategories,setCardCategories,cardDiscounts} = useContext(GiftCardDiscount);
 
 
 
  return (
    <div className='mr-1'
    onClick={()=>handleSelect(data)}
    >
     
      <Card
    
        className={ selectedCardCategory.category_id==data.category_id? "card-congratulations text-bg-white":"text-bg-black" }
        style={{ width: "450px", cursor: "pointer" }}
      >
       
        <CardBody>
          <div className="d-flex flex-column">
          <div className="d-flex justify-content-between"> 
        
        
              <p>Category Name</p>
             <UncontrolledDropdown className="chart-dropdown">
        <DropdownToggle
          color=""
          className="bg-transparent btn-sm border-0 p-50"
        >
          <MoreVertical size={18} className="cursor-pointer" color={selectedCardCategory.category_id==data.category_id? "white":'black'} />
        </DropdownToggle>
        <DropdownMenu top>
          <DropdownItem
            className="w-100 d-flex text-center"
       
            onClick={(e) =>{
              setDataRow(data);
            
            }}
          >
           Edit
          </DropdownItem>
       
          <DropdownItem
            className="w-100 d-flex text-center"
       
            onClick={async(e) =>{
            try{  
            await  Axios.delete(`${graphqlServerUrl}/admin/deleteCardCategory?id=${data.category_id}`);
            setCardCategories(cardCategories.filter(el=>el.category_id!=data.category_id));  
          }catch(e){
              notification({ title: "Error", message: e.message,type: "error"});
            }

       
            
            }}
          >
           Delete
          </DropdownItem>
             
          
  
        </DropdownMenu>
      </UncontrolledDropdown>
      </div>
            </div>
            <h1>{data.category_name}</h1>
            <p>Discount Available : {cardDiscounts.filter(e=>e.category_id==data.category_id).length}</p>
       
        </CardBody>
      </Card>
    </div>
  );
};
export default DebitsCards;
