
import React, { useContext, useEffect, useState } from "react";
import {
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  ModalFooter,
  Row,
  Col,
FormGroup,
Spinner
} from "reactstrap";

import { X } from "react-feather";
 import notification from "../../components/notification";
import { GiftCardDiscount } from ".";
import { saveCardDiscount,updateCardDiscount } from "../../Api";

import {AvForm,AvField} from "availity-reactstrap-validation-safe";
import moment from "moment";


const CardsModal = ({ modalOpen, handleModal ,dataRow }) => {
 const {cardCategories,merchant,merchants,cardDiscounts,setCardDiscounts}  = useContext(GiftCardDiscount)
  //console.log(modalOpen);
  // alert(modalOpen)
 const [discountType,setDiscountType]=useState('amount');
 const [validityType,setValdityType]=useState('onetime');
 const [categoryObj,setCategoryObj]=useState({});

  const CloseBtn = (
    <X className="cursor-pointer" size={15} onClick={handleModal} />
  );

  useEffect(() => {
      if(dataRow.id){
        setCategoryObj({...dataRow});
        setValdityType(dataRow.validity_type);
        setDiscountType(dataRow.discount_type);

      }
  },[dataRow.id])


  const [loader, setLoader] = useState(false);
  const handleSumbit = async (event, errors, value) => {
    if (errors.length) return;
    event.preventDefault();
    try {
    
      setLoader(true);
      if(!dataRow.id){
       
    
      //console.log(merchantObj);
      value.merchant_id=merchant.api_key;
       
      const discountObj = await saveCardDiscount(value);
      setCardDiscounts([discountObj, ...cardDiscounts]);
      
      }else{
        let updateObj={};
 
        for (const [key, val] of Object.entries(value)) {
          if(!val|| (typeof val == 'string'&&val.trim()==''))continue;
          if((key=='discount_from' || key=='discount_to') && moment(dataRow[key]).format('YYYY-MM-DD')!=moment(val).format('YYYY-MM-DD')) updateObj[key]=val;
          else if(value[key]!=dataRow[key])updateObj[key]=val;
        }
        console.log(updateObj,dataRow,value);
        
        await updateCardDiscount(dataRow.id,updateObj);
        setCardDiscounts([{...dataRow,...updateObj} ,...cardDiscounts.filter(e=>e.id!=dataRow.id)].sort((a,b)=>b.id-a.id));
      }
       setLoader(false);
    
      handleModal();
    } catch (e) {
      notification({ title: "Error", message: e.toString(), type: "error" });
      setLoader(false);
      //console.log(e);
    }
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={handleModal}
      backdrop='static'
      //   className='middle-lg'
      //   modalClassName='modal-slide-in'
      contentClassName="pt-0"
      className="modal-dialog-centered"
    >
      <ModalHeader className="" toggle={handleModal} close={CloseBtn} tag="div">
     {   (!dataRow.id?"Add":"Update")+"  Discount"}
      </ModalHeader>
      <AvForm onSubmit={handleSumbit}>
        <ModalBody>
          <Col>
          {!dataRow.id&&(
              <AvField required type="select" name="merchant_id"  label="Merchant"  >
              {  merchants.map(el=>
                  <option value={el.api_key } label ={el.comp_name}/>
                )
              }
                </AvField>
            )}
          <AvField required name="category_id"  
          value={categoryObj.category_id}
          onChange={e=>{
              const obj = cardCategories.find(e1=>e1.category_id==e.target.value);
              setCategoryObj(obj); 
              setValdityType(obj.validity_type);
              setDiscountType(obj.discount_type);
          }}
          type="select"  label="Category " >
           {
               cardCategories.map(res=>
                <option  value={res.category_id} label={res.category_name}/>
                )
           }
            </AvField>    
            <FormGroup tag={Row}>
             <Col>   
            <AvField name="discount_from"  value={moment( categoryObj.discount_from).format('YYYY-MM-DD')} type="date" label="Start" />
            </Col>
           <Col> 
            <AvField name="discount_to"  value={moment(  categoryObj.discount_to).format('YYYY-MM-DD')} type="date" label="End" />
            </Col>
            </FormGroup>
            <FormGroup tag={Row}>
            <Col>
            <AvField name="discount_type" label='Discount Type' 
             type='select'  value={categoryObj.discount_type ||  discountType }
             onChange={e=>setDiscountType(e.target.value)}
            >
           
            <option value="amount"  label="Amount" />   
            <option value="percent" label="Percent" />
            </AvField>
            </Col>
            </FormGroup>
             {
                 discountType == 'amount'?
                 
                 <AvField label="Max Discount" required name="max_discount_amount" type="number" value={ categoryObj.max_discount_amount} />
                 :null
             }
            
             <AvField type="number" label={discountType=='amount'?'Amount':'Percent'} name="discount" value={ categoryObj.discount} />
             <AvField name='validity_type' type='select'   label="Validity Type" value={ validityType } onChange={e=>setValdityType(e.target.value)} >  
             <option value="timeperiod" label="Time Period" />
             <option value="onetime" label="One Time" />
             <option value="everytime" label="Every Time" /> 
             <option value="nooftransaction" label="No of Transaction" />
            </AvField> 
            {
                validityType=='nooftransaction' ?  <AvField required name='no_of_transaction' type='number' label="No Of Transaction" value={  categoryObj.no_of_transaction} />:null
            }
          </Col>
        </ModalBody>

        <ModalFooter>
          <div className="d-flex justify-content-center">
            <Button.Ripple
              className="mr-2"
              type="submit"
              color={"primary"}
              // disabled={loader|| userDetails.role_type!='admin' && !userDetails['merchant'].includes('C')}
            >
              {loader ? <Spinner size="sm" className="mr-1" /> : null}{" "}
              <span>{dataRow.id?'Update':'Add'}</span>
            </Button.Ripple>
            <Button.Ripple color={"danger"} onClick={handleModal}>
              Close
            </Button.Ripple>
          </div>
        </ModalFooter>
      </AvForm>
    </Modal>
  );
};
export default CardsModal;
