import React, { Fragment, useEffect, useRef, useState } from "react";
import { ArrowRight } from "react-feather";
import { Label, Spinner, Media, FormGroup, Row, Col, Button } from "reactstrap";
import { AvForm, AvInput } from "availity-reactstrap-validation-safe";
import { merchantDetails } from "..";
import countries from "../../../utility/extras/countryiso_3.json";
import {TimeZone} from "../../../utility/extras/"
import { currencyCodes, getBase64FromUrl, phoneCodes } from "../../../utility/Utils";
import { updateMerchant, uploadImage } from "../../../Api";
import notification from "../../../components/notification";
import { graphqlServerUrl } from "../../../App";
import AvField from "availity-reactstrap-validation-safe/lib/AvField";
import { useDispatch, useSelector } from "react-redux";
import {
  GET_PARTNER,
  UPDATE_MERCHANT,
  GET_ROLE,
  GET_TYPE,
  GET_SUBTYPE
} from "../../../redux/actions/storeMerchant.";
import queryString from "querystring";
import defaultImage from '@src/assets/images/icons/user_image.png';
const Settings = () => {
  const { partners, merchants ,types,subtypes} = useSelector((state) => state.merchant);
  const logoRef = useRef();
  const [merchantData, setMerchantData] = useState([]);
  const merchant = queryString.parse(window.location.search);

  const [url, setUrl] = useState(
    `${graphqlServerUrl}/images/merchant/${merchant["?merchant_id"]}.png`
  );




  const [loading, setLoading] = useState(false);
  const [logoUploading, setLogoUploading] = useState(false);
  const dispatch = useDispatch();
  useEffect(() => {
 
      getBase64FromUrl(  `${graphqlServerUrl}/images/merchant/${merchant["?merchant_id"]}.png`)
      .then(res=>{console.log(res); setUrl(res&&res.includes('image/png')?res:defaultImage)})
      .catch(err=>setUrl(defaultImage));

    if(!types.length)dispatch(GET_TYPE())
    if(!subtypes.length)dispatch(GET_SUBTYPE())
  
    !partners.length && dispatch(GET_PARTNER());
    merchants.length && merchantDetails(merchants.find(e=>e.merchant_id==merchant["?merchant_id"]));
  }, []);

  useEffect(() => {}, [url]);
 

  const onSubmit = async (event, errors, value) => {
    try {
      event.preventDefault();
      let merchantObj = {};
      if (!merchantData.length) return;
      for (let i = 0; i < merchantData.length; i++) {
         if(merchantData[i]=='is_settle'||merchantData[i]=='google_oauth_login'||merchantData[i]=='is_oauthterm_open'){ value[merchantData[i]]= value[merchantData[i]]=='enable'?1:0}
         else if (merchantData[i] == "password") {
           console.log(value.password,value.confirmpassword);
          if (value.password != value.confirmpassword) {setMerchantData([]);return notification({title:'Password not matched with confirm password',type:'error'})}
            
        }
        merchantObj[merchantData[i]] = value[merchantData[i]];
        //console.log(merchantObj, i);
      }
      //console.log(merchantObj, value);
      // merchantObj.is_admin=1; 
         
      setLoading(true);
      if(Object.keys(merchantObj).length){
      await updateMerchant(merchant["?merchant_id"], {
        merchant: merchantObj,
      });
      notification({
        title: "Success",
        message: "Successfully update",
        type: "success",
      });
    }
      dispatch(UPDATE_MERCHANT(() => setLoading(false)));
      setMerchantData([]);
    } catch (e) {
      setLoading(false);
    }
  };

  const handleLogoUpload = ({
    target: {
      validity,
      files: [file],
    },
  }) => {
    if (file && file.type != "image/png")
      return notification({ type: "error", message: "Not a valid file" });
    if (validity.valid) {
      const formData = new FormData();
      //console.log(file);
      formData.append("logo", file);

      for (var key of formData.entries()) {
        //console.log(key[0] + ", " + key[1]);
      }
      setLogoUploading(true);

      formData.append(
        "data",
        JSON.stringify({
          path: "merchant",
          merchant_id: merchant["?merchant_id"],
        })
      );
      uploadImage(formData, ({ err, res }) => {
        if (err) {
       
        } else {
          notification({
            title: "Image",
            message: "successfully uploaded",
            type: "success",
          });
         
          logoRef.current.src = `${graphqlServerUrl}/images/merchant${
            merchantDetails().merchant_id
          }.png?${new Date().getTime()}`;
   
          setUrl(`${graphqlServerUrl}/images/merchant/${
            merchantDetails().merchant_id  
          }.png?${new Date().getTime()}`)
        }
      
        setLogoUploading(false);
      });
    }
  };

  const handleChange = (key) => {
    //console.log(key, merchantData);
    if (merchantData.indexOf(key) == -1)
      setMerchantData([...merchantData, key]);
  };

  return (
    <Fragment>
    <AvForm onSubmit={onSubmit} >
        <Col>
         <FormGroup tag={Row} className="w-100 mx-auto" >
           <Col md='6' className='d-flex  justify-content-center'>
           
        <img
          className="rounded mr-50"
          src={url}
          ref={logoRef}
          alt="Generic placeholder image"
          height="80"
          width="80"
        />
        <Media className="mt-75 ml-1" body>
          <Button.Ripple
            tag={Label}
            className="mr-75"
            size="sm"
            color="primary"
          >
            {logoUploading ? <Spinner size="sm" className="mr-1" /> : null}
            {logoUploading ? "Updating" : "Update"}
            <input
              type="file"
              onChange={handleLogoUpload}
              hidden
              accept="image/x-png"
            />
          </Button.Ripple>
          <p>Only PNG files allowed.</p>
        </Media>
    
           </Col>
         </FormGroup>
          <FormGroup tag={Row} >
           <Col md={6} >
            <AvField 
             label = 'Average Transaction'
             type='number'
             name='avg_txn'
             defaultValue={merchantDetails().avg_txn}
             onChange={(e) => handleChange("avg_txn")}
            />
           </Col> 
           <Col md={6} >
            <AvField 
             label = 'Highest Transaction'
             type='number'
             name='highest_txn'
             defaultValue={merchantDetails().highest_txn}
             onChange={(e) => handleChange("highest_txn")}
            />
           </Col>   
          </FormGroup>
          <FormGroup tag={Row} >
           <Col md={6} >
            <AvField 
             label = 'Average Sales'
             type='number'
             name='avg_sales'
             defaultValue={merchantDetails().avg_sales}
             onChange={(e) => handleChange("avg_sales")}
            />
           </Col> 
           <Col md={6} >
            <AvField 
             label = 'Risk Level'
             type='select'
             name='risk_level'
             
             defaultValue={merchantDetails().risk_level}
             onChange={(e) => handleChange("risk_level")}
            name={`risk_level`}
           >
                <option label={"select"} value={null} />
                 <option label={"LOW"} value="LOW" />
                 <option label={"MEDIUM"} value="MEDIUM" />
                 <option label={"HIGH"} value="HIGH" />  
            </AvField>
           </Col>   
          </FormGroup>
          <FormGroup tag={Row}>
          <Col md="6">
            <AvField
              label="Time Zone "
              type="select"
              value={merchantDetails().timezone}
              name="timezone"
              onChange={(e) => handleChange("timezone")}
            >
              {TimeZone.map((el) => (
                <option label={el} value={el} />
              ))}
            </AvField>
          </Col>
          <Col md="6">
            <Label>Enable Settlement Report</Label>
            <AvInput
                            type="select"
              value={merchantDetails().is_settle?'enable':'disable'}
              name="is_settle"
              onChange={(e) => handleChange("is_settle")}
            >
          
          <option label='Enable' value={'enable'} />
            <option label='Disable' value={'disable'}/> 
            </AvInput>
          </Col>
          </FormGroup>
          <FormGroup tag={Row} >
          <Col md="6">
            <Label>Enable Outh Step</Label>
            <AvInput
             
              type="select"
              value={merchantDetails().is_oauthterm_open?'enable':'disable'}
              name="is_oauthterm_open"
              onChange={(e) => handleChange("is_oauthterm_open")}
            >
           <option label='Enable' value={'enable'} />
            <option label='Disable' value={'disable'}/>  
            </AvInput>
          </Col>
          <Col md="6">
          <Label>Google Oauth Login</Label>
            <AvInput
            
              type="select"
              value={merchantDetails().google_oauth_login?'enable':'disable'}
              name="google_oauth_login"
              onChange={(e) => handleChange("google_oauth_login")}
            >
          <option label='Enable' value={'enable'} />
            <option label='Disable' value={'disable'}/>  
            </AvInput>
          </Col>
          </FormGroup>
          <FormGroup tag={Row}>
               <Col md="6">
                 <Label>Password</Label>
                  <AvInput type="password" name="password"  onChange={e=>handleChange(e.target.name)}  />
               </Col>
               <Col md="6">
                 <Label>Confirm Password</Label>
                  <AvInput type="text" name="confirmpassword"    />
               </Col>
            </FormGroup>
          <FormGroup tag={Row} >
          <Col md='6'>
        <Label>Category</Label>
            <AvInput
          
              placeholder="select--"
              type="select"
              value={merchantDetails().type}
              onChange={(e) => handleChange("type")}
              name={`type`}
            >
                 <option label={"select"} value={null} />
               { 
               types.length&&types.map(res=>
                  <option value={res.id} label={res.type_name} />
               )
              }
            </AvInput>
            </Col>
            <Col md='6'>
            <Label>Sub category</Label>
            <AvInput
              required
              placeholder="select--"
              type="select"
              defaultValue={merchantDetails().subtype}
              onChange={(e) => handleChange("subtype")}
              name={`subtype`}
            >
                <option label={"select"} value={null} />
              { 
               subtypes.length&&subtypes.map(res=>
                  <option value={res.id} label={res.subtype_name} />
               )
              }
            </AvInput>
            </Col>
           </FormGroup> 
          <div className="d-flex justify-content-end">
          <Button.Ripple
            type="submit"
            color="primary"
            className="btn-next"
            disabled={!merchantData.length || loading}
          >
            <span className="align-middle d-sm-inline-block ">
              {loading ? "Updating..." : "Update"}
            </span>
          </Button.Ripple>
          </div>
        </Col>
      </AvForm>
    </Fragment>
  );
};

export default Settings;
