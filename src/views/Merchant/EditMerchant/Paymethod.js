import React,{Fragment, useState} from 'react';
import { selectThemeColors } from '@utils'
import {Row,Col,Button}from 'reactstrap'
import Select, { components } from 'react-select'
import Label from 'reactstrap/lib/Label';
import {AvField,AvForm,AvInput} from 'availity-reactstrap-validation-safe';
const CurrencyCredentials=()=>{

    const onSubmit=(event,errors)=>{
            
    }
    const[isEcom,setEcom]=useState(true);
    // EUR_CVVON, EUR_3D, EUR_CVVOFF,
    const [ecomOptions,setEcomOptions]=useState([])
    const currencyOptions=[
        { value: 'USD', label: 'USD', color: '#00B8D9', isFixed: true },
        { value: 'GBP', label: 'GBP', color: '#0052CC', isFixed: true },
        { value: 'DKK', label: 'DKK', color: '#5243AA', isFixed: true },
        { value: 'EUR', label: 'EUR', color: '#FF5630', isFixed: false },
        { value: 'NOK', label: 'NOK', color: '#FF8B00', isFixed: false },
        { value: 'SEK', label: 'SEK', color: '#FFC400', isFixed: false }
    ]

  return(
      <Fragment>
          <AvForm onSubmit={onSubmit} >
          <Col>
           <Row>
               <Col>
            <Label>Payment Gateway</Label>
              <AvField type='select' 
                 name='payment_gateway'
              onChange={e=>{if(e.target.value=='ECOM'){setEcom(true);} else {setEcom(false);setEcomOptions([])}
            }}
           >
              <option label='Ecom' value='ECOM' defaultChecked />
              <option label='Rails bank' value='rails_bank' />
              </AvField>
              </Col>
              
              { isEcom&&<Col>
           <Label>Multi Select</Label>
            <Select
              isClearable={false}
              theme={selectThemeColors}
              isMulti
              name='colors'
              options={currencyOptions}
              
              onChange={e=>{//console.log(e); if(e){setEcomOptions([...e])}else{setEcomOptions([])}  }}
              className='react-select'
              classNamePrefix='select'
            />
            
            </Col>
              }
           </Row>
           { isEcom&&ecomOptions.length? ecomOptions.map(res=>
           <Row>
               
            <Col>
            <Label className='form-label' for='linkedin'>
              {res.value}EUR_CVVOFF
            </Label>
            <AvInput
              required
              type='text'
              id={`linkedin`}
              name={`ecom_${res.value.toLowerCase()}_cvvoff`}
            />
              </Col>  
            <Col>
            <Label className='form-label' for='linkedin'>
              {res.value}EUR_CVVON
            </Label>
            <AvInput
              required
              type='text'
              id={`linkedin`}
              name={`ecom_${res.value.toLowerCase()}_cvvon`}
            />
            </Col>
            <Col>
            <Label className='form-label' for='linkedin'>
             {res.value}EUR_3D
            </Label>
            <AvInput
              required
              type='text'
              id={`linkedin`}
              name={`ecom_${res.value.toLowerCase()}_3d`}
            />
            </Col>
            
            
           </Row>
           ):
           !isEcom&&(<Fragment>
           <Label>Api Key</Label>
           <AvInput
           type='text'
           name='api_key'
           />
           </Fragment>)
           }
           <Row className='justify-content-end' >
            <Button.Ripple type='submit' color='primary' className='mt-2' >Update </Button.Ripple>
           </Row>
          </Col>

          </AvForm>
      </Fragment>
  )

}

export default CurrencyCredentials;