import React, { Fragment, useEffect, useRef, useState } from "react";
import { ArrowRight } from "react-feather";
import { Label, Spinner, Media, FormGroup, Row, Col, Button } from "reactstrap";
import { AvForm, AvInput } from "availity-reactstrap-validation-safe";
import { merchantDetails } from "..";
import countries from "../../../utility/extras/countryiso_3.json";
import {TimeZone} from "../../../utility/extras/"
import { currencyCodes, phoneCodes } from "../../../utility/Utils";
import { updateMerchant, uploadImage } from "../../../Api";
import notification from "../../../components/notification";
import { graphqlServerUrl } from "../../../App";
import AvField from "availity-reactstrap-validation-safe/lib/AvField";
import { useDispatch, useSelector } from "react-redux";
import {
  GET_PARTNER,
  UPDATE_MERCHANT,
  GET_ROLE,
  GET_TYPE,
  GET_SUBTYPE
} from "../../../redux/actions/storeMerchant.";
import queryString from "querystring";
import moment from "moment";
import Flatpickr from "react-flatpickr";
import "@styles/react/libs/flatpickr/flatpickr.scss";
const AccountDetails = () => {
  const { partners, merchants ,types,subtypes} = useSelector((state) => state.merchant);
  const logoRef = useRef();
  const [merchantData, setMerchantData] = useState([]);
  const merchant = queryString.parse(window.location.search);
  const [date,setDate] = useState();
  const [url, setUrl] = useState(
    `${graphqlServerUrl}/images/merchant/${merchant["?merchant_id"]}.png`
  );
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();
  useEffect(() => {
    if(!types.length)dispatch(GET_TYPE())
    if(!subtypes.length)dispatch(GET_SUBTYPE())
    !partners.length && dispatch(GET_PARTNER());
    merchants.length && merchantDetails(merchants.find(e=>e.merchant_id==merchant["?merchant_id"]));
  }, []);

  useEffect(() => {}, [url]);


  const onSubmit = async (event, errors, value) => {
    try {
      event.preventDefault();
      let merchantObj = {};
      if (!merchantData.length) return;
      for (let i = 0; i < merchantData.length; i++) {
         if(merchantData[i]=='is_settle'||merchantData[i]=='google_oauth_login'||merchantData[i]=='is_oauthterm_open'){ value[merchantData[i]]= value[merchantData[i]]=='enable'?1:0}
        else if(merchantData[i] == "country") {
          
          merchantObj["country"] = value[merchantData[i]].value;
        } else if (merchantData[i] == "password") {
          if (value.password == null || value.password == "")
            delete value.password;
        }else if(merchantData[i]=='dob'){ value[merchantData[i]]=moment(date).format("YYYY-MM-DD");}
        merchantObj[merchantData[i]] = value[merchantData[i]];
   
      }
   
      // merchantObj.is_admin=1; 
         
      setLoading(true);
      if(Object.keys(merchantObj).length){
      await updateMerchant(merchant["?merchant_id"], {
        merchant: merchantObj,
      });
      notification({
        title: "Success",
        message: "Successfully update",
        type: "success",
      });
    }
      dispatch(UPDATE_MERCHANT(() => setLoading(false)));
      setMerchantData([]);
    } catch (e) {
      setLoading(false);
    }
  };

  const handleChange = (key) => {
    //console.log(key, merchantData);
    if (merchantData.indexOf(key) == -1)
      setMerchantData([...merchantData, key]);
  };

  return (
    <Fragment>
      <div className="content-header"></div>
      
      <AvForm onSubmit={onSubmit} autoComplete="off">
        <Row>
          <FormGroup tag={Col} md="6">
            <Label className="form-label" for={`username`}>
              Name
            </Label>
            <AvInput
              type="text"
              name={`name`}
              id={`username`}
              onChange={(e) => handleChange("name")}
              value={merchantDetails().name}
              placeholder="Enter your name"
            />
            <Label className="form-label" for={`email`}>
              Email
            </Label>
            <AvInput
              // required
              type="email"
              name={`email`}
              id={`email`}
              value={merchantDetails().email}
              placeholder="john.doe@email.com"
              aria-label="john.doe"
              onChange={(res) => handleChange("email")}
            />
            <Row>
              <Col md={"6"}>
                <Label>Phone Code</Label>
                <AvInput
                  // required
                  type="select"
                  name={`phone_code`}
                  value={merchantDetails().phonecode}
                  onChange={(res) => handleChange("phone_code")}
                >
                  {phoneCodes.map((res) => (
                    <option value={res.value} label={res.label} />
                  ))}
                </AvInput>
              </Col>
              <Col md="6">
                <Label>Phone</Label>
                <AvInput
                  // required
                  type="number"
                  name={`phone`}
                  value={merchantDetails().phone}
                  onChange={(res) => handleChange("phone")}
                />
              </Col>
            </Row>
            <Label>Country</Label>
            <AvInput
              
              type="select"
              value={merchantDetails().country || countries.find(el=> el.name.includes(merchantDetails().country))?.name }
              name={`country`}
              onChange={(res) => handleChange("country")}
            >
              <option label='select--' value={null}/>
              {countries.map((res) => (
                <option label={res.name} value={res.value} />
              ))}
            </AvInput>
            <Label>Open Banking gateway</Label>
            <AvInput
              
              placeholder="select--"
              type="select"
              defaultValue={merchantDetails().open_banking_gateway}
              onChange={(res) => handleChange("open_banking_gateway")}
              name={`open_banking_gateway`}
              
            >
              <option label ="Select--" value={null}/>
              <option label="YAPILY" value="YAPILY" />
              <option label="DISABLE" value="" />
            </AvInput>
          </FormGroup>
          <FormGroup tag={Col} md="6">
            <Label>Address</Label>
            <AvInput
              // required
              type="text"
              name={`address`}
              value={merchantDetails().address}
              onChange={handleChange("address")}
            />
            <Label>City</Label>
            <AvInput
              // required
              type="text"
              name={`city`}
              value={merchantDetails().city}
              onChange={(res) => handleChange("city")}
            />
            <Label>ZipCode</Label>
            <AvInput
              // required
              type="text"
              name={`zipcode`}
              value={merchantDetails().zipcode}
              onChange={(res) => handleChange("zipcode")}
            />
            <Label>Card GatePay</Label>
            <AvInput
              // required
              type="select"
              name={`card_gateway`}
              value={merchantDetails().card_gateway}
              onChange={(res) => handleChange("card_gateway")}
            >
              <option label="ECOM" value={"ECOM"} />
              <option label="STRIPE" value={"STRIPE"} />
              <option label="Disable" value={""} />
            </AvInput>
            <Label>Dob</Label>
            <Flatpickr
            className="form-control "
            name='reg_date'
            value={moment(merchantDetails().dob).format('YYYY-MM-DD')}
            onChange={(e) =>{
               handleChange('dob');setDate(e[0]);
            }
            }
          />
          </FormGroup>
        </Row>
        <Row>
          <Col md="6">
            <AvField
              label="Currency"
              type="select"
              // required
              placeholder="select--"
              className="mb-1"
              value={merchantDetails().currency}
              name="currency"
              onChange={(e) => handleChange("currency")}
            >
              {Object.keys(currencyCodes).map((res) => (
                <option label={res} value={res} />
              ))}
            </AvField>
          </Col>
          <Col md="6">
            <AvField
              label="Partner "
              type="select"
              value={merchantDetails().partner_id}
              name="partner_id"
              onChange={(e) => handleChange("partner_id")}
            >
              {partners.map((el) => (
                <option label={el.comp_name} value={el.partner_id} />
              ))}
            </AvField>
          </Col>
          
          
        </Row>
        {/* <Row>
         
          <div className='form-group form-password-toggle col-md-6'>
            <Label className='form-label' for={`password-val`}>
              Password
            </Label>  
            <AvInput
              // required
              type='password'
              value={password}
              name={`password-val`}
              id={`password-val`}
              
              onChange={e =>handleChange('password_hash')}
            />
          </div>
          <div className='form-group form-password-toggle col-md-6'>
            <Label className='form-label' for='confirm-password-val'>
              Confirm Password
            </Label>
            <AvInput
              // required
              type='password'                 
              value={confirmPassword}
              name='confirm-password-val'
              id='confirm-password-val'
              onChange={e => setConfirmPassword(e.target.value)}
            />
          </div>
        </Row> */}
        <div className="d-flex justify-content-end mt-1">
          <Button.Ripple
            type="submit"
            color="primary"
            className="btn-next"
            disabled={!merchantData.length || loading}
          >
            <span className="align-middle d-sm-inline-block ">
              {loading ? "Updating..." : "Update"}
            </span>
          </Button.Ripple>
        </div>
      </AvForm>
    </Fragment>
  );
};

export default AccountDetails;
