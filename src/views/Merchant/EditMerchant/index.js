import React,{useEffect, useState}from 'react';
import {Row,Col,TabContent,TabPane,Card,CardBody} from 'reactstrap'
import Tabs from './Tabs';
import General from './General';
import Bussiness from './Bussiness';
import Bank from './Bank';
import EditRole from './EditRole';
import { getAllMerchant } from '../../../Api';
import { merchantDetails } from '..';
import queryString from 'querystring';
import Loading from "@core/components/spinner/Loading-spinner";
import Settings from './Settings';
const EditMerchant=props=>{
const [activeTab,setActiveTab]=useState('1');
const toggleTab =(el)=>setActiveTab(el);
const [loading,setLoading]=useState(true);
const merchant_id =queryString.parse(window.location.search);
useEffect(()=>{
   if(loading){
    getAllMerchant().then(
      res=>{
      if(res.merchant){  let selectMerchant=res.merchant.find(res=>res.merchant_id==merchant_id['?merchant_id']); merchantDetails(selectMerchant); setLoading(false)}
      }
    ).catch(e=>console.log(e))
    }

})

if(loading) return <Loading/>
return(
<Row>
<Col className="mb-2 mb-md-0" md="3">
  <Tabs activeTab={activeTab} toggleTab={toggleTab} />
</Col>
<Col md="9">
  <Card>
    <CardBody>
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
          <General />
        </TabPane>
        <TabPane tabId="2">
          <Bussiness />
        </TabPane>
        <TabPane tabId="3">
          <Bank  />
        </TabPane>
        <TabPane tabId="4">
          <EditRole  />
        </TabPane>
        <TabPane tabId="5">
          <Settings  />
        </TabPane>
    
      </TabContent>
    </CardBody>
  </Card>
</Col>
</Row>
)
}
export default EditMerchant;