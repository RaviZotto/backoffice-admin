import React from 'react';
import { Nav, NavItem, NavLink } from "reactstrap";
import { User, Lock, CreditCard, Briefcase, Settings } from "react-feather";

const Tabs = ({ activeTab, toggleTab }) => {
  return (
    <Nav className="nav-left" pills vertical>
      <NavItem>
        <NavLink active={activeTab === "1"} onClick={() => toggleTab("1")}>
          <User size={18} className="mr-1" />
          <span className="font-weight-bold">General</span>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink active={activeTab === "2"} onClick={() => toggleTab("2")}>
          <Briefcase size={18} className="mr-1" />
          <span className="font-weight-bold">Bussiness Details</span>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink active={activeTab === "3"} onClick={() => toggleTab("3")}>
          <CreditCard size={18} className="mr-1" />
          <span className="font-weight-bold">Bank Details</span>
        </NavLink>
      </NavItem>
   
      <NavItem>
        <NavLink active={activeTab === "4"} onClick={() => toggleTab("4")}>
          <CreditCard size={18} className="mr-1" />
          <span className="font-weight-bold">Edit Role</span>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink active={activeTab === "5"} onClick={() => toggleTab("5")}>
          <Settings size={18} className="mr-1" />
          <span className="font-weight-bold">Settings</span>
        </NavLink>
      </NavItem>
   
    </Nav>
  );
};

export default Tabs;
