// ** React Imports
// ** Custom Components
import Avatar from "@components/avatar";
import React,{ useState, useContext, useEffect,Fragment } from "react";
import {useDispatch} from "react-redux";
// ** Third Party Components
import { Edit, Lock, Trash2 } from "react-feather";
import {
  Button,
  Col,
  CustomInput,
  FormGroup,
  Label,
  Row,
  Table,

  Spinner,
} from "reactstrap";
import Card from "reactstrap/lib/Card";
import CardBody from "reactstrap/lib/CardBody";
// // import { UPDATE_USER, ADD_USER, DELETE_USER } from "@graphql/mutations";
// import { GET_USERS } from "@graphql/queries";

// import { useMutation } from "@apollo/client";
import notification from "../../../components/notification";

import queryString from "querystring";
import { Axios } from "../../../Api";
import { graphqlServerUrl } from "../../../App";
import { merchantDetails } from "..";


const initialPermissions = {
   dashboard: "",
     transfers: "",
     invoices: "",
     paylinks: "",
     subscriptions: "",
     devices:"",
     accounts:"",
     cards:"",
     user:"",
     notification: "",
     settings:"",
     flair:""
  
};

const ManageUser = () => {
    const merchant = queryString.parse(window.location.search);
  const [phoneCode, setPhoneCode] = useState("+44");
  const dispatch = useDispatch();
  // const [updateUser, { loading: updatingUser }] = useMutation(UPDATE_USER);
  // const [deleteUser, { loading: deletingUser }] = useMutation(DELETE_USER);
  // const [addUser, { loading: addingUser }] = useMutation(ADD_USER);
  const[loading,setLoading]=useState(false);
 const [permissions, setPermissions] = useState(initialPermissions);
  const [selectedUser,setSelectedUser] = useState({})
 
  useEffect(() => { 
   
 Axios.get(`${graphqlServerUrl}/admin/getMerchantRole?permission_id=${merchantDetails().permission_id}&&id=${merchant['?merchant_id']}`)
   .then(res=>{if(res.status==200&&res.data)setSelectedUser(res.data.roles) })
    .catch((e: AxiosError)=>notification({title:'Error',message:e.response?e.response.data:e.toString(),type:'error'}))
  },[])





  const fieldsToUpdate = () => {
 
    let newPermissions = {};
    // for (let key of Object.keys(userData)) if (userData[key] != selectedUser[key]) user[key] = userData[key];

    for (let key of Object.keys(permissions))
      if (permissions[key] != selectedUser[key]) newPermissions[key] = permissions[key];

 
    let permLen = Object.keys(newPermissions).length;
    if (permLen == 0) {
      notification({ title: "Nothing changed!", type: "error" });
      return false;
    }

    let dataToReturn = {}
    if (permLen) dataToReturn.roles = newPermissions;
    return dataToReturn;
  };

  const handleFormSubmit = () => {
    
      // update user
      let dataToUpdate = fieldsToUpdate();
      //console.log(dataToUpdate,'2');
      if (!dataToUpdate)return;
      setLoading(true);
      Axios.put(`${graphqlServerUrl}/admin/updateMerchantRole?permission_id=${selectedUser.id}`,dataToUpdate)
      .then(res=>{if(res.status==200 && res.data){setLoading(false)
       setSelectedUser({...selectedUser, ...permissions});
      
      } })
      .catch((e: AxiosError)=>{setLoading(false); notification({title:'Error',message:e.response?e.response.data:e.toString(),type:'error'}) })
  
    
   
  };

  const handlePermissionsChange = (e) => {
    let [field, permCode] = e.target.id.split("-");
    let newValue = permissions[field];
    if (newValue?.includes(permCode)) newValue = newValue.replace(permCode, "");
    else newValue += permCode;
    newValue = newValue.split("").sort().join("");
    let newPermissions = { ...permissions };
    newPermissions[field] = newValue;
    setPermissions(newPermissions);
  };

  

  useEffect(() => {
    if (selectedUser&& selectedUser.id) {
     
      // let permissions = user.permissions;
      // delete permissions.__typename;
      let permissionObj={};
      Object.keys(permissions).forEach((el) => permissionObj[el] = selectedUser[el] );
  
      setPermissions(permissionObj);
        
    } else {
      
      setPermissions(initialPermissions);
    }
  }, [selectedUser]);

  return (
    <Card>
      <CardBody>
        <Row>
          <Col sm="12">
            
                  <Row>
    
                    <Col sm="12">
                      <div className="permissions border mt-1">
                        <h6 className="py-1 mx-1 mb-0 font-medium-2">
                          <Lock size={18} className="mr-25" />
                          <span className="align-middle">Permissions</span>
                        </h6>
                        <Table borderless striped responsive>
                          <thead className="thead-light">
                            <tr>
                              <th>Module</th>
                              <th>Read</th>
                              <th>Create</th>
                              <th>Update</th>
                              <th>Delete</th>
                            </tr>
                          </thead>
                          <tbody>
                            {Object.keys(permissions).map((el, i) => (
                              <tr id={`${el}`} key={i}>
                                <td id={`${el}-0`}>{el[0].toUpperCase() + el.slice(1)}</td>
                                <td>
                                  <CustomInput
                                    type="checkbox"
                                    id={`${el}-R`}
                                    onChange={handlePermissionsChange}
                                    checked={permissions[el]?.includes("R")}
                                  />
                                </td>
                                <td>
                                  <CustomInput
                                    onChange={handlePermissionsChange}
                                    type="checkbox"
                                    id={`${el}-C`}
                                    checked={permissions[el]?.includes("C")}
                                  />
                                </td>
                                <td>
                                  <CustomInput
                                    onChange={handlePermissionsChange}
                                    type="checkbox"
                                    id={`${el}-U`}
                                    checked={permissions[el]?.includes("U")}
                                  />
                                </td>
                                <td>
                                  <CustomInput
                                    onChange={handlePermissionsChange}
                                    type="checkbox"
                                    id={`${el}-D`}
                                    checked={permissions[el]?.includes("D")}
                                  />
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </Table>
                      </div>
                    </Col>
                    <Col className="d-flex flex-sm-row flex-column mt-2" sm="12">
                      <Button className="mb-1 mb-sm-0 mr-0 mr-sm-1" type="submit" color="primary"  onClick={e=>{e.preventDefault();handleFormSubmit()}}>
                        {loading ? <Spinner size="sm" color="white" className="mr-1" /> : null}
                         Update User
                      </Button>
                 
                   
                    </Col>
                  </Row>
           
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};
export default ManageUser;
