import React,{ Fragment, useState } from 'react'
import Select from 'react-select'
import { ArrowLeft, ArrowRight } from 'react-feather'
import Flatpickr from "react-flatpickr";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import { Label, FormGroup, Row, Col, Button } from 'reactstrap'
import { AvForm, AvInput } from 'availity-reactstrap-validation-safe'
import { selectThemeColors } from '@utils'
import {merchantDetails} from '..'
import '@styles/react/libs/react-select/_react-select.scss'
import { countries, phoneCodes } from '../../../utility/Utils'
import courtriesiso3 from '../../../utility/extras/countryiso_3.json'
import moment from 'moment'
import { updateMerchantBussiness } from '../../../Api'
import AvField from 'availity-reactstrap-validation-safe/lib/AvField';
import CardText from 'reactstrap/lib/CardText';
import queryString from 'querystring';
const PersonalInfo = () => {
  const [country,setCountry]=useState(null);
  const [Dob,setDob]=useState(null);
  const [loading,setLoading]=useState(false);
  const [bussiness,setBussiness]=useState([]);
  const [date,setDate]=useState(null);
  const merchant = queryString.parse(window.location.search);
  const onSubmit = async(event, errors,value) => {
    let bussinessObj={};
     try{       
    for(let i=0;i<bussiness.length;i++){
       if(bussiness[i]=='busns_country'){ bussinessObj['busns_country']=country;continue;}
       else if(bussiness[i]=='reg_date'){bussinessObj['reg_date']=date;continue;}
       else if(bussiness[i]=='dir_dob'){bussinessObj['dir_dob']=Dob;continue;}
    bussinessObj[bussiness[i]]=value[bussiness[i]];
    //console.log(bussinessObj,i)
    }
  
    
    setLoading(true);
     await updateMerchantBussiness(merchantDetails().merchant_id||merchant['?merchant_id'], {business:bussinessObj})
    setLoading(false);setBussiness([])
  }catch(e){
    //console.log(e);
  }
    event.preventDefault()
  }
  const handleChange=(key)=>{
    //console.log(key,bussiness);
    if(bussiness.indexOf(key)==-1)setBussiness([...bussiness,key]);
  }
 
  const countryOptions = [...countries  ]


  return (
    <Fragment>
   
      <AvForm onSubmit={onSubmit}>
        <Row>
          <FormGroup tag={Col} md='6' >
            <Label className='form-label' for={`first-name`}>
              Company Name
            </Label>
            <AvInput  type='text' name={`comp_name`} value={merchantDetails().comp_name}  onChange={e=>handleChange('comp_name')} id={`first-name`}  />
             <Label className='form-label  mt-1 ' for={`first-name`}>
            Registartion Date
            </Label>
            <Flatpickr
            className="form-control "
            name='reg_date'
            value={moment(merchantDetails().reg_date).format('YYYY-MM-DD')}
            onChange={(e) =>{
               handleChange('reg_date');setDate(e[0]);
            }
            }
          />
            <Label>City</Label>
           <AvField name='busns_city' type='text' value={merchantDetails().busns_city}  onChange={e=>handleChange('busns_city')} />
            <Label className='form-label' for={`first-name`}>
            Tax Number
            </Label>
            <AvInput  type='number' name={`tax_no`} id={`first-name`}  value={merchantDetails().tax_no} onChange={e=>handleChange('tax_no')} />
          </FormGroup>
         
          <FormGroup tag={Col} md='6'>
          <Label>Company Number</Label>
           <AvField name='comp_no'  type='text' value={merchantDetails().comp_no}  onChange={e=>handleChange('comp_no')} />
            <div className='row'>
          
              <Col md='7' className='mt-0' >
               <Label>Phone code</Label>
                <AvInput 
                type='select'
                name='busns_phone_code'

                value={merchantDetails().busns_phone_code}
                onChange={e=>handleChange('busns_phone_code')}
                >
                 {phoneCodes.map(res=>
                  <option value={res.value} label={res.label} />
                  )}
                </AvInput>
               </Col>
              <Col md='5'className='mt-0' >
            <Label className='form-label'    for={`company_number`}>
             Phone
            </Label>
            <AvInput  type='number' value={merchantDetails().phone_no}  onChange={e=>handleChange('phone_no')} name={`phone_no`} id={`last-name`}  />
           </Col>
            </div>
            <Label className='form-label' for={`company_number`}>
              Registraion Address
            </Label>
            <AvInput  type='text' value={merchantDetails().reg_address}   onChange={e=>handleChange('reg_address')}   name={`reg_address`} id={`last-name`}  />
            <Label className='form-label' for={`company_number`}>   
              Zip Code
            </Label>
            <AvInput  type='text'  onChange={e=>handleChange('busns_zipcode')}  value={merchantDetails().busns_zipcode} name={`busns_zipcode`} id={`last-name`}  />
          
          </FormGroup>
        </Row>
        <Row>
          <FormGroup tag={Col} md='6'>
            <Label className='form-label' for={`country`}>
             Bussiness Country
            </Label>
            <Select
              theme={selectThemeColors}
              isClearable={false}
              id={`country`}
              className='react-select'
              classNamePrefix='select'
              name='busns_country'
              options={countryOptions}
              defaultValue={ countryOptions.find(res=>res.value== merchantDetails().busns_country)}
              onChange={e=>{handleChange('busns_country');setCountry(e.value)}}
              
            />
          </FormGroup>
          <FormGroup tag={Col} md='6'>
          <Label className='form-label' for={`company_number`}>
              Website
            </Label>
            <AvInput  type='text' name={`website`} id={`last-name`} onChange={e=>handleChange('website')} value={merchantDetails().website} />
          </FormGroup>
          <Col >
          
          </Col>
          </Row>
        
        
        <div className='mt-1  d-flex justify-content-end'>
      
          <Button.Ripple type='submit' color='primary' className='btn-next'  disabled={bussiness.length==0 || loading}>
            <span className='align-middle  ' >{loading?"Updating...":"Update"}</span>
            <ArrowRight size={14} className='align-middle ml-sm-25 ml-0'></ArrowRight>
          </Button.Ripple>
        </div>
      </AvForm>
    </Fragment>
  )
}

export default PersonalInfo
