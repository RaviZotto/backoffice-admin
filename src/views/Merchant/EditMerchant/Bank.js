import React,{ Fragment, useState } from 'react'
import { ArrowLeft } from 'react-feather'
import { Label, FormGroup, Row, Col, Button } from 'reactstrap'
import { AvForm, AvInput } from 'availity-reactstrap-validation-safe'
import { updateMerchantBank } from '../../../Api'
import { merchantDetails } from '..'

const SocialLinks = () => {
  const [loading,setLoading]=useState(false);
  const [bank,setBank]=useState([]);
  const onSubmit = async(event, errors,value) => {
    try{     
      let bankObj={};  
      for(let i=0;i<bank.length;i++){
       
      bankObj[bank[i]]=value[bank[i]];
      //console.log(bankObj,i)
      }
    
      
      setLoading(true);
       await updateMerchantBank(merchantDetails().merchant_id, {bank:bankObj})
      setLoading(false);
      setBank([])
    }catch(e){
      setLoading(false);
      //console.log(e);
    }
      event.preventDefault()
  

  }
  const handleChange=(key)=>{
     if(bank.indexOf(key)==-1){setBank([...bank,key])}
  }

  return (
    <Fragment>
    
      <AvForm onSubmit={onSubmit}>
        <Row>
          <FormGroup tag={Col} md='6'>
            <Label className='form-label' for={`twitter`}>
              Account Holder
            </Label>
            <AvInput
          
              type='text'
              id={`twitter`}
              name={`acct_holder_name`}
              value={merchantDetails().acct_holder_name}
              onChange={e=>handleChange('acct_holder_name')}
       
            />
                 <Label className='form-label' for={`twitter`}>
              Account Number
            </Label>
            <AvInput
              
              type='text'
              id={`twitter`}
              name={`acct_no`}
              value={merchantDetails().acct_no}
              onChange={e=>handleChange('acct_no')}
       
            />
            <Label className='form-label' for={`twitter`}>
              Registration Number / Sort Code
            </Label>
            <AvInput
              
              type='text'
              id={`twitter`}
              name={`regno_sortcode`}
              value={merchantDetails().regno_sortcode}
              onChange={e=>handleChange('regno_sortcode')}
       
            />
          </FormGroup>
          <FormGroup tag={Col} md='6'>
            <Label className='form-label' for={`facebook`}>
              IBAN
            </Label>
            <AvInput
              
              type='text'
              id={`facebook`}
              name={`iban`}
              value={merchantDetails().iban}
              onChange={e=>handleChange('iban')}

            />
           <Label className='form-label' for={`google`}>
              BIC/SWIFT
            </Label>
            <AvInput
              type='text'
              id={`google`}
              name={`bic_swift`}
              value={merchantDetails().bic_swift}
              onChange={e=>handleChange('bic_swift')}
             
            />
         
            <Label className='form-label' for='linkedin'>
              Bank Name
            </Label>
            <AvInput
              required
              type='text'
              id={`linkedin`}
              name={`bank_name`}
              value={merchantDetails().bank_name}
              onChange={e=>handleChange('bank_name')}
            />
          </FormGroup>
        </Row>
        <div className='d-flex justify-content-end'>

          <Button.Ripple type='submit' color='success' disabled={bank.length==0} className='btn-submit'>
            Update
          </Button.Ripple>
        </div>
      </AvForm>
    </Fragment>
  )
}

export default SocialLinks
