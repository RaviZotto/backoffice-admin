import React,{ Fragment, useContext, useState } from 'react'
import { ArrowLeft } from 'react-feather'
import { Label, FormGroup, Row, Col, Button } from 'reactstrap'
import { AvForm, AvInput } from 'availity-reactstrap-validation-safe'
import {AddContext} from '.';
const SocialLinks = ({ stepper, type }) => {
  const {setBank}=useContext(AddContext);
  const onSubmit = async(event, errors,values) => {

    //console.log(values);
    if (errors.length) {
    
    }else{
     
     setBank(values);
     stepper.next();
         
    
    }
  }

  return (
    <Fragment>
    
      <AvForm onSubmit={onSubmit}>
        <Row>
          <FormGroup tag={Col} md='6'>
            <Label className='form-label' for={`twitter-${type}`}>
              Account Holder
            </Label>
            <AvInput
              
              type='text'
              id={`twitter-${type}`}
              name={`acct_holder_name`}
       
            />
                 <Label className='form-label' for={`twitter-${type}`}>
              Account Number
            </Label>
            <AvInput
              
              type='text'
              id={`twitter-${type}`}
              name={`acct_no`}
       
            />
            <Label className='form-label' for={`twitter-${type}`}>
              Registration Number / Sort Code
            </Label>
            <AvInput
              
              type='text'
              id={`twitter-${type}`}
              name={`regno_sortcode`}
       
            />
          </FormGroup>
          <FormGroup tag={Col} md='6'>
            <Label className='form-label' for={`facebook-${type}`}>
              IBAN
            </Label>
            <AvInput
              
              type='text'
              id={`facebook-${type}`}
              name={`acct_no`}

            />
           <Label className='form-label' for={`google-${type}`}>
              BIC/SWIFT
            </Label>
            <AvInput
              
              type='text'
              id={`google-${type}`}
              name={`bic_swift`}
             
            />
         
            <Label className='form-label' for='linkedin'>
              Bank Name
            </Label>
            <AvInput
              
              type='text'
              id={`linkedin-${type}`}
              name={`bank_name`}
            />
          </FormGroup>
        </Row>
        <div className='d-flex justify-content-between'>
          <Button.Ripple color='primary'  className='btn-prev' onClick={() => stepper.previous()}>
            <ArrowLeft size={14} className='align-middle mr-sm-25 mr-0'></ArrowLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button.Ripple>
          <Button.Ripple type='submit' color='primary'  className='btn-next'>
          Next
          </Button.Ripple>
        </div>
      </AvForm>
    </Fragment>
  )
}

export default SocialLinks
