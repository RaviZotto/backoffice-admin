import React, { useContext, useState } from "react";
import { Col, Label, Row, FormGroup, Button } from "reactstrap";
import { AvForm, AvInput, AvField } from "availity-reactstrap-validation-safe";
import { useHistory } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import { AddContext } from ".";
import { addMerchant } from "../../../Api";
import notification from "../../../components/notification";
import { ArrowLeft } from "react-feather";
import { TimeZone } from "../../../utility/extras";

const Settings = ({stepper}) => {
  const history = useHistory();
  const { bank, merchant, bussiness } = useContext(AddContext);
  const dispatch = useDispatch();
  const { partners, roles, types, subtypes } = useSelector(
    (state) => state.merchant
  );
  const [file, setFile] = useState(null);
  const [loading, setLoading] = useState(false);
  const handleFile = ({
    target: {
      validity,
      files: [file],
    },
  }) => {
    if (validity.valid) {
      setFile(file);
    }
  };

  const onSubmit = async (event, error, values) => {
    event.preventDefault();
    try {
      if (error.length) {
      } else {
       
        let formData = new FormData();
        formData.append("logo", file);
         delete values.image;
         if(values.password!=values.confirmpassword) {
           return notification({type: 'error', message:"Confirm Password not Matched",title:'Password validation Error'});
         }
         delete values.confirmpassword;
        formData.append(
          "data",
          JSON.stringify({ merchant1:{...merchant,...values}, bussiness, bank })
        );
        setLoading(true);
        await addMerchant(formData);
        setLoading(false);
        history.goBack();
      }
    } catch (e) {
      notification({ title: "Error", message:e.err?e.err: e.toString(), type: "error" });
      setLoading(false);
    }
  };

  return (
    <AvForm onSubmit={onSubmit}>
        <Col className="pt-2">
          <AvField
            type="file"
            className="ml-0"
            name="image"
            onChange={handleFile}
          />
        </Col>    
      <Row>
     
        <Col>
          <Label>Category</Label>
          <AvInput required placeholder="select--" type="select" name={`type`}>
            <option label={"select"} value={null} />
            {types.length &&
              types.map((res) => (
                <option value={res.id} label={res.type_name} />
              ))}
          </AvInput>
        </Col>
        <Col>
          <Label>Sub category</Label>
          <AvInput
            required
            placeholder="select--"
            type="select"
            name={`subtype`}
          >
            <option label={"select"} value={null} />
            {subtypes.length &&
              subtypes.map((res) => (
                <option value={res.id} label={res.subtype_name} />
              ))}
          </AvInput>
        </Col>
        <Col>
          <Label>Risk Level</Label>
          <AvInput
            required
            placeholder="select--"
            type="select"
            name={`risk_level`}
          >
            <option label={"select"} value={null} />
            <option label={"LOW"} value="LOW" />
            <option label={"MEDIUM"} value="MEDIUM" />
            <option label={"HIGH"} value="HIGH" />
          </AvInput>
        </Col>
      </Row>
      <Row className="mt-1">
         <Col >
            <AvField
             label='Password'
              required
              type="password"
              validate={{
                required: {value: true, errorMessage: 'Please Choose the password'},
                pattern: {value: /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])/, errorMessage: 'Your password must have atleast 1 upper character , 1 lower character , 1 special character and 1 number'},
                minLength: {value: 8, errorMessage: 'Your name must be between 6 and 16 characters'},
                maxLength: {value: 16, errorMessage: 'Your name must be between 6 and 16 characters'}
              }}
              name={`password`}
              
            />  
     </Col>
     <Col>
            <AvField
             label='Confirm Password'
              required
              type="text"
              required
              name={`confirmpassword`}
              
            />  
     </Col>
        <Col>
          <AvField name="partner_id" type="select" label="Partner" required>
            <option label={"select"} value={null} />
            {partners.map((el) => (
              <option label={el.comp_name} value={el.partner_id} />
            ))}
          </AvField>
        </Col>
       
      </Row>
      <Row>
          <Col md="6">
            <AvField
              label="Time Zone "
              type="select"
              required
              name="timezone"
              
            >
              <option value={null} label="select --"/>
              {TimeZone.map((el) => (

                <option label={el} value={el} />
              ))}
            </AvField>
          </Col>
          <Col>
          <Col>
          <AvField name="wallet_id" type="text" label="Wallet"  />
        </Col>
          </Col>
          </Row>
      <div className="d-flex justify-content-between">
        <Button.Ripple
          color="primary"
          disabled={loading}
          className="btn-prev"
          onClick={() => stepper.previous()}
        >
          <ArrowLeft
            size={14}
            className="align-middle mr-sm-25 mr-0"
          ></ArrowLeft>
          <span className="align-middle d-sm-inline-block d-none">
            Previous
          </span>
        </Button.Ripple>
        <Button.Ripple
          type="submit"
          color="success"
          disabled={loading}
          className="btn-next"
        >
          {loading?'Saving..':'Save'}
        </Button.Ripple>
      </div>
    </AvForm>
  );
};

export default Settings;
