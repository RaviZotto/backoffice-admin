

import React, {
  Fragment,
  useContext,
  useEffect,
  useState,
} from "react";
import { ArrowLeft, ArrowRight } from "react-feather";
import { Label, FormGroup, Row, Col, Button } from "reactstrap";
import {
  AvForm,
  AvInput,

} from "availity-reactstrap-validation-safe";


import {countries,currencyCountryCodes} from "@utils";
import { phoneCodes } from "../../../utility/Utils";
import { AddContext } from ".";
import AvField from "availity-reactstrap-validation-safe/lib/AvField";
import { useDispatch, useSelector } from "react-redux";
import { GET_PARTNER, GET_TYPE,GET_SUBTYPE } from "../../../redux/actions/storeMerchant.";
const AccountDetails = ({ stepper, type }) => {
  const [currency, setCurrency] = useState([]);
 
  const dispatch = useDispatch();
  const { partners ,roles,types,subtypes} = useSelector((state) => state.merchant);
  const { merchant, setMerchant } = useContext(AddContext);

  const [file, setFile] = useState("");
 const [generalField,setGeneralField]=useState({
   phone_code:"+44",
   currency: "GBP",
   country: "GB"
  

 })
  





  useEffect(() => {
  console.log(generalField)
  },[generalField])

 
  useEffect(() => {
    //console.log(generalRef.current);
    if (!partners.length) dispatch(GET_PARTNER());
    if(!types.length)dispatch(GET_TYPE())
    if(!subtypes.length)dispatch(GET_SUBTYPE())
  
    // if (!currency.length) {
    //   CurrencyFilter = CurrencyFilter.filter(
    //     (res, i) => CurrencyFilter.indexOf(res) == i
    //   );
    //   setCurrency(CurrencyFilter);
    // }
  }, []);

  const onSubmit = (event, errors, value) => {
    if (errors.length) {
    } else {
      //console.log(value);
      delete value["password-val"];
     setMerchant({ ...value, ...file,...generalField });
      stepper.next();
    }
    event.preventDefault();
  };

  return (
    <Fragment>
      <div className="content-header"></div>
      <AvForm onSubmit={onSubmit} autoComplete="off" model={generalField} >
        <Row>
          <FormGroup tag={Col} md="6">
            <Label className="form-label" for={`username-${type}`}>
              Name
            </Label>
            <AvInput
              autoComplete="off"
              required
              type="text"
              name={`name`}
              id={`username-${type}`}
              placeholder="Enter your name"
            />
            <Label className="form-label" for={`email-${type}`}>
              Email
            </Label>
            <AvInput
              required
              type="email"
              name={`email`}
              placeholder="Email"
              aria-label="john.doe"
            />
            <Row>
              <Col md={"6"}>
                <Label>Phone Code</Label>
                <AvInput
                  required
                  type="select"
                  name={`phone_code`}
                  value={generalField.phone_code}
                  
                >
                  {phoneCodes.map((res) => (
                    <option value={res.value} label={res.label} />
                  ))}
                </AvInput>
              </Col>
              <Col md="6">
                <Label>Phone</Label>
                <AvInput required type="number" name={`phone`} />
              </Col>
            </Row>
            <Label>Country</Label>
            <AvField
              required
              placeholder="select--"
              type="select"
              value={generalField.country}
              name={`country`}
              onChange={e=>{ 
                console.log(e.target.label,e.target.value,e.label)
               setGeneralField({
                 country:e.target.value,
                 phone_code:countries.find(e1=>e1.value==e.target.value)?.phone_code,
                 currency:currencyCountryCodes[e.target.value]
               }) 
              }}
            >
              {countries.map((res) => (
                <option label={res.label} value={res.value} />
              ))}
            </AvField>
            <Label>Open Banking gateway</Label>
            <AvInput
              required
              placeholder="select--"
              type="select"
              value="YAPILY"
              name={`open_banking_gateway`}
            >
              <option label={"YAPILY"} value="YAPILY" />
              <option label={"DISABLE"} value={""} />
            </AvInput>
          </FormGroup>
          <FormGroup tag={Col} md="6">
            <Label>Address</Label>
            <AvInput type="text" name={`address`} />
            <Label>City</Label>
            <AvInput type="text" name={`city`} />
            <Label>ZipCode</Label>
            <AvInput  type="text" name={`zipcode`} />
            <Label>Card GatePay</Label>
            <AvInput  type="select" name={`card_gateway`} defaultValue="ECOM">
              <option label="ECOM" value={"ECOM"} />
              <option label="STRIPE" value={"STRIPE"} />
              <option label="Disable" value={""} />
            </AvInput>
            <Label>Status</Label>
            <AvInput required value="Active" type="select" name={`status`}>
              <option label="Active">Active</option>
              <option label="Inactive">Inactive</option>
            </AvInput>
          </FormGroup>
        </Row>

        <Row>
          <div className="form-group form-password-toggle col-md-6">
          <AvField name="dob" label='DoB' required type="date"   validate={{ dateRange: { start: { value: -100, units: 'years', }, end: { value: -18, units: 'years', } } }}  />
           
           
          </div>
          <div className="form-group form-password-toggle col-md-6">
            <Label className="form-label" for="confirm-password-val">
              Currency
            </Label>
            <AvInput required type="select" value={currencyCountryCodes[generalField.country]} name="currency">
              {Object.entries(currencyCountryCodes).map((res) => (
                <option value={res[1]} label={res[1]} />
              ))}
            </AvInput>
          </div>

         
        </Row>
      
        <div className="d-flex justify-content-end">
          <Button.Ripple type="submit" color="primary" className="btn-next">
            <span className="align-middle d-sm-inline-block ">Next</span>
            <ArrowRight
              size={14}
              className="align-middle ml-sm-25 ml-0"
            ></ArrowRight>
          </Button.Ripple>
        </div>
      </AvForm>
    </Fragment>
  );
};

export default AccountDetails;
