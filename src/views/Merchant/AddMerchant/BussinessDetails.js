import React, { Fragment,  useContext, useState } from "react";
import Select from "react-select";
import { ArrowLeft, ArrowRight } from "react-feather";
import { Label, FormGroup, Row, Col, Button, CardTitle } from "reactstrap";
import { AvForm, AvInput } from "availity-reactstrap-validation-safe";
import { selectThemeColors } from "@utils";
import { AddContext } from ".";
import "@styles/react/libs/react-select/_react-select.scss";
import { countries, phoneCodes } from "../../../utility/Utils";
import AvField from "availity-reactstrap-validation-safe/lib/AvField";

// import "@availity/reactstrap-validation-date/styles.scss";
const PersonalInfo = ({ stepper, type }) => {
  const now = new Date().toJSON().split('T')[0];


let sevenYearsAgo = new Date();
sevenYearsAgo.setFullYear(sevenYearsAgo.getFullYear() - 100);
sevenYearsAgo = sevenYearsAgo.toJSON().split('T')[0];

  const [generalField,setGeneralField] = useState({ 
    busns_country:'GB',
    busns_phone_code:'+44'
  });
  const { setBussiness } = useContext(AddContext);
  const onSubmit = (event, errors, value) => {
    if (errors.length) {
    } else {
      setBussiness({ ...value, ...generalField });
      stepper.next();
    }
    event.preventDefault();
  };

  const countryOptions = [...countries];



  return (
    <Fragment>
      <AvForm onSubmit={onSubmit}>
        <Row>
          <FormGroup tag={Col} md="6">
            <Label className="form-label" for={`first-name-${type}`}>
              Company Name
            </Label>
            <AvInput
              required
              type="text"
              name={`comp_name`}
              id={`first-name-${type}`}
            />
            <Label className="form-label" for={`first-name-${type}`}>
              Registration Date
            </Label>
            {/* <AvInput required type='date' name={`reg_date`} value={moment(new Date()).format('DD/MM/YYYY')} id={`first-name-${type}`}  /> */}
            <AvField name="reg_date"   type="date" validate={{ dateRange: { start: { value: -100, units: 'years', }, end: { value: -1, units: 'days', } } }}  />
            <Label className="form-label" for={`first-name-${type}`}>
              Tax Number
            </Label>
            <AvInput
              required
              type="number"
              name={`tax_no`}
              id={`first-name-${type}`}
            />
              <Label className="form-label" for={`company_number`}>
              Website
            </Label>
            <AvInput type="text" name={`website`} id={`last-name-${type}`} />
          </FormGroup>

          <FormGroup tag={Col} md="6">
          
          
            <Label>Company Number</Label>
            <AvInput required type="text" name="comp_no" />
            <Row>
              <Col md="6">
                 <AvField type="select" name="busns_phone_code" 
                 value={generalField.busns_phone_code}
                 label="Phone Code">
                  {phoneCodes.map((res) => (
                    <option value={res.value} label={res.label} />
                  ))}
                </AvField>
              </Col>
              <Col md="6">
         
                <AvField
                  required
                  label="Phone"
                  type="number"
                  name={`phone_no`}
                  id={`last-name-${type}`}
                />
              </Col>
            </Row>
            <Label className="form-label" for={`company_number`}>
              Registraion Address
            </Label>
            <AvInput
              type="text"
              name={`reg_address`}
              id={`last-name-${type}`}
            />
            <Label className="form-label" for={`company_number`}>
              Zip Code
            </Label>
            <AvInput
            
              type="text"
              name={`busns_zipcode`}
              id={`last-name-${type}`}
            />
          </FormGroup>
        </Row>
        <Row>
          <FormGroup tag={Col} >
            <Label className="form-label" for={`country-${type}`}>
              Bussiness Country
            </Label>
            <Select
              theme={selectThemeColors}
              isClearable={false}
              id={`country-${type}`}
              className="react-select"
              classNamePrefix="select"
              name="busns_country"
              value={countryOptions.find(e=>e.value==generalField.busns_country)}
              onChange={(e) => {
                console.log(e)
                setGeneralField({ 
               busns_country: e.value, 
               busns_phone_code:e.phone_code
              })}}
              options={countryOptions}
              defaultValue={countryOptions[0]}
            />
          </FormGroup>
          <FormGroup tag={Col}>
          <Label className="form-label" for={`country-${type}`}>
              Bussiness City
            </Label>
           <AvInput type='text' required name='busns_city' />
          </FormGroup>
        
        </Row>
      
      
        <div className=" mt-1 d-flex justify-content-between">
          <Button.Ripple
            color="primary"
            className="btn-prev"
            onClick={() => stepper.previous()}
          >
            <ArrowLeft
              size={14}
              className="align-middle mr-sm-25 mr-0"
            ></ArrowLeft>
            <span className="align-middle d-sm-inline-block d-none">
              Previous
            </span>
          </Button.Ripple>
          <Button.Ripple type="submit" color="primary" className="btn-next">
            <span className="align-middle ">Next</span>
            <ArrowRight
              size={14}
              className="align-middle ml-sm-25 ml-0"
            ></ArrowRight>
          </Button.Ripple>
        </div>
      </AvForm>
    </Fragment>
  );
};

export default PersonalInfo;
