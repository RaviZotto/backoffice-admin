import React,{ createContext, useMemo, useRef, useState } from 'react';
import {Button} from 'reactstrap';
import Wizard from '@components/wizard'
import General from './General'
import BussinesDetails from './BussinessDetails';
import PaymentGateway from './CurrencyCredentials';
import BankDetails from './BankDetails'
import { Fragment } from 'react'
import { ArrowLeft } from 'react-feather'
import {useHistory} from 'react-router-dom';
import Settings from './Settings';
export const AddContext = createContext(null);


const WizardHorizontal = () => {
  const history = useHistory();
  const [stepper, setStepper] = useState(null)
  const ref = useRef(null)
const[merchant,setMerchant]=useState({});
const [bussiness,setBussiness]=useState({});
const [bank,setBank]=useState({});
const [paymethod,setPaymethod]=useState({});
   const Addprovider=useMemo(()=>({
    bussiness,
    setBussiness,
    merchant,
    setMerchant,
    setBank,
    bank,
    paymethod,
    setPaymethod 


   }),[merchant,bussiness,bank,paymethod])


  const steps = [

  
    {
      id: 'general-details',
      title: 'General Details',
    
      content: <General stepper={stepper} type='wizard-horizontal' />
    },
    {
      id: 'bussiness-details',
      title: 'Bussiness Details',
      content: <BussinesDetails stepper={stepper} type='wizard-horizontal' />
    },
  
    {
      id: 'bank-details',
      title: 'Bank Details',
      content: <BankDetails stepper={stepper} type='wizard-horizontal' />
    },
    { 
      id:'settings',
      title:'Settings',
      content: <Settings stepper={stepper} type='wizard-horizontal' />
    },
   

  ]

  return (
    <AddContext.Provider value={Addprovider}>
    <Button.Ripple color='primary' className='mb-2' onClick={e=>history.goBack()} ><ArrowLeft size={12} className='mr-2'  />Go Back</Button.Ripple>
    <div className='horizontal-wizard'>
      <Wizard instance={el => setStepper(el)} ref={ref} steps={steps} />
    </div>
    </AddContext.Provider>
  )
}

export default WizardHorizontal
