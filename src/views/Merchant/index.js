import React, { useEffect,useState } from 'react';
 import { makeVar } from '@apollo/client';
import { Fragment } from 'react';
// import { GET_MERCHANT } from '../../graphql/queries';
import Loading from "@core/components/spinner/Loading-spinner";
import Table from './Table';
import { useHistory } from 'react-router';
import { useDispatch } from 'react-redux';
import { UPDATE_MERCHANT } from '../../redux/actions/storeMerchant.';
import { userDetails } from '../../utility/Utils';
export const merchantDetails=makeVar({});
export const merchantAllDetails=makeVar([]);

const Merchant =props=>{
    const dispatch=useDispatch();
    const[loading,setLoading]=useState(true);
    const history=useHistory()
//   const {data,loading, error}= useQuery(GET_MERCHANT);
   const [refetch,setRefetch]=useState(false);
   const [merchant,setMerchant]=useState([]);
   const [message,setMessage]=useState(false);
   

  useEffect(()=>{
     if(!userDetails) userDetails = JSON.parse(localStorage.getItem('userDetails'));
      if(loading){
      dispatch(UPDATE_MERCHANT(()=>{setTimeout(()=>setLoading(false),1000)}));
      
    }

    } )

  
if(loading)return<Loading/>;

return  (
        <Fragment>
            <Table setMessage={setMessage} merchant= {merchant} refetch={()=>setRefetch(true)} history={history} />
            <div className={{zIndex:'999999999'}}>
         
            </div>
        </Fragment>

    )
}

export default Merchant;