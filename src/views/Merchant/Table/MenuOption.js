import React from 'react';
import { MoreVertical } from 'react-feather';
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from 'reactstrap';
import {deleteMerchant} from '@src/Api';
import notification from "@src/components/notification";
import { merchantDetails } from "..";

const MenuOption = ({data,className,style,dispatch,userDetails,history}) => {
                               
    const deleteMerchantById=async(id)=>{ 
        try{ 
            await deleteMerchant(id);
             dispatch({type:"DEL_MERCHANT",payload:id})
        }catch(e){ 
          notification({ title: "Error", message: e.toString(),type: "error"});
        }
      }



    return (
        <UncontrolledDropdown className="chart-dropdown  " >
        <DropdownToggle
          color=""
          className="bg-transparent btn-sm border-0 p-50"
        >
          <MoreVertical size={18} className={`cursor-pointer ${className} `} style={style} />
        </DropdownToggle>
        <DropdownMenu top  className="position-absolute  mr-0  "   >
          <DropdownItem
            className="w-100 d-flex text-center"
             onClick={(e) =>{
            
            history.push(`/merchant-section?merchant_id=${data.merchant_id}`);
           
            }}
          >
            View
          </DropdownItem>
          <DropdownItem
            className="w-100 d-flex text-center"
            disabled={!( !userDetails.role_type ||userDetails.role_type=='admin'|| userDetails.role_type&&userDetails['merchant'].includes('U'))}
             onClick={(e) =>{
                
                history.push(`/edit-merchant?merchant_id=${data.merchant_id}`);
                merchantDetails(data)
           
            }}
          >
            Edit
          </DropdownItem>
        
          <DropdownItem
            className="w-100 d-flex"
            onClick={(e) =>{
                 deleteMerchantById(data.merchant_id)
            }
          }
          >
            Delete
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    )
}

export default MenuOption
