import React,{ useContext, Fragment, useEffect, useState, useMemo } from "react";
import CustomHeader from "./Header";
import Avatar from "@components/avatar";

// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge } from "reactstrap";
import moment from "moment";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";


import CustomInput from "reactstrap/lib/CustomInput";
import { TOGGLE_MERCHANT, UPDATE_MERCHANT } from "../../../redux/actions/storeMerchant.";
import { useDispatch, useSelector } from "react-redux";
import MenuOption from "./MenuOption";


const Table = ({history,refetch,setMessage}) => {
  
  const dispatch =useDispatch()
  const [rowId,setRowId]=useState(null);
  const [selectedPartner, setSelectedPartner]=useState();
  const [searchQuery,setSearchQuery]=useState();
  const [loading,setLoading]=useState(true);
  const {merchants}=useSelector(state=>state.merchant);
  const {adminDetails:userDetails}=useSelector(state=>state.auth);
  const [merchant,setMerchant]=useState([]);
  
  useEffect(()=>{
    if(merchants.length){
      setMerchant([...merchants]);
      
    }
    
     
  },[merchants])


  const renderClient = (row, i) => {
    const stateNum = i % 6,
      states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
      color = states[stateNum];
  
    return <Avatar color={color || "primary"} className="mr-1" content={row.comp_name || "John Doe"} initials />;
  };
   const columns = useMemo(()=>

  ( [
    {
      name: "Name", 
      selector: "name",
      sortable: true,
      minWidth: "150px",
      
      cell: (row,i) => {
        // let date = moment(row.date_of_transaction);
        return (
          <Fragment>
             {renderClient(row, i)}
         <span className='text-wrap'>{row.comp_name}</span>
          </Fragment>
        );
      },
    },
    {
      name: "Phone",
      selector: "phone",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.phonecode+row.phone}</span>
        </Fragment>
      ),
    },
    {
      name: "Email",
      selector: "email",
      minWidth: "170px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span className='text-wrap '>{row.email}</span>
        </Fragment>
      ),
    },
    {
      name:"Merchant Key",
      minWidth:"100px",
      selector:"api_key",
      cell:(row)=><p>{row.api_key}</p>
    },
    {
      name:"Secret Key",
      minWidth:"100px",
      maxWidth:"200px",
      selector:"secret_key",
      cell:(row)=><p>{row.secret_key}</p>
    },
    {
      name: "Action",
      maxWidth: "50px",
      selector: "",
      sortable: true,
      cell: (row) => (
        <Fragment>
        <div className="d-flex " style={{padding:'1em'}}>
          <MenuOption history={history} data={row}  userDetails={userDetails} dispatch={dispatch}/>
          {/* <Eye size={15} className="cursor-pointer"  onClick={e=>history.push(`/merchant-section?merchant_id=${row.merchant_id}`)} />
         {( !userDetails.role_type ||userDetails.role_type=='admin'|| userDetails.role_type&&userDetails['merchant'].includes('U'))?<Edit size={15} className="ml-1 cursor-pointer" onClick={e=>{history.push(`/edit-merchant?merchant_id=${row.merchant_id}`);merchantDetails(row)}} />:null}
          {rowId==row.merchant_id&&Delete?<Spinner size="sm" />:<Trash size={15} className=" cursor-pointer" onClick={e=>{setRowId(row.merchant_id);setDelete(true)}} />} */}
        </div>
        </Fragment>
      ),
    },

  
    {
      name: "Status",
      minWidth: "110px",
      selector: "status",
      sortable: true,
      cell: (row) => (
        <div style={{zIndex:0}}>
        <CustomInput
          type="switch"
          className={row.id ? "custom-control-success " : ""}
          id={`status-${row.id}`}
         defaultChecked={row.status==1 ? true: false}
          disabled={(userDetails.role_type&& userDetails.role_type!='admin' && userDetails.role_type&&!userDetails['merchant'].includes('U'))}
          name="customSwitch"
          onChange={el=>{dispatch(TOGGLE_MERCHANT(row.merchant_id,el.target.checked))}}
          inline
        />
        </div>
      ),
    },

  ]),[userDetails!=null])

  const dataToRender = () => {
    let tempData=merchant;
    //console.log(selectedPartner,tempData)
     if(selectedPartner){
        //console.log(selectedPartner);
        tempData=tempData.filter(el=>el.partner_id==selectedPartner);
     }
     if(searchQuery){
       let searchClone=searchQuery;
        searchClone=searchClone.toLowerCase();
       tempData=tempData.filter(el=> el.open_banking_gateway&& el.open_banking_gateway.toLowerCase()==searchClone ||   el.api_key==searchClone || el.comp_name&& el.comp_name.toLowerCase().includes(searchClone)
       || el.railsbank_id?.startsWith(searchClone)  || el.email&&el.email.toLowerCase().includes(searchClone)||el.phone&&el.phone_code&&(el.phone_code+el.phone).includes(searchClone)
       );
     }
    return tempData;
  };

  return (
    <div className="invoice-list-wrapper">
      <CustomHeader setMessage={setMessage} history={history} setSelectedPartner={setSelectedPartner} setSearchQuery={setSearchQuery} />
      <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>
    </div>
  );
};

export default Table;
