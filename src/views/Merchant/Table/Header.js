import React, { useEffect, useState } from "react";
import { Fragment } from "react";
import { PlusCircle, Search } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import { Row, Col, Button, Input, Card } from "reactstrap";
import {userDetails} from '..'
import { GET_PARTNER } from "../../../redux/actions/storeMerchant.";
import styled  from 'styled-components'
import { STORE_ADMINDETAIL } from "../../../redux/actions/auth";
const CustomHeader = ({ history ,setSelectedPartner,setSearchQuery,setMessage}) => {
const [partner,setPartner]=useState([{}]);
const dispatch=useDispatch();
const [user,setUser]=useState({})
const {partners}=useSelector(state=>state.merchant);
const {adminDetails:userDetails} = useSelector(state=>state.auth)
  useEffect(() => {
   if(!userDetails||Object.keys(userDetails).length)dispatch(STORE_ADMINDETAIL(JSON.parse(localStorage.getItem('userDetails'))))
    if(partners&&!partners.length){
       dispatch(GET_PARTNER())
    }else{
      setPartner(partners.map(el=>({label:el.comp_name,value:el.partner_id})));
    }
    

  }, [partners])
  useEffect(()=>{if(userDetails&&Object.keys(userDetails).length){setUser(userDetails)}},[userDetails])
  return (
    <Fragment>
      <Button.Ripple
        color="primary"
        className="pl-1 mb-2"
        onClick={(e) => history.push("/Merchant-manage")}
        disabled={ userDetails.role_types&& userDetails.role_type!='admin'? userDetails.role_type&&!userDetails['merchant'].includes('C'):false}
      >
        <PlusCircle size={15} className="mr-1" />
        Add Merchant
      </Button.Ripple>
      <Card className='p-1 '>
        <Row className="align-items-center justify-content-between">
        <Col md={6} sm={12} className="align-items-center d-flex" > 
        <Search/>       
        <Input name='serachQuery' style={{border:'none'}} placeholder='search by name'
        onChange={el=>setSearchQuery(el.target.value)}
        />
  
        </Col>
     
        <Col md={4} sm={12} >
    
        <Selectnew
         placeholder='select partner'
         id='partner_select'
         style={{"zIndex":99999}}
         menuPlacement="bottom"
         menuPosition="relative"
         options={partner}
         onChange={el=> {setSelectedPartner(el.value)}}
                  
        />
        </Col>
       </Row>
        </Card>
    </Fragment>
  );
};

export default CustomHeader;
const Selectnew = styled(Select)`
   z-index:1;
`
