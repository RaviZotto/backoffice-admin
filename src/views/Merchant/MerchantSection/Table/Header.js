import { Row,Card, Col, Button, Input } from "reactstrap";
import Flatpickr from "react-flatpickr";
import { Search } from "react-feather";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import React,{ useContext, useState } from "react";
import moment from "moment";

const CustomHeader = () => {
  // const { dates, setDates, searchQuery, setSearchQuery, statusQuery, setStatusQuery } = useContext(TransactionsContext);

  const [state, setState] = useState({
    startDate: new Date().getUTCDate(),
    endDate: new Date().getUTCDate(),
  });

  return (
    <Card className="p-1 pr-0">
    <div className="row row-flex-wrap pr-sm-0">
      <div className="col-md-2 col-6  ">
        <Flatpickr
          className="form-control"
          // value={dates.startDate}
          onChange={(e) =>
            setState({
              ...state,
              startDate: moment(e[0]).format("YYYY-MM-DD"),
            })
          }
        />
      </div>
      <div className="  col-md-2 col-6   ">
        <Flatpickr
          className="form-control "
          // value={dates.endDate}
          onChange={(e) =>
            setState({ ...state, endDate: moment(e[0]).format("YYYY-MM-DD") })
          }
        />
      </div>
      <div className=" col-12 col-md-1 ">
        <Button
          className="w-100 mb-2 mt-1 mb-md-0 mt-md-0  "
          color="primary"
          // onClick={() => setDates({ ...state })}
        >
          <Search size={17} />
        </Button>
      </div>

      <div className="col-12 col-md-2  mb-sm-0 mb-1 ml-md-auto">
        <Input
          id="search-invoice"
          type="text"
          // onChange={(e) => setSearchQuery(e.target.value)}
          placeholder="Search"
        />
      </div>
      <div className=" col-md-2 col-12 mr-sm-0 col-12">
        <Input
          className="form-control"
          type="select"
          // onChange={(e) => setStatusQuery(`${e.target.value}`)}
        >
          <option value="">Select Status</option>
          <option value="1">Success</option>
          <option value="2">Pending</option>
          <option value="0">Failed</option>
        </Input>
      </div>
    </div>
  </Card>
  );
};

export default CustomHeader;
