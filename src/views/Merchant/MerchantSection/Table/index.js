import React,{ useContext, Fragment, useEffect, useState } from "react";
import CustomHeader from "./Header";

// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge } from "reactstrap";
import moment from "moment";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import Spinner from "reactstrap/lib/Spinner";

const Table = () => {



  const columns = [
    {
      name: "Name",
      selector: "name",
      sortable: true,
      minWidth: "110px",
      cell: (row) => {
        // let date = moment(row.date_of_transaction);
        return (
          <Fragment>
         <span>{row.name}</span>
          </Fragment>
        );
      },
    },
    {
      name: "Phone",
      selector: "phone",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.phone_code+row.phone}</span>
        </Fragment>
      ),
    },
    {
      name: "Email",
      selector: "email",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.email}</span>
        </Fragment>
      ),
    },

    {
      name: "Action",
      minWidth: "110px",
      selector: "",
      sortable: true,
      cell: (row) => (
        <div className="column-action d-flex align-items-center">
          {/* <Eye size={15} className="cursor-pointer"  onClick={e=>history.push(`/Merchant-section?merchant_id=${row.merchant_id}`)} />
          <Edit size={15} className="mx-1 cursor-pointer" onClick={e=>{history.push(`/edit-merchant?merchant_id=${row.merchant_id}`);merchantDetails(row)}} />
          {rowId==row.merchant_id&&Delete?<Spinner size="sm" />:<Trash size={15} className=" cursor-pointer" onClick={e=>{setRowId(row.merchant_id);setDelete(true)}} />} */}
        </div>
      ),
    },
  ];

  const dataToRender = () => {
    let tempData=[]
 
    return tempData;
  };

  return (
    <div className="invoice-list-wrapper">
      <CustomHeader  />
      <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={[]}
          />
        </div>
      </Card>
    </div>
  );
};

export default Table;
