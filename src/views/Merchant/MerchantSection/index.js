import React, {
  createContext,
  Fragment,
  useEffect,
  useMemo,
  useState,
} from "react";
import { Nav, NavItem, NavLink, TabContent, TabPane, Card } from "reactstrap";
import Accounts from "./Accounts/Accounts";
import Device from "./Device/Device";
import Cards from "./Card/Cards";
import queryString from "querystring";
import { getAllLedgerAccount, getMerchant, getAllDevice, getAdminTransactions, getHanpointKey } from "../../../Api";
import ManageKeys from "./ManageKeys/ManageKeys";
import { useHistory } from "react-router";
import Loading from "@core/components/spinner/Loading-spinner";
import { useQuery } from "@apollo/client";
import { GET_TRANSACTIONS } from "../../../graphql/queries";
import moment from "moment";
import Fees from "../../merchantFee/Fees";
import notification from "../../../components/notification";
import { GET_CURRENCY } from "../../../redux/actions/transfer";
import { useDispatch, useSelector } from "react-redux";
export const SectionContext = createContext();
const MerchantSection = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const {currencies} = useSelector(state => state.transfer);
  const [merchant, setMerchant] = useState({});
  const [loader, setLoading] = useState(true);
  const [accounts, setAccounts] = useState([]);
  const [refetch, setRefetch] = useState(false);
  const [cards, setCards] = useState([]);
  const [device, setDevice] = useState([]);
  const merchant_id = queryString.parse(window.location.search);
  const [transactions, setTransactions] = useState([]);
  const [credentials,setCredentials]=useState({});
  const [dates, setDates] = useState(() => {
    let temp = new Date();
    let end_date = moment(temp).format("YYYY-MM-DD");
    let start_date = temp.setDate(temp.getDate() - 6);
    start_date = moment(start_date).format("YYYY-MM-DD");
    return { start_date, end_date };
  });
  // const { data, error, loading } = useQuery(GET_TRANSACTIONS, {
  //   variables: { ...dates, merchant_id: parseInt(merchant_id["?merchant_id"]) },
  // });

  useEffect(() => {
    (async () => {
      try {
        let transaction = await getAdminTransactions({...dates,merchant_id:merchant_id["?merchant_id"],payment_mode:4});
        //console.log(transaction);

        if (transaction.length) setTransactions(transaction);
      } catch (e) {
        notification({ title: "Error", message: e, type: "error" });
      }
    })();
  }, [dates]);




  useEffect(() => {
    
    if (loader || refetch) {
      (async()=>{
      try {
        if(!currencies.length)dispatch(GET_CURRENCY())
        const {merchant} = await getMerchant(merchant_id["?merchant_id"])
          setMerchant(merchant);
            //console.log(res.merchant);
           const {Accounts} = await getAllLedgerAccount({ merchant_id: merchant.merchant_id });
             setAccounts(Accounts.length?Accounts : []);
             const {device}= await  getAllDevice();
              setDevice(device.length?device: []);
              const {handpoint} = await getHanpointKey(merchant_id["?merchant_id"]);
             console.log(handpoint);
              setCredentials(handpoint?handpoint:{});
              setLoading(false);
              setRefetch(false);
               
                   
                //console.log(e);
            
           } catch (e) {
             console.log(e);
             notification({title:"Error",message:typeof e=='object'?e.message:e,type:"error"})
        setLoading(false);
        setRefetch(false);
        //console.log(e.toString());
      }
    })()
    }
  }, [refetch]);



  const sectionProvider = useMemo(
    () => ({
      merchant,
      transactions,
      setTransactions,
      history,
      dates,
      setDates,
      cards,
      device,
      setDevice,
      setCards,
      setMerchant,
      accounts,
      setAccounts,
      refetch,
      currencies,
      setRefetch,
      credentials,
      setCredentials,
    }),
    [merchant, credentials, currencies, dates, device,  refetch, transactions, accounts, cards]
  );
  const [active, setActive] = useState("1");
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggle = (tab) => {
    setActive(tab);
  };
  if (loader) return <Loading />;

  return (
    <SectionContext.Provider value={sectionProvider}>
      <Card className="p-1 ">
        <Nav pills>
          <NavItem>
            <NavLink
              active={active === "1"}
              onClick={() => {
                toggle("1");
              }}
            >
              Accounts
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              active={active === "2"}
              onClick={() => {
                toggle("2");
              }}
            >
              Device
            </NavLink>
          </NavItem>

          <NavItem>
            <NavLink
              active={active === "3"}
              onClick={() => {
                toggle("3");
              }}
            >
              Cards
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              active={active === "4"}
              onClick={() => {
                toggle("4");
              }}
            >
              ManageKeys
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              active={active === "5"}
              onClick={() => {
                toggle("5");
              }}
            >
              Fees
            </NavLink>
          </NavItem>
        </Nav>
      </Card>
      <TabContent className="py-50" activeTab={active}>
        <TabPane tabId="1">{active == "1" && <Accounts />}</TabPane>
        <TabPane tabId="2">{active == "2" && <Device />}</TabPane>
        <TabPane tabId="3">{active == "3" && <Cards />}</TabPane>
        <TabPane tabId="4">{active == "4" && <ManageKeys />}</TabPane>
        <TabPane tabId="5">{active == "5" && <Fees />}</TabPane>
      </TabContent>
      {/* <RouterOulet {...props}/> */}
    </SectionContext.Provider>
  );
};
export default MerchantSection;
