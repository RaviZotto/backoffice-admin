import { Button, Card ,Col,Modal,ModalBody,ModalFooter,ModalHeader,Row, Spinner} from "reactstrap";
import React, { useContext, useEffect, useState ,Fragment} from "react";
import { Edit, PlusCircle, Trash, X } from "react-feather";
import AccountModal from "./AccountModal";
import CardBody from "reactstrap/lib/CardBody";
import { SectionContext } from "..";
import PerfectScrollBar from "react-perfect-scrollbar";
import Table from "./Table";
import EditAccountModal from "./EditAccount";
import { deletCurrencyAccount } from "../../../../Api";
import { userDetails } from "../../../../utility/Utils";
import MenuOption from "../MenuOption";
import notification from "../../../../components/notification";





const AccountCard=({data,setData,handleSelect,accounts, selectedAccount,setAccounts})=>{
 const [loader,setLoader]=useState(false);
 const [id,setId]=useState(null);
 
 
 const deleteCurrency= async(Id)=>{
   
    if(Id==null)return;
     setLoader(true);
    try{ 
        await deletCurrencyAccount(Id)
          setAccounts(accounts.filter(el=>el.id!=Id))
        console.log(1);
      }catch(e){
       notification({ title: "Error", message: e.message?e.message:e, type: "error"});
     } //console.log(e);
  
  }
   
  // const ConfirmModal = ()=>{ 
  //   const handleModal = () =>setId(null);
  //   const CloseBtn = (
  //     <X className="cursor-pointer" size={15} onClick={handleModal} />
  //   );
  //   return(
  //     <div>

  //      <Modal
  //       isOpen={id!=null}
  //       toggle={handleModal}
  //       //   className='middle-lg'
  //       //   modalClassName='modal-slide-in'
  //       contentClassName="pt-0"
  //       className="modal-dialog-centered"
  //      >
  //       <ModalHeader className="" toggle={handleModal} close={CloseBtn} tag="div">
  //       Confirmation Account Deletion
  //      </ModalHeader>
  //       <ModalBody>
  //         Are you sure that you want to delete the currency account after that you will  not able to recover it? 
  //       </ModalBody>
  //       <ModalFooter>
  //         <div className="d-flex justify-content-center">
  //           <Button.Ripple
  //             className="mr-2"
  //             type="submit"
  //             color={"primary"}
  //             disabled={  loader || userDetails.role_type&&userDetails.role_type!='admin' && !userDetails['merchant'].includes('D') }
  //             onClick={el=>deleteCurrency(id)}
  //           >
  //             {loader ? <Spinner size="ism" className="mr-1" /> : null}{" "}
  //             <span>Delete</span>
  //           </Button.Ripple>
  //           <Button.Ripple color={"danger"} onClick={handleModal}>
  //             Close
  //           </Button.Ripple>
  //         </div>
  //       </ModalFooter>
  //      </Modal>
  //     </div>
  
  //   )
  // } 



  // const assingIban=async(params)=>{ 
   
  //    let {api_key,secret_key}=merchant;
  //    //console.log(api_key,secret_key);
  //    setLoader(true);
  //    let token1=  Buffer.from(`${api_key}:${secret_key}`).toString("base64")
  //    axios.post(`${graphqlServerUrl}/api/sync-ledger-details`,params,{'headers':{'Authorization':`Basic ${token1}`}})
  //    .then((res) => {
  //      if (res.status == "200" && res.data) {
  //       setLoader(false);
  //       refetch(); 
  //       notification({ title: "Iban", message:"Iban successfully Assigned", type: "success"})  
  //      }
       
  //    })
  //    .catch((e: AxiosError) => {
    
  //      setLoader(false);
  //    notification({ title: "Error", message:e.response?e.response.data.message:e.toString(), type: "error"})
  //   //console.log(e);
      
  //    });

  //  }


  


  const selected=data.id==selectedAccount.id;
  const textStyle={color:selected?"white":"black"};
  return (
    <div>
      {/* <ConfirmModal Id={id}  /> */}
    <Card
      className="cursor-pointer ml-1 mr-1"
      color={ selected?"primary":"" }
      style={{   minWidth: "250px" }}
      onClick={()=>handleSelect(data)}
    >
      <CardBody style={{ marginBottom: "-1.5rem" }}>
            <MenuOption  style={{top:'0.1em',left:'14em',...textStyle}} data={data} className="position-absolute"  setData={setData}  deleteAccount={deleteCurrency} />
           {/* <Edit  style={{top:'0.2em',left:'14em',...textStyle}} size={15} className='position-absolute' onClick={()=>setData(data)} />
          {!data.gateway? <Trash  style={{}}  onClick={()=>{ setId(data.id) }} size={15} className='position-absolute' />:null} */}
           {/* <Trash  style={{top:'0.2em',left:'16em',...textStyle}}  onClick={()=>{ setId(data.id) }} size={15} className='position-absolute' /> */}
        <div className="d-flex justify-content-end">
        {/* {data.account_id&&!data.iban?<Badge  disabled={loader} onClick={e=>assingIban({currency:data.currencycode,ledger_id:data.account_id})} style={{top:'0.2em',left:'12em'}} color='primary' className="position-absolute">Iban</Badge>:null} */}
         
          <div  style={{...textStyle}}  className=" font-small-2   text-uppercase">{data.gateway}</div>
          
        </div>
        <div className="position-absolute " style={{top:'0.2em'}} >
          <div style={{...textStyle}} className="font-small-2 font-weight-bold mr-2 text-uppercase">{data.account_name}</div>
          <div style={{...textStyle}} className="font-small-2 font-weight-bold mr-2 text-uppercase">{data?.uk_account_no?` ${data.uk_account_no}  ${data.sortcode}`:data.iban}</div>
          <div style={{...textStyle}} className="font-small-2 font-weight-bold mr-2 text-uppercase">{data.bic}</div>
          <div style={{...textStyle}} className="font-small-2 font-weight-bold mr-2 text-uppercase">{}</div>
          </div>
      </CardBody>
      <hr style={{ borderColor: "#d0d2d6" }} />
      <CardBody style={{ marginTop: "-1.3rem" }}>
        <div style={{...textStyle}} className="font-small-1">Balance</div>
        <h3 style={{ marginTop: "0.2rem", ...textStyle}}>
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency:data.currencycode,
          })
            .format(data.current_balance)
            .replace(/^(\D+)/, "$1 ")}
        </h3>
      </CardBody>
    </Card>
    </div>
  )
}


const AccountSection = (props) => {
  const {accounts,setAccounts,credentials}=useContext(SectionContext);
  const [modal, setModal] = useState(false);
  const [roles,setRoles]=useState();
  const [data,setData]=useState({});
 const closeEditModal=()=>setData({});
  const [selectedAccount,setSelectedAccount]=useState({});
 
  
 

 const handleSelect=(el)=>{
    setSelectedAccount(el);
 }
 
 const handleModal = () => {
    setModal(!modal);
  };


  return (
    <Fragment>
    
      <Button.Ripple color="primary" onClick={(e) => handleModal()} 
      disabled={ userDetails.role_type&&userDetails.role_type!='admin' && !userDetails['merchant'].includes('C')}
      >
        
        <PlusCircle size={12} className="mr-2" /> Add Accounts
      </Button.Ripple>
      <AccountModal accounts={accounts}   credentials={credentials} modalOpen={modal} data={accounts} handleModal={() => setModal(!modal)} />
      {/* <AccountModal data={data} handleModal={() => setModal(!modal)} /> */}
      <div className="scrollcontrol">
      <Row className='p-2 flex-nowrap'>
       
      { 
        accounts.map(res=>
          <div className='col' style={{flexGrow:0}}>
        <AccountCard  key={res.id} setAccounts={setAccounts} accounts={accounts} data={res} setData={(el)=>setData(el)}  selectedAccount={selectedAccount} handleSelect={(el)=>handleSelect(el)}  /> 
        </div>
        )}
       {data.id&& <EditAccountModal data={data} handleModal={()=>closeEditModal()} />}
        </Row>
        </div>
        <Table selectedAccount={selectedAccount} />
    </Fragment>

  );
};

export default AccountSection;
