import AvForm from "availity-reactstrap-validation-safe/lib/AvForm";
import React, { useContext, useEffect, useState ,Fragment} from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import FormGroup from "reactstrap/lib/FormGroup";
import Label from "reactstrap/lib/Label";
import Select from "react-select";
import { X } from "react-feather";
import AvInput from "availity-reactstrap-validation-safe/lib/AvInput";
import { SectionContext } from "..";
import { updateCurrencyAccount } from "../../../../Api";
import Spinner from "reactstrap/lib/Spinner";
import { userDetails } from "../../../../utility/Utils";
import AvField from "availity-reactstrap-validation-safe/lib/AvField";


const EditAccountModal = ({ handleModal, data }) => {
//console.log(data)
  // alert(modalOpen)
  const { merchant, history, setRefetch } = useContext(SectionContext);
  const CloseBtn = (
    <X className="cursor-pointer" size={15} onClick={handleModal} />
  );
const [update,setUpdated]=useState({udpateInfo:{},res:true})
const [state,setState]=useState({
    account_name:data.account_name,
    bic:data.bic,
    iban:data.iban,
    uk_account_no:data.uk_account_no,
    sortcode:data.sortcode
})

  
    useEffect(()=>{
        let updatedInfo={};
        for(let key of Object.keys(state)){
            if(state[key]!==data[key]) updatedInfo[key]=state[key];
        } 
         if(Object.keys(updatedInfo).length)setUpdated({ udpateInfo: updatedInfo,res:false});
         else setUpdated({res:true});
    },[state])
   

  const [loader, setLoader] = useState(false);
  const handleSumbit = (event, errors, value) => {
    event.preventDefault();

      const { id } = JSON.parse(
        JSON.stringify(data)
      );
       
      // //console.log(merchant_id,railsbank_id)
      //console.log(merchant, value);
      setLoader(true);
            updateCurrencyAccount({
         
       ...update.udpateInfo,id
      })
        .then((res) => {
          if (res.success) {
            setLoader(false);
            handleModal()
            setRefetch(true);
          }
        })
        .catch((e) => {
          //console.log(e);
          setLoader(false);
        });
    
  };

  return (
    <Modal
      isOpen={data.id}
      toggle={handleModal}
      //   className='middle-lg'
      //   modalClassName='modal-slide-in'
      contentClassName="pt-0"
      className="modal-dialog-centered"
    >
      <ModalHeader className="" toggle={handleModal} close={CloseBtn} tag="div">
        Update account
      </ModalHeader>
      <AvForm onSubmit={handleSumbit}>
        <ModalBody>
          <FormGroup>
            <Label>Account Name</Label>
            <AvInput  onChange={e=>setState({...state,account_name:e.target.value})} required type="text" className="form-control" name="account_name"  value={data.account_name} />
          </FormGroup>
          <FormGroup>
            <Label>Currency</Label>
           
           <AvInput 
            type='text'
            value={data.currencycode}
            name='account_name'
            disabled
           />
 
          </FormGroup>
          <FormGroup>
            <Label>Account type</Label>
             <AvInput disabled value={(data.gateway? data.gateway.toUpperCase():"Currency")+" Account"}  name='currency' />
          </FormGroup>

          
              <FormGroup>
                <Label>Iban</Label>
                <AvInput
                required
                  type="text"
                  className="form-control"
                  onChange={e=>setState({...state,iban:e.target.value})} 
                  value={state.iban}
                  name="iban"
                  disabled={data.account_id }
                />
              </FormGroup>
              <FormGroup>
                <Label>Bic</Label>
                <AvInput
                   disabled={data.account_id }
                  type="text"
                  className="form-control"
                  value={data.bic}
                  onChange={e=>setState({...state,bic:e.target.value})} 
                  name="bic"
                />
              </FormGroup>
              { data.currencycode=='GBP'?
              <Fragment>
              <FormGroup>
                <Label>Uk Sort code</Label>
                <AvInput
                   disabled={data.account_id  }
                  type="text"
                  className="form-control"
                  onChange={e=>setState({...state,sortcode:e.target.value})} 
                  value={data.sortcode}
                  name="sortcode"
                />

              </FormGroup>
              <FormGroup>
                <Label>Uk Account </Label>
                <AvInput
                  disabled={data.account_id }
                  value={data.uk_account_no}
                  onChange={e=>setState({...state,uk_account_no:e.target.value})} 
                  type="text"
                  className="form-control"
                  name="uk_account_no"
                />
                
              </FormGroup>
              </Fragment>:null
                }
                    {/* { data.gateway=='IBANQ'?
                     <>
                     <Label>Balance</Label>
            <AvInput name='current_balance' type='number' value={data.current_balance}    onChange={e=>setState({...state,current_balance:e.target.value})} /></> :
            null} */}
         <FormGroup>
   
            <Label>Transfer Limit</Label>
            <AvInput name='txn_limit' type='number' value={data.limit_amt}    onChange={e=>setState({...state,limit_amt:e.target.value})} />
          </FormGroup>
        </ModalBody>

        <ModalFooter>
          <div className="d-flex justify-content-center">
            <Button.Ripple
              className="mr-2"
              type="submit"
              color={"primary"}
              disabled={ update.res || loader||userDetails?.role_type!='admin' && !(userDetails['merchant']?.includes('U')) }
              
            >
              {loader ? <Spinner size="sm" className="mr-1" /> : null}{" "}
              <span>Update</span>
            </Button.Ripple>
            <Button.Ripple color={"danger"} onClick={handleModal}>
              Close
            </Button.Ripple>
          </div>
        </ModalFooter>
      </AvForm>
    </Modal>
  );
};
export default EditAccountModal;
