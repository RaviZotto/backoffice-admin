import AvForm from "availity-reactstrap-validation-safe/lib/AvForm";
import React, { useContext, useState ,Fragment} from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import FormGroup from "reactstrap/lib/FormGroup";
import Label from "reactstrap/lib/Label";
import Select from "react-select";
import { X } from "react-feather";
import AvInput from "availity-reactstrap-validation-safe/lib/AvInput";
import { SectionContext } from "..";
import { createAccountForLedger } from "../../../../Api";
import Spinner from "reactstrap/lib/Spinner";
import { AvField } from "availity-reactstrap-validation-safe";
import notification from "../../../../components/notification";
import { STORE_MERCHANTACCOUNTS } from "../../../../redux/actions/transfer";
import { useDispatch } from "react-redux";

const AccountModal = ({ modalOpen, handleModal, data,credentials }) => {
  const dispatch = useDispatch()
  //console.log(modalOpen);
  // alert(modalOpen)
  const[gateway,setGateway]=useState(null);
  const { merchant, history, setRefetch ,currencies,accounts} = useContext(SectionContext);
  const acctOption=[ "Capture Account","Bank Account"];
  const CloseBtn = (
    <X className="cursor-pointer" size={15} onClick={handleModal} />
  );

  const [loader, setLoader] = useState(false);
  const [accountType, setAccountType] = useState(0);
  const [currency, setCurrency] = useState(null);
  const handleSumbit = (event, errors, value) => {
    event.preventDefault();
    if (errors.length ) {
      
    } else {
      const { merchant_id,railsbank_id } = JSON.parse(
        JSON.stringify(merchant)
      );
      // //console.log(merchant_id,railsbank_id)
      //console.log(merchant, value);
      if(gateway=='nuapay') delete value.currency_code;
      if(gateway=='ibanq'&&!credentials.ibanq_client_id) return notification({type: 'error',message: 'Plz first add ibanq credentials in order to create ibanq account',title:"Validation Error"})
      if(gateway=='railsbank' && !railsbank_id) return notification({type: 'error',message: 'Railsbank is not activated for the merchant',title:'Error'});
      setLoader(true);
      value.current_balance=String(Number(accountType==1?0:value.current_balance).toFixed(2));
      value.limit_amt=String(Number(value.limit_amt).toFixed(2));
      if(value.gateway)value.gateway=value.gateway.toUpperCase();
      
      createAccountForLedger({
      currency:(gateway=='nuapay')?'EUR':currency,
       
      
        merchant_id,
        type:accountType== 0?'capture_account':'bank_account',
        ...value
      })
        .then((res) => {
          if (res.message) {
            setTimeout(() =>{
            handleModal();
            dispatch(STORE_MERCHANTACCOUNTS(()=>setLoader(false)))
            setRefetch(true);
            },2000)
          }
        })
        .catch((e) => {
          notification({title:'Error', message:"Internal Server Error",type:'error'})
          //console.log(e);
          setLoader(false);
        });
    }
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={handleModal}
      //   className='middle-lg'
      //   modalClassName='modal-slide-in'
      contentClassName="pt-0"
      className="modal-dialog-centered"
    >
      <ModalHeader className="" toggle={handleModal} close={CloseBtn} tag="div">
        Add account
      </ModalHeader>
      <AvForm onSubmit={handleSumbit}>
        <ModalBody>
          <FormGroup>
            <Label>Account Name</Label>
            <AvInput  required type="text" className="form-control" name="account_name" />
          </FormGroup>
           <FormGroup>
            <Label>Account type</Label>
            <Select
              placeholder='select--'
              onChange={(e) => setAccountType(e.value)}
              options={acctOption.map((res, i) => ({
                label: res,
                value: i,
              }))}
            />
          </FormGroup>
          <FormGroup>
           
            {accountType == 0 ? (
              <>
               <Label>Currency</Label>
              <Select
                required
                placeholder="select--"
                name="currency_code"
                options={[ "EUR", "BMD", "GBP", "DKK", "USD"].filter(e=>!data.find(e1=>!e1.gateway&&e1.currencycode==e)).map(
                  (res) => ({
                    label: res,
                    value: res,
                  })
                )}
                onChange={(e) => setCurrency(e.value)}
              />
               <div className="mt-1"> 
                  <AvField  label="Initial Balance"  type="number" name='current_balance' value={0}  />
               </div>
               </>
            ) : (
              <>
                <AvField label='Gateway' name='gateway' type='select' onChange={e=>setGateway(e.target.value)}  >
                <option value={null} label='select-- gateway'/> 
               <option value='nuapay' label='Nuapay'/>
               <option value='railsbank' label='Railsbank'/>
               <option value='ibanq' label='Ibanq'/>
              </AvField>
           
              <Label>Currency</Label>
              { gateway!='nuapay'?<Select
                required
                placeholder="select--"
                name="currency_code"
                defaultValue={'EUR'}
               
                options={  gateway=='ibanq'?currencies.filter(e=> 
                    
                 !data.find(e1=>e1.gateway=='IBANQ'&&e1.currencycode==e.currency)).map(res=>({label:res.currency,value:res.currency})) : ["EUR", "GBP","USD","AUD"].map((res) => ({
                  label: res,
                  value: res,
                }))}
                onChange={(e) => setCurrency(e.value)}
              />:<AvInput type="text"  name="currency_code" disabled={true} value='EUR' />}
              
              </>
            )}
          </FormGroup>
          <FormGroup>
            <Label>Transfer Limit</Label>
            <AvInput  required name='limit_amt' type='number' defaultValue={100}/>
          </FormGroup>
         

          {/* {accountType==0? (
            <Fragment>
              <FormGroup>
                <Label>Iban</Label>
                <AvInput
                required
                  type="text"
                  className="form-control"
                  name="iban"
                />
              </FormGroup>
              <FormGroup>
                <Label>Bic</Label>
                <AvInput
              
                  type="text"
                  className="form-control"
                  name="bic"
                />
              </FormGroup>
              <FormGroup>
                <Label>Uk Sort code</Label>
                <AvInput
              
                  type="text"
                  className="form-control"
                  name="sortcode"
                />

              </FormGroup>
              <FormGroup>
                <Label>Uk Account </Label>
                <AvInput
                  
                  type="text"
                  className="form-control"
                  name="uk_account_no"
                />
                
              </FormGroup>
            </Fragment>
          ) : null} */}
        </ModalBody>

        <ModalFooter>
          <div className="d-flex justify-content-center">
            <Button.Ripple
              className="mr-2"
              type="submit"
              color={"primary"}
              disabled={loader}
            >
              {loader ? <Spinner size="sm" className="mr-1" /> : null}{" "}
              <span>Add</span>
            </Button.Ripple>
            <Button.Ripple color={"danger"} onClick={handleModal}>
              Close
            </Button.Ripple>
          </div>
        </ModalFooter>
      </AvForm>
    </Modal>
  );
};
export default AccountModal;
