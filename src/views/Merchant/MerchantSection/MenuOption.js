import React from 'react';
import { MoreVertical } from 'react-feather';
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from 'reactstrap';
const MenuOption = ({data,className,style,setData,deleteAccount}) => {
    
    return (
        <UncontrolledDropdown className="chart-dropdown  " >
        <DropdownToggle
          color=""
          className="bg-transparent btn-sm border-0 p-50"
        >
          <MoreVertical size={18} className={`cursor-pointer ${className} `} style={style} />
        </DropdownToggle>
        <DropdownMenu top  className="position-absolute  mr-0  "   >
          <DropdownItem
            className="w-100 d-flex text-center"
             onClick={(e) =>{
            
                   setData(data)
           
            }}
          >
            Edit
          </DropdownItem>
        
          <DropdownItem
            className="w-100 d-flex"
            onClick={(e) =>{
                 deleteAccount(data.id)
            }
          }
          >
            Delete
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    )
}

export default MenuOption
