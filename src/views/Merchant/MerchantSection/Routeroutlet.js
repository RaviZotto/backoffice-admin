import React from 'react';
import { Switch, Route } from "react-router-dom";
import Cards from './Card/Cards';
import Accounts from './Accounts/Accounts';
import Device from './Device/Device';
const RouterOulet=props=>{

    //console.log(props);
  const { match } = props;
  return (
    <Switch>
      <Route
        exact={true}
        path={match.url + "/accounts"}
        render={({ ...props }) => <Accounts {...props} />}
      />
      <Route
        exact={true}
        path={match.url + "/device"}
        render={({ ...props }) => <Device {...props} />}
      />
      <Route
        exact={true}
        path={match.url + "/cards"}
        render={({ ...props }) => <Cards {...props} />}
      />
    </Switch>);
}

export default RouterOulet;