import AvField from 'availity-reactstrap-validation-safe/lib/AvField';
import AvForm from 'availity-reactstrap-validation-safe/lib/AvForm';
import React, { useContext, useEffect, useState } from 'react'
import { X } from 'react-feather';
import {Col, FormGroup, Modal,ModalBody,ModalFooter,ModalHeader, Spinner,Button} from 'reactstrap';
import { SectionContext } from '..';
import { saveIFXCredentials, updateIFXCredentials } from '../../../../Api';
import notification from '../../../../components/notification';

const IFXCardView = ({ifxModal,setIFXModal}) => {
   
   
   const onSubmit=async(event,errors,values)=>{
    try{ 
    if(errors.length){return;}
       else{
        setLoader(true);
           if(credentials.id){
           let updateObj={};
        for(const [key,val] of Object.entries(values)){
                console.log(val,data[key]); 
              if(val != data[key] ) updateObj[key]=val;
              }  
            
              await updateIFXCredentials({id:data.id,...updateObj});
              setCredentials({...credentials,...updateObj});
           }
           else{
             const data = await  saveIFXCredentials({...values,merchant_id:merchant.merchant_id});
            
             setCredentials({id:data.handpoint_id,...data});
           }
           setLoader(false);
           handleModal();
       }
    }catch(e){
        setLoader(false);
        notification({title: "Error",type: "error",message:typeof e == 'object'?e.message:e.toString()});
    }
   }
   
   
   
   
   
    const{credentials,setCredentials,merchant}=useContext(SectionContext);
    const handleModal =()=>{ setIFXModal(false); };
    const [loader,setLoader]=useState(false);
    const CloseBtn = (
        <X className="cursor-pointer" size={15} onClick={handleModal} />
      );
    const [data,setData]=useState({
        ibanq_client_id:"",
        ibanq_client_secret:"",
        ibanq_username:"",
        ibanq_password:"",
    })
    useEffect(() => {
        if(credentials.id){
            setData({...credentials});
        }
    },[credentials.id])
    
    return (
        <Modal
      isOpen={ifxModal}
      toggle={handleModal}
      backdrop='static'
      //   className='middle-lg'
      //   modalClassName='modal-slide-in'
      contentClassName="pt-0"
      className="modal-dialog-centered"
      
    >
         <ModalHeader className="" toggle={handleModal} close={CloseBtn} tag="div">
     {   (!credentials.id?"Add":"Update")}
      </ModalHeader>
      
        <AvForm onSubmit={onSubmit}>
        <ModalBody>
         <FormGroup tag={Col}>
          
         <AvField 
          name="ibanq_client_id"
          label="Client Id"
          defaultValue={data.ibanq_client_id}
         />
            
         <AvField 
         label="Client Secret"
          name="ibanq_client_secret"
          defaultValue={data.ibanq_client_secret}
         />
           
         <AvField 
         label="User Name"
          name="ibanq_username"
          defaultValue={data.ibanq_username}
         />
            
         <AvField 
          label="Password"
          name="ibanq_password"
          defaultValue={data.ibanq_password}
         />
         </FormGroup>   
        </ModalBody>
        <ModalFooter>
        <div className="d-flex justify-content-center">
            <Button.Ripple
              className="mr-2"
              type="submit"
              color={"primary"}
              // disabled={loader|| userDetails.role_type!='admin' && !userDetails['merchant'].includes('C')}
            >
              {loader ? <Spinner size="sm" className="mr-1" /> : null}{" "}
              <span>{credentials.id?'Update':'Add'}</span>
            </Button.Ripple>
            <Button.Ripple color={"danger"} onClick={handleModal}>
              Close
            </Button.Ripple>
          </div>   
        </ModalFooter>
        </AvForm>
        
        </Modal>
        
    );
}

export default IFXCardView
