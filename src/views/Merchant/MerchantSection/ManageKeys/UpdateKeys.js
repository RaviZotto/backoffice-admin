import React,{Fragment, useContext, useEffect, useState} from 'react';
import {Row,Col,Button, ModalHeader,ModalFooter,ModalBody,Modal,Spinner}from 'reactstrap'
import Label from 'reactstrap/lib/Label';
import {AvField,AvForm,AvInput} from 'availity-reactstrap-validation-safe';
import { UpdateMerchantCredentials,deleteMerchantCredentials} from '../../../../Api';
import { RefreshCcw, Trash,X } from 'react-feather';
import notification from '../../../../components/notification';
import { userDetails } from '../../../../utility/Utils';

const UpdateKeys=({Keys,refetch})=>{
  const[rowId,setRowId]=useState(null);
  const[loader,setLoader]=useState(false);
  const ConfirmModal = ()=>{ 
    const handleModal = () =>setRowId(null);
    const CloseBtn = (
      <X className="cursor-pointer" size={15} onClick={handleModal} />
    );
    return(
      <div>

       <Modal
        isOpen={rowId!=null}
        toggle={handleModal}
        //   className='middle-lg'
        //   modalClassName='modal-slide-in'
        contentClassName="pt-0"
        className="modal-dialog-centered"
       >
        <ModalHeader className="" toggle={handleModal} close={CloseBtn} tag="div">
        Confirmation Role Deletion
       </ModalHeader>
        <ModalBody>
          Are you sure that you want to delete the Ecom keys after that you will  not able to recover it? 
        </ModalBody>
        <ModalFooter>
          <div className="d-flex justify-content-center">
            <Button.Ripple
              className="mr-2"
              type="submit"
              color={"primary"}
              disabled={  loader }
              onClick={el=>deleteEcomKeys(rowId)}
            >
              {loader ? <Spinner size="sm" className="mr-1" /> : null}{" "}
              <span>Delete</span>
            </Button.Ripple>
            <Button.Ripple color={"danger"} onClick={handleModal}>
              Close
            </Button.Ripple>
          </div>
        </ModalFooter>
       </Modal>
      </div>
  
    )
  } 



  
    const onSubmit=async(event,errors,values)=>{
        // try{
        // //console.log(update.updateInfo)
        // let keyUpdate={...state,merchant_id:Keys[0].merchant_id}
        // await addOrUpdateMerchantCredentials({data:keyUpdate,action:'update'});
        // setUpdated({updateInfo:{},res:true});
        // setState({});
        // refetch();
        // }catch(e){
        //     //console.log(e);
        // }
    }

    useEffect(() => {
     //console.log(Keys);
       setState([...Keys])
     
    },[])


const[state,setState]=useState([{}]);

    // EUR_CVVON, EUR_3D, EUR_CVVOFF,
    const [ecomOptions,setEcomOptions]=useState([{}])
    var currencyOptions=[
        { value: 'USD', label: 'USD', color: '#00B8D9', isFixed: true },
        { value: 'GBP', label: 'GBP', color: '#0052CC', isFixed: true },
        { value: 'DKK', label: 'DKK', color: '#5243AA', isFixed: true },
        { value: 'EUR', label: 'EUR', color: '#FF5630', isFixed: false },
        { value: 'NOK', label: 'NOK', color: '#FF8B00', isFixed: false },
        { value: 'SEK', label: 'SEK', color: '#FFC400', isFixed: false }
    ]
     
   const updateEcomKeys=async(id)=>{ 
     try{
      let keyObj=state.filter(e=>e.id==id);
      keyObj=keyObj[keyObj.length-1];
    
     let keyComp=Keys.find(el=>el.id==id);
      let ecomObj={};
      for(let k of Object.keys(keyObj)) {
         
         if(keyObj[k]!=keyComp[k]){
           //console.log(k); 
           ecomObj[k]=keyObj[k];}

      }
         
        if(!Object.keys(ecomObj).length){notification({title:'Nothing changed',type:'error'});setState([{}]);return} 
          ecomObj.id=keyObj.id;
        await  UpdateMerchantCredentials({data:ecomObj})
       refetch()
       notification({title:"Success",type:'success',message:'successfully updated'})
       
      
    }catch(e){
       notification({title:"Error",message:e.toString(),type:'error'})
    }
   }

  const deleteEcomKeys=async(id)=>{
          try{
            setLoader(true);
         await deleteMerchantCredentials(id);
         setRowId(null);
         setLoader(false);
         notification({title:"Success",type:'success',message:'successfully deleted'});
         refetch()
          }catch(e){
            setLoader(false);
            notification({title:"Error",message:e.toString(),type:'error'})
          }
  }
  if(!Keys.length)return null;

  return(
      <Fragment>
        <ConfirmModal/>
          <AvForm onSubmit={onSubmit} >
          <Col>
          {Keys.length&&Keys.map((res,i)=>
           <Row className='row-md-nowrap align-items-center '>
               
            <Col>
            <Label className='form-label text-nowrap' for='linkedin'>
              {res.currency} CVVOFF
            </Label>
            <AvInput key={i+'off'}
              
              type='text'
              id={`linkedin`}
              value={res.cvv_off}
              name={`ecom_${res.currency}_cvvoff`}
              onChange={e=>{   setState([...state].map(el=>(el.id==res.id)?Object.assign(el, { cvv_off: e.target.value }):el )  ) }}
            />
              </Col>  
            <Col>
            <Label className='form-label text-nowrap' for='linkedin'>
              {res.currency} CVVON
            </Label>
            <AvInput key={i+'on'}
                onChange={e=>{ setState([...state].map(el=>(el.id===res.id)?Object.assign(el, { cvv_on: e.target.value }):el )  ) }}
                value={res.cvv_on}
              type='text'
              id={`linkedin`}
              name={`ecom_${res.value}_cvvon`}
            />
            </Col>
            <Col>
            <Label className='form-label text-nwrap' for='linkedin'>
             {res.currency} 3D
            </Label>
            <AvInput key={i+'3d'}
              onChange={e=>{setState([...state,{...state.find(el=>res.id==el.id),['3d']:e.target.value} ]) }}
   
              type='text'
              id={`linkedin`}
              value={res['3d']}
              name={`ecom_${res.currency}_3d`}
            />
            </Col>
            <Col className='pt-2 pl-0 col-1 mr-1' >
            <Button key={res.id} color='primary' onClick={el=>updateEcomKeys(res.id)} disabled={userDetails.role_type&& userDetails.role_type!='admin' && !userDetails['merchant'].includes('U')} >  
            <RefreshCcw size={15} key={res.id}/>
            </Button>
            </Col>
            <Col className='pt-2 pl-0 col-1'>
              <Button key={res.id} color='primary' onClick={el=>setRowId(res.id)} disabled={userDetails.role_type&& userDetails.role_type!='admin' && !userDetails['merchant'].includes('D')} >
            <Trash size={15} key={res.id}/>
            </Button>
            </Col>
           </Row>
           )}
           
         
          
          </Col>

          </AvForm>
      </Fragment>
  )

}

export default UpdateKeys;