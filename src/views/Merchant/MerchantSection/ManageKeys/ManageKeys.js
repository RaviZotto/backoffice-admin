
import React, { useContext, useEffect, useState } from "react";
import { Card, Row, CardTitle, Col,Button, Input, Label, FormGroup } from "reactstrap";
import CardBody from "reactstrap/lib/CardBody";
import { SectionContext } from "..";
import { activateRailsBank, Axios, changeEcomStatus, getHanpointKey, getMerchantCredentials, updateHanpointKey } from "../../../../Api";
import '../index.scss'
import AddKeys from "./AddKeys";
import UpdateKeys from "./UpdateKeys";
import Loading from "@core/components/spinner/Loading-spinner";
import notification from "../../../../components/notification";

import { graphqlServerUrl } from "../../../../App";
import { AxiosResponse, AxiosError } from "axios";
import { userDetails } from "../../../../utility/Utils";
import { Edit } from "react-feather";
import IFXCardView from "./IFXCardView"
import Merchant from "../..";
const ManageKeys = () => {
  
  var handpointKeyStore='';
  const { merchant, history,setRefetch,refetch,credentials } = useContext(SectionContext);
  const [activate, setActivate] = useState(false);
  const[keys,setKeys]=useState([]);
  const[loading,setLoading]=useState(true);
  const[temp,setTemp]=useState('');
  const[toggle,setToggle]=useState(false);
  const[handpoint,setHandpoint]=useState(null);
  const [ifxModal,setIfxModal]=useState(false);
  const saveDefaultEcomUsers=(obj)=>{
    
    Axios.post(`${graphqlServerUrl}/admin/saveDefaultEcomUser`,obj)
    .then(res=>{
      if(res.status==200&&res.data){notification({message:'Default Added',type:'success'});
    setRefetch(true);}
    }
      
    )
    .catch((e:AxiosError)=>{ 
      notification({title:'Error',message:e.response?e.response.data:e.toString(),type:'error'})
    })
  }
  
  const saveDefaultEcomKeys=(id)=>{ 

    Axios.post(`${graphqlServerUrl}/admin/saveDefaultEcomkey`,id)
    .then(res=>{
      if(res.status==200&&res.data){notification({message:'Default Added',type:'success'});
     setRefetch(true)}
    }
      
    )
    .catch((e:AxiosError)=>{ 
      notification({title:'Error',message:e.response?e.response.data:e.toString(),type:'error'})
    })
  }


  
  useEffect(()=>{
    if(refetch || loading){
        
        if(credentials.handpoint_key){setTemp(credentials['handpoint_key']);
      
        setTemp(credentials['handpoint_key']);
        setHandpoint(credentials['handpoint_key']); 
         }
      getMerchantCredentials(merchant.merchant_id).then(res=>{
       
        
        if(res.credenitals){setKeys(res.credenitals); }
        setRefetch(false);setLoading(false)
       
      }).catch(e=>{//console.log(e);
        setKeys([]);
        setRefetch(false);setLoading(false)});
    
    }
  },[refetch])

  const updateEcomStatus=async(param)=>{ 
      try{ 
        await changeEcomStatus(param);
        notification({message:"successfully updated",type:'success'})
        setRefetch(true);
      }catch(e){ 
        notification({title:"Error",message:e.toString(),type:'error'})
      }
  }



  useEffect(() => {
    // //console.log(userDetails,userDetails.role_type!='admin' && !userDetails['merchant'].includes('W'))
    if (activate) {
        const checkValues=["email","dob","name","country"];
         for(let key of checkValues ) {
          console.log(key,merchant[key]) 
          if(!merchant[key]){setActivate(false);notification({type:'error', message:`Merchant ${key} value missing!`,title:"Missing Field"});return ;} }
        activateRailsBank(merchant.merchant_id).then((res) => {
          if (res.Activated) {setRefetch(true);setActivate(false)}
        }).catch(e=>{
    
  
        notification({title:'error',message:e.toString() ,type:'error'})
          setActivate(false);
        });
      
    }
  }, [activate]);
  if(loading) return <Loading/>
  return (
    <Card  className="mx-auto manageCard">
      <IFXCardView ifxModal={ifxModal} setIFXModal={setIfxModal} />
      <CardBody>
        <CardTitle>RailsBank</CardTitle>
        <hr />
        <Row>
          <Col><span className='text-nowrap'>Railsbank Status:</span></Col>
          <Col>{merchant.railsbank_id ? "Enabled" : "Disabled"} </Col>
          {!merchant.railsbank_id && (
            <Col>
              <Button.Ripple disabled={ userDetails.role_type&&userDetails.role_type!='admin' && !userDetails['merchant'].includes('U')} onClick={(e) => setActivate(true)} color='primary' disabled={activate} >
                
                {activate ? "Activating" : "Activate"}
              </Button.Ripple>
            </Col>
          )}
        </Row>
        <hr/>
        <Row className="justify-content-between px-1" >
        <CardTitle >IBANQ</CardTitle>
       <Button.Ripple color="primary py-0 my-0" onClick={el=>setIfxModal(true)} > {!credentials.id?"Add":"Update"}  </Button.Ripple>
        </Row>
        <hr/>
        <CardTitle>HandPoint</CardTitle>
        <FormGroup tag={Row}>
          
          <Col md="6" sm='6'>
          <Label>Hand point key</Label>
          <Input class="form-control"  type='text'  value={handpoint} onChange={e=>setHandpoint(e.target.value)}  />
          </Col>
          <Col md="6" sm='6' className='d-flex justify-content-end mt-0 p-1 pt-2 '>
            <Button
        
            onClick={e=>updateHanpointKey({handpoint_key:handpoint,merchant_id:merchant.merchant_id}).
            then(res=>{if(res.success){setRefetch(true);notification({ title:'Handpoint key',message:'Successfully updated',type:'success'}) }})
            .catch(err=>{
              notification({title:'Error',message:err,type:'error'})
            })
            } color="primary" disabled={handpoint== temp||userDetails.role_type&&userDetails.role_type!='admin' && !userDetails['merchant'].includes('U') }>Update</Button>
          </Col>
        </FormGroup>
        <hr/>
      
        <CardTitle>Ecom </CardTitle>
        <hr/>
        <Row>
        <Col><span className='text-nowrap'>Ecom Status:</span></Col>
          <Col>{merchant.card_gateway!=null?"Enabled" : "Disabled"}  </Col>
            <Col className='d-flex justify-content-end'><Button  disabled={userDetails.role_type&&userDetails.role_type!='admin' && !userDetails['merchant'].includes('U')} color='primary' onClick={e=>updateEcomStatus({status:( merchant.card_gateway=='Ecom'||merchant.card_gateway=='ECOM')?0:1,merchant_id:merchant.merchant_id})}> {merchant.card_gateway=='ECOM'||merchant.card_gateway=='Ecom'?'Deactivate':'Activate'} </Button> </Col>
        </Row>
        <hr/>
     
        {merchant.card_gateway=='ECOM'|| merchant.card_gateway=='Ecom'?<>
         {typeof credentials=='object' && !credentials['ecom_username']? <Button.Ripple onClick={e=>saveDefaultEcomUsers({operation:credentials.merchant_id?'update':'add',id:merchant.merchant_id})}  color='primary'   >
          Save Default Ecom users
         </Button.Ripple>:null
          }
         <div className="d-flex justify-content-end">
         <Button   onClick={()=>setToggle(!toggle)} color='primary' > {!toggle?"Update Ecom keys":"Add Ecom keys"} </Button>
         </div>
         {!keys.length? <Button.Ripple onClick={e=>saveDefaultEcomKeys({merchant_id:merchant.merchant_id})}  color='primary'   >
          Save Default Ecom key
         </Button.Ripple>:null
          }
         {toggle?<UpdateKeys Keys={keys} refetch={()=>setRefetch(true)}/>:<AddKeys refetch={()=>setRefetch(true)} Keys={keys} />}
         </>:null
        }
         </CardBody>
    </Card>
  );
};
export default ManageKeys;
