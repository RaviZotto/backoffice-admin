import React,{Fragment, useContext, useEffect, useRef, useState} from 'react';
import { selectThemeColors } from '@utils'
import {Row,Col,Button}from 'reactstrap'
import Select from 'react-select'
import Label from 'reactstrap/lib/Label';
import {AvForm,AvInput} from 'availity-reactstrap-validation-safe';
import { SectionContext } from '..';
import { addOrUpdateMerchantCredentials } from '../../../../Api';
import notification from '../../../../components/notification';
import { userDetails } from '../../../../utility/Utils';

const AddKeys=({refetch,Keys})=>{
const multiRef=useRef();
const[ecomKey,setEcomKeys]=useState({})
  const {merchant}=useContext(SectionContext);
    const onSubmit=async(event,errors,values)=>{
        event.preventDefault();
       if(errors.length==0){
          //console.log(ecomKey);
     
        try{
            let keyUpdate={...ecomKey, merchant_id:merchant.merchant_id}
            await addOrUpdateMerchantCredentials({data:keyUpdate,action:'add'});
            setEcomOptions([]);
             //console.log(multiRef.current, multiRef);
            notification({title:'Ecom Keys ',message:`succussfully added`,type:'success'})
            refetch();
            }catch(e){
                //console.log(e);
            }
        }
    }
    const[isEcom,setEcom]=useState(true);
    const[currencyOpt,setCurrencyOpt]=useState([]);
    // EUR_CVVON, EUR_3D, EUR_CVVOFF,
    const [ecomOptions,setEcomOptions]=useState([])
    var currencyOptions=[
        { value: 'USD', label: 'USD', color: '#00B8D9', isFixed: true },
        { value: 'GBP', label: 'GBP', color: '#0052CC', isFixed: true },
        { value: 'DKK', label: 'DKK', color: '#5243AA', isFixed: true },
        { value: 'EUR', label: 'EUR', color: '#FF5630', isFixed: false },
        { value: 'NOK', label: 'NOK', color: '#FF8B00', isFixed: false },
        { value: 'SEK', label: 'SEK', color: '#FFC400', isFixed: false }
    ]

     useEffect(()=>{

         if(Keys&&Keys.length){
           let dupKey=[...Keys];
            dupKey=dupKey.map(res=>res.currency);
            //console.log(dupKey,'11');  
            currencyOptions=currencyOptions.filter(res=>{
                 if(dupKey.indexOf(res.label)==-1)return true;
            })
            setCurrencyOpt([...currencyOptions])
            //console.log(currencyOptions);
         }else{
            setCurrencyOpt([...currencyOptions])
         }
     },[Keys])

  return(
      <Fragment>
          <AvForm onSubmit={onSubmit} >
          <Col>
            <Label>Currency</Label>
            <Select
              isClearable={true}
              ref={multiRef}
              theme={selectThemeColors}
              isMulti
              name='colors'
              menuPlacement="top"
              menuPosition="relative"
              options={currencyOpt}
              onChange={e=>{ if(e){setEcomOptions([...e])}else{setEcomOptions([])}  }}
              className='react-select'
              classNamePrefix='select'
            />
            
            
          
           { ecomOptions.length?ecomOptions.map((res,i)=>
           <Row>
               
            <Col key={i} sm='4' >
            <Label className='form-label text-nowrap' for='linkedin'>
              {res.value} CVVOFF
            </Label>
            <AvInput
              required
              type='text'
              id={`linkedin`}
              onChange={el=>{let val={};val[res.value]={...ecomKey[res.value],cvv_off:el.target.value};   setEcomKeys({...ecomKey,...val})}}
              name={`ecom_${res.value.toLowerCase()}_cvvoff`}
            />
              </Col>  
            <Col key={i} sm='4'>
            <Label className='form-label text-nowrap' for='linkedin'>
              {res.value} CVVON
            </Label>
            <AvInput
              required
              type='text'
              onChange={el=>{let val={};val[res.value]={...ecomKey[res.value],cvv_on:el.target.value};   setEcomKeys({...ecomKey,...val})}}
              id={`linkedin`}
              name={`ecom_${res.value.toLowerCase()}_cvvon`}
            />
            </Col>
            <Col key={i}>
            <Label className='form-label text-nowrap' for='linkedin'>
             {res.value} 3D
            </Label>
            <AvInput
              required
              onChange={el=>{let val={};val[res.value]={...ecomKey[res.value],'3d':el.target.value};   setEcomKeys({...ecomKey,...val})}}
              type='text'
              id={`linkedin`}
              name={`ecom_${res.value.toLowerCase()}_3d`}
            />
            </Col>
            
            
           </Row>
           ):null}
       
          <Button.Ripple type='submit' color='primary' className=' mt-1 float-right' disabled={ecomOptions.length==0 ||userDetails.role_type!='admin' && !userDetails['merchant'].includes('C')}>
            <span className='align-middle d-sm-inline-block '>Add</span>
            
          </Button.Ripple>
          
          </Col>

          </AvForm>
      </Fragment>
  )

}

export default AddKeys