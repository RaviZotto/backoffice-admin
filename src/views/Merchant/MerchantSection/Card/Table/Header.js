import { Row,Card, Col, Button, Input } from "reactstrap";
import Flatpickr from "react-flatpickr";
import { Search } from "react-feather";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import React,{ useContext, useState } from "react";
import moment from "moment";
import { SectionContext } from "../..";

const CustomHeader = ({setSelectedCardAccountId,setSearchQuery}) => {
   const { dates, setDates ,cards} = useContext(SectionContext);
    
  const [state, setState] = useState({
    start_date: dates.start_date,
    end_date: dates.end_date,
  });

  return (
    <Card className="p-1 pr-0">
    <div className="row row-flex-wrap pr-sm-0">
      <div className="col-md-2 col-6  ">
        <Flatpickr
          className="form-control"
           value={dates.start_date}
          onChange={(e) =>
            setState({
              ...state,
              start_date: moment(e[0]).format("YYYY-MM-DD"),
            })
          }
        />
      </div>
      <div className="  col-md-2 col-6   ">
        <Flatpickr
          className="form-control "
           value={dates.end_date}
         
          onChange={(e) =>
            setState({ ...state, end_date: moment(e[0]).format("YYYY-MM-DD") })
          }
        />
      </div>
      <div className=" col-12 col-md-1 ">
        <Button
          disabled={state.start_date==dates.start_date&&state.end_date==dates.end_date}
          className="w-100 mb-2 mt-1 mb-md-0 mt-md-0  "
          color="primary"
           onClick={() => setDates({ ...state })}
        >
          <Search size={17} />
        </Button>
      </div>

      <div className="col-12 col-md-2  mb-sm-0 mb-1 ml-md-auto">
        <Input
          id="search-invoice"
          type="text"
          onChange={(e) => setSearchQuery(e.target.value)}
          placeholder="Search"
        />
      </div>
      <div className=" col-md-2 col-12 mr-sm-0 col-12">
        <Input
          className="form-control"
          type="select"
          onChange={(e) => setSelectedCardAccountId(`${e.target.value}`)}
        >  <option value={0} label="select--" />
           {cards.map(e=> 
                e.status!='awaiting-activation'&&
               <option value={e.currency_account_id} label={e.name_on_card+e.truncated_pan}/>
            )}
        </Input>
      </div>
    </div>
  </Card>
  );
};

export default CustomHeader;
