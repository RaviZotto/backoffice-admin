import React, { PureComponent, useState } from "react";
import { CheckCircle, MoreVertical } from "react-feather";
import {
  Card,
  CardBody,
  CardHeader,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  DropdownToggle,
  Spinner,
  Button,
  UncontrolledDropdown,
  UncontrolledButtonDropdown
} from "reactstrap";

import produce from "immer";
import "../index.scss";

import logo2 from "../../../../assets/images/logo/master-card2.png";
import { DropdownItem, DropdownMenu } from "reactstrap/lib";
import { useDispatch } from "react-redux";
import { MODIFY_CARD_STATUS } from "../../../../redux/actions/transfer";
import { currencyCodes } from "../../../../utility/Utils";

// const ModalPopUP = ({ merchant_id, openModal, closeModal, callback }) => {
//   const [reason, setReason] = useState("");
//   const dispatch = useDispatch();
//   return (
//     <div className="vertically-centered-modal">
//       <Modal
//         isOpen={openModal.toggle}
//         toggle={() => closeModal({})}
//         className="modal-dialog-centered modal-sm"
//       >
//         <ModalHeader toggle={() => closeModal({})}></ModalHeader>
//         <ModalBody>
//           <Input
//             onChange={(e) => setReason(e.target.value)}
//             placeholder={`Plz enter the reason to ${openModal.action} card`}
//           />
//         </ModalBody>
//         <ModalFooter className="d-flex justify-content-end">
//           <Button.Ripple
//             disabled={!reason || !reason.trim()}
//             color="primary"
//             onClick={(e) =>
//               dispatch(
//                 MODIFY_CARD_STATUS(
//                   merchant_id,
//                   { id: openModal.id, action: openModal.action, reason },
//                   () => {
//                     callback(true);
//                     closeModal({});
//                   }
//                 )
//               )
//             }
//           >
//             submit
//           </Button.Ripple>
//         </ModalFooter>
//       </Modal>
//     </div>
//   );
// };

const DebitsCards = ({ data, accounts, key, setCards, cards, setRefetch }) => {
  const dispatch = useDispatch();
  // const updateCardState = ( action) => {
  //     let cardsTemp=[...cards];
  //     //console.log(cardsTemp,key);

  //     cardsTemp[key].status = action;

  //   setCards(cardsTemp);
  // };
  const [loader, setLoader] = useState(false);
  const accountObj =
    accounts.find((e) => e.id == data.currency_account_id) || {};
  return (
    <div>
      {     (data.status != "active"&&!data.status.includes("activated")) || data.status == "not-created" ? (
        <div
          className="mx-1"
          style={{
            background: "white",
            opacity: "0.85",
            width: "450px",
            height: "267px",

            position: "absolute",
            zIndex: "111",
          }}
        >
          <Button
            outline
            style={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
            onClick={(e) => {
              setLoader(true);
              dispatch(
                MODIFY_CARD_STATUS(
                  data.merchant_id,
                  { action: "activate", id: data.id },
                  () => {
                    setRefetch(true);
                    setLoader(false);
                  }
                )
              );
            }}
            disabled={loader}
          >
            {loader ? <Spinner size="sm" className="mr-1" /> : null}
            Activate
          </Button>
        </div>
      ) : null}
      <Card
        className={"card-congratulations mx-1"}
        style={{ width: "450px", cursor: "pointer" }}
      >
        <CardBody>
          <div className="position-absolute  " style={{ left:"26rem",top:'0.5rem'}}>
            <p>{data.card_type}</p>
          </div>
          <div className="d-flex flex-column">
            <div>Balance</div>
            <div className="d-flex justify-content-between">
              <h1 style={{ color: "white" }}>
                {" "}
                {new Intl.NumberFormat("en-US", {
                  style: "currency",
                  currency: accountObj.currencycode,
                })
                  .format(accountObj.current_balance)
                  .replace(/^(\D+)/, "$1 ")}
              </h1>
              <Settings key={key} data={data} callback={setRefetch}  />
            </div>
            <div className="d-flex justify-content-between my-3">
              <div>
                <img src={logo2} />
              </div>
              <h3 style={{ color: "white", letterSpacing: "3px" }}>
                **** **** **** {data.truncated_pan}
              </h3>
            </div>
            <div className="d-flex">
              <div className="d-flex flex-column">
                <div className="font-small-2">VALID THRU</div>
                <h5 style={{ color: "white" }}>{data.expiry_date}</h5>
              </div>
              <div className="d-flex flex-column mx-3">
                <div className="font-small-2">CARD HOLDER</div>
                <h5 style={{ color: "white" }}>{data.name_on_card}</h5>
              </div>
              <div className="d-flex flex-column">
                <div className="font-small-2">CARD STATUS</div>
                <h5 style={{ color: "white" }}>
                  <CheckCircle size={15} style={{ marginRight: "0.5rem" }} />
                  {data.status}
                </h5>
              </div>
            </div>
          </div>
        </CardBody>
      </Card>
    </div>
  );
};
export default DebitsCards;

const Settings = ({ data, callback }) => {
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState({
    toggle: false,
    id: 0,
    action: "",
  });
  return (
    <>
      {/* <ModalPopUP
        merchant_id={data.merchant_id}
        callback={callback}
        openModal={openModal}
        closeModal={setOpenModal}
      /> */}
      <UncontrolledDropdown className="chart-dropdown">
        <DropdownToggle
          color=""
          className="bg-transparent btn-sm border-0 p-50"
        >
          <MoreVertical size={18} className="cursor-pointer" color="white" />
        </DropdownToggle>
        <DropdownMenu top>
          <DropdownItem
            className="w-100 d-flex text-center"
            disabled={data.status == "suspend"}
            onClick={(e) =>{
          
              dispatch(
                MODIFY_CARD_STATUS(
                  data.merchant_id,
                  { action:'suspend',reason: "card-lost", id: data.id },
                  () => {
                    callback(true);
                  }
                )
              );
            }}
          >
            Freeze
          </DropdownItem>
          <DropdownItem
            className="w-100"
            disabled={data.status == "close"}
         
          >
            <UncontrolledDropdown direction='right' className="chart-dropdown d-flex  ">
              <DropdownToggle  color="" className="bg-transparent btn-sm border-0 p-50 ">
                Close
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem 
                 onClick={(e) =>{
              
                  dispatch(
                    MODIFY_CARD_STATUS(
                      data.merchant_id,
                      { reason: "card-stolen",action:'close', id: data.id },
                      () => {
                        callback(true);
                      }
                    )
                  );
                }}
                
                >
                  Stolen
                </DropdownItem>
                <DropdownItem 
                  onClick={(e) =>{
               
                    dispatch(
                      MODIFY_CARD_STATUS(
                        data.merchant_id,
                        { reason: "card-destroyed",action:'close', id: data.id },
                        () => {
                          callback(true);
                         
                        }
                      )
                    );
                  }}
                >
                  Destroyed
                </DropdownItem>
             
              </DropdownMenu>
            </UncontrolledDropdown>
          </DropdownItem>
          <DropdownItem
            className="w-100 d-flex"
            onClick={(e) =>
              dispatch(
                MODIFY_CARD_STATUS(
                  data.merchant_id,
                  { id: data.id, action: "reset_pin" },
                  () => callback(true)
                )
              )
            }
          >
            Reset Pin
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    </>
  );
};
