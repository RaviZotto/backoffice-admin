import React, { useContext, useEffect, useState } from 'react';
import { Button ,Card,Row} from 'reactstrap';
import {PlusCircle} from 'react-feather';
import CardsModal from './CardsModal';
import { Axios, getAllCards } from '../../../../Api';
import { SectionContext } from '..';
import DebitsCards from './DebitsCards';
import Table from './Table';
import { Fragment } from 'react';

import PerfectScrollBar from "react-perfect-scrollbar";
import axios from 'axios';
import notification from '../../../../components/notification';
import Loading from "@core/components/spinner/Loading-spinner";
import { userDetails } from '../../../../utility/Utils';
const CardsSection = props=>{
    const[loading,setLoading]=useState(true);
    const[refetch,setRefetch]=useState(false);
    const{merchant,accounts,cards,setCards}=useContext(SectionContext);
    const[modal,setModal]=useState(false);
    useEffect(()=>{
        if(!cards.length || refetch){
          //console.log(merchant);

          setLoading(true);
        //  (async()=>{ 
        //     try{ 
        //       let {data} = await axios.get(`https://live.railsbank.com/v1/customer/cards?holder_id=60d98ce0-cda8-4e04-a847-0a73fb471621`,{ headers: { 'Authorization': 'API-Key r91iywtet08lsb6slgz34ms1g3jykae6#fl5y62ech0moc9xa32742921xllg8spsmnmqkdyuc4a8wwheghnlnadc762fnnkc'}});
        //       //console.log(data);
        //       setCards(data);
        //       setLoading(false);
        //     }catch(e){ 
        //         //console.log(e);
        //         setLoading(false);
        //         setRefetch(false);
        //       notification({title:"Error",message:e.response?e.response.data: e.toString(),type:'error'})
        //     }
        //  })()
       setTimeout(()=>{
        getAllCards(merchant.merchant_id).then(res=>{
             if(res.Cards){setCards(res.Cards); setRefetch(false);setLoading(false);}
        }).catch(e=>{//console.log(e);
          setRefetch(false);setLoading(false)});
    },3000);
    }else { setLoading(false)}
  },[refetch])
    
   if(loading)return<Loading />

    return (
        <Fragment>
        <Button.Ripple color='primary'  onClick={()=>setModal(!modal)} className='mb-1' > <PlusCircle size={12} className='mr-2' 
         disabled={userDetails.role_type&&userDetails.role_type!='admin' && !userDetails['merchant'].includes('C')}
        /> Add Cards</Button.Ripple>
        {modal&&<CardsModal modalOpen={modal} handleModal={()=>setModal(!modal)} refetch={()=>setRefetch(true)} />}
        <PerfectScrollBar>
       <Row className='p-1 flex-nowrap'>
    
        {cards.map((res,i)=>{
         //console.log(i);
         return(   <DebitsCards key={i} data={res} cards={cards} setCards={setCards} accounts={accounts} setRefetch={setRefetch} />);
        })}
       
        </Row>
        </PerfectScrollBar>
        <Table/>
        </Fragment>
    )
}

export default CardsSection;