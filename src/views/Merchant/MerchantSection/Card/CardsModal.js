import AvForm from "availity-reactstrap-validation-safe/lib/AvForm";
import React, { useContext, useState } from "react";
import {
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  ModalFooter,
  Row,
  Col,
} from "reactstrap";
import FormGroup from "reactstrap/lib/FormGroup";
import Label from "reactstrap/lib/Label";
import Select from "react-select";
import { X } from "react-feather";
import { selectThemeColors } from "@utils";
import AvInput from "availity-reactstrap-validation-safe/lib/AvInput";
import { SectionContext } from "..";
import { createAccountForLedger, createCardForLedger } from "../../../../Api";
import "@styles/react/libs/react-select/_react-select.scss";
import Spinner from "reactstrap/lib/Spinner";
import { countries, phoneCodes, userDetails } from "../../../../utility/Utils";
import { Fragment } from "react";
import notification from "../../../../components/notification";


const CardsModal = ({ modalOpen, handleModal, refetch }) => {
  //console.log(modalOpen);
  // alert(modalOpen)
  const { merchant, history, setRefetch, accounts } =
    useContext(SectionContext);
  const CloseBtn = (
    <X className="cursor-pointer" size={15} onClick={handleModal} />
  );

  const [loader, setLoader] = useState(false);
  const [currency, setCurrency] = useState(null);
  const [card_type,setCardType]=useState('virtual');
  const [ledger, setLedgerId] = useState(null);
  const handleSumbit = async (event, errors, value) => {
    if(errors.length)return;
    event.preventDefault();
    try {
      setLoader(true)
      let {phone,phone_code,address_street,address_number,address_city,address_postal_code,address_iso_country}=value;
      delete value.phone;
      delete value.phone_code;
      delete value.address_street;
      delete value.address_number;
      delete value.address_postal_code;
      delete value.address_iso_country;
      delete value.address_city;
      let card_delivery_address ={
          address_iso_country,
          address_street,
          address_number,
          address_city,
          address_postal_code
      }
      await createCardForLedger({ ...value,telephone:phone_code+phone, card_delivery_address, account_id: String(ledger) ,merchant_id:String(merchant.merchant_id)});
      refetch();
      handleModal();
    } catch (e) {
      notification({title:"Error",message:e||"something went wrong",type:'error'})
      setLoader(false);
      //console.log(e);
    }
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={handleModal}
      //   className='middle-lg'
      //   modalClassName='modal-slide-in'
      contentClassName="pt-0"
      className="modal-dialog-centered"
    >
      <ModalHeader className="" toggle={handleModal} close={CloseBtn} tag="div">
        Add Card
      </ModalHeader>
      <AvForm onSubmit={handleSumbit}>
        <ModalBody>
        <FormGroup tag={Row}>
            <Col>
            <Label>Card Name</Label>
              <AvInput type="text" className="form-control" name="name_on_card" />
              </Col>
              </FormGroup>
              
          <Row>
          
            <FormGroup tag={Col}>
              
              <Label>Phone Code</Label>
              <AvInput  type="select" name="phone_code" required >
              <option value={null} label='select--'  />
                {phoneCodes.map(el=>
                   <option value={el.value} label={el.label}/>
                  )}
                </AvInput>
              <Label>Railsbank Account</Label>
              <Select
                required
                placeholder="select--"
                name="card_type"
                className="react-select"
                theme={selectThemeColors}
                classNamePrefix="select"
                
                options={[{label:'Select--',value:null},...accounts.filter(e=>e.gateway=='RAILSBANK').map((res) => ({
                  label: res.account_name,
                  value: res.id,
                }))]}
                onChange={(e) => setLedgerId(e.value)}
              />
            </FormGroup>
            <FormGroup tag={Col}>
             
              <Label>Phone</Label>
              <AvInput required type="text" name="phone" />
              <Label>Card Type</Label>
              <AvInput type="select" name="card_type" value={card_type} onChange={e=>setCardType(e.target.value)}>
                <option value={"virtual"} label={"Virtual"} />
                <option value={"physical"} label={"Physical"} />
              </AvInput>
            </FormGroup>
          </Row>
         { card_type=='physical'? <Fragment>
          <Label>Address</Label>
          <AvInput name="address_street" type="text" />
          <Label>House No</Label>
          <AvInput name="address_number" type="text" />
          <Row>
            <FormGroup tag={Col}>
                <Label>Country</Label>
              <AvInput type="select" name="address_iso_country" required>
              <option value={null} label='select--'  />
                {countries.map((res) => (
                  <option value={res.value} label={res.label} />
                ))}
              </AvInput>
              <Label>Delivery Name</Label>
              <AvInput 
              type='text'
               name='card_delivery_name'
              />
              <Label>City</Label>
              <AvInput 
              type='text'
               name='address_city'
              />

            </FormGroup>
            <FormGroup tag={Col}>
                <Label>ZipCode</Label>
                <AvInput 
                name='address_postal_code'
                type='text'
                />
                  <Label>Carrier Type</Label>
                <AvInput 
                required
                name='card_carrier_type'
                type='select'
                defaultValue='standard'
                >
                    <option value={null} label='select--'  />
                {['renewal','replacement','standard'].map(el=>
                 <option value={el} label={el.toUpperCase()}/>
                )
                }  
                </AvInput>
                <Label>Delivery Mode</Label>
                <AvInput
                required
                name='card_delivery_method'
                type='select'
                >
                  <option value={null} label='select--'  />
                 {["standard-first-class", "international-mail", "dhl", "courier"].map(
                     res=>
                     <option value={res} label={res.toUpperCase()}/>
                 )}
                </AvInput>
            </FormGroup>
            
         
          </Row>
          </Fragment>:null
        }
        </ModalBody>

        <ModalFooter>
          <div className="d-flex justify-content-center">
            <Button.Ripple
              className="mr-2"
              type="submit"
              color={"primary"}
              disabled={loader|| userDetails.role_type!='admin' && !userDetails['merchant'].includes('C')}
            >
              {loader ? <Spinner size="sm" className="mr-1" /> : null}{" "}
              <span>Add</span>
            </Button.Ripple>
            <Button.Ripple color={"danger"} onClick={handleModal}>
              Close
            </Button.Ripple>
          </div>
        </ModalFooter>
      </AvForm>
    </Modal>
  );
};
export default CardsModal;
