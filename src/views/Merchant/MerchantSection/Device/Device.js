import React, { useContext, useEffect, useState } from 'react';
import { Button ,Card} from 'reactstrap';
import {Edit, PlusCircle, Trash} from 'react-feather';
import DeviceModal from './DeviceModal';
import {deleteRegDevice, getAllRegDevice} from  '../../../../Api'
import { SectionContext } from '..';
import Loading from "@core/components/spinner/Loading-spinner";
import PerfectScrollBar from "react-perfect-scrollbar";
import Row from 'reactstrap/lib/Row';
import CardBody from 'reactstrap/lib/CardBody';
import type from './img/A60.png'
import EditModal from './EditModal';
import Table from './Table';
import { Fragment } from 'react';
import { userDetails } from '../../../../utility/Utils';
import MenuOption from '../MenuOption';
import notification from '../../../../components/notification';


export  const Device = ({ data,handleDevice,setSelected, selectedDevice,devices,setDevices }) => {

 const deleteDevice=async(id)=>{
   try{
      await deleteRegDevice(id);
      setDevices(devices.filter(el=>el.id!=id));
    }catch(e){ 
     notification({ title: "Error", message: e.toString(),type: "error"})
    }
 }
 
    const selected = selectedDevice.id==data.id;
    const textStyle={color:selected?"white":"black"}
    return (
      <div
     
      >
        <Card
        className="cursor-pointer"
        onClick={el=>handleDevice(data)}
        color={ selected?"primary":"" }
         >
          <CardBody className="mb-n1">
            <MenuOption className='position-absolute' setData={setSelected}  style={{height:'1.5em',left:'12em',top:'0.1em',...textStyle}}  data={data} deleteAccount={deleteDevice} />
            {/* <Edit  size={15} className='position-absolute' style={{height:'1.5em',left:'12em',top:'0.1em',...textStyle}} onClick={el=>{setSelected(data) }} />*/}
            <div className="d-flex cursor-pointer flex-row justify-content-between align-items-center"> 
              <div className="mr-1">
                <h4
                  style={{
                    width: "max-content",
                    ...textStyle
                  }}
                >
                  {data.type}
                </h4>
                <p
                  className="font-small-2 font-weight-bold text-uppercase"
                  style={{ marginBottom: "-0.4rem", width: "max-content",...textStyle }}
                >
                  {data.device_name}
                </p>
                <p style={{ letterSpacing: "0.05rem",...textStyle }} className="font-small-2">
                  {data.serial}
                </p>
                <p className="font-small-3 font-weight-bold" style={{...textStyle}}>
                  {data.device_currency}
                </p>
              </div>
              <div className="ml-4">
                <img src={type} style={{ width: "60px" }}></img>
              </div>
            </div>
          </CardBody>
        </Card>
      </div>
    );
  };
  




const DeviceSection = props=>{
    const {merchant}=useContext(SectionContext);
   const [loader,setLoader]=useState(true);
   const [refetch,setRefetch]=useState(false);
   const[regDevice,setRegDevice]=useState([]);
   const [selectedData,setSelectedData]=useState({})
   useEffect(()=>{

       if(loader || refetch){
     getAllRegDevice(merchant.merchant_id).then(res=>{
         if(res.device){ setRegDevice(res.device);
            setLoader(false);
            setRefetch(false);
         }
     }).catch(e=>{//console.log(e);
        setLoader(false);
        setRefetch(false);
    });
    }
   },[refetch])
    const handleDevice=(el)=>setSelectedDevice(el);
    const[modal,setModal]=useState(false);
    const handleModal=()=>setModal(!modal);
    const[selectedDevice,setSelectedDevice]=useState({});
    if(loader)return <Loading/>
    return (
        <Fragment>
        <Button.Ripple color='primary'  onClick={handleModal} > <PlusCircle size={12} className='mr-2'    disabled={ userDetails.role_type&&userDetails.role_type!='admin' && !userDetails['merchant'].includes('C')} /> Add Devices</Button.Ripple>
         <DeviceModal modalOpen={modal} handleModal={()=>handleModal()} refetch={()=>setRefetch(true)} />
         <EditModal modalOpen={selectedData.id} handleModal={()=>setSelectedData({})}  data={selectedData} refetch={()=>setRefetch(true)} />
          <PerfectScrollBar>
              <Row className='p-2 flex-nowrap'>
          {   
              regDevice.map(res=>
                <div className='col' style={{flexGrow:0}}>
                <Device key={res.id}  devices={regDevice} setDevices={setRegDevice} selectedDevice={selectedDevice} handleDevice={handleDevice} data={res} setSelected={el=>setSelectedData(el)} refetch={el=>setRefetch(true)} />
                </div>
                )
          }
          </Row>
          </PerfectScrollBar>
          <Table selectedDevice={selectedDevice} />
         </Fragment>   
    )
}

export default DeviceSection;