import AvForm from "availity-reactstrap-validation-safe/lib/AvForm";
import React, { useContext, useEffect, useState } from "react";
import {
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  ModalFooter,
  Row,
  Col,
} from "reactstrap";
import FormGroup from "reactstrap/lib/FormGroup";
import Label from "reactstrap/lib/Label";
import { X } from "react-feather";
import AvField from "availity-reactstrap-validation-safe/lib/AvField";
import { SectionContext } from "..";
import AvInput from "availity-reactstrap-validation-safe/lib/AvInput";
import Spinner from "reactstrap/lib/Spinner";
import { upateRegDevice} from "../../../../Api";
// import { userDetails } from "../../../../utility/Utils";
import userPersmission from '@hooks/userPersmission';
const EditModal = ({ modalOpen, handleModal, refetch,data }) => {
  // alert(modalOpen)
 const {userDetails} = userPersmission()
  const { device, accounts, merchant } = useContext(SectionContext);
  const [loader, setLoader] = useState(false);
  const CloseBtn = (
    <X className="cursor-pointer" size={15} onClick={handleModal} />
  );
  const [currency, setCurrency] = useState(null);
  const [update,setUpdated]=useState({udpateInfo:{},res:true})
  const [state,setState]=useState({
      account_id:data.account_id,
      device_name:data.device_name,
      mid:data.mid,
      serial:data.serial,
      tid:data.tid,
      device_currency:data.device_currency,
      payment_gateway:data.payment_gateway
  })
  
 



      useEffect(()=>{
          let updatedInfo={};
          for(let key of Object.keys(state)){
              if(state[key]!==data[key]) updatedInfo[key]=state[key];
          } 
           if(Object.keys(updatedInfo).length)setUpdated({ udpateInfo: updatedInfo,res:false});
           else setUpdated({res:true});
      },[state])
     
  const handleSumbit = async (event, error, value) => {
    event.preventDefault();
    try {
    
      if (error.length) {
        //console.log(value);
      } else {
        setLoader(true);
        await  upateRegDevice({
          data: { ...update.udpateInfo,device_id: data.device_id },
        });
         handleModal()
        setLoader(false);
        refetch();
      }
    } catch (e) {
      setLoader(false);
    }
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={handleModal}
      //   className='middle-lg'
      //   modalClassName='modal-slide-in'
      contentClassName="pt-0"
      className="modal-dialog-centered"
    >
      <ModalHeader className="" toggle={handleModal} close={CloseBtn} tag="div">
        Add Device
      </ModalHeader>
      <AvForm onSubmit={handleSumbit}>
        <ModalBody>
          <Row>
            <FormGroup tag={Col} md="6">
              <Label>Device Type</Label>
              <AvField
                type="text"
                className="form-control"
                value={data.type}
                name="type"
                disabled
              />
           

              <Label>Account</Label>
              <AvInput
               required
                type="select"
                name="account_id"
                className="mb-1"
                value={state.account_id || data.account_id}
                onChange={(e) => {
                  let currencyAcc = accounts.find(
                    (res) => res.id == e.target.value
                  )
                  currencyAcc&&setCurrency(currencyAcc.currencycode);
                 currencyAcc&& setState({...state,account_id:e.target.value,device_currency:currencyAcc.currencycode})
                }}
              >  
               <option label="select" value={""}/>
                {accounts &&
                  accounts.map(
                    (res, i) =>
                      !res.railsbank_ledger_id && (
                        <option
                          value={res.id}
                          label={res.account_name}
                        />
                      )
                  )}
              </AvInput>

              <Label>Device Name</Label>
              <AvField
                type="text"
                className="form-control"
                name="device_name"
                value={data.device_name}
                onChange={el=>setState({...state,device_name:el.target.value})}
              />
              <Label>Serial Number</Label>
              <AvField
                required
                type="text"
                value={data.serial}
                onChange={el=>setState({...state,serial:el.target.value})}
                className="form-control"
                name="serial"
              />
            </FormGroup>
            <FormGroup tag={Col} md="6">
              <Label>Currency</Label>
              <AvInput
                disabled
                name="device_currency"
                className="mb-1"
                value={state.device_currency || data.device_currency}
              />
              <Label>MID</Label>
              <AvField type="text" value={data.mid} onChange={el=>setState({...state,mid:el.target.value})} className="form-control" name="mid" />
              <Label>Status</Label>
              <AvField
                type="select"
                className="form-control"
                name="status"
                onChange={el=>setState({...state,status:el.target.value})}
                value={data.status}
              >
                <option label="Active" defaultChecked value="Active" />
                <option label="Inactive" value="Inactive" />
              </AvField>
              <Label>TID</Label>
              <AvField
                required
                type="text"
                className="form-control"
                name="tid"
                value={data.tid}
                onChange={el=>setState({...state,tid:el.target.value})}
              />
            </FormGroup>
          </Row>
          <Row>
            <div className="col ">
              <Label>Payment Gateway</Label>
              <AvInput
                type="select"
                className="form-control "
                name="payment_gateway"
                value={data.payment_gateway}
                onChange={el=>setState({...state,payment_gateway:el.target.value})}
              >
                {["HANDPOINT", "NOVELPAY"].map((res, i) => (
                  <option value={res} label={res} />
                ))}
              </AvInput>
            </div>
          </Row>
        </ModalBody>

        <ModalFooter>
          <div className="d-flex justify-content-center">
            <Button.Ripple
              className="mr-2"
              type="submit"
              color={"primary"}
              disabled={loader || update.res ||(userDetails?.role_type!='admin' && !userDetails['merchant']?.includes('U'))}
            >
              {loader ? <Spinner size="sm" /> : null} update
            </Button.Ripple>
            <Button.Ripple color={"danger"} onClick={handleModal}>
              Close
            </Button.Ripple>
          </div>
        </ModalFooter>
      </AvForm>
    </Modal>
  );
};
export default EditModal
