import AvForm from "availity-reactstrap-validation-safe/lib/AvForm";
import React, { useContext, useState } from "react";
import {
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  ModalFooter,
  Row,
  Col,
} from "reactstrap";
import FormGroup from "reactstrap/lib/FormGroup";
import Label from "reactstrap/lib/Label";
import Select from "react-select";
import Currency from "../../../../utility/extras/currencyCodes.json";
import { X } from "react-feather";
import AvField from "availity-reactstrap-validation-safe/lib/AvField";
import MerchantSection, { SectionContext } from "..";
import AvInput from "availity-reactstrap-validation-safe/lib/AvInput";
import Spinner from "reactstrap/lib/Spinner";
import { addRegDevice } from "../../../../Api";
import notification from "../../../../components/notification";
import { userDetails } from "../../../../utility/Utils";

const AccountModal = ({ modalOpen, handleModal, refetch }) => {
  // alert(modalOpen)
  const { device, accounts, merchant } = useContext(SectionContext);
  const [loader, setLoader] = useState(false);
  const CloseBtn = (
    <X className="cursor-pointer" size={15} onClick={handleModal} />
  );
  const [currency, setCurrency] = useState(null);

  const handleSumbit = async (event, error, value) => {
    event.preventDefault();
    try {
    
      if (error.length) {
        //console.log(value);
      } else {
        setLoader(true);
        await addRegDevice({
          data: { ...value, merchant_id: merchant.merchant_id },
        });
         handleModal()
        setLoader(false);
        refetch();
      }
    } catch (e) {
      notification({title:"Error",message:e.toString(),type:'error'})
      setLoader(false);
    }
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={handleModal}
      //   className='middle-lg'
      //   modalClassName='modal-slide-in'
      contentClassName="pt-0"
      className="modal-dialog-centered"
    >
      <ModalHeader className="" toggle={handleModal} close={CloseBtn} tag="div">
        Add Device
      </ModalHeader>
      <AvForm onSubmit={handleSumbit}>
        <ModalBody>
          <Row>
            <FormGroup tag={Col} md="6">
              <Label>Device Type</Label>
              <AvField
                type="select"
                className="form-control"
                value={device[0]}
                name="type"
              >
                {device.map((res) => (
                  <option value={res} label={res} />
                ))}
              </AvField>

              <Label>Account</Label>
              <AvInput
               required
                type="select"
                name="account_id"
                placeholder='select'
                className="mb-1"
                onChange={(e) => {
                  let currencyAcc = accounts.find(
                    (res) => res.id == e.target.value
                  );
                  currencyAcc&&setCurrency(currencyAcc.currencycode);
                }}
              >
                <option label="select--" value={null}/>
                {accounts &&
                  accounts.map(
                    (res, i) =>
                      !res.railsbank_ledger_id && (
                        <option
                          value={parseInt(res.id)}
                          label={res.account_name}
                        />
                      )
                  )}
              </AvInput>

              <Label>Device Name</Label>
              <AvField
                type="text"
                className="form-control"
                name="device_name"
              />
              <Label>Serial Number</Label>
              <AvField
                required
                type="text"
                className="form-control"
                name="serial"
              />
            </FormGroup>
            <FormGroup tag={Col} md="6">
              <Label>Currency</Label>
              <AvInput
                disabled
                name="device_currency"
                className="mb-1"
                value={currency}
              />
              <Label>MID</Label>
              <AvField type="text" className="form-control" name="mid" />
              <Label>Status</Label>
              <AvField
                type="select"
                className="form-control"
                name="status"
                value={"Active"}
              >
                <option label="Active" defaultChecked value="Active" />
                <option label="Inactive" value="Inactive" />
              </AvField>
              <Label>TID</Label>
              <AvField
                required
                type="text"
                className="form-control"
                name="tid"
              />
            </FormGroup>
          </Row>
          <Row>
            <div className="col ">
              <Label>Payment Gateway</Label>
              <AvInput
                type="select"
                className="form-control "
                name="payment_gateway"
                defaultValue={"HandPoint"}
              >
                {["HandPoint", "NovelPay"].map((res) => (
                  <option value={res.toUpperCase()} label={res} />
                ))}
              </AvInput>
            </div>
          </Row>
        </ModalBody>

        <ModalFooter>
          <div className="d-flex justify-content-center">
            <Button.Ripple
              className="mr-2"
              type="submit"
              color={"primary"}
              disabled={loader||  userDetails.role_type&&userDetails.role_type!='admin' && !userDetails['merchant'].includes('C')}
            >
              {loader ? <Spinner size="sm" /> : null} Add
            </Button.Ripple>
            <Button.Ripple color={"danger"} onClick={handleModal}>
              Close
            </Button.Ripple>
          </div>
        </ModalFooter>
      </AvForm>
    </Modal>
  );
};
export default AccountModal;
