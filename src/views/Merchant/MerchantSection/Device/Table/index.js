import React,{ useContext, Fragment, useEffect, useState } from "react";
import CustomHeader from "./Header";

// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge } from "reactstrap";
import moment from "moment";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import Spinner from "reactstrap/lib/Spinner";
import { SectionContext } from "../..";
const Table = ({selectedDevice}) => {
const{transactions}=useContext(SectionContext);
const [statusQuery,setStatusQuery]=useState('');
const [searchQuery,setSearchQuery]=useState('');

const columns = [
  {
    name: "Date",
    selector: "date_of_transaction",
    sortable: true,
    minWidth: "110px",
    cell: (row) => {
      let date = moment(row.date_of_transaction);
      return (
        <Fragment>
          <div className="mt-1">
            <h6 className="font-small-3">{date.format("YYYY-MM-DD")}</h6>
            <h6 className="text-secondary font-small-2">
              {date.format("HH:mm:ss")}
            </h6>
          </div>
        </Fragment>
      );
    },
  },
  {
    name: "Status",
    selector: "status",
    minWidth: "110px",
    sortable: true,
    cell: (row) => (
      <Fragment>
        <div
          className={
            row.status == 0
              ? "bg-danger"
              : row.status == 1
              ? "bg-success"
              : "bg-primary"
          }
          style={{
            height: "10px",
            width: "10px",
            borderRadius: "50%",
            display: "inline-block",
            marginRight: "5px",
          }}
        />
        <span>
          {row.status == 0
            ? "Failed"
            : row.status == 1
            ? "Success"
            : "Pending"}
        </span>
      </Fragment>
    ),
  },
  {
    name: "Transaction ID",
    selector: "txn_no",
    sortable: false,
    minWidth: "200px",
    cell: (row) => {
      return <div className="font-small-3">{row.txn_no}</div>;
    },
  },
  {
    name: "Order ID",
    selector: "order_id",
    sortable: false,
    minWidth: "130px",
    cell: (row) => {
      return <div className="font-small-3">{row.order_id}</div>;
    },
  },
  {
    name: "Payment Type",
    selector: "pay_method",
    sortable: false,
    minWidth: "150px",
    cell: (row) => <Badge color="primary">{row.pay_method}</Badge>,
  },
  {
    name: "Amount",
    selector: "amount",
    sortable: false,
    cell: (row) => (
      <h6
        className={`${
          row.paid_type == "cr" ? "text-success" : "text-danger"
        } font-weight-bolderer`}
      >
        {new Intl.NumberFormat("en-US", {
          style: "currency",
          currency: row.currency,
        })
          .format(row.amount)
          .replace(/^(\D+)/, "$1 ")}
      </h6>
    ),
  },

];

  const dataToRender = () => {
    if (!selectedDevice.id) return [];
    let tempData = transactions;
    if (selectedDevice.id)
      tempData = tempData.filter(
        (el) => el.device_seriel==selectedDevice.serial
      );

    if (searchQuery)
      tempData = tempData.filter((el) => {
        let temp = JSON.parse(JSON.stringify(el));
        let sq = searchQuery.toLowerCase();
        temp.my_txn_type = el.paid_type == "cr" ? "credit" : "debit";
        temp.my_status =
          el.status == "1"
            ? "success"
            : el.status == "0"
            ? "failed"
            : "pending";
        temp.pay_method = `${temp.pay_method}`.toLowerCase();
        let searchFields = [
          "my_status",
          "txn_no",
          "order_id",
          "pay_method",
          "amount",
          "my_txn_type",
        ];
        for (let field of searchFields)
          if (`${temp[field]}`.includes(sq) || `${temp[field]}`.startsWith(sq))
            return true;
        return false;
      });
    if (statusQuery)
      tempData = tempData.filter((el) => el.status == statusQuery);
    return tempData;
  };

  return (
    <div className="invoice-list-wrapper">
      <CustomHeader  setSearchQuery={setSearchQuery} setStatusQuery={setStatusQuery} />
      <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>
    </div>
  );
};

export default Table;
