import flatpickr from "flatpickr";
import React, { useContext, useEffect, useState } from "react";
import { selectThemeColors } from "@utils";
import Flatpickr from "react-flatpickr";
import {  Search } from "react-feather";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import Select from "react-select";
import { CardBody, Card, CardText, Button } from "reactstrap";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import moment from "moment";
import Spinner from "reactstrap/lib/Spinner";
import { FlairCardReport } from ".";

export default function (){

const {setMerchant,dates,setDates,merchant,merchants,currencyOption,currency,setCurrency,currencies}=useContext(FlairCardReport); 
const [state,setState]=useState({...dates})
return (
    <div>
      <Card>
        <CardBody>
          <Row>
            <Col md={5} sm={12} className="mb-1 mb-md-0 ">
                <labe>Merchant</labe>
              <Select
                placeholder='select merchant'
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
                 value={{value:merchant,label:merchants.find(e=>e.merchant_id==merchant)&&merchants.find(e=>e.merchant_id==merchant).comp_name||'Select--' }}
                options={[ {label:'Select--',value:0}, ...merchants.map((el) => ({
                  label: el.comp_name,
                  value: el.merchant_id,
                })) ]}  
                
                

                onChange={el=>{setMerchant(el.value)}}
              />
            </Col>
            <Col md={7} sm={12} className="mb-sm-1   d-flex pt-sm-2 justify-content-end">
              <Row className=' justify-content-end' >
                <div className="col-md-4 col-6  ">
                  <Flatpickr
                    className="form-control"
                    value={dates.start_date}
                    onChange={(e) =>
                      setState({
                        ...state,
                        start_date: moment(e[0]).format("YYYY-MM-DD"),
                      })
                    }
                  />
                </div>
                <div className="  col-md-4 col-6   ">
                  <Flatpickr
                   
                   
                    className="form-control "
                    value={dates.end_date}
                    onChange={(e) =>
                       setState({...state,end_date:moment(e[0]).format("YYYY-MM-DD")})
                    }
                  />
                </div>
                 
                <div className=" col-12 col-md-1 mr-1 d-flex justify-content-center"> 
                  <Button
                    className="flex-fill mb-2 mt-1 mb-md-0 mt-md-0 "
                    color="primary"
                    
                    onClick={() => 
                        setDates({...state})
                    }
                  >
                    <Search size={17} />
                  </Button>
                  
                  </div>
                  
                  <Col md={2} className='  w-100  mx-md-auto px-0 mx-1 mt-1 mt-md-0' >
               
               <Select 
               placeholder='Currency'
               theme={selectThemeColors}
               className="react-select"
               classNamePrefix="select"
               value={{label:currencyOption[0], value:currencyOption[0]}}
               onChange={e=>setCurrency(e.value)}
               options={[ ...currencyOption.map((el) => ({
                 label: el,
                 value: el,
               })) ]}  
               />
               </Col>
   
                  
                </Row>
                </Col>
             
          </Row>
        </CardBody>
      </Card>
    </div>
  );
}
