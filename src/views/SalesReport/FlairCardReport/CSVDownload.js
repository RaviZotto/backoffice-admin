import React, { useContext, useEffect } from "react";
import CSVDownload from "react-json-to-csv";
import {  FlairCardReport} from ".";
const formatData = ({transactions,selectedCard,currency}) => {
  let dataTemp = [...transactions];
  const formattedData =[];
  if(currency)dataTemp =dataTemp.filter(el=>el.currency==currency);
  if(selectedCard.id)dataTemp=dataTemp.filter(el=>el.card_id == selectedCard.card_number);
  //console.log(formattedData);

  //    formattedData = Array.slice(data,0,data.length-1);
  //console.log(formattedData);
  for (let i = 0; i < dataTemp.length; ++i) {
    let newObj = {};
    // //console.log(formatObj);
    let serachF = [
      "transaction_date",
      "type",
      "transaction_id",
      "amount",
      "message",
      "currency",
    ];
    for (let j of serachF) {
      //console.log(data[i][j]);

      newObj[j] = dataTemp[i][j];
    }

    //console.log(data[i]["pay_type"], i);

    //   delete data[i]['pay_type']; delete data[i]['status']; delete data[i]['id']; delete data[i]['currency_account_id']; delete data[i]['refund_status'] ;delete data[i]['refund_type']

    newObj.paid_type = ['Cashback','Topup'].includes(dataTemp[i]["type"])  ? "Credit" : "Debit";
    formattedData.push(newObj);
  }

  return formattedData;
};

const CsvPreview = () => {
const {statementCSV,setStatementCSV,transactions,dates,currency,selectedCard, merchant}=useContext(FlairCardReport)
  useEffect(() => {
     ;
    if (statementCSV) {
      let a = document.getElementById("csvClass");
      //console.log(a);
      a.click();
      a.remove();
      setStatementCSV(false);
    }
  }, [statementCSV]);

  return (
    <div className="d-none">  
    {
       transactions.length?<CSVDownload
      data={formatData({transactions,selectedCard,currency})}
      filename={`FlairCardReport-${merchant}-${dates.start_date}-${dates.end_date}.csv`}
    >
      <button id="csvClass" />
    </CSVDownload>:null}
    </div>
  );
};
export default CsvPreview;
