import React, { createContext, useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { UPDATE_MERCHANT } from '@src/redux/actions/storeMerchant.'
import moment from 'moment';
import Loading from "@core/components/spinner/Loading-spinner";
import Header from './Header';
import { Axios, getFlairCardTransactions, getGiftsCards } from '../../../Api';
import notification from '../../../components/notification';
import PerfectScrollBar from "react-perfect-scrollbar";
import { Row } from 'reactstrap';
import DebitsCards from './CardView';
import { GET_CURRENCY } from '../../../redux/actions/transfer';
import FlairCardStats from '@src/components/ui-elements/cards/statistics/FlairCardStats';
import CSVDownload from './CSVDownload';
import Table from './Table';
import SideBar from './SideBar';
export const FlairCardReport = createContext(null);

const reportDefault ={
    amount:{
    credit:0,
    debit:0,
    total:0,
    payasgo:0,
    subscription:0,
    cashback:0,
    topup:0,
    },
    count:{
        credit:0,
        debit:0,
        total:0,
        payasgo:0,
        subscription:0,
        cashback:0,
        topup:0,
    }
}

const index = () => {
    const dispatch = useDispatch();
    const [dates, setDates] = useState(() => {
        let end_date = moment(new Date()).format('YYYY-MM-DD');
        let start_date = moment(new Date()).format('YYYY-MM-DD');
        return { start_date, end_date };
    })
    const [report,setReport]=useState(reportDefault);
    const [merchant, setMerchant] = useState(null);
    const { merchants } = useSelector(state => state.merchant);
    const { currencies } = useSelector(state => state.transfer);
    const [loading, setLoading] = useState({ l1: false, l2: false,l3:false });
    const [cards, setCards] = useState([]);
    const [currency, setCurrency] = useState(null);
    const [currencyOption,setCurrencyOption]=useState([]);
    const [transactions, setTransactions] = useState([]);
    const [cardTxn,setCardTxn]=useState([]);
    const [selectedCard, setSelectedCard] = useState({});
    const [statementCSV,setStatementCSV] = useState(false);
    const [selectedTransaction,setSelectedTransaction] = useState({});
    const handleSelect = (data) => {
        if (selectedCard.id) { setSelectedCard({}) } else { 
            setCurrency(data.currency);
            setSelectedCard(data) }
    }
    useEffect(() => {

        if (!merchants.length) {
            setLoading({ ...loading, l1: true });
            dispatch(UPDATE_MERCHANT(() => setLoading({ ...loading, l1: false })));

        }
        if (!currencies.length) {
            setLoading({ ...loading, l1: true });
            dispatch(GET_CURRENCY(() => setLoading({ ...loading, l1: false })));
        }

    }, []);

    // useEffect(() => {
    //     (async () => {
    //         try {
    //             if (merchant) {
    //                 setLoading({ ...loading, l2: true });
    //                 const { cards } = await getGiftsCards(merchant);
    //                 setCards(cards.length ? cards : []);

    //             }
    //             setLoading({ ...loading, l2: false });
    //             let merchantObj = merchants.find(e=>e.merchant_id==merchant);
    //             setTransactions(transactions.filter(el=>el.merchant_id==merchantObj.api_key))
    //         } catch (e) {
    //             setLoading({ ...loading, l2: false })
    //             notification({ title: "Error", message: e.toString(), type: 'error' })
    //         }
    //     }
    //     )()
    // }, [merchant]);

    useEffect(() => {
        (async () => {
          try{
            
                setLoading({ ...loading, l3:true })
             let merchantObj = merchants.find(e=>e.merchant_id==merchant);
             const {transactions} = await getFlairCardTransactions({...dates,merchant_id:merchant?merchantObj.api_key:undefined});
             //console(transactions,'33')
              setTransactions(transactions.length?transactions:[]);    
              setLoading({...loading, l3:false}); 
            
          }catch(e){
            setLoading({...loading, l3:false});
          }

        })()
   
    }, [dates])

    useEffect(() => {
       
        if(transactions.length){
          
           let setT= new Set(),setTxn= new Set();
         
           let txnArray = [...transactions]; 
          if(merchant){
             let merchantObj = merchants.find(el=>el.merchant_id==merchant);
             txnArray = transactions.filter(el=>el.merchant_id==merchantObj.api_key || el.merchant_id==merchant);
          }
           for(let obj of txnArray){setT.add(obj.currency);setTxn.add(obj.card_id)}
           setCurrencyOption(Array.from(setT));setCardTxn(Array.from(setTxn));
           txnArray=txnArray.filter(el=>el.currency==currency||transactions[0]);
      
          //console(transactionArray,reportDefault);    
          const reportTemp={   amount:{
            credit:0,
            debit:0,
            total:0,
            payasgo:0,
            subscription:0,
            cashback:0,
            topup:0,
            },
            count:{
                credit:0,
                debit:0,
                total:0,
                payasgo:0,
                subscription:0,
                cashback:0,
                topup:0,
            }};

        //   setReport({...reportDefault});
         
          for(let obj of txnArray){
              let type = obj.type.toLowerCase();
            
              switch(type){
               case 'card':  reportTemp.count.debit+=1; reportTemp.amount.debit+=obj.amount;  break;
               case 'payasgo': reportTemp.count.debit+=1; reportTemp.amount.debit+=obj.amount;reportTemp.amount.payasgo+=obj.amount; report.count.payasgo+=1;break;
               case 'subscription': reportTemp.count.debit+=1; reportTemp.amount.debit+=obj.amount;reportTemp.amount.subscription+=obj.amount; reportTemp.count.subscription+=1;   break;
               case 'topup': reportTemp.count.debit+=1; reportTemp.amount.credit+=obj.amount;reportTemp.amount.topup+=obj.amount; reportTemp.count.topup+=1; break;
               case 'cashback': reportTemp.count.credit+=1; reportTemp.amount.credit+=obj.amount;reportTemp.amount.cashback+=obj.amount; reportTemp.count.cashback+=1; break;
               default : break;

              }
          }
        
             setReport({...reportTemp});
        }
        


    },[currency,transactions,selectedCard,merchant])

    const flairCardProvider = useMemo(() => ({
        selectedCard,
        handleSelect,
        merchants,
        merchant,
        selectedTransaction,
        setSelectedTransaction,
        setMerchant,
        dates,
        setDates,
        cards,
        statementCSV,
        setStatementCSV,
        currencies,
        setCurrency,
        currency,
        setCards,
        transactions,
        setTransactions,
        setCurrencyOption,
        currencyOption
    }), [merchant,currencyOption,merchants,  statementCSV,selectedTransaction, transactions, currency, selectedCard, cards, merchants])

    if (loading.l1 || loading.l3) return <Loading />

    return (
        <FlairCardReport.Provider value={flairCardProvider}>
            <Header />
            <CSVDownload  />
        {   transactions.length? <FlairCardStats cols={{ xl: '3', sm: '6' }} reports={report} 
           currency={currency||transactions[0].currency}
           setStatementCSV={(el)=>{}}
           />:null }
           
         {loading.l3?
          <Loading/>:
          
         transactions.length&&merchants.length&&<Table />
           
        }
        <SideBar selectedTransaction={selectedTransaction} setSelectedTransaction={setSelectedTransaction}/>
        </FlairCardReport.Provider>
    )
}

export default index
