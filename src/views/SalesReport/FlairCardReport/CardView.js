
import React, { useContext } from "react";
import {CheckCircle, Edit, MoreVertical} from 'react-feather'
import {Card,CardBody, CardHeader, DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";


import logo2 from "@src/assets/images/logo/master-card2.png";
import { FlairCardReport, GiftCard } from ".";


const DebitsCards = ({
  data,
  
  
}) => {

  const {handleSelect,selectedCard } = useContext(FlairCardReport);
  return (
    <div className='mr-1'
    onClick={()=>handleSelect(data)}
    >
     
      <Card
    
        className={ selectedCard.id==data.id? "card-congratulations text-bg-white":"text-bg-black" }
        style={{ width: "450px", cursor: "pointer" }}
      >
       
        <CardBody>
          <div className="d-flex flex-column">
          <div>Balance</div>
            <div className="d-flex justify-content-between">
          
            <h1 >   {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency:(data.currency?data.currency:'EUR'),
          })
            .format(data.balance)
            .replace(/^(\D+)/, "$1 ")}</h1>

            </div>
       
            <div className="d-flex">
              <div className="d-flex flex-column">
                <div className="font-small-2">VALID THRU</div>
                <h5 >{data.expiry_date}</h5>
              </div>
              <div className="d-flex flex-column mx-3">
                <div className="font-small-2">CARD HOLDER</div>
                <h5 >{data.customer_name}</h5>
              </div>
              <div className="d-flex flex-column">
                <div className="font-small-2">CARD STATUS</div>
                <h5 >
                  <CheckCircle size={15} style={{ marginRight: "0.5rem" }} />
                  {data.status?"Active":"InActive"}
                </h5>
              </div>
            </div>
          </div>
        </CardBody>
      </Card>
    </div>
  );
};
export default DebitsCards;
