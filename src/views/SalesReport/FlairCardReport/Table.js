import React,{Fragment,useContext} from 'react'
import moment from 'moment';
import {Badge, Button, Card, CardBody, CardHeader, Spinner} from 'reactstrap';
import DataTable from "react-data-table-component";
import {ChevronDown, Eye, Edit,Trash, Download} from 'react-feather';
import {FlairCardReport} from '.';
const Table = () => {
 const {selectedCard,transactions,currency,statementCSV,setStatementCSV,merchant,merchants, setSelectedTransaction}=useContext(FlairCardReport);
    const columns =[
   
        {

           name:'Name',
           select:'merchant_id',
           sortable:true,
           minWidth:"110px",
            cell:(row)=>{
                let merchantObj = merchants.find(el=>el.api_key == row.merchant_id);
                 
                 return(
                      <p>{merchantObj.comp_name||merchantObj.name}</p>
                 );
            }
          },
            {
            name: "Date",
            selector: "transaction_date",
            sortable: true,
            minWidth: "110px",
            cell: (row) => {
              let date = moment(row.transaction_date);
              return (
                <Fragment>
                  <div className="mt-1">
                    <h6 className="font-small-3">{date.format("YYYY-MM-DD")}</h6>
                    <h6 className="text-secondary font-small-2">
                      {date.format("HH:mm:ss")}
                    </h6>
                  </div>
                </Fragment>
              );
            },
          },
       
          {
            name: "Transaction ID",
            selector: "txn_no",
            sortable: false,
            minWidth: "200px",
            cell: (row) => {
              return <div className="font-small-3">{row.transaction_id}</div>;
            },
          },
    
          {
            name: "Type",
            selector: "type",
            sortable: false,
            minWidth: "150px",
            cell: (row) => <Badge color="primary">{row.type}</Badge>,
          },
          {
            name: "Amount",
            selector: "amount",
            sortable: false,
            cell: (row) => (
              <h6
                className={`${
                  ['Topup','Cashback'].includes(row.type) ? "text-success" : "text-danger"
                } font-weight-bolderer`}
              >
                {new Intl.NumberFormat("en-US", {
                  style: "currency",
                  currency: row.currency,
                })
                  .format(row.amount)
                  .replace(/^(\D+)/, "$1 ")}
              </h6>)
          },
          {
            name: "Action",
            selector: "Action",
            sortable: false,
            minWidth: "150px",
            cell: (row) => (
              <Fragment>
             <Eye size={17} className='mr-1 cursor-pointer' onClick={el=>setSelectedTransaction(row)} />
              
                
              </Fragment>
            )
          },
            
        
        ];
    
    const dataToRender = () => {
        let tempData= transactions || [];
        console.log(transactions,currency,selectedCard);
        if(merchant){
          let merchantObj = merchants.find(el=>el.merchant_id==merchant);
          tempData = transactions.filter(el=>el.merchant_id==merchantObj.api_key || el.merchant_id==merchant);
       }
       if(currency)tempData=tempData.filter(el=>el.currency_id==currency||tempData[0].currency);
       
       return tempData;
    }
    
    
   
    
        return (
            <Card>
             <CardHeader className="justify-content-end">
             <Button.Ripple color="primary" tooltip={"Excel"}
             disabled={statementCSV || !transactions.length}
             onClick={() =>setStatementCSV(true)}
             >
           { !statementCSV? <Download size={17} />:<Spinner size="sm"/>}
             
             </Button.Ripple>
             </CardHeader> 
             <CardBody className="invoice-list-dataTable">
              <DataTable
                className="react-dataTable"
                noHeader
                pagination
                columns={columns}
                responsive={true}
                sortIcon={<ChevronDown />}
                data={dataToRender()}
              />
              </CardBody>
            </Card>
            
            
        )
    }
    



  


export default Table
