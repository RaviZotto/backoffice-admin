// ** Third Party Components
import React from 'react'
import { useContext, useState } from "react";
import { Table, Button, Col } from "reactstrap";
import Sidebar from "@components/sidebar";
import moment from "moment";
// import "@styles/base/pages/app-invoice.scss";

const TransactionSidebar = ({setSelectedTransaction,selectedTransaction}) => {

  
  const closeSidebar = () => setSelectedTransaction({});

  console.log(selectedTransaction,'343');
  return (
    <Sidebar
      size="lg"
      open={selectedTransaction.id ? true : false}
      title="Transaction Details"
      headerClassName=""
      contentClassName="p-0"
      toggleSidebar={closeSidebar}
    >
      <Table style={{ tableLayout: "fixed" }}  className='mb-0' borderless={true} striped={true}>
        {selectedTransaction.id ? (
          <tbody>
                <tr>
              <td>Transaction Date</td>
              <td>
                {moment(selectedTransaction.transaction_date).format("YYYY-MM-DD")}
              </td>
            </tr>
            <tr>
              <td>Client Id</td>
              <td>
                {selectedTransaction.client_id}
              </td>
            </tr>
            <tr>
              <td>Merchant Id</td>
              <td>
                {selectedTransaction.merchant_id}
              </td>
            </tr>
            <tr>
              <td>Card Id</td>
              <td>
                {selectedTransaction.card_id}
              </td>
            </tr>
            <tr>
              <td>Device Id</td>
              <td>
                {selectedTransaction.device_id}
              </td>
            </tr>
            <tr>
              <td>Type</td>
              <td>
                {selectedTransaction.type}
              </td>
            </tr>
            <tr>
              <td>Transaction Id</td>
              <td>
                {selectedTransaction.transaction_id}
              </td>
            </tr>
            <tr>
              <td>Amount</td>
              <td>
                {selectedTransaction.amount} 
              </td>
            </tr>
            <tr>
              <td>Currency</td>
              <td>
                {selectedTransaction.currency} 
              </td>
            </tr>
            <tr>
              <td>Cashback</td>
              <td>
                {selectedTransaction.cashback_amount} 
              </td>
            </tr>
            </tbody>):null
        }
      </Table>
      
    </Sidebar>
  );
};

export default TransactionSidebar;