import { useLazyQuery, useQuery } from "@apollo/client";
import moment from "moment";
import {Button} from 'reactstrap';
import {ArrowLeft, ArrowRight} from 'react-feather';
import SalesReport from '@src/components/analytics/DeviceReport';
import React, { createContext, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GET_TRANSACTIONS } from "../../../graphql/queries";
import { UPDATE_DEVICES, UPDATE_MERCHANT } from "../../../redux/actions/storeMerchant.";
import CsvPreview from "./CsvPreview";
import PdfDownload from "./PdfDownload";
import Table from "./Table";
import Header from "./Table/Header";
import Loading from "@core/components/spinner/Loading-spinner";
import notification from "../../../components/notification";
import { getAdminTransactions } from "../../../Api";
export const DeviceContext = createContext();


export const  dataToRender = ({transactions,merchants,merchantId}) => {
    let tempData=transactions;

  
    let tempObj=[];
  if(tempData){
   
    tempData.forEach(el => {
      
        let exist = tempObj.find(e=>e.merchant_id==el.merchant_id);
        // //console.log(exist);
        if(exist)exist[el.currency]=  (exist[el.currency])?Number(exist[el.currency])+Number(el.amount):Number(el.amount);
        else{ 
          let merchantObj=merchants.find(res=>res.merchant_id==el.merchant_id);
          tempObj.push({merchant_id:el.merchant_id, comp_name:merchantObj&&merchantObj.comp_name, [el.currency]: el.amount});      
    
      }
      });
    if(merchantId)tempObj=tempObj.filter(el=>el.merchant_id==merchantId);
   
    //console.log(tempObj);
    
  }     
 
  return tempObj;
}



export default function index() {
  const dispatch = useDispatch();
  const { merchants,devices } = useSelector((state) => state.merchant);

  
    const [dates, setDates] = useState({
      start_date: moment(new Date()).format("YYYY-MM-DD"),
      end_date: moment(new Date()).format("YYYY-MM-DD"),
    });
  const [showView,setShowView]=useState({});

  const [merchantId, setMerchantId] = useState(null);
  const [transactions, setTransactions] = useState([]);
  const [formattedTxn,setFormattedTxn] = useState([])
  const [statement, setStatement] = useState({});
  const [statementCSV, setStatementCSV] = useState(false);
  const[Devices,setDevices]=useState([]);
  const[deviceGateway,setDeviceGateway] = useState(null);
  const[report,setReport]=useState([]);
  const [selectedDevice,setSelectedDevice]=useState({});
 const [loading,setLoading]=useState(true);
  useEffect(() => {
  
    (async () => {
      try {
        setLoading(true);
        if (!merchants.length) dispatch(UPDATE_MERCHANT());
        if(!devices.length)dispatch(UPDATE_DEVICES());
        let transaction = await getAdminTransactions({...dates,merchant_id:merchantId?merchantId:undefined,payment_mode:2});
   
        if (transaction.length) {setTransactions(transaction);}
        setLoading(false)
      } catch (e) {
        setLoading(false);
        notification({ title: "Error", message: e, type: "error" });
      }
    })();
  }, [dates]);


 
  useEffect(()=>{ 

  
    if(Object.keys(showView).length){ 
      let deviceArr=[...devices];
      deviceArr=deviceArr.filter(e=>e.merchant_id==showView.merchant_id);

      console.log(deviceArr)
      if(deviceGateway)deviceArr=deviceArr.filter(e=>String(e.payment_gateway).toLowerCase()==deviceGateway);
      setDevices(deviceArr||[]);
       

      let txnArray=[...transactions];
      txnArray = txnArray.filter(e=>e.merchant_id==showView.merchant_id);
    
      if(selectedDevice)txnArray= txnArray.filter(e=>e.device_seriel==selectedDevice.serial);
      console.log(txnArray); 
      let objArr={ 
        success:0,
        pending:0,
        failure:0,
        credit:0,
        debit:0,
        refund:0,
        internal_transfer:0,
        online:0,
        device:0,
        payment_link:0,
        sepa_transfer:0,
        swift_transfer:0,
        express_transfer:0,
        fx_internal_transfer:0,
        fx_swift_transfer:0,
        debit_card_spend:0,
        flair_card_topup:0,
        total:0
      }
   
    

     for(let obj of txnArray){
      objArr.total+=1;
      obj.paid_type=='cr'?objArr.credit+=obj.amount:objArr.debit+=obj.amount;
      obj.status==0?objArr.failure+=1:(obj.status==1?objArr.success+=1:objArr.pending+=1)
        objArr[obj['txn_type']]+=obj.amount;
     }
     setReport(objArr)
    }
  
  },[selectedDevice, showView,deviceGateway])
  const DeviceProvider = useMemo(
    () => ({
      formattedTxn,
     setFormattedTxn,
      transactions,
      loading,
     setLoading,
      setTransactions,
      dates,
      setDates,
      merchantId,
      setMerchantId,
      statementCSV,
      setStatementCSV,
      merchants,
      statement,
      showView,
      selectedDevice,
      setSelectedDevice,
      setDevices,
      Devices,
      report,
      setShowView,
      setStatement,
      deviceGateway,
      setDeviceGateway
    }),
    [report,deviceGateway,setReport, merchants, Devices,selectedDevice, showView, transactions,formattedTxn, dates,loading, merchantId,statementCSV,statement]
  );

  return (
    <DeviceContext.Provider value={DeviceProvider}>
      <Header />
      { loading?<Loading/>:
      <>
      { !showView.merchant_id? <Table />:
      <>
      <Button.Ripple color="primary"  onClick={e=>{setShowView({});setSelectedDevice({}) }} >
      <ArrowLeft size={17} />
      <span>Back</span>
      </Button.Ripple> 
      <SalesReport  />
      </> 
    }
    </>  }
      {statement.loading&&<PdfDownload />}
      {statementCSV&&<CsvPreview/>}
    </DeviceContext.Provider>
  );
}

