import flatpickr from "flatpickr";
import React, { useContext, useEffect, useState } from "react";
import { selectThemeColors } from "@utils";
import Flatpickr from "react-flatpickr";
import { DownloadCloud, FilePlus, FileText, Search } from "react-feather";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import { useSelector } from "react-redux";
import Select from "react-select";
import { CardBody, Card, CardText, Button,Input } from "reactstrap";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import {DeviceContext} from '..';
import moment from "moment";
import Spinner from "reactstrap/lib/Spinner";

export default function (){
const {partners,merchants}=useSelector(state=>state.merchant);
const {setMerchantId,dates,setDates,setDeviceGateway,merchantId,showView,setStatementCSV,statement, setStatement,gateway}=useContext(DeviceContext); 
const [state,setState]=useState({...dates});
return (
    <div>
      <Card>
        <CardBody>
          <Row>
          { !showView.merchant_id?
             <>
            <Col md={4} sm={12} className="mb-sm-1 ">
                <label>Merchant</label>
              <Select
                placeholder='select merchant'
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
                defaultValue={{label:'Select--',value:null}}
                options={[ {label:'Select--',value:null}, ...merchants.map((el) => ({
                  label: el.comp_name,
                  value: el.merchant_id,
                })) ]}  
                
                

                onChange={el=>{setMerchantId(el.value)}}
              />
            </Col>
         
            <Col md={8} sm={12} className="mb-sm-1  pt-sm-2">
              <Row className='mx-auto'>
                <div className="col-md-4 col-6  ">
                  <Flatpickr
                    className="form-control"
                    value={dates.start_date}
                    onChange={(e) =>
                      setState({
                        ...state,
                        start_date: moment(e[0]).format("YYYY-MM-DD"),
                      })
                    }
                  />
                </div>
                <div className="  col-md-4 col-6   ">
                  <Flatpickr
                   
                   
                    className="form-control "
                    value={dates.end_date}
                    onChange={(e) =>
                       setState({...state,end_date:moment(e[0]).format("YYYY-MM-DD")})
                    }
                  />
                </div>
                <div className="  col-12 col-md-1 mr-1  d-flex justify-content-center ">
                  <Button
                    className="flex-fill mb-2 mt-1 mb-md-0 mt-md-0 "
                    color="primary"
                    disabled={!merchantId&&(state.start_date==dates.start_date&&state.end_date==dates.end_date)}
                    onClick={() => 
                       setDates({...state})
                    }
                  >
                    <Search size={17} />
                  </Button>
                  </div>
                  <div className=" col-12 col-md-1 mr-1  d-flex justify-content-center ">
                  <Button
               
                     data-toggle="tooltip" data-placement="top" title="CSV"
                    className="flex-fill mb-2 mt-1 mb-md-0 mt-md-0  "
                    color='primary'
                    onClick={el=>setStatementCSV(true)}
                  >
                    <FilePlus size={17} />
                  </Button>
                  </div>
                  <div className='col-12 col-md-1 mr-1  d-flex justify-content-center'>
                  <Button
                   disabled={statement.loading}
                   data-toggle="tooltip" data-placement="top" title="PDF"
                    className="flex-fill mb-2 mt-1 mb-md-0 mt-md-0  "
                    color='primary'
                    onClick={el=>setStatement({action:'download',loading:true})}
                 >
                  {statement.loading?<Spinner size="sm" />:<DownloadCloud size={17}  />}
                  </Button>
                  
                </div>
                </Row>
                </Col>
                </>: 
                <>
                <Col><h3>{showView.comp_name+" "}</h3> </Col>
                <Col md={5} >
               
                   <label>Gateway</label>
                  <Input
                  id='gateway'
                  name='gateway'
                  type='select'
                  value={gateway}
                  onChange={e=>setDeviceGateway(e.target.value.toLowerCase())}
                  >
                  <option value={null}  label="Select Gateway--"/>
                   <option label="Handpoint" value="Handpoint" />
                   <option label="NovelPay" value="Novelpay" />

                  </Input>
                  </Col>
                </>

             }
          </Row>
        </CardBody>
      </Card>
    </div>
  );
}
