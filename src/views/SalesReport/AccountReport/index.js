import React, { useEffect, useState } from 'react'
import Table from './Table'
import Header from './Table/Header'
import Loading from "@core/components/spinner/Loading-spinner";
import PdfDownload from './Pdfdownload';
import CsvPreview from "./CsvPreview"
import { STORE_MERCHANTACCOUNTS } from '../../../redux/actions/transfer';
import { useDispatch, useSelector } from 'react-redux';
export const dataToRender = ({merchantId,accounts}) => {
  let tempData=[];
    // let tempObj={merchant_id:0,comp_name:"",EUR:0,DMK:0,GBP:0,BMD:0}
    //console.log(accounts);
    accounts.filter(e=>!e.railsbank_ledger_id).forEach(el => {
      
        let exist = tempData.find(e=>e.merchant_id==el.merchant_id);
        //console.log(exist);
        if(exist)exist[el.currencycode]=  (exist[el.currencycode])?Number(exist[el.currencycode])+Number(el.current_balance):Number(el.current_balance);
        else tempData.push({merchant_id:el.merchant_id, comp_name:el.comp_name, [el.currencycode]: el.current_balance});      
    });
    if(merchantId)tempData=tempData.filter(el=>el.merchant_id==merchantId);
    //console.log(tempData);     
    return tempData;

  
}



export default function index() {
    const [loading,setLoading]=useState(true);
    const {merchantsAccounts}=useSelector(state=>state.transfer);
    const dispatch = useDispatch()
    const [merchantId,setMerchantId]=useState(null);
    const [statement,setStatement]=useState({});
    const [statementCSV,setStatementCSV]=useState(false);
     useEffect(() => {
      if(!merchantsAccounts.length)dispatch(STORE_MERCHANTACCOUNTS(()=>setLoading(false))) 
      setLoading(false);
     })
   if(loading)return <Loading/>
    return (
        <div>
          <Header setMerchantId={setMerchantId}  accounts={merchantsAccounts} setStatement={setStatement}  statement={statement} setStatementCSV={setStatementCSV} /> 
          <Table merchantId={merchantId} />  
          {statement.loading&& <PdfDownload accounts={merchantsAccounts} setStatement={setStatement}  statement={statement}  merchantId={merchantId}/>}
         {statementCSV&&<CsvPreview accounts={merchantsAccounts} setStatementCSV={setStatementCSV}  statementCSV={statementCSV} merchantId={merchantId}/>}
        </div>
    )
}
