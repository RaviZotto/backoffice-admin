import flatpickr from "flatpickr";
import React, { useContext, useEffect, useState } from "react";
import { selectThemeColors } from "@utils";
import Flatpickr from "react-flatpickr";
import { Download, DownloadCloud, File, FilePlus, FileText, Search } from "react-feather";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import { useSelector } from "react-redux";
import Select from "react-select";
import { CardBody, Card, CardText, Button } from "reactstrap";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import Spinner from "reactstrap/lib/Spinner";


export default function ({setMerchantId,setStatementCSV,statement,statementCSV,setStatement}){
const {merchants}=useSelector(state=>state.merchant);
 

  return (
    <div>
      <Card>
        <CardBody>
          <Row>
            <Col md={6} sm={12} className="mb-sm-1">
                
              <Select
                placeholder='select merchant'
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
                options={merchants.map((el) => ({
                  label: el.comp_name,
                  value: el.merchant_id,
                }))}
                onChange={el=>{setMerchantId(el.value)}}
              />
            </Col>
            <Col md={6} sm={12} className="mb-sm-1"  > 
            <Row>
            <div className=" col-12 col-md-5 mr-1 ">
                  <Button
               
                     data-toggle="tooltip" data-placement="top" title="CSV"
                    className="w-100 mb-2 mt-1 mb-md-0 mt-md-0  "
                    color='primary'
                    onClick={el=>setStatementCSV(true)}
                  >
                      {statementCSV?<Spinner size="sm" className="mr-1"/>:<FilePlus size={17} className='mr-1' />}
                    Csv
                  </Button>
                
                  </div>
                  <div className='col-12 col-md-5 mr-1'>
                  <Button
                   disabled={statement.loading}
                   data-toggle="tooltip" data-placement="top" title="PDF"
                    className="w-100 mb-2 mt-1 mb-md-0 mt-md-0  "
                    color='primary'
                    onClick={el=>setStatement({action:'download',loading:true})}
                 >
                  {statement.loading?<Spinner size="sm" className="mr-1"/>:<DownloadCloud size={17} className='mr-1' />}
                    Pdf
                  </Button>
                  
                </div>

            </Row>
            </Col>
             
          </Row>

        </CardBody>
      </Card>
    </div>
  );
}
