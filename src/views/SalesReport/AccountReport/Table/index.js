import React, { useContext, useEffect, useState } from 'react'
import Loading from "@core/components/spinner/Loading-spinner";
import { ChevronDown, Eye, Edit,Trash } from "react-feather";
import DataTable from "react-data-table-component";
import moment from 'moment';
import { Fragment } from 'react';
import { GET_MERCHANT, GET_PARTNER, UPDATE_MERCHANT} from "../../../../redux/actions/storeMerchant.";
import Badge from 'reactstrap/lib/Badge';
import { useDispatch, useSelector } from 'react-redux';
import notification from '../../../../components/notification';
import { STORE_MERCHANTACCOUNTS } from '../../../../redux/actions/transfer';

export default function index({merchantId}) {  
const dispatch = useDispatch()
const {merchantsAccounts}=useSelector(state=>state.transfer);
const {merchants}=useSelector(state=>state.merchant);
const [loading,setLoading]=useState(true);




useEffect(() => {
 if(loading){   
 if(!merchantsAccounts.length)dispatch(STORE_MERCHANTACCOUNTS())
 if(!merchants.length)dispatch(UPDATE_MERCHANT());
 setLoading(false);
 }
})





const columns =[
   
    {
        name: "Merchant",
        selector: "comp_name",
        sortable: true,
        minWidth: "110px",
        cell: (row) => {
          return (
            <Fragment>
              <div className="mt-1">
                <h6 className="font-small-3">{row.comp_name}</h6>
           </div>
            </Fragment>
          );
        },
      },
    
     
      {
        name: "EUR",
        selector: "EUR",
        sortable: false,
        cell: (row) => (
          <h6
           
          >
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: 'EUR',
            })
              .format(!row.EUR?0:row.EUR)
              .replace(/^(\D+)/, "$1 ")}
          </h6>
        ),
      },
      {
        name: "USD",
        selector: "USD",
        sortable: false,
        cell: (row) => (
          <h6
           
          >
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: 'USD',
            })
              .format(!row.USD?0:row.USD)
              .replace(/^(\D+)/, "$1 ")}
          </h6>
        ),
      },
      {
        name: "BDT",
        selector: "BDT",
        sortable: false,
        cell: (row) => (
          <h6
           
          >
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: 'BDT',
            })
              .format(!row.BDT?0:row.BDT)
              .replace(/^(\D+)/, "$1 ")}
          </h6>
        ),
      },
      {
        name: "GBP",
        selector: "GBP",
        sortable: false,
        cell: (row) => (
          <h6
           
          >
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: 'GBP',
            })
              .format(!row.GBP?0:row.GBP)
              .replace(/^(\D+)/, "$1 ")}
          </h6>
        ),
      },
      {
        name: "DMK",
        selector: "DMK",
        sortable: false,
        cell: (row) => (
          <h6
           
          >
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: 'DMK',
            })
              .format(!row.DMK?0:row.DMK)
              .replace(/^(\D+)/, "$1 ")}
          </h6>
        ),
      },
          {
        name: "BMD",
        selector: "BMD",
        sortable: false,
        cell: (row) => (
          <h6
           
          >
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: 'BMD',
            })
              .format(!row.BMD?0:row.BMD)
              .replace(/^(\D+)/, "$1 ")}
          </h6>
        ),
      },
      {
        name: "DKK",
        selector: "DKK",
        sortable: false,
        cell: (row) => (
          <h6
           
          >
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: 'DKK',
            })
              .format(!row.DKK?0:row.DKK)
              .replace(/^(\D+)/, "$1 ")}
          </h6>
        ),
      },
    
    ];

const dataToRender = () => {
    let tempData=[];
      // let tempObj={merchant_id:0,comp_name:"",EUR:0,DMK:0,GBP:0,BMD:0}
      //console.log(merchantsAccounts);
      merchantsAccounts.filter(el=>!el.gateway).forEach(el => {
        
          let exist = tempData.find(e=>e.merchant_id==el.merchant_id);
          //console.log(exist);
          if(exist)exist[el.currencycode]=  (exist[el.currencycode])?Number(exist[el.currencycode])+Number(el.current_balance):Number(el.current_balance);
          else tempData.push({merchant_id:el.merchant_id, comp_name:el.comp_name, [el.currencycode]: Number(el.current_balance)});      
      });
      if(merchantId)tempData=tempData.filter(el=>el.merchant_id==merchantId);
      //console.log(tempData);     
      return tempData;

    
  }


if(loading)return <Loading />

    return (
        <div>
         <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
        </div>
    )
}
