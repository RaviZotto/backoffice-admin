import React, { useContext, useEffect, useState } from "react";
import CSVDownload from "react-json-to-csv";
import { useSelector } from "react-redux";
import { dataToRender, MerchantContext } from ".";
const formatData = (data) => {
  let formattedData = [];
  //console.log(formattedData);

  //    formattedData = Array.slice(data,0,data.length-1);
  //console.log(formattedData);
  for (let i = 0; i < data.length; ++i) {
    let newObj = {};
    // //console.log(formatObj);
    let serachF = [
      "date_of_transaction",
      "pay_method",
      "order_id",
      "amount",
      "txn_no",
      "message",
      "currency",
    ];
    for (let j of serachF) {
      //console.log(data[i][j]);

      newObj[j] = data[i][j];
    }

    //console.log(data[i]["pay_type"], i);

    //   delete data[i]['pay_type']; delete data[i]['status']; delete data[i]['id']; delete data[i]['currency_account_id']; delete data[i]['refund_status'] ;delete data[i]['refund_type']

    newObj.paid_type = data[i]["paid_type"] == "cr" ? "Credit" : "Debit";
    formattedData.push(newObj);
  }

  return formattedData;
};

const CsvPreview = () => {
const {statementCSV,setStatementCSV,transactions,dates,merchantId}=useContext(MerchantContext)
const {merchants} = useSelector(state=>state.merchant);
  useEffect(() => {
     ;
    if (statementCSV) {
      let a = document.getElementById("csvClass");
      //console.log(a);
      a.click();
      a.remove();
      setStatementCSV(false);
    }
  }, [statementCSV]);

  return (
    <CSVDownload
      data={dataToRender({transactions,merchantId,merchants})}
      filename={`Total sales Report-${dates.start_date}-${dates.end_date}.csv`}
    >
      <button id="csvClass" />
    </CSVDownload>
  );
};
export default CsvPreview;
