import { useLazyQuery, useQuery } from "@apollo/client";
import moment from "moment";
import {Button} from 'reactstrap';
import {ArrowLeft} from 'react-feather';
import React, { createContext, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GET_TRANSACTIONS } from "../../../graphql/queries";
import { UPDATE_MERCHANT } from "../../../redux/actions/storeMerchant.";
import CsvPreview from "./CsvPreview";
import PdfDownload from "./PdfDownload";
import Table from "./Table";
import Header from "./Table/Header";
import SalesReport from "../../../components/analytics/SalesReport";
import Loading from "@core/components/spinner/Loading-spinner";
import { GET_CURRENCY } from "../../../redux/actions/transfer";
import notification from "../../../components/notification";
import { getAdminTransactions } from "../../../Api";
export const MerchantContext = createContext();


export const  dataToRender = ({transactions,merchants,merchantId}) => {
    let tempData=transactions;

    // //console.log(transactions);
  
    let tempObj=[];
  if(tempData){
    
    tempData.forEach(el => {
      //console.log(el);
        let exist = tempObj.find(e=>e.merchant_id==el.merchant_id);
         
        if(exist)exist[el.currency]=  (exist[el.currency])?Number(exist[el.currency])+Number(el.amount):Number(el.amount);
        else{ 
          let merchantObj=merchants.find(res=>res.merchant_id==el.merchant_id);
          tempObj.push({merchant_id:el.merchant_id, comp_name:merchantObj&&merchantObj.comp_name, [el.currency]: el.amount});      
    
      }
      });
    if(merchantId)tempObj=tempObj.filter(el=>el.merchant_id==merchantId);
   
    //console.log(tempObj);
    
  }     
 
  return tempObj;
}



export default function index() {
  const dispatch = useDispatch();
  const { merchants } = useSelector((state) => state.merchant);
  const { currencies } = useSelector((state) => state.transfer);

  const [dates, setDates] = useState({
    start_date: moment(new Date()).format("YYYY-MM-DD"),
    end_date: moment(new Date()).format("YYYY-MM-DD"),
  });



  

const [merchantId, setMerchantId] = useState(null);
  const [transactions, setTransactions] = useState([]);
  const [formattedTxn,setFormattedTxn] = useState([])
  const [statement, setStatement] = useState({});
  const [showView,setShowView] = useState({});
  const [statementCSV, setStatementCSV] = useState(false);
  const [currency,setCurrency] = useState(null);
  const[report,setReport] = useState({});
  const [currencyOption,setCurrencyOption] = useState([]);
  const [loading,setLoading]=useState(true);

  useEffect(() => {
    (async () => {
      try {
        setLoading(true);
        let transaction = await getAdminTransactions({...dates,merchant_id:merchantId?merchantId:undefined});
        //console.log(transaction);
        if (transaction.length) {setTransactions(transaction);setLoading(false);}
      } catch (e) {
        setLoading(false);
        notification({ title: "Error", message: e, type: "error" });
      }
    })();
  }, [dates]);


  useEffect(()=>{ 

    if (!merchants.length) dispatch(UPDATE_MERCHANT());
    if(!currencies.length) dispatch(GET_CURRENCY());
    if(Object.keys(showView).length){ 
      let txnArray=[...transactions];
      let objArr={ 
        success:0,
        pending:0,
        failure:0,
        credit:0,
        debit:0,
        refund:0,
        internal:0,
        sepa:0,
        swift:0,
        incoming:0,
        express:0,
        online:0,
        device:0,
        payment_link:0,
        debit_card_spend:0,
        flair_card_topup:0,
        total:0,
        bank:0
      }
     txnArray = txnArray.filter(e=>e.merchant_id==showView.merchant_id);
     let setT = new Set();
     for(let obj of txnArray){setT.add(obj.currency);}
     setCurrencyOption(Array.from(setT));
     txnArray = txnArray.filter(e=>e.currency==(currency||txnArray[0].currency));
     console.log(txnArray);
     for(let obj of txnArray){
      objArr.total+=1;
      obj.paid_type=='cr'?objArr.credit+=obj.amount:objArr.debit+=obj.amount;
      obj.status==0?objArr.failure+=1:(obj.status==1?objArr.success+=1:objArr.pending+=1)
       if(obj.payment_mode=="1") {//console.log(obj); 
          objArr.online+=obj.amount;}
        else if(obj.payment_mode=="2"){objArr.device+=obj.amount;}
        else if(obj.payment_mode=="3"){objArr.payment_link+=obj.amount;}
        else if(obj.payment_mode=="4"){objArr.bank+=obj.amount;
              if(obj.transfer_type==1)objArr.sepa+=obj.amount;
              else if(obj.transfer_type==2)objArr.swift+=obj.amount;
              else if (obj.transfer_type==3)objArr.internal+=obj.amount;
              else if(obj.transfer_type==4)objArr.express+=obj.amount;
              else if(obj.transfer_type==5 || !obj.transfer_type)objArr.incoming+=obj.amount;
              
        }  
     }
     setReport(objArr)
    }else {
      setReport({})
    }
  
  },[transactions, showView,currency])


  const MerchantProvider = useMemo(
    () => ({
      formattedTxn,
      setFormattedTxn,
      transactions,
      loading,
      setLoading,
      setTransactions,
      dates,
      setDates,
      merchantId,
      setMerchantId,
      statementCSV,
      setStatementCSV,
      statement,
      setStatement,
      showView,
      setShowView    ,
      currency,
      setCurrency,
      report,
      setReport,
      setCurrencyOption,
      currencyOption  
    }),
    [ currency,report,currencyOption,  showView, transactions,formattedTxn, dates,loading,  merchantId,statementCSV,statement]
  );
  return (
    <MerchantContext.Provider value={MerchantProvider}>
      <Header />
      { loading?<Loading/>:
      <>
      { !showView.merchant_id? <Table />:
      <>
      <Button.Ripple color="primary"  onClick={e=>setShowView({})} >
      <ArrowLeft size={17} />
      <span>Back</span>
      </Button.Ripple> 
      <SalesReport  />
      </> 
    }
    </>  }
      {statement.loading&&<PdfDownload />}
      {statementCSV&&<CsvPreview/>}
    </MerchantContext.Provider>
  );
}

