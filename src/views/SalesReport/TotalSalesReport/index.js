import { useQuery } from "@apollo/client";
import moment from "moment";
import React, { createContext, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GET_TRANSACTIONS } from "../../../graphql/queries";
import { UPDATE_MERCHANT } from "../../../redux/actions/storeMerchant.";
import CsvPreview from "./CsvPreview";
import PdfDownload from "./PdfDownload";
import Header from "./Table/Header";
import SalesReport from "../../../components/analytics/TotalSalesReport";
import Loading from "@core/components/spinner/Loading-spinner";
import { GET_CURRENCY } from "../../../redux/actions/transfer";
import notification from "../../../components/notification";
import { getAdminTransactions } from "../../../Api";
export const TotalSalesContext = createContext();


export const  dataToRender = ({transactions,merchants,merchantId}) => {
  let tempData=transactions;
  let tempObj=[];
  if(tempData){
    
    tempData.forEach(el => {
      //console.log(el);
        let exist = tempObj.find(e=>e.merchant_id==el.merchant_id);
         
        if(exist)exist[el.currency]=  (exist[el.currency])?Number(exist[el.currency])+Number(el.amount):Number(el.amount);
        else{ 
          let merchantObj=merchants.find(res=>res.merchant_id==el.merchant_id);
          tempObj.push({merchant_id:el.merchant_id, comp_name:merchantObj&&merchantObj.comp_name, [el.currency]: el.amount});      
      }
      });
    if(merchantId)tempObj=tempObj.filter(el=>el.merchant_id==merchantId);
   
 }     
 
  return tempObj;
}



export default function index() {
  const dispatch = useDispatch();
  const { merchants } = useSelector((state) => state.merchant);


  const [dates, setDates] = useState({
    start_date: moment(new Date()).format("YYYY-MM-DD"),
    end_date: moment(new Date()).format("YYYY-MM-DD"),
  });





 
  const [currencyOption,setCurrencyOption]=useState([]);  
  const [merchantId, setMerchantId] = useState({});
  const [transactions, setTransactions] = useState([]);
  const [formattedTxn,setFormattedTxn] = useState([])
  const [statement, setStatement] = useState({});
  const [showView,setShowView] = useState({});
  const [statementCSV, setStatementCSV] = useState(false);
  const [currency,setCurrency]=useState("GBP");
  const [report,setReport]=useState({})
  const [loading,setLoading]=useState(true);
  

  useEffect(() => {
    (async () => {
      try {
        setLoading(true);
        let transaction = await getAdminTransactions({...dates,merchant_id:merchantId.value?merchantId.value:undefined});
        //console.log(transaction);
        let setObj = new Set();
        if (transaction.length) {
      
          for(let obj of transaction) {
              setObj.add(obj.currency);
          }
          console.log(setObj);
          setCurrency(transaction[0].currency);
           const merchantObj= merchants.find(el=>el.merchant_id==transaction[0].merchant_id);
          setMerchantId({value:transaction[0].merchant_id,label:merchantObj?merchantObj.comp_name:"select--"});
          setCurrencyOption(Array.from(setObj));
          setTransactions(transaction);setLoading(false);}
      } catch (e) {
        setLoading(false);
        notification({ title: "Error", message: e, type: "error" });
      }
    })();
  }, [dates]);



  useEffect(()=>{
  
    if (!merchants.length) dispatch(UPDATE_MERCHANT());

     let txnArray=[];
     if(transactions.length)
     {

     txnArray=[...transactions];
     txnArray = txnArray.filter(e=>e.merchant_id == merchantId.value);
     let setT=new Set();
     for(let objT of txnArray)setT.add(objT.currency);setCurrencyOption(Array.from(setT));
      //  if(txnArray.length)setCurrency(txnArray[0].currency);
     txnArray = txnArray.filter(e=>e.currency==currency);
     }

    let objArr={ 
    success:0,
    pending:0,
    failure:0,
    credit:0,
    debit:0,
    refund:0,
    online:0,
    device:0,
    bank:0,
    payment_link:0,
    sepa:0,
    swift:0,
    internal:0,
    express:0,
    incoming:0,
    debit_card_spend:0,
    flair_card_topup:0,
    total:0

   };
 
  

  
   
  for(let obj of txnArray ){ 
      objArr.total+=1;
    obj.paid_type=='cr'?objArr.credit+=obj.amount:objArr.debit+=obj.amount;
    obj.status==0?objArr.failure+=1:(obj.status==1?objArr.success+=1:objArr.pending+=1)
      
        if(obj.payment_mode=="1") { objArr.online+=obj.amount;}
        else if(obj.payment_mode=="2"){objArr.device+=obj.amount;}
        else if(obj.payment_mode=="3"){objArr.payment_link+=obj.amount;}
        else if(obj.payment_mode=="4"){objArr.bank+=obj.amount;
              if(obj.transfer_type==1)objArr.sepa+=obj.amount;
              else if(obj.transfer_type==2)objArr.swift+=obj.amount;
              else if (obj.transfer_type==3)objArr.internal+=obj.amount;
              else if(obj.transfer_type==4)objArr.express+=obj.amount;
              else if(obj.transfer_type==5)objArr.incoming+=obj.amount;
       }                                               
         
  }
   
   
   setReport({...objArr});
  },[transactions, merchantId,currency])

  const DeviceProvider = useMemo(
    () => ({
      formattedTxn,
      setFormattedTxn,
      transactions,
      currency,
      setCurrency,
      loading,
      
     report,
     setReport,
      setTransactions,
      dates,
      setDates,
      merchantId,
      setMerchantId,
      statementCSV,
      setStatementCSV,
      statement,
      setStatement,
      showView,
      setShowView,
      setCurrencyOption,
      currencyOption,      
    }),
    [currency ,report ,currencyOption,  showView, transactions,formattedTxn, dates,loading,  merchantId,statementCSV,statement]
  );
   if(loading)return <Loading />
  return (
    <TotalSalesContext.Provider value={DeviceProvider}>
      <Header />
    
      <SalesReport  />
    
      {statement.loading&&<PdfDownload />}
      {statementCSV&&<CsvPreview/>}
    </TotalSalesContext.Provider>
  );
}

