import flatpickr from "flatpickr";
import React, { useContext, useEffect, useState } from "react";
import { selectThemeColors } from "@utils";
import Flatpickr from "react-flatpickr";
import { DownloadCloud, FileText, Search } from "react-feather";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import { useSelector } from "react-redux";
import Select from "react-select";
import { CardBody, Card, CardText, Button } from "reactstrap";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import {TransferContext} from '..';
import moment from "moment";
import Spinner from "reactstrap/lib/Spinner";



export default function (){
  // const paymentMode={
  //   1:'sepa_transfer',
  //   2:'swift_transfer',
  //   3:'internal_transfer',
  //   4:'express_transfer',
  //   5:'incoming_transfer'
  // }
const {merchants}=useSelector(state=>state.merchant);

const {setMerchantId,dates,setDates,merchantId,currency,setCurrency,showView,transfer,setTransfer,currencyOption,transferOption, setStatementCSV,statement,setStatement}=useContext(TransferContext); 
const [state,setState]=useState({...dates})
var valueObj= {};
useEffect(() => {
console.log(currencyOption,transferOption,'343');
},[currencyOption,transferOption])
return (
    
      <Card>
        <CardBody>
          <Row>
          { !showView.merchant_id?
              <>
            <Col md={5} sm={12} className="mb-sm-1 ">
                <label>Merchant</label>
              <Select
                placeholder='select merchant'
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
             
                options={[ {label:'Select--',value:null}, ...merchants.map((el) => ({
                  label: el.comp_name,
                  value: el.merchant_id,
                })) ]}  
                
                

                onChange={el=>{setMerchantId(el.value); merchants.find(e=>e.merchant_id==showView.merchant_id )}}
              />
            </Col>
            
            <Col md={7} sm={12} className="mb-sm-1  pt-sm-2">
              <Row className='mx-auto'>
                <div className="col-md-4 col-6  ">
                  <Flatpickr
                    className="form-control"
                    value={dates.start_date}
                    onChange={(e) =>
                      setState({
                        ...state,
                        start_date: moment(e[0]).format("YYYY-MM-DD"),
                      })
                    }
                  />
                </div>
                <div className="  col-md-4 col-6   ">
                  <Flatpickr
                   
                   
                    className="form-control "
                    value={dates.end_date}
                    onChange={(e) =>
                       setState({...state,end_date:moment(e[0]).format("YYYY-MM-DD")})
                    }
                  />
                </div>
                <div className=" col-12 col-md-1 mr-1 d-flex justify-content-center ">
                  <Button
                    className="flex-fill  mb-2 mt-1 mb-md-0 mt-md-0 "
                    color="primary"
                    disabled={!merchantId || (state.start_date==dates.start_date&&dates.end_date==dates.end_date)}
                    onClick={() => 
                         setDates({...state})
                    }
                  >
                    <Search size={17} />
                  </Button>
                  </div>
                  <div className=" col-12 col-md-1 mr-1 d-flex justify-content-center">
                  <Button
               
                     data-toggle="tooltip" data-placement="top" title="CSV"
                    className=" mb-2 mt-1 mb-md-0 mt-md-0 flex-fill "
                    color='primary'
                    onClick={el=>setStatementCSV(true)}
                  >
                    <FileText size={17} />
                  </Button>
                  </div>
                  <div className='col-12 col-md-1 mr-1  d-flex justify-content-center'>
                  <Button
                   disabled={statement.loading}
                   data-toggle="tooltip" data-placement="top" title="PDF"
                    className=" mb-2 mt-1 mb-md-0 mt-md-0  flex-fill "
                    color='primary'
                    onClick={el=>setStatement({action:'download',loading:true})}
                 >
                 {statement.loading?<Spinner size="sm"/>:<DownloadCloud size={17}  />}
                  </Button>
                  
                </div>
                </Row>
                </Col>
                </>
                    : <>
                      <Col md={5}>
                      <label>Search Currency </label>  
                      <Select 
                placeholder=' Currency'
                theme={selectThemeColors}
                className="react-select relative"
                classNamePrefix="Search currency"
              defaultValue={{label:currencyOption[0],value:currencyOption[0]}}
                onChange={e=>setCurrency(e.value)}
                options={[ ...currencyOption.map((el) => ({
                  label: el,
                  value: el,
                })) ]}  
                />
                      </Col>
                      {/* <Col md={4} className='mx-md-auto' > 
                    <label>Transfer Type </label>
                     <Select
                     styles={{width:'75px'}}
                placeholder='select gateway'
                theme={selectThemeColors}
                className="react-select "
                classNamePrefix="select"
               defaultValue={{label:paymentMode[transferOption[0]?transferOption[0]:5].replace('_transfer','').toUpperCase(),value:transferOption[0]}}
                //  value={{label:gateway.find(e=>e.id==Gateway).gateway_name,value:Gateway}}
                options={[ {label:'Select',value:null},...transferOption.map(el=>({label: paymentMode[!el?5:el].replace('_transfer','').toUpperCase(),value:!el?5:el}))]}
                onChange={el=>{setTransfer(el.value)}}
              />
              </Col> */}
                   </>   
                     }
          </Row>
        </CardBody>
      </Card>
    
  );
}
