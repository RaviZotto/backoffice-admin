import moment from "moment";
import {ArrowLeft} from 'react-feather';
import {Button} from 'reactstrap';
import React, { createContext, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GET_TRANSACTIONS } from "../../../graphql/queries";
import { UPDATE_MERCHANT } from "../../../redux/actions/storeMerchant.";
import CsvPreview from "./CsvPreview";
import PdfDownload from "./PdfDownload";
import Table from "./Table";
import Header from "./Table/Header";
import SalesReport from "../../../components/analytics/TransferReport";
import Loading from "@core/components/spinner/Loading-spinner";
import { GET_CURRENCY, GET_GATEWAY } from "../../../redux/actions/transfer";
import notification from "../../../components/notification";
import { getAdminTransactions } from "../../../Api";
export const TransferContext = createContext();


export const  dataToRender = ({transactions,merchants,merchantId}) => {
    let tempData=transactions;

  
  
    let tempObj=[];
  if(tempData){

    tempData.forEach(el => {
      
        let exist = tempObj.find(e=>e.merchant_id==el.merchant_id);
        // //console.log(exist);
        if(exist)exist[el.currency]=  (exist[el.currency])?Number(exist[el.currency])+Number(el.amount):Number(el.amount);
        else{ 
          let merchantObj=merchants.find(res=>res.merchant_id==el.merchant_id);
          tempObj.push({merchant_id:el.merchant_id, comp_name:merchantObj&&merchantObj.comp_name, [el.currency]: el.amount});      
    
      }
      });
    if(merchantId)tempObj=tempObj.filter(el=>el.merchant_id==merchantId);
   
    //console.log(tempObj);
    
  }     
 
  return tempObj;
}



export default function index() {
  const dispatch = useDispatch();
  const { merchants } = useSelector((state) => state.merchant);
  const { gateway ,currencies} = useSelector((state) => state.transfer);
 
  const [dates, setDates] = useState({
    start_date: moment(new Date()).format("YYYY-MM-DD"),
    end_date: moment(new Date()).format("YYYY-MM-DD"),
  });

  const [merchantId, setMerchantId] = useState(null);
  const [transactions, setTransactions] = useState([]);
  const [formattedTxn,setFormattedTxn] = useState([])
  const [statement, setStatement] = useState({});
  const [statementCSV, setStatementCSV] = useState(false);
  const[showView,setShowView] = useState({});
  const [transfer,setTransfer] = useState();
  const [currency,setCurrency] = useState();
  const [currencyOption,setCurrencyOption]=useState([]);
  const [transferOption,setTransferOption]=useState([]);
  const[report,setReport]=useState({});
  const [loading,setLoading]= useState(true);
  const paymentMode={
    1:'sepa_transfer',
    2:'swift_transfer',
    3:'internal_transfer',
    4:'express_transfer',
    5:'incoming_transfer'
  }
  useEffect(() => {
    if (!merchants.length) dispatch(UPDATE_MERCHANT());
    if(!gateway.length) dispatch(GET_GATEWAY());
    if(!currencies.length) dispatch(GET_CURRENCY());
    (async () => {
      try {
        setLoading(true);
        let transaction = await getAdminTransactions({...dates,merchant_id:merchantId?merchantId:undefined,payment_mode:4});
   
        if (transaction.length) {setTransactions(transaction);setLoading(false);}
      } catch (e) {
        setLoading(false);
        notification({ title: "Error", message: e, type: "error" });
      }
    })();
  }, [dates]);




  useEffect(()=>{ 

  
    if(Object.keys(showView).length){ 
      let txnArray=[...transactions];
      let objArr={ 
        success:0,
        pending:0,
        failure:0,
        credit:0,
        debit:0,
        refund:0,
        internal_transfer:0,
       
        payment_link:0,
        sepa_transfer:0,
        swift_transfer:0,
        express_transfer:0,
        fx_internal_transfer:0,
        fx_swift_transfer:0,
        incoming_transfer:0,
        fees:0, 
        margin_amount:0,
        total:0
      }
     let  objNum={
        sepa_transfer:0,
        swift_transfer:0,
        express_transfer:0,
        fx_internal_transfer:0,
        fx_swift_transfer:0,
        internal_transfer:0,
        incoming_transfer:0,
        credit:0,
        debit:0,
        total:0
    

      }
     txnArray = txnArray.filter(e=>e.merchant_id==showView.merchant_id);
     let setType = new Set(),setT=new Set();
      for(let obj of txnArray){setType.add(obj.transfer_type);setT.add(obj.currency);}
      setCurrencyOption(Array.from(setT));
      txnArray = txnArray.filter(e=>e.currency==(currency||txnArray[0].currency));

      // if(transfer)txnArray = txnArray.filter(e=> e['transfer_type']==transfer || (!e['transfer_type']&& transfer==0))

      for(let obj of txnArray){
   
      objArr.total+=1;
      obj.paid_type=='cr'?objArr.credit+=obj.amount:objArr.debit+=obj.amount;
      obj.status==0?objArr.failure+=1:(obj.status==1?objArr.success+=1:objArr.pending+=1)
        
      
     
        if(!obj['transfer_type'])objArr.incoming_transfer+=obj.amount;
        else objArr[paymentMode[obj['transfer_type']]]+=obj.amount;
        // console.log(paymentMode[obj['transfer_type']])
        objArr.fees=Number(obj.fees)+Number(obj.fixedfee)+Number(obj.transfer_fee);
        objArr['margin_amount']=obj.margin_amount;
        
        if(!obj['transfer_type'])objNum.incoming_transfer+=1;
        else objNum[paymentMode[obj['transfer_type']]]+=1;
        obj.paid_type=="cr"?objNum.credit+=1:objNum.debit+=1;
     }
     setReport({...objArr,objNum});
    }
  
  },[transactions, showView,currency,transfer])



  const DeviceProvider = useMemo(
    () => ({
      formattedTxn,
      setFormattedTxn,
      transactions,
      loading,
      setLoading,
      setTransactions,
      dates,
      setDates,
      merchantId,
      setMerchantId,
      statementCSV,
      setStatementCSV,
      statement,
      setStatement,
      setShowView,
      showView,
      report,
      setReport,
      currency,
      setCurrency,
      merchants,
     transfer,
     setTransfer,
     currencyOption,
     setCurrencyOption,
     
    }),
    [report,currency,transfer, currencyOption,   showView, transactions,formattedTxn, dates, merchantId,statementCSV,loading,statement]
  );

  return (
    <TransferContext.Provider value={DeviceProvider}>
      <Header />

      { loading?<Loading/>:
      <>
      { !showView.merchant_id? <Table />:
      <>
      <Button.Ripple color="primary"  onClick={e=>setShowView({})} >
      <ArrowLeft size={17} />
      <span>Back</span>
      </Button.Ripple> 
      <SalesReport  />
      </> 
    }
    </>  }
      {statement.loading&&<PdfDownload />}
      {statementCSV&&<CsvPreview/>}
    </TransferContext.Provider>
  );
}

