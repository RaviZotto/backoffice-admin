import React, { createContext, useEffect, useMemo, useState } from 'react';
import Header from './Table/Header';
import Table from './Table';
import moment from 'moment';
import { getSuspiciousTxn } from '../../../Api';
import notification from '../../../components/notification';
import Loading from "@core/components/spinner/Loading-spinner";
import { GET_PARTNER, UPDATE_MERCHANT } from '../../../redux/actions/storeMerchant.';
import { useDispatch, useSelector } from 'react-redux';
import SideBar from   './SideBarView';
export const SuspiciousTxnContext = createContext(null);
function  index() {
    const {merchants,partners}=useSelector((state) => state.merchant);
    const dispatch=useDispatch();
   let temp = new Date();
   temp = temp.setDate(temp.getDate()-6);
  const [loading,setLoading]=useState(true);
  const [refetch,setRefetch]=useState(false); 
  const [transactions,setTransactions]=useState([]);
   const [merchantId,setMerchantId]=useState(0);  
   const [dates,setDates]= useState({start_date:moment(temp).format('YYYY-MM-DD'),end_date: moment(new Date()).format("YYYY-MM-DD")});
   const [txnObj,setTxnObj]=useState({});
    const txnProvider = useMemo(()=>({transactions,setTransactions,txnObj,setTxnObj,refetch,setRefetch,dates,setDates,merchantId,setMerchantId,setLoading,loading}),[merchantId,txnObj,transactions,dates]);
    
    useEffect(()=>{
        
       if(!merchants.length )dispatch(UPDATE_MERCHANT());
       if(!partners.lenght)dispatch(GET_PARTNER()); 
       if(refetch||loading){
       getSuspiciousTxn(dates,({err,res})=>{
           
           if(res) setTransactions(res.filter(e=>!e.remove_txn)||[]);
           else notification({type:'error',message:err,title:"Error"})
           setLoading(false);
           setRefetch(false);
       })
    }
    },[dates,refetch])
    if(loading) return <Loading/>
    return (
        <SuspiciousTxnContext.Provider value={txnProvider}>
            <Header/>
            <Table/>
            <SideBar />
        </SuspiciousTxnContext.Provider>
    )
}

export default  index;
    
