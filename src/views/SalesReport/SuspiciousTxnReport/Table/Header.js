import flatpickr from "flatpickr";
import React, { useContext, useEffect, useState } from "react";
import { selectThemeColors } from "@utils";
import Flatpickr from "react-flatpickr";
import { DownloadCloud, FileText, Search } from "react-feather";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import { useSelector } from "react-redux";
import Select from "react-select";
import { CardBody, Card, CardText, Button } from "reactstrap";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import moment from "moment";
import { SuspiciousTxnContext } from "..";
import Spinner from "reactstrap/lib/Spinner";

export default function () {
  const {dates,setDates,merchantId,setMerchantId,setLoading}=useContext(SuspiciousTxnContext)
  const { merchants, partners } = useSelector((state) => state.merchant);
  const [Merchant,setMerchant]=useState([])
 const [partner, setPartner] = useState(null);
const [state,setState]=useState({...dates});
  useEffect(() => {
     partner&&partners.length&&setMerchant(merchants.filter(e=>e.partner_id==partner).map(el=>({label:el.comp_name,value:el.merchant_id,id:el.partner_id})))
      //console.log(merchants,partners)
    },[merchants,partner,partners])

  return (
    <div>
      <Card>
        <CardBody>
          <Row>
            <Col md={2} sm={12} className="mb-sm-1">
                <label>Parnter</label>
              <Select
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
                options={[{label:'Select',value:0}, ...partners.map((el) => ({
                  label: el.comp_name,
                  value: el.partner_id,
                }))]}
                onChange={el=>{setPartner(el.value);if(el.value==0)setMerchantId(0);}}
              />
            </Col>
            <Col md={3} sm={12} className="mb-sm-1">
                <label>Merchant</label>
              <Select
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
                options={Merchant}
                onChange={el=>setMerchantId(el.value)}
              />
            </Col>
            <Col md={7} sm={12} className="mb-sm-1 d-flex justify-content-end  pt-sm-2">
           
              <Row >
                <div className="col-md-2"/>
                <div className="col-md-4 col-6  ">
                  <Flatpickr
                    className="form-control"
                    value={dates.start_date}
                    onChange={(e) =>
                      setState({
                        ...dates,
                        start_date: moment(e[0]).format("YYYY-MM-DD"),
                      })
                    }
                  />
                </div>
                <div className="  col-md-4 col-6   ">
                  <Flatpickr
                    className="form-control "
                    value={dates.end_date}
                    onChange={(e) =>
                      setState({
                        ...dates,
                        end_date: moment(e[0]).format("YYYY-MM-DD"),
                      })
                    }
                  />
                </div>
                <div className="  d-flex justify-content-center col-12 col-md-1  mr-1 ">
                  <Button
                    className="flex-fill  mb-2 mt-1 mb-md-0 mt-md-0 "
                    color="primary"
                     disabled={state.start_date==dates.start_date&&state.end_date==dates.end_date}
                    onClick={() => {
                              setDates({ ...state })
                              setLoading(true)   ;
                    }
                    }
                  >
                    <Search size={17} />
                  </Button>
                </div>
                {/* <div className='col-12 col-md-1 mr-1'>
                  <Button
                    
                     data-toggle="tooltip" data-placement="top" title="CSV"
                    className="w-100 mb-2 mt-1 mb-md-0 mt-md-0  "
                    color='primary'
                    onClick={el=>setStatementCSV(true)}
                  >
                    <FileText size={17} />
                  </Button>
                  </div>
                  <div className='col-12 col-md-1 mr-1'>
                  <Button
                   disabled={statement.loading || !selectedAccount.id}
                   data-toggle="tooltip" data-placement="top" title="PDF"
                    className="w-100 mb-2 mt-1 mb-md-0 mt-md-0  "
                    color='primary'
                    onClick={el=>setStatement({action:'download',loading:true})}
                 >
                   {statement.loading?<Spinner size="sm"/>:<DownloadCloud size={17} /> }
                  </Button>
                  </div> */}
              </Row>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </div>
  );
}
