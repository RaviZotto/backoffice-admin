import React, { useContext, useEffect, useState } from 'react'
import Loading from "@core/components/spinner/Loading-spinner";
import { ChevronDown, Eye, Edit,Trash, EyeOff, MoreVertical } from "react-feather";
import DataTable from "react-data-table-component";
import moment from 'moment';
import { Fragment } from 'react';
import { GET_MERCHANT, GET_PARTNER, UPDATE_MERCHANT, UPDATE_SUSPICIOUS_TXN} from "../../../../redux/actions/storeMerchant.";
import Badge from 'reactstrap/lib/Badge';
import { useDispatch, useSelector } from 'react-redux';
import notification from '../../../../components/notification';
import { SuspiciousTxnContext } from '..';
import { CustomInput, DropdownItem, DropdownMenu, DropdownToggle, Spinner, UncontrolledDropdown } from 'reactstrap';
import { userDetails } from '../../../Merchant';



export default function index() {  
const dispatch = useDispatch()
const {partners,merchants}=useSelector(state=>state.merchant);
const [loading,setLoading]=useState(true);
const {transactions,merchantId,setRefetch,setTxnObj}=useContext(SuspiciousTxnContext);




useEffect(() => {
 if(loading){   
 if(!partners.length)dispatch(GET_PARTNER());
 if(!merchants.length)dispatch(UPDATE_MERCHANT());
 setLoading(false);
 }
})





const columns =[
   
    {
      name:'Merchant Id',
      minwidth:"110px",
      cell:(row)=>{
      
          
         return(
           <div>
             <span>{row.merchant_id}</span>
           </div>
         )
      }
    },
    {

        name: "Date",
        selector: "date_of_transaction",
        sortable: true,
        minWidth: "110px",
        cell: (row) => {
          let date = moment(row.date_of_transaction);
          return (
            <Fragment>
              <div className="mt-1">
                <h6 className="font-small-3">{date.format("YYYY-MM-DD")}</h6>
                <h6 className="text-secondary font-small-2">
                  {date.format("HH:mm:ss")}
                </h6>
              </div>
            </Fragment>
          );
        },
      },
      {
        name: "Status",
        selector: "status",
        minWidth: "110px",
        sortable: true,
        cell: (row) => (
          <Fragment>
          <Badge color='primary'>
                 {row.status}
               </Badge>
            
          </Fragment>
        ),
      },
      {
        name: "Transaction ID",
        selector: "txn_no",
        sortable: false,
        minWidth: "200px",
        cell: (row) => {
          return <div className="font-small-3">{row.txn_no}</div>;
        },
      },
   
      {
        name: "Payment Type",
        selector: "pay_method",
        sortable: false,
        minWidth: "150px",
        cell: (row) => <Badge color="primary">{row.pay_method}</Badge>,
      },
      {
        name: "Amount",
        selector: "amount",
        sortable: false,
        cell: (row) => (
          <h6
            className={`${
              row.paid_type == "cr" ? "text-success" : "text-danger"
            } font-weight-bolderer`}
          >
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: row.currency,
            })
              .format(row.transaction_amount)
              .replace(/^(\D+)/, "$1 ")}
          </h6>
        ),
      },
      {
        name: "Action",
        selector: "action",
        sortable: false,
        cell: (row) => (
          <div key={row.id}>
              <UncontrolledDropdown className='chart-dropdown'>
          <DropdownToggle color='' className='bg-transparent btn-sm border-0 p-50'>
            <MoreVertical size={18} className='cursor-pointer' />
          </DropdownToggle>
          <DropdownMenu top>
            <DropdownItem className='w-100'  disabled={row.status=='Blocked'} onClick={e=>dispatch(UPDATE_SUSPICIOUS_TXN(row.id,'Blocked ',()=>setRefetch(true)))} >Block Transaction</DropdownItem>
            <DropdownItem className='w-100'  disabled={row.status=='Forwarded'} onClick={e=>dispatch(UPDATE_SUSPICIOUS_TXN(row.id,'Forwarded',()=>setRefetch(true)))} >Forward To Risk</DropdownItem>
            <DropdownItem className='w-100'  disabled={row.status=='Investigating'} onClick={e=>dispatch(UPDATE_SUSPICIOUS_TXN(row.id,'Investigating',()=>setRefetch(true)))} >Put In Investigation</DropdownItem>
            <DropdownItem className='w-100'   onClick={e=>dispatch(UPDATE_SUSPICIOUS_TXN(row.id,'remove_txn',()=>setRefetch(true)))} >Remove Transaction</DropdownItem>
            <DropdownItem className='w-100'  onClick={e=>setTxnObj(row) } >View details</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown> 
          </div>
           
          
        ),
      }
 
     
    
    ];

const dataToRender = () => {
    let tempData=[];
   
    if(transactions){
      
    tempData =transactions;
    if(merchantId)tempData = tempData.filter(e=>e.merchant_id==merchantId);
     
     tempData.forEach(el => {
         
       let temp = JSON.parse(JSON.stringify(el));

       temp.my_txn_type = el.paid_type == "cr" ? "credit" : "debit";
       temp.my_status =
         el.status == "1"
           ? "success"
           : el.status == "0"
           ? "failed"
           : "pending";
       temp.pay_method = `${temp.pay_method}`.toLowerCase();
     })
    }
   return tempData;
}


if(loading)return <Loading />

    return (
        <div>
         <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
        
        </div>
    )
}
