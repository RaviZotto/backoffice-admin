// ** Third Party Components
import React,{ useContext } from "react";
import { Table, Button } from "reactstrap";
import Sidebar from "@components/sidebar";
import { SuspiciousTxnContext } from ".";
import moment from "moment";

const payment_Mode=(flag)=>{
     switch(flag){
       case 1: return 'Online';
       case 2: return 'Device';
       case 3: return 'Paylinks';
       case 4: return 'Bank';
       default : return ;
     }
}


const TransactionSidebar = () => {
  const {
     txnObj,
     setTxnObj
  } = useContext(SuspiciousTxnContext);

  const closeSidebar = () => setTxnObj({});

  if (!txnObj) return null;

  return (
    <Sidebar
      size="lg"
      open={txnObj.id ? true : false}
      title="Transaction Details"
      headerClassName="mb-2"
      contentClassName="p-0"
      toggleSidebar={closeSidebar}
    >
      <Table style={{ tableLayout: "fixed" }} borderless={true} striped={true}>
        {txnObj ? (
          <tbody>
            <tr>
             <td>Sender Name</td>
              <td>{txnObj.comp_name}</td>
            </tr>  
            <tr>
             <td>Beneficiary Name</td>
              <td>{txnObj.benef_name}</td>
            </tr>  
            <tr>
              <td>Date</td>
              <td>{moment(txnObj.date_of_transaction).format("YYYY-MM-DD")}</td>
            </tr>
            <tr>
              <td>Time</td>
              <td>{moment(txnObj.date_of_transaction).format("HH:mm:ss")}</td>
            </tr>
            <tr>
              <td>Status</td>
              <td>{txnObj.status }</td>
            </tr>
            {/* <tr>
              <td>Account</td>
              <td>{accounts.find((el) => el.id == transaction.currency_account_id).account_name}</td>
            </tr> */}
            <tr>
              <td>Transaction ID</td>
              <td>
                <div style={{ wordWrap: "break-word" }}>{txnObj.txn_no}</div>
              </td>
            </tr>
            <tr>
              <td>Card No</td>
              <div className='mt-1 ml-1' style={{ wordWrap: "break-word"  }}>{txnObj.card_no}</div>
            </tr>
            <tr>
              <td>Amount</td>
              <td>
                {txnObj.transaction_amount} {txnObj.currency}
              </td>
            </tr>
            <tr>
              <td>Payment Type</td>
              <td>{txnObj.paid_type == "cr" ? "Credit" : "Debit"}</td>
            </tr>
            <tr>
              <td>Payment Mode</td>
              <td>{payment_Mode(Number(txnObj.payment_mode))}</td>
            </tr>
            <tr>
              <td>Payment Method</td>
              <td>{txnObj.pay_method}</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>
                <div className="font-small-3">{txnObj.reason}</div>
              </td>
            </tr>
          
            </tbody>
            ) : null}
        </Table>
    </Sidebar>
  );
};

export default TransactionSidebar;
