import React, { useContext, useState } from "react";
import {  TxnContext } from ".";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Label,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Spinner,
} from "reactstrap";
import { Formik, Field, Form, ErrorMessage } from "formik";
import { formatMoney, currencyCodes } from "@utils";
import * as Yup from "yup";
import notification from "../../../components/notification";
import { makeRefundTxn } from "../../../Api";

const RefundModal =  () => {
  const {
    refundModal,
    setRefundModal,
    selectedTxn: transaction,
    dates,
    refetch
    
  } = useContext(TxnContext);
  const [loading, setLoading] = useState(false);

  const handleRefund = async(data) => {
    try {
      //console.log(data,transaction);
      setLoading(true)
      await makeRefundTxn({
        endtoendid: transaction.endtoendid,
        amount: data.amount,
        merchant_id:transaction.merchant_id,
      });
      notification({title:"Refund",message:"Successfull refund made",type:'success'});
     setLoading(false);
      setRefundModal(false);
      refetch()
    } catch (e) {
     setLoading(false);
       notification({title:"Error",message:e.toString(),type:'error'});
    }
  };


  return (
    <Modal isOpen={refundModal} toggle={() => setRefundModal(!refundModal)}>
      <ModalHeader toggle={() => setRefundModal(!refundModal)}>
        Refund Transaction
      </ModalHeader>
      <Formik
        initialValues={{ amount: transaction.amount }}
        validationSchema={Yup.object().shape({
          amount: Yup.number()
            .max(
              transaction.amount,
              `Amount can't be greater than ${formatMoney(
                transaction.amount,
                transaction.currency
              )}`
            )
            .required("Required"),
        })}
        onSubmit={handleRefund}
      >
        {({ errors, touched }) => (
          <Form>
            <ModalBody>
              <Label for="amount" className="form-label">
                Amount
              </Label>
              <InputGroup className="input-group-merge">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText
                    style={{
                      borderColor: errors.amount && touched.amount ? "red" : "",
                    }}
                  >
                    {currencyCodes[transaction.currency]}
                  </InputGroupText>
                </InputGroupAddon>
                <Field
                  className={`form-control ${
                    errors.amount && touched.amount && "is-invalid"
                  }`}
                  name="amount"
                  id="amount"
                  type="number"
                />
              </InputGroup>
              <ErrorMessage name="amount" />
            </ModalBody>
            <ModalFooter>
              <Button color="primary" type="submit" className="mr-1">
                {loading ? (
                  <Spinner size="sm" className="mr-1" color="white" />
                ) : null}
                Accept
              </Button>
              <Button.Ripple
                color="secondary"
                outline
                onClick={() => setRefundModal(false)}
              >
                Cancel
              </Button.Ripple>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

export default RefundModal;
