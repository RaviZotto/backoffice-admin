import React, { createContext, useEffect, useMemo, useState } from "react";
import Header from "./Table/Header";
import Table from "./Table";
import { GET_TRANSACTIONS } from "../../../graphql/queries";
import { useLazyQuery } from "@apollo/client";
import { Card, CardBody, Row } from "reactstrap";
import { STORE_MERCHANTACCOUNTS } from "../../../redux/actions/transfer";
import { useDispatch, useSelector } from "react-redux";
import PerfectScrollBar from "react-perfect-scrollbar";
import moment from "moment";
import PdfDownload from "./pdfDownload";
import CsvPreview from "./csvPreview";
import SideBar from "./Sidebar";
import RefundModal from "./RefundModal";
import { getAdminTransactions, getAllLedgerAccount } from "../../../Api";
import notification from "../../../components/notification";
export const TxnContext = createContext();

export default function index() {
  var temp = new Date();
  temp = temp.setDate(temp.getDate() - 6);
  const [refundModal, setRefundModal] = useState(false);
  const [dates, setDates] = useState({
    start_date: moment().startOf('month').format("YYYY-MM-DD"),
    end_date: moment().endOf('month').format("YYYY-MM-DD"),
  });
  const [statementCSV, setStatementCSV] = useState(false);
  const dispatch = useDispatch();
  const [selectedTxn, setSelectedTxn] = useState({});
  const [selectedAccount, setSelectedAccount] = useState({});
  const [merchantId, setMerchantId] = useState(null);
  const handleSelect = (el) => { if(el.account_id==selectedAccount.account_id){setSelectedAccount({})}
    else
    setSelectedAccount(el)};
  const { merchantsAccounts } = useSelector((state) => state.transfer);
  const [statement, setStatement] = useState({});
  const [loading, setLoading] = useState(false);
  const [refetch, setRefetch] = useState(false);
  const [transactions, setTransactions] = useState([]);
//  const [Accounts,setAccounts]=useState([]);



  useEffect(() => {
    if (!merchantsAccounts.length) dispatch(STORE_MERCHANTACCOUNTS());
  }, [merchantsAccounts]);

  useEffect(() => {
 

    if (refetch || loading  ) {
      (async () => {
        try {
          let transaction = await getAdminTransactions({ ...dates ,merchant_id:merchantId?merchantId:undefined});

          if (transaction.length) {
            setTransactions(transaction);
           
        
          }
          setLoading(false);
          setRefetch(false);
        } catch (e) {
          
          setLoading(false);setRefetch(false);
          notification({ title: "Error", message: e, type: "error" });
        }
      })();
    }
  }, [dates, refetch]);


  // useEffect(()=>{
  //   if(merchantId){
  //   (async()=>{

  //  const {Accounts} = await  getAllLedgerAccount({merchant_id:merchantId});
  //  setAccounts(Accounts||[]);
  //   })()
  // }
  // },[merchantId])

  const TxnReportProvider = useMemo(
    () => ({
     
      accounts: merchantsAccounts,
      loading,
      refetch: () => setRefetch(true),
      setTransactions,
      transactions,
      setRefetch,
      setLoading,
      transactions,
      setStatement,
      statement,
      setSelectedAccount,
      selectedAccount,
      dates,
      selectedTxn,
      setSelectedTxn,
      setDates,
      merchantId,
      setMerchantId,
      statementCSV,
      setStatementCSV,
      refundModal,
      setRefundModal,
      // Accounts,
      // setAccounts,
  
    }),
    [
      // Accounts,
      selectedTxn,
       dates,
      refundModal,
      statementCSV,
      statement,
      merchantsAccounts,
      selectedAccount,
      merchantId,
      loading,
      transactions
    ]
  );

  return (
    <TxnContext.Provider value={TxnReportProvider}>
      <Header  />
      <SideBar />
      <PerfectScrollBar>
        <Row className="p-2 flex-nowrap">
          {merchantId &&
            merchantsAccounts.length &&
            merchantsAccounts
              .filter((e) => e.merchant_id == merchantId)
              // Accounts
              .map((res) => (
                <div className="col " style={{ flexGrow: 0 }}>
                  <AccountCard
                    key={res.account_id}
                    data={res}
                    selectedAccount={selectedAccount}
                    handleSelect={(el) => handleSelect(el)}
                  />
                </div>
              ))}
        </Row>
      </PerfectScrollBar>
      {statementCSV && <CsvPreview />}
      {statement.loading && <PdfDownload />}
      <RefundModal />
      <Table loading1={loading}  />
    </TxnContext.Provider>
  );
}

const AccountCard = ({ data, handleSelect, selectedAccount }) => {
  const selected = data.account_id == selectedAccount.account_id;
  //  const selected = data.id == selectedAccount.id;
  const textStyle = { color: selected ? "white" : "black" };
  return (
    <Card
      className="cursor-pointer ml-1 mr-1"
      color={selected ? "primary" : ""}
      style={{ minWidth: "250px" }}
      onClick={() => handleSelect(data)}
    >
      <CardBody style={{ marginBottom: "-1.5rem" }}>
        <div className="d-flex justify-content-between">
          <div
            style={{ ...textStyle }}
            className="font-small-2 font-weight-bold mr-2 text-uppercase"
          >
            {data.account_name}
          </div>
          <div
            style={{ ...textStyle }}
            className="font-small-2   text-uppercase"
          >
            {data.gateway}
          </div>
        </div>
      </CardBody>
      <hr style={{ borderColor: "#d0d2d6" }} />
      <CardBody style={{ marginTop: "-1.3rem" }}>
        <div style={{ ...textStyle }} className="font-small-1">
          Balance
        </div>
        <h3 style={{ marginTop: "0.2rem", ...textStyle }}>
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: data.currencycode,
          })
            .format(data.current_balance)
            .replace(/^(\D+)/, "$1 ")}
        </h3>
      </CardBody>
    </Card>
  );
};
