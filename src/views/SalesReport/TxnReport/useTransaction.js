

const _ = StyleSheet.create({
  h1: { fontSize: "18pt", fontWeight: "bold" },
  h3:{fontSize:"16pt",fontWeight:900,fontStyle:"bold"},
  h6: { fontSize: "12pt" },
  h5: { fontSize: "12pt" },
  p: { fontSize: "11pt", color: "#000", marginVertical: "2pt" },
  p2: { fontSize: "9pt", color: "#000" },
  br1: { marginVertical: "6pt" },
  br2: { marginVertical: "3pt" },
  startPage: {
    flexDirection: "column",
    backgroundColor: "#fff",
    paddingVertical: 50,
    paddingHorizontal: 30,
    // fontFamily: "Montserrat",
  },
  statementPage: {
    paddingHorizontal: 50,
    // fontFamily: "Montserrat",
    backgroundColor: "#fff",
    paddingTop: 50,
    paddingBottom: 42,
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headerRight: {
    flexDirection: "column",
    alignItems: "flex-end",
    fontWeight:'fontWeightBold'
  },
  details: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  detailsRight: {
    alignItems: "flex-end",
  },
  address: {
    textOverflow: "wrap",
    width: "120pt",
  },
  addressRight: {
    textOverflow: "wrap",
    width: "120pt",
    textAlign: "right",
  },
  table: {
    // border: "1pt solid #DCDCDC",
   
    padding: "10pt",
    paddingLeft:'0pt',
    
  },
  tablemax:{
    borderTop:"2pt solid #000",
    borderBottom:"2pt solid #000",
     padding: "10pt",
    paddingTop:"1pt",
    paddingLeft:'0pt'
  },
  tr: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems:'center'
  },
  td: {
    width: "130pt",
    marginVertical:"1pt",
    
  },
   td1: {
    width: "130pt",
    marginVertical:"1pt",
    textAlign:'right',
     marginRight:"10pt"
  },
     td2: {
    width: 70,
    marginVertical:"1pt",
       
    textAlign:'right',
     
  },
 
  statementTable: {
    border: "1pt solid #DCDCDC",
  },
  sr: {
    flexDirection: "row",
    
  },
  sd: {
    // padding: "5pt",
    textAlign: "center",
    width: "99pt",
    fontWeight:'bold'
    // justifyContent: "center",
  },
  sh: { fontSize: "10pt" },
});

const { H1,TD1, H5,H3, H6,TD2, P, BR1, BR2, TR, TD, SR, SD, P2, SH } = {
  H1: ({ children }) => <Text style={_.h1}>{children}</Text>,
 H3: ({ children }) => <Text style={_.h3}>{children}</Text>,
  H6: ({ children }) => <Text style={_.h6}>{children}</Text>,
  TD1:({children}) =><View style={_.td1}>{children}</View> ,
  TD2:({children}) =><View style={_.td2}>{children}</View> ,
  H5: ({ children }) => <Text style={_.h5}>{children}</Text>,
  P: ({ children }) => <Text style={_.p}>{children}</Text>,
  P2: ({ children }) => <Text style={_.p2}>{children}</Text>,
  BR1: ({ children }) => <View style={_.br1}>{children}</View>,
  BR2: ({ children }) => <View style={_.br2}>{children}</View>,
  TR: ({ children }) => <View style={_.tr}>{children}</View>,
  TD: ({ children }) => <View style={_.td}>{children}</View>,
  SR: ({ children,style }) => <View style={[_.sr,style]}>{children}</View>,
  SD: ({ children,style }) => <View style={[_.sd,style]}>{children}</View>,
  SH: ({ children }) => <Text style={_.sh}>{children}</Text>,
};

 const PDF = ({ user, statement }) => {
  // const { range,account, period, transactions, totals } = statement;

  return (
    <Document>
     <Page size="A4" style={_.startPage}>
        <View style={_.header}>
          <View>
            <Image style={{ width: "105pt" }} src={ "https://paymentz.z-pay.co.uk/images/merchant/3.png" } />
          </View>

          <View style={[_.headerRight,{fontWeight:'fontWeightBold'}]}>
            <H3>STATEMENT OF ACCOUNT</H3>
            <BR1 />
           
          </View>
        </View>
        <BR1 />
        
        <View style={_.details}>
          <View style={_.table}>
            <TR>
            <TD><H6>Account Number:</H6></TD>
            <P>1223232323223</P>
            </TR>
            <BR2/>
            <TR>
            <TD><H6>Statement Date:</H6></TD>
            <P>07/12/2021</P>
              
            </TR>
            <BR2/>
             <TR>
            <TD><H6>Period Covered:</H6></TD>
                <TD>
               <P>01/11/2021 to</P>
                  <BR2/>
                  <P>30/11/2021 </P>
            </TD>
                  </TR>
           </View>
            <View style={{display:'flex',justifyContent:'center'}}>
             <P>Page 1 of 1</P>
        </View>
          </View>

         
        <BR1 />
       
        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <View>
          <H3>Zotto LTD</H3>
          <BR2/>
          <P>Zotto LTD</P>
             <BR2/>
          <P>Zotto LTD</P>
          </View>
        <View style={_.table}>
          <TR>
            <TD1>
              <P>Opening Balance</P>
            </TD1>
            <P>375,800.00</P>
          </TR> 
          <BR2 />
          <TR>
            <TD1>
              <P>Total Credit Amount</P>
            </TD1>
            <P>510,000.00</P>
          </TR>
          <BR2 />
           <TR>
            <TD1>
              <P>Total Debit Amount</P>
            </TD1>
             
            <P>510,000.00</P>
          </TR>
                 <BR2 />
          <TR>
            
            <TD1>
              <P>Closing Balance</P>
            </TD1>
            <P>
              343,34343.00
            </P>
          </TR>
        
          <BR2 />
         
        
          <TR>
            <TD1>
              <P>Account Type</P>
            </TD1>
            <P>Current Account</P>
          </TR>
          <BR2/>
           <TR>
             
            <TD1>
              <P>Number of Transaction</P>
            </TD1>
             <TD2>
            <P>2</P>
               </TD2>
          </TR>
        </View>
       </View>
        <BR2 />
        <BR2 />
       <H1>Transactions</H1> 
       <View style={_.tablemax}>
      <SR>
            <SD>
              <SH>Date</SH>
            </SD>

            <SD style={{widht:500,textAlign:'center'}}>
              <SH>Description</SH>
            </SD>
            <SD>
              <SH>Credit</SH>
            </SD>
            <SD>
              <SH>Debit</SH>
            </SD>
       
            <SD>
              <SH>Balance</SH>
            </SD>
        
            <SD>
              <SH>Fee</SH>
            </SD>
          </SR>
         <SR style={{backgroundColor:'#e6e6e6'}} >
         <SD style={{backgroundColor:'#e6e6e6',alignItems:'center',paddingVertical:3}}>
          <P>12/11/2021</P>
           </SD>
           <SD style={{textAlign:'left'}}>
            <P>hey this is you boy ravi kumar nice to meet you</P>
           </SD>
            <SD style={{textAlign:'left'}}>
            <P></P>
           </SD>
            <SD style={{textAlign:'left'}}>
            <P>5,400</P>
           </SD>
         </SR>
         
           <SR style={{backgroundColor:'white'}} >
         <SD style={{backgroundColor:'#e6e6e6',alignItems:'center',paddingVertical:3}}>
          <P>12/11/2021</P>
           </SD>
           <SD style={{textAlign:'left'}}>
            <P>hey this is you boy ravi kumar nice to meet you</P>
           </SD>
            <SD style={{textAlign:'left'}}>
            <P></P>
           </SD>
            <SD style={{textAlign:'left'}}>
            <P>5,400</P>
           </SD>
         </SR>
       </View>
      </Page>
    </Document>
  );
};


ReactPDF.render(<PDF/>)