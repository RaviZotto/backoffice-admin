import React, { Fragment, useContext, useEffect, useRef, useState } from "react";
import { Page, Text, View, Document, StyleSheet, BlobProvider, pdf, Font, Image, usePdf } from "@react-pdf/renderer";
import fontFile from "../../../assets/fonts/Montserrat-Regular.ttf";
import moment from "moment";
import { downloadDoc, formatMoney } from "@utils";
import printDoc from "print-js";
import { TxnContext } from ".";
import { useSelector } from "react-redux";
import { getBase64FromUrl, monthOptions, yearOptions } from "../../../utility/Utils";
import PDFMerger from 'pdf-merger-js/browser';
import {graphqlServerUrl} from '@src/App';
import { useGetImage } from "../../../utility/hooks/useGetImage";
Font.register({
  family: "Montserrat",
  src: fontFile,
});

// const _ = StyleSheet.create({
//   h1: { fontSize: "18pt", fontWeight: "bold" },
//   h6: { fontSize: "12pt" },
//   h5: { fontSize: "12pt" },
//   p: { fontSize: "11pt", color: "#626262", marginVertical: "5pt" },
//   p2: { fontSize: "9pt", color: "#626262" },
//   br1: { marginVertical: "10pt" },
//   br2: { marginVertical: "5pt" },
//   startPage: {
//     flexDirection: "column",
//     backgroundColor: "#fff",
//     paddingVertical: 50,
//     paddingHorizontal: 50,
//     fontFamily: "Montserrat",
//   },
//   statementPage: {
//     paddingHorizontal: 50,
//     fontFamily: "Montserrat",
//     backgroundColor: "#fff",
//     paddingTop: 50,
//     paddingBottom: 42,
//   },
//   header: {
//     flexDirection: "row",
//     alignItems: "center",
//     justifyContent: "space-between",
//   },
//   headerRight: {
//     flexDirection: "column",
//     alignItems: "flex-end",
//   },
//   details: {
//     flexDirection: "row",
//     justifyContent: "space-between",
//   },
//   detailsRight: {
//     alignItems: "flex-end",
//   },
//   address: {
//     textOverflow: "wrap",
//     width: "120pt",
//   },
//   addressRight: {
//     textOverflow: "wrap",
//     width: "120pt",
//     textAlign: "right",
//   },
//   table: {
//     border: "1pt solid #DCDCDC",
//     padding: "10pt",
//   },
//   tr: {
//     flexDirection: "row",
//     justifyContent: "flex-start",
//   },
//   td: {
//     width: "200pt",
//   },
//   statementTable: {
//     border: "1pt solid #DCDCDC",
//   },
//   sr: {
//     flexDirection: "row",
//     borderBottom: "1pt solid #dcdcdc",
//   },
//   sd: {
//     padding: "5pt",
//     textAlign: "center",
//     width: "99pt",
//     justifyContent: "center",
//   },
//   sh: { fontSize: "10pt" },
// });

// const { H1, H5, H6, P, BR1, BR2, TR, TD, SR, SD, P2, SH } = {
//   H1: ({ children }) => <Text style={_.h1}>{children}</Text>,
//   H6: ({ children }) => <Text style={_.h6}>{children}</Text>,
//   H5: ({ children }) => <Text style={_.h5}>{children}</Text>,
//   P: ({ children }) => <Text style={_.p}>{children}</Text>,
//   P2: ({ children }) => <Text style={_.p2}>{children}</Text>,
//   BR1: ({ children }) => <View style={_.br1}>{children}</View>,
//   BR2: ({ children }) => <View style={_.br2}>{children}</View>,
//   TR: ({ children }) => <View style={_.tr}>{children}</View>,
//   TD: ({ children }) => <View style={_.td}>{children}</View>,
//   SR: ({ children }) => <View style={_.sr}>{children}</View>,
//   SD: ({ children }) => <View style={_.sd}>{children}</View>,
//   SH: ({ children }) => <Text style={_.sh}>{children}</Text>,
// };

// export const PDF = ({ user, statement }) => {
//   const { range,account, period, transactions, totals } = statement;

//   return (
//     <Document>
//     { !range? <Page size="A4" style={_.startPage}>
//         <View style={_.header}>
//           <View>
//             <Image style={{ width: "105pt" }} src={ user.image } />
//           </View>

//           <View style={_.headerRight}>
//             <H1>Zotto Statement</H1>
//             <BR1 />
//             <H6>Statement Date</H6>
//             <P>{moment().format("YYYY-MM-DD")}</P>
//           </View>
//         </View>
//         <BR1 />
//         <BR1 />
//         <View style={_.details}>
//           <View>
//             <H6>Recipient</H6>
//             <BR1 />
//             <P>{user.comp_name}</P>
//             <View style={_.address}>
//               <P>{user.address}</P>
//             </View>
//             <BR2 />
//             <P>{user.email}</P>
//             <P>
//               {user.phone_code} {user.phone}
//             </P>
//           </View>

//           <View style={_.detailsRight}>
//             <H6>Zotto LTD</H6>
//             <BR1 />
//             <View style={_.addressRight}>
//               <P>Staines Upon Thames, TW18 3BA, UK</P>
//             </View>
//             <BR2 />
//             <P>info@zotto.io</P>
//             <P>+44 808 169 7040</P>
//           </View>
//         </View>
//         <BR1 />
//         <BR1 />
//         <BR1 />
//         <View style={_.accDetails}>
//           <H6>Account Summary:</H6>
//         </View>
//         <BR1 />
//         <View style={_.table}>
//           {account.account_id ? <TR>
//             <TD>
//               <P>Account Name</P>
//             </TD>
//             <P>{account.account_name}</P>
//           </TR> : null}
//           <BR2 />
//           <TR>
//             <TD>
//               <P>Total Transactions</P>
//             </TD>
//             <P>{totals.length}</P>
//           </TR>
//           <BR2 />
//           <TR>
//             <TD>
//               <P>Transaction Period</P>
//             </TD>
//             <P>
//               {period[0]} {period[1]}
//             </P>
//           </TR>
//           <BR2 />
//           {account.account_id ? <TR>
//             <TD>
//               <P>Current Balance</P>
//             </TD>
//             <P>{formatMoney(account.current_balance, account.currencycode)}</P>
//           </TR> : null}
//           <BR2 />
//           <TR>
//             <TD>
//               <P>Total Amount Credited</P>
//             </TD>
//             <P>{totals.credited}</P>
//           </TR>
//           <BR2 />
//           <TR>
//             <TD>
//               <P>Total Amount Debited</P>
//             </TD>
//             <P>{totals.debited}</P>
//           </TR>
//           <BR2 />
//           <TR>
//             <TD>
//               <P>Total Fees</P>
//             </TD>
//             <P>{totals.fees}</P>
//           </TR>
//         </View>
//         <BR1 />
//         <BR1 />
//         <P>Check detailed account statement on next page.</P>
//         <H6>Feel free to reach us at support@zotto.io, if you have any questions.</H6>
//         <P>
//           {
//             "Zotto LTD (Company No. 09548832) is registered by the Financial Conduct Authority to conduct payment activities."
//           }
//         </P>
//       </Page>:null}
//       <Page size="A4" style={_.statementPage}>
//         {!range?<View style={_.accDetails}>
//           <H6>Account Statement:</H6>
//         </View>:null}
//         <BR1 />
//         <View style={_.statementTable}>
//           <SR>
//             <SD>
//               <SH>Date</SH>
//             </SD>

//             <SD>
//               <SH>Transaction Type</SH>
//             </SD>
//             <SD>
//               <SH>Payment Mode</SH>
//             </SD>
//             <SD>
//               <SH>Amount</SH>
//             </SD>
       
//             <SD>
//               <SH>Fee</SH>
//             </SD>
//           </SR>
//           { transactions.map((el) => (
//             <SR key={el.id}>
//               <SD>
//                 <P2>{moment(el.date_of_transaction).format("YYYY-MM-DD")}</P2>
//                 <P2>{moment(el.date_of_transaction).format("HH:mm:ss")}</P2>
//               </SD>

//               <SD>
//                 <P2>{el.paid_type == "cr" ? "Credit" : "Debit"}</P2>
//               </SD>
//               <SD>
//                 <P2>{el.pay_method}</P2>
//               </SD>
//               <SD>
//                 <P2>{formatMoney(el.amount, el.currency)}</P2>
//               </SD>

//               <SD>
//                 <P2>{formatMoney(el.fees ? el.fees : 0, el.currency)}</P2>
//               </SD>
//             </SR>
//           ))}
//         </View>
//       </Page>
//     </Document>
//   );
// };



const _ = StyleSheet.create({
  h1: { fontSize: "18pt", fontWeight: "bold" },
  h3:{fontSize:"16pt",fontWeight:900},
  h6: { fontSize: "12pt" },
  h5: { fontSize: "12pt" },
  p: { fontSize: "11pt", color: "#000", marginVertical: "2pt" },
  p2: { fontSize: "9pt", color: "#000" },
  br1: { marginVertical: "6pt" },
  br2: { marginVertical: "3pt" },
  startPage: {
    flexDirection: "column",
    backgroundColor: "#fff",
    paddingVertical: 50,
    paddingHorizontal: 30,
    // fontFamily: "Montserrat",
  },
  statementPage: {
    paddingHorizontal: 50,
    // fontFamily: "Montserrat",
    backgroundColor: "#fff",
    paddingTop: 50,
    paddingBottom: 42,
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headerRight: {
    flexDirection: "column",
    alignItems: "flex-end",
    fontWeight:'fontWeightBold'
  },
  details: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  detailsRight: {
    alignItems: "flex-end",
  },
  address: {
    textOverflow: "wrap",
    width: "120pt",
  },
  addressRight: {
    textOverflow: "wrap",
    width: "120pt",
    textAlign: "right",
  },
  table: {
    // border: "1pt solid #DCDCDC",
   
    padding: "10pt",
    paddingLeft:'0pt',
    
  },
  tablemax:{
    borderTop:"2pt solid #000",
    borderBottom:"2pt solid #000",
     padding: "10pt",
    paddingTop:"1pt",
    paddingLeft:'0pt'
  },
  tr: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems:'center'
  },
  td: {
    width: "130pt",
    marginVertical:"1pt",
    
  },
   td1: {
    width: "130pt",
    marginVertical:"1pt",
    textAlign:'right',
     marginRight:"10pt"
  },
     td2: {
    width: 70,
    marginVertical:"1pt",
       
    textAlign:'right',
     
  },
 
  statementTable: {
    border: "1pt solid #DCDCDC",
  },
  sr: {
    flexDirection: "row",
    paddingVertical:0
    
  },
  sd: {
    // padding: "5pt",
    textAlign: "center",
    width: "99pt",
    
    fontWeight:'bold',
    marginVertical:'auto'
    // justifyContent: "center",
  },
  sh: { fontSize: 12,fontWeight:950, },
});

const { H1,TD1, H5,H3, H6,TD2, P, BR1, BR2, TR, TD, SR, SD, P2, SH } = {
  H1: ({ children }) => <Text style={_.h1}>{children}</Text>,
 H3: ({ children }) => <Text style={_.h3}>{children}</Text>,
  H6: ({ children }) => <Text style={_.h6}>{children}</Text>,
  TD1:({children}) =><View style={_.td1}>{children}</View> ,
  TD2:({children}) =><View style={_.td2}>{children}</View> ,
  H5: ({ children }) => <Text style={_.h5}>{children}</Text>,
  P: ({ children }) => <Text style={_.p}>{children}</Text>,
  P2: ({ children }) => <Text style={_.p2}>{children}</Text>,
  BR1: ({ children }) => <View style={_.br1}>{children}</View>,
  BR2: ({ children }) => <View style={_.br2}>{children}</View>,
  TR: ({ children }) => <View style={_.tr}>{children}</View>,
  TD: ({ children }) => <View style={_.td}>{children}</View>,
  SR: ({ children,style }) => <View style={[_.sr,style]}>{children}</View>,
  SD: ({ children,style }) => <View style={[_.sd,style]}>{children}</View>,
  SH: ({ children }) => <Text style={_.sh}>{children}</Text>,
};

 const PDF = ({ user, statement }) => {
   const { range,account, period, transactions, totals } = statement;

  return (
    <Document>
     <Page size="A4" style={_.startPage}>
        <View style={[_.header,{marginTop:50}]}>
          <View style={{flexDirection:'row'}}>
            
            <Image style={{marginTop:20, width: "105pt",marginRight:23 }} src={ "https://paymentz.z-pay.co.uk/images/merchant/3.png" } />
            
              <View style={{width:150}}>
             <P>Staines Upon Thames, TW18 3BA, UK</P>
             <P>info@zotto.io </P>
                <P>+44 808 169 7040</P>
            </View>
          </View>
            

          <View style={[_.headerRight,{fontWeight:900}]}>
            <H3>STATEMENT OF ACCOUNT</H3>
            <BR1 />
           
          </View>
        </View>
        <BR1 />
        
        <View style={_.details}>
          <View style={_.table}>
            <TR>
            <TD><H6>Account Number:</H6></TD>
            <P>{account.account_number||account.iban_no}</P>
            </TR>
            <BR2/>
            <TR>
            <TD><H6>Statement Date:</H6></TD>
            <P>{moment(new Date()).format('DD/MM/YYYY')}</P>
              
            </TR>
            <BR2/>
             <TR>
            <TD><H6>Period Covered:</H6></TD>
                <TD>
               <P>{period[0]} to</P>
                  <BR2/>
                  <P>{period[1]} </P>
            </TD>
                  </TR>
           </View>
            <View style={{display:'flex',justifyContent:'center'}}>
             <P>Page 1 of 1</P>
        </View>
          </View>

         
        <BR1 />
       
        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <View>
          <H6>{user.comp_name||user.name}</H6>
          <BR2/>
          <P>{user.address}</P>
             <BR2/>
          <P>{user.email}</P>
          <BR2/>
          <P>{user.phone_code} {user.phone}</P>
          </View>
        <View style={_.table}>
          <TR>
            <TD1>
              <P>Opening Balance</P>
            </TD1>
            <P>{formatMoney(totals.open_balance)}</P>
          </TR> 
          <BR2 />
          <TR>
            <TD1>
              <P>Total Credit Amount</P>
            </TD1>
            <P>{totals.credited}</P>
          </TR>
          <BR2 />
           <TR>
            <TD1>
              <P>Total Debit Amount</P>
            </TD1>
             
            <P>{totals.debited}</P>
          </TR>
                 <BR2 />
          <TR>
            
            <TD1>
              <P>Closing Balance</P>
            </TD1>
            <P>
              {formatMoney(account.current_balance)}
            </P>
          </TR>
        
          <BR2 />
         
          <TR>
            
            <TD1>
              <P>Currency</P>
            </TD1>
            <P>
              {account.currencycode}
            </P>
          </TR>
          <BR2/>
          <TR>
            <TD1>
              <P>Account Type</P>
            </TD1>
            <P>{account.account_type?'Currency':"Bank"} Account</P>
          </TR>
          <BR2/>
           <TR>
             
            <TD1>
              <P>Number of Transaction</P>
            </TD1>
             <TD2>
            <P>{totals.length}</P>
               </TD2>
          </TR>
        </View>
       </View>
        <BR2 />
        <BR2 />
        <H1>Transactions</H1> 
       <View style={_.tablemax}>
      <SR>
            <SD>
              <SH>Date</SH>
            </SD>

            <SD style={{width:500,textAlign:'center'}}>
              <SH>Description</SH>
            </SD>
            <SD>
              <SH>Credit</SH>
            </SD>
            <SD>
              <SH>Debit</SH>
            </SD>
       
            <SD>
              <SH>Balance</SH>
            </SD>
            </SR>
            {transactions.map((el,id)=>(        
       
         <SR style={{backgroundColor:`${(id%2)!=0? '#e6e6e6':'#fff'}`}} >
         <SD style={{backgroundColor:'#e6e6e6',fontSize:8}}>
          <P>{moment(el.date_of_transaction).format("DD/MM/YYYY")}</P>
          {/* <P>{moment(el.date_of_transaction).format("HH:mm:ss")}</P> */}
           </SD>
           <SD style={{width:500,textAlign:'center'}}>
            <P>{el.message}  </P>
           </SD>
            <SD style={{textAlign:'left',marginHorizontal:'auto'}}>
            <P></P>
           </SD>
            <SD >
            <P>{el.paid_type == "cr" ? formatMoney(el.amount):null}</P>
           </SD>
             <SD >
             <P>{el.paid_type == "dr" ? formatMoney(el.amount):null}</P>
           </SD>
           <SD >
             <P>{ formatMoney(el.balance)}</P>
           </SD>
         </SR>
      
            ))
         }
       </View>
      </Page>
    </Document>
  );
};



export default function StatementPDF() {

  const { merchants } = useSelector(state => state.merchant);
  const { transactions, dates, setStatement, statement, selectedAccount, merchantId } = useContext(TxnContext)

  const deleteResolvedAction = () => {
    setStatement({});

  };

  const handleAction = (data, action) => {
    if (!data) { console.log(data, '123'); deleteResolvedAction(); return; }
    if (action == "print") {
      printDoc(data);
      deleteResolvedAction();
    } else if (action == "download") { downloadDoc(data, () => deleteResolvedAction()); }
  };

  useEffect(() => {
    if (transactions.length && statement.loading) {
      (async () => {
        const user = merchants.length ? merchants.find(e => e.merchant_id == merchantId) : null;
        console.log(selectedAccount)
        let transactionsArr = selectedAccount.account_id ? transactions.filter(e => e.currency_account_id == selectedAccount.account_id) : transactions.filter(el => el.merchant_id == merchantId);
        const start = moment(dates.start_date).format('DD/MM/YYYY');
        const end = moment(dates.end_date).format('DD/MM/YYYY');
        // console.log(month, year, moment(dates.start_date).month());
        let period = new Array(start, end);
        let totals = {
          debited: 0,
          credited: 0,
          fees: 0,
          length: 0,
          open_balance:0
        };
        let temp_balance = selectedAccount.current_balance;
        transactionsArr=transactionsArr.reverse();
        transactionsArr.forEach((el) => {
          if (selectedAccount.account_id) {
            el.balance = temp_balance;
            console.log(temp_balance,el.fees);
            temp_balance = (el.paid_type == "cr") ? temp_balance - el.amount  : temp_balance+el.amount 
           
          }
          totals.open_balance=temp_balance;

          if (el.paid_type == "cr") totals.credited += el.amount;
          else totals.debited +=el.amount;
          totals.fees += el.fees ? el.fees : 0;
          totals.length += 1;
        });

        totals.debited = formatMoney(totals.debited);
        totals.credited = formatMoney(totals.credited);
        totals.fees = formatMoney(totals.fees);
      
        const no_of_per_thousand = transactionsArr.length / 500;
        user.image=await useGetImage(user.image||`${graphqlServerUrl}/images/merchant/${user.merchant_id}.png`)
        let finalBlobArr = [];


        for (let i = 0; i < no_of_per_thousand; ++i) {
          const transactionReduced = transactionsArr.splice(i, i + 500);
          const pdfBlob = await pdf(<PDF user={user} statement={{range:i, period, transactions: transactionReduced, account: selectedAccount.account_id ? selectedAccount : {}, totals }} />).toBlob();

          finalBlobArr.push(pdfBlob);
        }
        // console.log(finalBlobArr);
        const merger = new PDFMerger();

        await Promise.all(finalBlobArr.map(async (file) => await merger.add(file)));
       const url = URL.createObjectURL(await merger.saveAsBlob());
       
        handleAction(url, statement.action);
      })()
    }
     
  }, [statement.action, transactions])



  return (
    <div style={{ display: "none" }}>
      {/* <BlobProvider document={<PDF user={userObj} statement={statementObj} />}>
        {({ url, loading }) => {
          return loading&&!url ? null : (
            <Fragment>{setTimeout(() => handleAction(url, statement.action), 0) ? null : null}</Fragment>
          );
        }}
      </BlobProvider> */}
    </div>
  );
}

