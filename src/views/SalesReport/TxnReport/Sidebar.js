// ** Third Party Components
import React,{ useContext } from "react";
import { Table, Button } from "reactstrap";
import Sidebar from "@components/sidebar";
import { TxnContext } from ".";
import moment from "moment";

const TransactionSidebar = () => {
  const {
    selectedTxn: transaction,
    setSelectedTxn,
    setRefundModal,
  } = useContext(TxnContext);

  const closeSidebar = () => setSelectedTxn({});

  if (!transaction) return null;

  return (
    <Sidebar
      size="lg"
      open={transaction.id ? true : false}
      title="Transaction Details"
      headerClassName="mb-2"
      contentClassName="p-0"
      toggleSidebar={closeSidebar}
    >   
      <Table style={{ tableLayout: "fixed" }} borderless={true} striped={true}>
        {transaction.id ? (
          <tbody>
            <tr>
              <td>Date</td>
              <td>{moment(transaction.date_of_transaction).format("YYYY-MM-DD")}</td>
            </tr>
            <tr>
              <td>Time</td>
              <td>{moment(transaction.date_of_transaction).format("HH:mm:ss")}</td>
            </tr>
            <tr>
              <td>Status</td>
              <td>{transaction.status == 0 ? "Failed" : transaction.status==1?"Success":"Pending"}</td>
            </tr>
            {/* <tr>
              <td>Account</td>
              <td>{accounts.find((el) => el.id == transaction.currency_account_id).account_name}</td>
            </tr> */}
            <tr>
              <td>Transaction ID</td>
              <td>
                <div style={{ wordWrap: "break-word" }}>{transaction.txn_no}</div>
              </td>
            </tr>
            <tr>
              <td>Order ID</td>
              <td>{transaction.order_id}</td>
            </tr>
            <tr>
              <td>Amount</td>
              <td>
                {transaction.amount} {transaction.currency}
              </td>
            </tr>
            <tr>
              <td>Payment Type</td>
              <td>{transaction.paid_type == "cr" ? "Credit" : "Debit"}</td>
            </tr>
            <tr>
              <td>Payment Method</td>
              <td>{transaction.pay_method}</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>
                <div className="font-small-3">{transaction.message}</div>
              </td>
            </tr>
            <tr>
              <td>Refunded</td>
              <td>{transaction.refund_type != "0" ? "Yes" : "No"}</td>
            </tr>
            {transaction.refund_type != "0" ? (
              <tr>
                <td>Refunded Amount</td>
                <td>
                  {transaction.refunded_amount} {transaction.currency}
                </td>
              </tr>
            ) : null}
            <tr>
              <td>Refundable</td>
              <td>{transaction.refund_status == "1" ? "Yes" : "No"}</td>
            </tr>
          </tbody>
        ) : (
          <tbody></tbody>
        )}
      </Table>
      {transaction.refund_status == "1" ? (
        <div className="ml-2 mr-2 mt-2">
          <Button.Ripple color="danger" block onClick={() => setRefundModal(true)}>
            Refund Transaction
          </Button.Ripple>
        </div>
      ) : null}
    </Sidebar>
  );
};

export default TransactionSidebar;
