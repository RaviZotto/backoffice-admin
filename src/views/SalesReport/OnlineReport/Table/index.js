import React, { useContext, useEffect, useState } from 'react'
import Loading from "@core/components/spinner/Loading-spinner";
import { ChevronDown, Eye, Edit,Trash } from "react-feather";
import DataTable from "react-data-table-component";
import { Fragment } from 'react';
import {  UPDATE_MERCHANT} from "../../../../redux/actions/storeMerchant.";
import { useDispatch, useSelector } from 'react-redux';
import { OnlineContext } from '..';



export default function index() {  
const dispatch = useDispatch()
const {merchants,devices}=useSelector(state=>state.merchant);
const {transactions,merchantId,loading,setShowView}=useContext(OnlineContext);

useEffect(() => {
    
 if(!merchants.length)dispatch(UPDATE_MERCHANT());

})





const columns =[
   
  {
      name: "Merchant",
      selector: "comp_name",
      sortable: true,
      minWidth: "110px",
      cell: (row) => {
        return (
          <Fragment>
            <div className="mt-1">
              <h6 className="font-small-3">{row.comp_name}</h6>
         </div>
          </Fragment>
        );
      },
    },
  
   
    {
      name: "EUR",
      selector: "EUR",
      sortable: false,
      cell: (row) => (
        <h6
         
        >
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: 'EUR',
          })
            .format(!row.EUR?0:row.EUR)
            .replace(/^(\D+)/, "$1 ")}
        </h6>
      ),
    },
    {
      name: "USD",
      selector: "USD",
      sortable: false,
      cell: (row) => (
        <h6
         
        >
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: 'USD',
          })
            .format(!row.USD?0:row.USD)
            .replace(/^(\D+)/, "$1 ")}
        </h6>
      ),
    },
    {
      name: "BDT",
      selector: "BDT",
      sortable: false,
      cell: (row) => (
        <h6
         
        >
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: 'BDT',
          })
            .format(!row.BDT?0:row.BDT)
            .replace(/^(\D+)/, "$1 ")}
        </h6>
      ),
    },
    {
      name: "GBP",
      selector: "GBP",
      sortable: false,
      cell: (row) => (
        <h6
         
        >
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: 'GBP',
          })
            .format(!row.GBP?0:row.GBP)
            .replace(/^(\D+)/, "$1 ")}
        </h6>
      ),
    },
    {
      name: "DMK",
      selector: "DMK",
      sortable: false,
      cell: (row) => (
        <h6
         
        >
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: 'DMK',
          })
            .format(!row.DMK?0:row.DMK)
            .replace(/^(\D+)/, "$1 ")}
        </h6>
      ),
    },
        {
      name: "BMD",
      selector: "BMD",
      sortable: false,
      cell: (row) => (
        <h6
         
        >
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: 'BMD',
          })
            .format(!row.BMD?0:row.BMD)
            .replace(/^(\D+)/, "$1 ")}
        </h6>
      ),
    },
    {
      name: "DKK",
      selector: "DKK",
      sortable: false,
      cell: (row) => (
        <h6
         
        >
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: 'DKK',
          })
            .format(!row.DKK?0:row.DKK)
            .replace(/^(\D+)/, "$1 ")}
        </h6>
      ),
    },
    {
      name: "Action",
      selector: "action",
      sortable: false,
      cell: (row) => (
         <Eye size={17} className='cursor-pointer'  onClick={e=>setShowView(row)} />
      ),
    },
  
  ];

const dataToRender = () => {
    let tempOnline=transactions.filter(el=>el.payment_mode=='1');
    let tempData=[];
    // //console.log(tempOnline);
  if(merchants&&tempOnline){
    tempOnline.forEach(el => {
      
        let exist = tempData.find(e=>e.merchant_id==el.merchant_id);
        // //console.log(exist);
        if(exist)exist[el.currency]=  (exist[el.currency])?Number(exist[el.currency])+Number(el.amount):Number(el.amount);
        else{ 
          let merchantObj=merchants.find(res=>res.merchant_id==el.merchant_id);
          tempData.push({merchant_id:el.merchant_id, comp_name:merchantObj&&merchantObj.comp_name, [el.currency]: el.amount});      
    
      }
      });
    if(merchantId)tempData=tempData.filter(el=>el.merchant_id==merchantId);
    //console.log(tempData);
  }     
    return tempData;
}


if(loading)return <Loading />

    return (
        <div>
         <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
        </div>
    )
}
