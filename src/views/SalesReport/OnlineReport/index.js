import { useLazyQuery,useQuery } from "@apollo/client";
import moment from "moment";
import {ArrowLeft, ArrowRight} from 'react-feather';
import {Button} from 'reactstrap';
import React, { createContext, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GET_TRANSACTIONS } from "../../../graphql/queries";
import { UPDATE_MERCHANT } from "../../../redux/actions/storeMerchant.";
import CsvPreview from "./CsvPreview";
import PdfDownload from "./PdfDownload";
import Table from "./Table";
import Header from "./Table/Header";
import SalesReport from "../../../components/analytics/OnlineReports";
import Loading from "@core/components/spinner/Loading-spinner";
import { GET_CURRENCY, GET_GATEWAY } from "../../../redux/actions/transfer";
import notification from "../../../components/notification";
import { getAdminTransactions } from "../../../Api";
export const OnlineContext = createContext();


export const  dataToRender = ({transactions,merchants,merchantId}) => {
    let tempData=transactions;

  
  
    let tempObj=[];
  if(tempData){

    tempData.forEach(el => {
      
        let exist = tempObj.find(e=>e.merchant_id==el.merchant_id);
        // //console.log(exist);
        if(exist)exist[el.currency]=  (exist[el.currency])?Number(exist[el.currency])+Number(el.amount):Number(el.amount);
        else{ 
          let merchantObj=merchants.find(res=>res.merchant_id==el.merchant_id);
          tempObj.push({merchant_id:el.merchant_id, comp_name:merchantObj&&merchantObj.comp_name, [el.currency]: el.amount});      
    
      }
      });
    if(merchantId)tempObj=tempObj.filter(el=>el.merchant_id==merchantId);
   
    //console.log(tempObj);
    
  }     
 
  return tempObj;
}



export default function index() {
  const dispatch = useDispatch();
  const { merchants } = useSelector((state) => state.merchant);
  const {gateway} = useSelector((state) => state.transfer);


  const [dates, setDates] = useState({
    start_date: moment(new Date()).format("YYYY-MM-DD"),
    end_date: moment(new Date()).format("YYYY-MM-DD"),
  });

  const [merchantId, setMerchantId] = useState(null);
  const [transactions, setTransactions] = useState([]);
  const [formattedTxn,setFormattedTxn] = useState([])
  const [statement, setStatement] = useState({});
  const [statementCSV, setStatementCSV] = useState(false);
  const[showView,setShowView] = useState({});
  const [Gateway,setGateway] = useState({value:0,label:'Select--'});
  const [currency,setCurrency] = useState(null);
  const[report,setReport]=useState({});
  const [loading,setLoading]= useState(true);
  const [gatewayOption,setGatewayOption]=useState([]);
  const [currencyOption,setCurrencyOption]=useState([]);

  useEffect(() => {
    if (!merchants.length) dispatch(UPDATE_MERCHANT());
    if (!gateway.length) dispatch(GET_GATEWAY());
    (async () => {
      try {
        setLoading(true);
        let transaction = await getAdminTransactions({...dates,merchant_id:merchantId?merchantId:undefined,payment_mode:1});
   
        if (transaction.length) {setTransactions(transaction);}
        setLoading(false);
      } catch (e) {
        setLoading(false);
        notification({ title: "Error", message: e, type: "error" });
      }
    })();
  }, [dates]);




  useEffect(()=>{ 

  
    if(Object.keys(showView).length&&gateway.length){ 
      let txnArray=[...transactions];
      let objArr={ 
        success:0,
        pending:0,
        failure:0,
        credit:0,
        debit:0,
        refund:0,
        internal_transfer:0,
        online:0,
        device:0,
        payment_link:0,
        sepa_transfer:0,
        swift_transfer:0,
        express_transfer:0,
        fx_internal_transfer:0,
        fx_swift_transfer:0,
        debit_card_spend:0,
        flair_card_topup:0,
        total:0
      }
      txnArray = txnArray.filter(e=>e.merchant_id==showView.merchant_id);
      let setT=new Set(),setG=new Set();
      for(let obj of txnArray){setT.add(obj.currency);setG.add(obj.gateway_id);}
      setCurrencyOption(Array.from(setT));setGatewayOption(gateway.filter(el=>Array.from(setG).includes(el.id)).map(el1=>({label:el1.gateway_identifier,value:el1.id})));   
     txnArray = txnArray.filter(e=>e.currency==(currency||txnArray[0].currency));
      
     txnArray = txnArray.filter(e=>e.gateway_id==(Gateway.value||txnArray[0].gateway_id));
    //console.log(txnArray,Gateway);
     for(let obj of txnArray){
      objArr.total+=1;
      obj.paid_type=='cr'?objArr.credit+=obj.amount:objArr.debit+=obj.amount;
      obj.status==0?objArr.failure+=1:(obj.status==1?objArr.success+=1:objArr.pending+=1)
        objArr[obj['txn_type']]+=obj.amount;
     }
     setReport(objArr)
    }
  
  },[transactions, showView,currency,Gateway])



  const DeviceProvider = useMemo(
    () => ({
      formattedTxn,
      setFormattedTxn,
      transactions,
      loading,
      setLoading,
      setTransactions,
      dates,
      setDates,
      merchantId,
      setMerchantId,
      statementCSV,
      setStatementCSV,
      statement,
      setStatement,
      setShowView,
      showView,
      report,
      setReport,
      currency,
      setCurrency,
      merchants,
     Gateway,
     setGateway,
     currencyOption,
     setCurrencyOption,
     gatewayOption,
     setGatewayOption,
    }),
    [report,currency,Gateway,gatewayOption,currencyOption, showView, transactions,formattedTxn, dates, merchantId,statementCSV,loading,statement]
  );

  return (
    <OnlineContext.Provider value={DeviceProvider}>
      <Header />

      { loading?<Loading/>:
      <>
      { !showView.merchant_id? <Table />:
      <>
      <Button.Ripple color="primary"  onClick={e=>setShowView({})} >
      <ArrowLeft size={17} />
      <span>Back</span>
      </Button.Ripple> 
      <SalesReport  />
      </> 
    }
    </>  }
      {statement.loading&&<PdfDownload />}
      {statementCSV&&<CsvPreview/>}
    </OnlineContext.Provider>
  );
}

