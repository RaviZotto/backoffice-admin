import React,{ Fragment, useContext } from "react";
import { Page, Text, View, Document, StyleSheet, BlobProvider, Font, Image } from "@react-pdf/renderer";
import logo from "../../../assets/images/logo/logo.png";
import fontFile from "../../../assets/fonts/Montserrat-Regular.ttf";
import moment from "moment";
import { downloadDoc, formatMoney } from "@utils";
import printDoc from "print-js";
import { useSelector } from "react-redux";
import { dataToRender, OnlineContext } from ".";


Font.register({
  family: "Montserrat",
  src: fontFile,
});

const _ = StyleSheet.create({
  h1: { fontSize: "18pt", fontWeight: "bold" },
  h6: { fontSize: "12pt" },
  h5: { fontSize: "12pt" },
  p: { fontSize: "11pt", color: "#626262", marginVertical: "5pt" },
  p2: { fontSize: "9pt", color: "#626262" },
  br1: { marginVertical: "10pt" },
  br2: { marginVertical: "5pt" },
  startPage: {
    flexDirection: "column",
    backgroundColor: "#fff",
    paddingVertical: 50,
    paddingHorizontal: 50,
    fontFamily: "Montserrat",
  },
  statementPage: {
    paddingHorizontal: 50,
    fontFamily: "Montserrat",
    backgroundColor: "#fff",
    paddingTop: 50,
    paddingBottom: 42,
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headerRight: {
    flexDirection: "column",
    alignItems: "flex-end",
  },
  details: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  detailsRight: {
    alignItems: "flex-end",
  },
  address: {
    textOverflow: "wrap",
    width: "120pt",
  },
  addressRight: {
    textOverflow: "wrap",
    width: "120pt",
    textAlign: "right",
  },
  table: {
    border: "1pt solid #DCDCDC",
    padding: "10pt",
  },
  tr: {
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  td: {
    width: "200pt",
  },
  statementTable: {
    border: "1pt solid #DCDCDC",
  },
  sr: {
    flexDirection: "row",
    borderBottom: "1pt solid #dcdcdc",
  },
  sd: {
    padding: "5pt",
    textAlign: "center",
    width: "99pt",
    justifyContent: "center",
  },
  sh: { fontSize: "10pt" },
});

const { H1, H5, H6, P, BR1, BR2, TR, TD, SR, SD, P2, SH } = {
  H1: ({ children }) => <Text style={_.h1}>{children}</Text>,
  H6: ({ children }) => <Text style={_.h6}>{children}</Text>,
  H5: ({ children }) => <Text style={_.h5}>{children}</Text>,
  P: ({ children }) => <Text style={_.p}>{children}</Text>,
  P2: ({ children }) => <Text style={_.p2}>{children}</Text>,
  BR1: ({ children }) => <View style={_.br1}>{children}</View>,
  BR2: ({ children }) => <View style={_.br2}>{children}</View>,
  TR: ({ children }) => <View style={_.tr}>{children}</View>,
  TD: ({ children }) => <View style={_.td}>{children}</View>,
  SR: ({ children }) => <View style={_.sr}>{children}</View>,
  SD: ({ children }) => <View style={_.sd}>{children}</View>,
  SH: ({ children }) => <Text style={_.sh}>{children}</Text>,
};

const PDF = ({ statement:transactions,dates }) => {
  
   
  return (
    <Document>
     
      <Page size="A4" style={_.statementPage}>
        <View style={_.accDetails}>
          <H6>Online Sales Report: FROM {dates.start_date} TO {dates.end_date} </H6>
        </View>
        <BR1 />
        <View style={_.statementTable}>
        <SR>
            <SD>
              <SH>Company Name</SH>
            </SD>
            <SD>
              <SH>USD</SH>
            </SD>
            <SD>
              <SH>EUR</SH>
            </SD>
            <SD>
              <SH>GBP</SH>
            </SD>
            <SD>
              <SH>BDT</SH>
            </SD>
            <SD>
              <SH>BMD</SH>
            </SD>
            <SD>
              <SH>DMK</SH>
            </SD>
            <SD>
              <SH>DKK</SH>
            </SD>
          
          </SR>
          {transactions.map((el) => (
            <SR key={el.id}>
              <SD>
                <P2>{el.comp_name}</P2>
            
              </SD>
              <SD>
                <P2>{formatMoney(el.USD, 'USD')}</P2>
              </SD>
              <SD>
              <P2>{formatMoney(el.EUR, 'EUR')}</P2>
              </SD>
              <SD>
              <P2>{formatMoney(el.GBP, 'GBP')}</P2>
              </SD>
              <SD>
              <P2>{formatMoney(el.BDT, 'BDT')}</P2>
              </SD>
              <SD>
              <P2>{formatMoney(el.BMD, 'BMD')}</P2>
              </SD>
              <SD>
              <P2>{formatMoney(el.DMK, 'DMK')}</P2>
              </SD>
              <SD>
              <P2>{formatMoney(el.DKK, 'DKK')}</P2>
              </SD>
            </SR>
          ))}
        </View>
      </Page>
    </Document>
  );
};

export default function StatementPDF() {
   const {statement,transactions,setStatement,dates,merchantId} = useContext(OnlineContext) 
   const {merchants} = useSelector(state=>state.merchant);
   const deleteResolvedAction = () => {
    setStatement({});
  };

  const handleAction = (data, action) => {
    if (!data) return;
    if (action == "print") {
      printDoc(data);
      deleteResolvedAction();
    } else if (action == "download") {downloadDoc(data, () => deleteResolvedAction());}
  };

  if ( !statement || !statement.action) return null;
  return (
    <div style={{ display: "none" }}>
      <BlobProvider document={<PDF dates={dates} statement={dataToRender({transactions,merchants,merchantId})} />}>
        {({ url, loading }) => {
          return loading ? null : (
            <Fragment>{setTimeout(() => handleAction(url, statement.action), 0) ? null : null}</Fragment>
          );
        }}
      </BlobProvider>
    </div>
  );
}
