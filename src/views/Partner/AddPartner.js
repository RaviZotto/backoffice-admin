import React, { useEffect, useState } from 'react'
import {Button, FormGroup,Col,Label,Row,InputGroup,InputGroupAddon,InputGroupText,Card,CardHeader,CardTitle,CardBody} from 'reactstrap'
import * as Yup from "yup";
import Select from 'react-select'
import {countries as countryOptions,selectThemeColors} from '@utils'
import { Formik, Field, Form } from "formik";
import notification from '../../components/notification'
import { useDispatch, useSelector } from 'react-redux'
import { addPartner, updatePartner } from '../../Api'
import Spinner from 'reactstrap/lib/Spinner'
import { GET_PARTNER, UPDATE_PARTNER } from '../../redux/actions/storeMerchant.'
import { userDetails } from '../../utility/Utils';

export default function AddPartner({setForm}) {
  const [phoneCode, setPhoneCode] = useState("+44");
 const {partnerObj}=useSelector(state=>state.merchant);
 const[loading,setLoading]=useState(false);
 const dispatch=useDispatch();
const initialPartner={
  name: "",
  email: "",
  phone: "",
  zipcode: "",
  role_permissions: "",
  phone_code: "+44",
  country: "GB",
  address:"",
  comp_name:"",
  tax_no:"",
  registration_no:"",
  acct_holder_name:"",
  bic:"",
  iban_acct_no:"",
  swift_code:"",
  sort_code:"",
  bank_name:"",
  address:"",
  city:""


}

const bankKeys=['bank_name','acct_holder_name','bic','swift_code','iban_acct_no','sort_code',"acct_holder_name"]
const generalKeys=[
  'name',
  'email',
  'country',
  'zipcode',
  'phone',
  'phone_code',
  'role_permissions',
  'registration_no',
  'comp_name',
  'address',
  'tax_no',
  'city'
]
 
const [generalDetails,setGeneralDetails]=useState(()=>{
     let general={};   
   
      for(let key of generalKeys){
        general[key]=initialPartner[key];
      }
      return general;
})

const [bankDetails,setbankDetails]=useState(()=>{
      let bank={};
     for(let key of bankKeys){
    bank[key]=initialPartner[key];
  }
  return bank;
})

useEffect(() => {

  let bank={},general={};
  if(Object.keys(partnerObj).length>0){
 
  for(let key of bankKeys){
    bank[key]=partnerObj[key];
  }
  setbankDetails({...bank});
  for(let key of generalKeys){
    general[key]=partnerObj[key];
  }
   setGeneralDetails({...general})
}
}
, [partnerObj])


const handleFormSubmit=async(value)=>{
  try{
  
  let general={},bank={};
  if(Object.keys(partnerObj).length>0){
    //console.log(value);
    for(let key of generalKeys ){
      if(value[key]!=partnerObj[key]) general[key]=value[key];
   }
   for(let key of bankKeys){
     if(value[key]!=partnerObj[key]) bank[key]=value[key]
   }
    if(Object.keys(general).length>0 || Object.keys(bank).length>0)
     { setLoading(true); 
       dispatch(UPDATE_PARTNER({general,bank,partner_id:partnerObj['partner_id']},()=>{
       dispatch(GET_PARTNER(()=>setLoading(false)));
       setLoading(false);
       }));
       //console.log(general,bank)
     }
    else {notification({title:"Error",message:"Nothing is changed!!",type:'error'})} 
    //console.log(general,bank)
  }else{

    for(let key of generalKeys ){
      if(value[key]!=partnerObj[key]) general[key]=value[key];
   }
   for(let key of bankKeys){
     if(value[key]!=partnerObj[key]) bank[key]=value[key]
   }
    setLoading(true)
    await addPartner({general,bank});
     dispatch(GET_PARTNER(()=>setLoading(false)));
    notification({title:'Success',message:'Successfully added',type:'success'});
  }
}catch(e){
  setLoading(false)
 notification({title:'error',message:e,type:'error'})
}

}

const formSchema = Yup.object().shape({
  // name: Yup.string().required(),
  // email: Yup.string().email("Invalid Email").required("Required"),
  // zipcode: Yup.number().required("Required"),
  // phone: Yup.number().required(),
  // phone_code: Yup.string(),
  // country: Yup.string(),
  // role_permissions:Yup.string(),
  // comp_name:Yup.string(),
  // tax_no:Yup.number('Number required').notRequired(),
  // registration_no:Yup.string().required("Required"),
  // bank_name:Yup.string().required("Required"),
  // bic:Yup.string().notRequired(),
  // swift_code:Yup.string().notRequired(),
  // acct_holder_name:Yup.string().notRequired(),
  // iban_acct_no:Yup.string().notRequired(),
  // sort_code:Yup.string().notRequired() ,
  // address:Yup.string(),
  // city:Yup.string(),
  // iban_acct_no:Yup().string() 

});


return (
        <div>
         <Card>
            <CardHeader>
                 <CardTitle>General Details</CardTitle>
            </CardHeader>
            <hr/>
            
   
            <Formik
              initialValues={{...generalDetails,...bankDetails}}
              enableReinitialize
              onSubmit={handleFormSubmit}
              validationSchema={formSchema}
            >
              {({ errors, touched, values, initialPartner }) => (
                <CardBody>
                <Form>
                  <Col>
                  <Row>
                    <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="name" className="form-label">
                          Name
                        </Label>
                        <Field
                          name="name"
                          id="name"
                          placeholder="John Doe"
                          className={`form-control ${errors.name && touched.name && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="name" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="email" className="form-label">
                          Email
                        </Label>
                        <Field
                          name="email"
                          type="email"
                          id="email"
                          placeholder="example@domain.com"
                          className={`form-control ${errors.email && touched.email && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="email" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="country" className="form-label">
                          Country
                        </Label>
                        <Select
                          theme={selectThemeColors}
                          className="react-select"
                          classNamePrefix="select"
                          defaultValue={countryOptions.find((el) => el.value == values.country) || countryOptions[277]}
                          options={countryOptions}
                          isClearable={false}
                          onChange={(obj) => {
                            values.phone_code = obj.phone_code;
                            values.country = obj.value;
                            setPhoneCode(obj.phone_code);
                          }}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="zipcode" className="form-label">
                          Zip Code
                        </Label>
                        <Field
                          name="zipcode"
                          id="zipcode"
                          placeholder="112233"
                          className={`form-control ${errors.zipcode && touched.zipcode && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="zipcode" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="phone" className="form-label">
                          Contact
                        </Label>
                        <InputGroup className="input-group-merge">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText style={{ borderColor: errors.phone && touched.phone ? "red" : "" }}>
                              { phoneCode || "+44"}
                            </InputGroupText>
                          </InputGroupAddon>
                          <Field
                            name="phone"
                            id="phone"
                            placeholder="763-242-9206"
                            className={`form-control ${errors.phone && touched.phone && "is-invalid"}`}
                          />
                        </InputGroup>
                        {/* <ErrorMessage name="phone" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                    </Col>
                    <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="role" className="form-label">
                          Address
                        </Label>
                        <Field
                          name="address"
                          id="address"
                          placeholder="Address"
                          className={`form-control ${errors.address && touched.address && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="address" className="form-label">
                         Role
                        </Label>
                        <Field
                          name="role_permissions"
                          id="role_permissions"
                          placeholder="Role"
                          className={`form-control ${errors.role_permissions && touched.role_permissions && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="role" className="form-label">
                          City
                        </Label>
                        <Field
                          name="city"
                          id="city"
                          placeholder="Company Name"
                          className={`form-control ${errors.city&& touched.city && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="role" className="form-label">
                          Tax Number
                        </Label>
                        <Field
                          name="tax_no"
                          id="tax_no"
                          placeholder="Tax Number"
                          className={`form-control ${errors.tax_no && touched.tax_no && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      <Col md="6" sm="12">
                      <FormGroup>
                        <Label for="role" className="form-label">
                          Company Name
                        </Label>
                        <Field
                          name="comp_name"
                          id="comp_name"
                          placeholder="Company Name"
                          className={`form-control ${errors.comp_name && touched.comp_name && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      <Col md="6" sm="12">
                      <FormGroup>
                        <Label for="role" className="form-label">
                          Registraion Number
                        </Label>                        <Field
                          name="registration_no"
                          id="registration_no"
                          placeholder="Reg No."
                          className={`form-control ${errors.registration_no && touched.registration_no && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      </Row>
                      <CardTitle>Bank Details</CardTitle>
                      <hr/>
                      <Row>
                      <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="role" className="form-label">
                         Acct Holder Name
                        </Label>
                        <Field
                           name="acct_holder_name"
                          id="acct_holder_name"
                          placeholder="Acct Holder Name"
                          className={`form-control ${errors.acct_holder_name && touched.acct_holder_name && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="role" className="form-label">
                          Bank Name
                        </Label>
                        <Field
                          name="bank_name"
                          id="bank_"
                          placeholder="Reg No."
                          className={`form-control ${errors.reg_no && touched.reg_no && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="role" className="form-label">
                          Sort Code
                        </Label>
                        <Field
                          name="sort_code"
                          id="sort_code"
                          placeholder="Sort Code"
                          className={`form-control ${errors.sort_code && touched.sort_code && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="role" className="form-label">
                          Iban
                        </Label>
                        <Field
                          name="iban_acct_no"
                          id="iban_acct_no"
                          placeholder="Iban"
                          className={`form-control ${errors.iban_acct_no && touched.iban_acct_no && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="role" className="form-label">
                          Bic
                        </Label>
                        <Field
                          name="bic"
                          id="bic"
                          placeholder="Bic"
                          className={`form-control ${errors.bic && touched.bic && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      <Col md="4" sm="12">
                      <FormGroup>
                        <Label for="role" className="form-label">
                          Swift Code
                        </Label>
                        <Field
                          name="swift_code"
                          id="swift_code"
                          placeholder="Swift Code"
                          className={`form-control ${errors.swift_code && touched.swift_code && "is-invalid"}`}
                        />
                        {/* <ErrorMessage name="role" component="div" className="field-error text-danger" /> */}
                      </FormGroup>
                      </Col>
                      </Row>
                       <Row className='p-1'>
                         
                         <Button className='mr-1' type='submit'  color='primary' outline disabled={Object.keys(partnerObj).length>0?userDetails.role_type&&userDetails.role_type!='admin' && !userDetails.role_type&&userDetails['partner'].includes('U'): userDetails.role_type&& userDetails.role_type!='admin' && !userDetails['partner'].includes('C')} >
                            {loading?<Spinner size="sm" className="mr-1"/>:null} {Object.keys(partnerObj).length>0?"Update":"Add"}
                         </Button>
                          <Button outline color='secondary' onClick={()=>setForm(false)} >
                           Close
                         </Button>
                         
                       </Row>
                      </Col>
                      </Form>
            </CardBody>
              )}
          </Formik>
         </Card>
        </div>
    )
}
