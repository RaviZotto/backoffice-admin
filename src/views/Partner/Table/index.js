import React,{ useContext, Fragment, useEffect, useState } from "react";
import CustomHeader from "./Header";
import Avatar from "@components/avatar";

// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge } from "reactstrap";
import Loading from "@core/components/spinner/Loading-spinner";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import CustomInput from "reactstrap/lib/CustomInput";
import { useDispatch, useSelector } from "react-redux";
import { GET_PARTNER, STORE_PARTNER_OBJ, TOGGLE_PARTNER } from "../../../redux/actions/storeMerchant.";
import { makeVar } from "@apollo/client";
import { userDetails } from "../../../utility/Utils";
export const partnerObj=makeVar({});
const Table = ({setForm}) => {
  const dispatch=useDispatch();
  const{partners}=useSelector(state=>state.merchant);
  const [loading,setLoading]=useState(true);
  const [partner,setPartner]=useState([]);
  
  useEffect(()=>{
    //console.log(partners)
    if(!userDetails) userDetails=JSON.parse(localStorage.getItem('userDetails'));
    if(!partners.length)dispatch(GET_PARTNER(()=>setLoading(false)));
    else setPartner([...partners]);setLoading(false);
  },[partners.length,partners])
  
  if(loading) return <Loading/>

  const renderClient = (row, i) => {
    const stateNum = i % 6,
      states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
      color = states[stateNum];
  
    return <Avatar color={color || "primary"} className="mr-1" content={row.comp_name || "John Doe"} initials />;
  };

  const columns = [
    {
      name: "Name",
      selector: "name",
      sortable: true,
      minWidth: "110px",
      
      cell: (row,i) => {
        // let date = moment(row.date_of_transaction);
        return (
          <Fragment>
             {renderClient(row, i)}
         <span className='text-nowrap' >{row.comp_name}</span>
          </Fragment>
        );
      },
    },
    {
      name: "Phone",
      selector: "phone",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.phone_code+row.phone}</span>
        </Fragment>
      ),
    },
    {
      name: "Email",
      selector: "email",
      minWidth: "170px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span className='text-wrap '>{row.email}</span>
        </Fragment>
      ),
    },

    {
      name: "Action",
      maxWidth: "50px",
      selector: "",
      sortable: true,
      cell: (row) => (
        <Fragment>
        <div className="d-flex  " style={{padding:'1em'}} >
          <Edit className="cursor-pointer" disabled={userDetails.role_type&& userDetails.role_type!='admin' && !userDetails['merchant'].includes('U')}  onClick={el=>{el.preventDefault();dispatch(STORE_PARTNER_OBJ(row.id));setForm(true)}} size={15} />
          {/* {rowId==row.merchant_id&&Delete?<Spinner size="sm" />:<Trash size={15} className=" cursor-pointer" onClick={e=>{setRowId(row.merchant_id);setDelete(true)}} />} */}
        </div>
        </Fragment>
      ),
    },
    {
      name: "Status",
      minWidth: "110px",
      selector: "status",
      sortable: true,
      cell: (row) => (
        <CustomInput
          type="switch"
          className={row.id ? "custom-control-success" : ""}
          id={`status-${row.id}`}
         defaultChecked={row.status==1 ? true: false}
          name="customSwitch"
          disabled={userDetails.role_type&&userDetails.role_type!='admin' && !userDetails['partner'].includes('U')} 
          onChange={el=>dispatch(TOGGLE_PARTNER(row.id))}
          inline
        />
      ),
    },
  ];

  const dataToRender = () => {
    let tempData=partner;
 
    return tempData;
  };

  return (
    <div className="invoice-list-wrapper">
      {/* <CustomHeader  /> */}
      <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>
    </div>
  );
};

export default Table;
