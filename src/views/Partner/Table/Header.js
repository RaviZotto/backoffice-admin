import React from "react";
import { Fragment } from "react";
import { PlusCircle, Search } from "react-feather";
import { Row, Col, Button, Input, Card } from "reactstrap";
const CustomHeader = ({ history }) => {
  return (
    <Fragment>
      <Button.Ripple
        color="primary"
        className="pl-1 mb-2"
        onClick={(e) => history.push("/Merchant-manage")}
      >
        <PlusCircle size={15} className="mr-1" />
        Add Merchant
      </Button.Ripple>
      <Card>
        <div className='d-flex col-4 align-items-center p-1 '> 
        <Search/>       
        <Input name='serachQuery' style={{border:'none'}} placeholder='search by name' />
        </div>
        </Card>
    </Fragment>
  );
};

export default CustomHeader;
