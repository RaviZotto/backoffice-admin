import React ,{useState} from 'react'
import Table, { partnerObj } from './Table'
import { SlideDown } from "react-slidedown";
import "react-slidedown/lib/slidedown.css";
import {Button} from 'reactstrap'
import { User } from 'react-feather';
import AddPartner from './AddPartner';
import { useDispatch } from 'react-redux';
import { CLR_PARTNER_OBJ } from '../../redux/actions/storeMerchant.';
import { userDetails } from '../../utility/Utils';
export default function index() {
   const[partnerForm,setPartnerForm]=useState(false);
   const dispatch=useDispatch();
   const {role_type,partner}=userDetails||{};
    return (
        <div>
         <Button.Ripple  disabled={role_type&&role_type!='admin' && !partner.includes('C')} color='primary' className='mb-1' onClick={el=>{setPartnerForm(true);dispatch(CLR_PARTNER_OBJ())}} ><User size={15} className='mr-1'/>Add Partner</Button.Ripple>   
         {partnerForm ? (
        <SlideDown>
          <AddPartner setForm={setPartnerForm} />
        </SlideDown>
      ) : null}
         <Table setForm={setPartnerForm} />
        </div>
    )
}
