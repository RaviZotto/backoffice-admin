import React, { useEffect, useState } from 'react';
import Transaction from './Transaction';
import ExchangeRate from './ExchangeRate'
import { GET_CURRENCY, GET_EXCHANGE_RATE, GET_GATEWAY, SAVE_TRANSACTION_FEE } from '../../redux/actions/transfer';
import { GET_PARTNER, UPDATE_MERCHANT } from '../../redux/actions/storeMerchant.';
import { useDispatch, useSelector } from 'react-redux';
import Loading from "@core/components/spinner/Loading-spinner";
const index = () => {
    const dispatch = useDispatch()
    const {partners,merchants} = useSelector(state=>state.merchant);
    const {gateway,currencies,transactionFee,exchange_rate} = useSelector(state=>state.transfer);
    const [loading,setLoading]=useState(true);
    const [refetch, setRefetch]=useState(false);
    useEffect(() => {
        setLoading(true); 
     
        if(!exchange_rate){dispatch(GET_EXCHANGE_RATE(()=>setLoading(false)))}
        if(!partners.length){dispatch(GET_PARTNER(()=>setLoading(false)))}
        if(!merchants.length){dispatch(UPDATE_MERCHANT(()=>setLoading(false)))}
        if(!gateway.length){dispatch(GET_GATEWAY(()=>setLoading(false)))}
        if(!currencies.length){dispatch(GET_CURRENCY(()=>setLoading(false)))}
        if(!transactionFee||!transactionFee.length){dispatch(SAVE_TRANSACTION_FEE(()=>setLoading(false)));}
        setLoading(false);
    })
    if(loading) return <Loading/>
    return (
        <div>
        <Transaction />    
        {/* <ExchangeRate/> */}
        </div>
    )
}

export default index
