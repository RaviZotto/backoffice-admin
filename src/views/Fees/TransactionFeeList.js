import React, { Fragment, useEffect, useState } from "react";
import {
  Card,
  Col,
  Input,
  Button
} from "reactstrap";
import { ChevronDown, Edit, MoreVertical, PlusCircle } from "react-feather";
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import DataTable from "react-data-table-component";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { UPDATE_MERCHANT } from "../../redux/actions/storeMerchant.";
import Loading from "@core/components/spinner/Loading-spinner";
import Avatar from "@components/avatar";
import { DropdownItem } from "reactstrap/lib";
import SlideDown from "react-slidedown";
import CustomOption from "./CustomOption";
import { GET_CURRENCY, GET_GATEWAY, SAVE_TRANSACTION_FEE } from "../../redux/actions/transfer";




const Table = ({ merchants, setAction,transactionFee=[] ,setForm,merchantId}) => {
  const renderClient = (row, i) => {
    const stateNum = i % 6,
      states = [
        "light-success",
        "light-danger",
        "light-warning",
        "light-info",
        "light-primary",
        "light-secondary",
      ],
      color = states[stateNum];

    return (
      <Avatar
        color={color || "primary"}
        className="mr-1"
        content={row.comp_name || "John Doe"}
        initials
      />
    );
  };

  const columns = [
    {
      name: "Name",
      selector: "comp_name",
      sortable: true,
      minWidth: "330px",
      cell: (row, i) => {
        const merchantObj=merchants.find(e=>e.merchant_id == row.merchant_id)||{}
        return (
          <Fragment>
            <div className="mt-1">
              {renderClient(merchantObj, i)}
              <span className="text-nowrap">{merchantObj.comp_name}</span>
            </div>
          </Fragment>
        );
      },
    },
    {
      name: "Currency",
      selector: "currency",
      minWidth: "50px",
      sortable: true,
      cell: (row) => (
        <Fragment>
          <span>{row.currency}</span>
        </Fragment>
      ),
    },
    {
      name: "gateway",
      selector: "gateway",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
          <span>{row.gateway}</span>
        </Fragment>
      ),
    },
    {
      name: "Fee",
      selector: "fee",
      minWidth: "50px",
      sortable: true,
      cell: (row) => (
        <Fragment>
          <span>{row.fee}</span>
        </Fragment>
      ),
    },
    {
      name: "Action",
      selector: "action",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         <Edit  size={15} className='cursor-pointer' onClick={e=>{setAction({...row});setForm(true)}} />
        </Fragment>
      ),
    },
  ];

  const dataToRender = () => {
      let txnFee=[...transactionFee];
      if(merchantId){txnFee=txnFee.filter(e=>e.merchant_id==merchantId);}
      return txnFee;
  };

  return (
    <div className="invoice-list-wrapper mt-1  relative">
     
      <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>
    </div>
  );
};

const TransactionFeeList = () => {
  const [merchantId,setMerchantId]=useState(0);
  const dispatch = useDispatch();
  const { merchants } = useSelector((state) => state.merchant);
  const { gateway, currencies, transactionFee } = useSelector(
    (state) => state.transfer
  );
  const[refetch, setRefetch]=useState(false)
  const [loading, setLoading] = useState(true);
  const [action, setAction] = useState({});
  const [form,setForm]=useState(false);
  useEffect(() => {
     if(refetch){dispatch(SAVE_TRANSACTION_FEE(()=>setRefetch(false)))}
     if(!merchants.length)dispatch(UPDATE_MERCHANT(()=> setLoading(false)))
     if(!gateway.length)dispatch(GET_GATEWAY(()=> setLoading(false)))
     if(!currencies.length)dispatch(GET_CURRENCY(()=> setLoading(false)))
    if( !transactionFee || !transactionFee.length)dispatch(SAVE_TRANSACTION_FEE(()=>setLoading(false)));
    else setLoading(false);
    
  },[refetch]);

  if (loading) return <Loading />;

  return (
    <div className='d-flex flex-column flex-wrap'>
      <Button.Ripple  color='primary' onClick={e=>setForm(true)}><PlusCircle size={15} color='white' /> Add</Button.Ripple> 
      {form? (
        <SlideDown>
          <CustomOption form={true} setAction={setAction}  setRefetch={setRefetch}  action={action} merchants={merchants} setForm={setForm} gateway={gateway} currencies={currencies} />
        </SlideDown>
      ) : null}
      <Header  setMerchantId={setMerchantId} merchants={merchants} />
      <Table  merchantId={merchantId} setForm={setForm} action={action}merchants={merchants} transactionFee={transactionFee} setAction={setAction} />
    </div>
  );
};

export default TransactionFeeList;


export const Header =({setMerchantId,merchants})=>{ 
  return ( 
     <div className="card p-1 mt-1">
       <Col md={6} sm={12}>
         <Input
          type='select'
           onChange={e=>setMerchantId(e.target.value)}
         >
          <option label="select Merchant" value={0} />
          {merchants.map(res=>
            <option  label={res.comp_name} value={res.merchant_id}/>
            )}
            
         </Input>
       </Col> 
       
     </div>
  )
}