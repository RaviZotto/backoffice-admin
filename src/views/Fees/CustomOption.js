import React, { useEffect, useState }from 'react';
import { CardFooter,FormGroup ,Col, CardBody,Card,Button,Table ,Input} from 'reactstrap';
import { CardText } from 'reactstrap/lib';
import {RefreshCcw, Trash} from 'react-feather'
import { addOrUpdateTransactionFee, Axios, deleteTransactionfee } from '../../Api';
import notification from '../../components/notification';
import { userDetails } from '../Merchant';
import {AvForm,AvField} from 'availity-reactstrap-validation-safe';
import { graphqlServerUrl } from '../../App';
const CustomOption = ({setAction,action,setForm,currencies,gateway,merchants,setRefetch}) => {
    
    return (
      <>
        <Card className='relative '>
            <CardBody>
            
            <AddTransactionFee currencies={currencies} gateway={gateway} action={action } merchants={merchants} setRefetch={setRefetch} />
            
            </CardBody>
           
        </Card>
        <Button.Ripple color="primary" onClick={e=>{setAction({});setForm(false)}}>
          Close
        </Button.Ripple>
        </>
    )
}

export default CustomOption



const AddTransactionFee=({gateway,action,currencies,merchants,setRefetch})=>{ 
  
  const [loading,setLoading]=useState(false);
  const [key,setKey]=useState([]);
  const handleChange=(action)=>{if(key.includes(action))return;
    setKey([...key,action])
  }
  const onSubmit= (event, error, values) => {
    event.preventDefault();
    if (error.length) return;
    //console.log(values);
    setLoading(true);
    if(!action.id){
    addOrUpdateTransactionFee({data:{...values},action:'add',merchant_id:values.merchant_id},({err,res})=>{
      if(err)notification({type:'error',message:err,title:'Error'});
      else {notification({type:'success',message:'Successfully update',title:'Transaction Fee'})
     setLoading(false);
     setRefetch(true);
    }
     
  })
}else{ 
  let dataObj={};
  for(let keys of key){
    dataObj[`${keys}`]=values[`${keys}`];
  }
  addOrUpdateTransactionFee({data:{...dataObj,id:action.id},action:'update'},({err,res})=>{
    if(err)notification({type:'error',message:err,title:'Error'});
    else {notification({type:'success',message:'Successfully update',title:'Transaction Fee'})
    setRefetch(true);    
    setLoading(false);}
  });
}
  };
    return (
         <AvForm onSubmit={onSubmit}>
             {!action.id?
               <FormGroup tag={Col}>
               
            <AvField required label="Merchant" name="merchant_id" type="select"
             
            >

              <option label="select--" value="" defaultChecked />
              {merchants.map((e) => (
                <option label={e.comp_name} value={e.merchant_id} />
              ))}
            </AvField>
            </FormGroup>: null}
            <FormGroup tag={Col}>
            <AvField required label="Currency" name="currency" type="select"
              onChange={e=>handleChange(e.target.name)}
             value={action.currency}
            >

              <option label="select--" value="" defaultChecked />
              {currencies.map((res) => (
                <option label={res.currency} value={res.currency} />
              ))}
            </AvField>
            <AvField type="select" label="Gateway" name="gateway" value={action.gateway}  onChange={e=>handleChange(e.target.name)}>
              <option label="select--" value="" defaultChecked />
              {gateway.map((res) => (
                <option
                  label={res.gateway_name.toUpperCase()}
                  value={res.gateway_name.toUpperCase()}
                />
              ))}
            </AvField>
          </FormGroup>
          <FormGroup tag={Col}>
            <AvField
              required
              type="number"
              label="Fixed Fee"
              name="fixed_fee"
              value={action.fixed_fee}
              onChange={e=>handleChange(e.target.name)}
            />
            <AvField type="number" required label="Fee" name="fee" onChange={e=>handleChange(e.target.name)} value={action.fee} />
          </FormGroup>
          <hr/>
          <div className='d-flex justify-content-end'>
            <Button.Ripple type="submit" color="primary"  disabled={loading} >{action.id?'Update':'Add'}</Button.Ripple>
          </div>
         </AvForm>
    )
}



// const EditTransactionFee=({merchantId, txnFeeData})=>{ 
    
//         const updateTxnfee = ({ id, fee, fixed_fee }) => {
         
//           let dataObj = txnFeeData.find((res) => res.id == id);
//           if (dataObj.fixed_fee != fixed_fee || dataObj.fee != fee) {
//             addOrUpdateTransactionFee(
//               { data: { id, fee, fixed_fee } },
//               ({ err, res }) => {
//                 if (err)
//                   notification({ type: "error", title: "Error", message: err });
//                 else
//                   notification({ type: "success", message: "Successfully updated!" });
                
//               }
//             );
//           }else {notification({ type: "error", title: "Error", message:'Nothing changed'})}
//           // //console.log(id,fixed_fee,fee)
//         };
//         const deleteTxnFee = (id) => {
//           deleteTransactionfee(id, ({ err, res }) => {
//             if (err) notification({ type: "error", title: "Error", message: err });
//             else {notification({ type: "success", message: "Successfully updated!" });
//                   setTxnFeeArr(txnFeeArr.filter(e=>e.id!=id));}
//           });
//         };
      
//         const [txnFeeArr, setTxnFeeArr] = useState([]);
//         useEffect(() => {
//           setTxnFeeArr(txnFeeData.filter((e) => e.merchant_id == merchantId));
          
//         }, [merchantId]);
      
//         return (
//           <Table responsive className='relative'>
//             <thead>
//               <th>Gateway</th>
//               <th>Currency</th>
//               <th>Fixed Fee</th>
//               <th>Fee</th>
//               <th>Update</th>
//               <th>Delete</th>
//             </thead>
//             {txnFeeArr.length ? (
//               <tbody>
//                 {txnFeeArr.map((res, i) => (
//                   <tr key={i}>
//                     <td>
//                       <CardText>{res.gateway}</CardText>
//                     </td>
      
//                     <td>
//                       <CardText>{res.currency}</CardText>
//                     </td>
      
//                     <td style={{ minWidth: "100px" }}>
//                       <Input
//                         name="fixed_fee"
//                         type="number"
//                         className="text-center"
//                         defaultValue={res.fixed_fee}
//                         onChange={el=>{  let newArr=[...txnFeeArr];  newArr[i]={...newArr[i],fixed_fee:el.target.value};   setTxnFeeArr([...newArr]) }  }
//                       />
//                     </td>
      
//                     <td className="" style={{ minWidth: "100px" }}>
//                       <Input
//                         name="fee"
//                         type="number"
//                         className="text-center"
//                         defaultValue={res.fee}
//                         onChange={el=>{ let newArr=[...txnFeeArr];  newArr[i]={...newArr[i],fee:el.target.value};   setTxnFeeArr([...newArr]) }} 
//                       />
//                     </td>
//                     <td>
//                       <Button.Ripple
//                         color="primary"
//                         onClick={(el) =>
//                           updateTxnfee({ ...txnFeeArr.find((el) => el.id == res.id) })
//                         }
//                         disabled={
//                           userDetails.role_type &&
//                           userDetails.role_type != "admin" &&
//                           !userDetails["settings"].includes("U")
//                         }
//                       >
//                         <RefreshCcw size={15} className="cursor-pointer" />
//                       </Button.Ripple>
//                     </td>
      
//                     <td>
//                       <Button.Ripple
//                         color="primary"
//                         onClick={(el) => deleteTxnFee(res.id)}
//                         disabled={
//                           userDetails.role_type &&
//                           userDetails.role_type != "merchant" &&
//                           !userDetails["settings"].includes("D")
//                         }
//                       >
//                         <Trash size={15} className="cursor-pointer" />
//                       </Button.Ripple>
//                     </td>
//                   </tr>
//                 ))}
//               </tbody>
//             ) : (
//               <tbody className="d-flex justify-content-center ">
//                 <td className="mx-auto">No fees added yet</td>
//               </tbody>
//             )}
//           </Table>
//         );
      
      
// }