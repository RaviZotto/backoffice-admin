import React, { useEffect, useRef, useState } from "react";
import { AvForm, AvInput, AvField } from "availity-reactstrap-validation-safe";
import { useDispatch, useSelector } from "react-redux";
import { addOrUpdateTransactionFee, Axios, deleteTransactionfee } from "../../Api";
import { AxiosError } from "axios";
import { graphqlServerUrl } from "../../App";
import {
  Card,
  FormGroup,
  Button,
  Col,
  CardBody,
  Table,
  CardFooter,
  CardText,
  Input,
} from "reactstrap";
import notification from "../../components/notification";
import { userDetails } from "../Merchant";
import { RefreshCcw, Trash } from "react-feather";
import ExchangeRate from "./ExchangeRate";

function Transaction() {
  const [loading, setLoading] = useState({ l1: false, l2: true });
  const [type, setType] = useState("all");
  const { partners, merchants } = useSelector((state) => state.merchant);
  const { gateway, currencies, transactionFee } = useSelector(
    (state) => state.transfer
  );
  const [merchantId, setMerchantId] = useState(0);

  const avFormRef = useRef();

  const onSumbitTxnFee = (event, error, values) => {
    event.preventDefault();
    if (error.length) return;
    //console.log(values);
    setLoading({ ...loading, l1: true });
    let txnObj = {};
    txnObj["fixed_fee"] = values.fixed_fee;
    delete values.fixed_fee;
    txnObj["currency"] = values.currency;
    delete values.currency;
    txnObj["fee"] = values.fee;
    delete values.fee;
    txnObj["gateway"] = values.gateway;
    delete values.gateway;

    Axios.post(`${graphqlServerUrl}/admin/saveTransactionFee`, {
      ...values,
      txnObj,
    })
      .then((res) => {
        if (res.status == 200 && res.data)
          notification({
            title: "Transaction Fee",
            type: "success",
            message: "Saved successfully",
          });
        setLoading({ ...loading, l1: false });
      })
      .catch((e: AxiosError) => {
        notification({
          title: "Transaction Fee",
          message: e.response ? e.response.data : e.toString(),
          type: "error",
        });
        setLoading({ ...loading, l1: false });
      });
  };

  return (
    <Card className="w-50 flex mx-auto ">
      <AvForm key="2" onSubmit={onSumbitTxnFee} ref={avFormRef.current}>
        <CardBody>
       
          <FormGroup tag={Col}>
            <AvField
              requied
              label="Type"
              name="type"
              type="select"
              onChange={(e) => setType(e.target.value)}
            >
              <option value={null} label={"select"} />
              <option label="All" value={"all"} />
              <option label="partner" value={"partner"} />
              
            </AvField>
          </FormGroup>
          <FormGroup tag={Col}>
            {type != "all" ? (
              <AvField
                required
                label={type.toUpperCase()}
                name={type + "_id"}
                type="select"
                onChange={(e) => {
                  if (type == "merchant") setMerchantId(e.target.value);
                  else setMerchantId(0);
                }}
              >
                <option value={null} label={"select"} />
                {type == "partner"
                  ? partners.map((e) => (
                      <option label={e.comp_name} value={e.id} />
                    ))
                  : merchants.map((e) => (
                      <option label={e.comp_name} value={e.merchant_id} />
                    ))}
              </AvField>
            ) : null}
          </FormGroup>

          <FormGroup tag={Col}>
            <AvField required label="Currency" name="currency" type="select">
              <option label="select--" value="" defaultChecked />
              {currencies.map((res) => (
                <option label={res.currency} value={res.currency} />
              ))}
            </AvField>
            <AvField type="select" label="Gateway" name="gateway">
              <option label="select--" value="" defaultChecked />
              {gateway.map((res) => (
                <option
                  label={res.gateway_name.toUpperCase()}
                  value={res.gateway_name.toUpperCase()}
                />
              ))}
            </AvField>
          </FormGroup>
          <FormGroup tag={Col}>
            <AvField
              required
              type="number"
              label="Fixed Fee"
              name="fixed_fee"
            />
            <AvField type="number" required label="Fee" name="fee" />
          </FormGroup>
        </CardBody>

        <CardFooter>
          <div className=" d-flex col-12 justify-content-end">
            <Button color="primary" disabled={loading.l1} type="submit">
              {loading.l1 ? "Saving..." : "Save"}
            </Button>
            
          </div>
           <hr/>
          <ExchangeRate />  
        </CardFooter>
        
      </AvForm>
    </Card>
  );
}

export default Transaction;
