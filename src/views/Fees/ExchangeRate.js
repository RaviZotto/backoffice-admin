import React, { useEffect, useState } from 'react';
import {Row,FormGroup, Col, Button,Card,CardBody,Label,Input} from 'reactstrap';
import { useDispatch, useSelector } from "react-redux";
import { GET_EXCHANGE_RATE, UPDATE_EXCHANGE_RATE } from "../../redux/actions/transfer";
import { CardFooter } from 'reactstrap/lib';
function ExchangeRate() {
    const dispatch=useDispatch();
    const {exchange_rate} = useSelector(state=>state.transfer);
    const [exchangeRate,setExchangeRate]=useState(0);
    
    useEffect(()=>{
        //console.log(exchange_rate);
        setExchangeRate(exchange_rate);     
    },[exchange_rate])

    return (
        <div className="d-flex justify-content-between align-items-center">
        <FormGroup tag={Col}>
                <Label>Exchange Rate</Label>
                <Input   type="number"  onChange ={e=>setExchangeRate(e.target.value)}  value={exchangeRate} name='exchange_rate'  className=" form-control " />
                
                
               </FormGroup> 
               
               <Button className="mt-1"  color='primary' disabled={exchange_rate==exchangeRate}  onClick={e=>dispatch(UPDATE_EXCHANGE_RATE(exchangeRate,()=>dispatch(GET_EXCHANGE_RATE())))} >Update</Button>
               </div>
      
    )
}

export default ExchangeRate
