import React,{ useRef, Fragment,useLayoutEffect, useEffect, useState } from "react";
import Avatar from "@components/avatar";
import moment from 'moment'
import { selectThemeColors, countries as countryOptions } from "@utils";
// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash, Trash2, PlusCircle, Cpu } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge,CardBody,FormGroup,Button, Input, Label } from "reactstrap";
import Loading from "@core/components/spinner/Loading-spinner";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import { useDispatch, useSelector } from "react-redux";
import { ADD_TRANSFERFEE, DEL_TRANSFERFEE, GET_CURRENCY, STORE_TRANSFERFEE, UPDATE_TRANSFERFEE } from "../../redux/actions/transfer";
import Select from "react-select";
import { Formik ,Field,Form, ErrorMessage} from "formik";
import SlideDown from "react-slidedown";
import Row from "reactstrap/lib/Row";
import  currencyCodes  from "../../utility/extras/currencyCodes.json";
import Col from "reactstrap/lib/Col";
import Spinner from "reactstrap/lib/Spinner";
import * as Yup from "yup";
import { makeVar } from "@apollo/client";

import { UPDATE_MERCHANT } from "../../redux/actions/storeMerchant.";
import userPermission from "@hooks/userPersmission";
import { temperature } from "chroma-js";
export const rowData=makeVar({});
const TransferFee = () => {
    const {userDetails} = userPermission();
    const dispatch=useDispatch();
    const {role_type}=userDetails||{};
    const [loading,setLoading]=useState(true);
    const {transferFee,currencies}=useSelector(state=>state.transfer);
    const {merchants}=useSelector(state=>state.merchant);
    const [Form,setForm]=useState(false);
    const [action,setAction]=useState({});
    const[merchantId,setMerchantId]=useState(null);

   
    useEffect(()=>{
       
     if(!merchants.length)dispatch(UPDATE_MERCHANT(() => setLoading(false)));
    if(!transferFee.length)dispatch(STORE_TRANSFERFEE(()=>setLoading(false)));
    if(!currencies.length)dispatch(GET_CURRENCY(()=>setLoading(false)))
    else setLoading(false);    
},[transferFee])
  

useLayoutEffect(() => {
  if(Form){
   document.querySelector('.buttonadd').style.marginTop ='25px';
  }




  return ()=>{
   const el = document.querySelector('.buttonadd');
   if(el)el.style.marginTop ='0px';
  }
}, [Form])



  if(loading) return <Loading/>

  const renderClient = (name, i) => {
    const stateNum = i % 6,
      states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
      color = states[stateNum];
  
    return <Avatar color={color || "primary"} className="mr-1" content={name|| "John Doe"} initials />;
  };


  const conditionalRowStyles = [
    {
      when: row => row.id == action?.id,
      style: {
        backgroundColor: '#7367f0',
        color: 'white',
        '&:hover': {
          cursor: 'pointer',
        },
      },
    }

  ];
  const columns = [
    {

      
      name: "Merchant",
      selector: "merchant_id",
      sortable: true,
      minWidth: "110px",
      
      cell: (row,i) => {
        // let date = moment(row.date_of_transaction);
        const merchantObj = merchants.find(e=>e.merchant_id==row.merchant_id)||{};
        return (
          <Fragment>
             {renderClient(merchantObj.comp_name, i)}
         <span className='text-nowrap' >{merchantObj.comp_name}</span>
          </Fragment>
        );
      },
    },
    {


      name: "Currency",
      selector: "currency",
      sortable: true,
      minWidth: "110px",
      
      cell: (row,i) => {
        // let date = moment(row.date_of_transaction);
        return (
         <span className='text-nowrap' >{row.currency}</span>
        
        );
      },
    },
    {
      name: "fee",
      selector: "fee",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.fee}</span>
        </Fragment>
      ),
    },
    {
        name: "Status",
        selector: "status",
        minWidth: "110px",
        sortable: true,
        cell: (row) => (
          <Fragment>
           
            <span className={`${row.status?"text-success":"text-danger"} fw-5`} >{row.status?"Active":"Inactive"}</span>
          </Fragment>
        ),
      },

      {
        name: "Created",
        selector: "created",
        minWidth: "50px",
        sortable: true,
        cell: (row) => {
            let date = moment(row.created);
         return ( <Fragment>
             <div class='break-word d-block d-flex flex-column'>  
            <span  >{date.format('YYYY-MM-DD')}</span>
            <span  >{date.format('HH:mm:ss')}</span>
            </div>
          </Fragment>)
        },
      },


    {
      name: "Action",
      maxWidth: "50px",
      selector: "",
      sortable: true,
      cell: (row,i) => (
        <Fragment>
        <div className="d-flex " style={{padding:'1em'}}>
          {/* <Trash2 className=" cursor-pointer mr-1"  onClick={el=>{el.preventDefault();dispatch(DEL_TRANSFERFEE(row.id))   }} size={15} /> */}
          <Edit className=" cursor-pointer" size={15} onClick={el=>{ el.preventDefault(); setAction(row);setForm(true)}} />
          {/* {rowId==row.merchant_id&&Delete?<Spinner size="sm" />:<Trash size={15} className=" cursor-pointer" onClick={e=>{setRowId(row.merchant_id);setDelete(true)}} />} */}
        </div>
        </Fragment>
      ),
    },
    
  ];

  const dataToRender = () => {
    let tempData=transferFee||[];
     
      if(merchantId && merchantId!='0'){tempData =transferFee.filter(e=>e.merchant_id==merchantId)};
      
      return tempData;
  };

  return (
    <>
         {Form ? (
        <SlideDown>
          <CustomForm currency={currencies} userDetails={userDetails} setAction={setAction}  setForm={setForm} action={action}  merchants={merchants}/>
        </SlideDown>
      ) : null}
     <Button.Ripple  className='mb-1 buttonadd'      disabled={role_type&&role_type!='admin' && !userDetails['transfer'].includes('U')}    color='primary' onClick={el=>{setForm(true);rowData({})}}>
      <PlusCircle size={15} className="mr-1" />
       Add Txn Fee
       </Button.Ripple>  
    <div className="invoice-list-wrapper">
     <Header setMerchant={setMerchantId} merchants={merchants}/> 
     <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable tableshow"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
            conditionalRowStyles={conditionalRowStyles}
          />
        </div>
      </Card>
    </div>
    </>
  );
};

export default TransferFee;

const CustomForm =({setForm,userDetails, action,merchants,setAction,currency:currencies})=>{ 
  const dispatch =useDispatch()
const [loading,setLoading]=useState(false); 
const [intialValue,setInitialValues]=useState(()=>({
  fee:0, 
  currency: 'GBP',
  fixed_fee:0, 
  fee_type:0,
  fx_fee:0
}))





useEffect(() => {
if(action.id){  
setInitialValues({...action})
console.log(action,34)
}
},[action.id])



const schema = Yup.object().shape({
  fee:Yup.number("Number is required").required(),
  fixed_fee:Yup.number("Number is required").required(),
  fee_type:Yup.number("Number is required").required(),
  fx_fee:Yup.number()

})

const handleSubmit=(value) => {
   
   setLoading(true);
   if(action.id){
    delete value.currency;
    let updateValues={};
    
    for(const [key,val] of Object.entries(value))if(value[key]!=action[key])updateValues[key]=val;
     console.log(updateValues);
    if(!Object.keys(updateValues).length)return notification({title:"nothing changed",type:"error"})
    dispatch(UPDATE_TRANSFERFEE({id:action.id,...updateValues},()=>setLoading(false)));
   }else{
    dispatch(ADD_TRANSFERFEE(value,()=>setLoading(false)))
   }
  }

return ( 
  <Card  >
    <CardBody >
      <Formik
      onSubmit={handleSubmit}
      initialValues={intialValue}
      validationSchema={schema}
      enableReinitialize
      >
        { ({values,errors,touched,setValue})=>( 
           <Form>
             <Row>
               {!action.id?
             <FormGroup tag={Col} md='12'>
                <label>Merchant</label>
                <Select
              theme={selectThemeColors}
              className="react-select"
              classNamePrefix="select"
              isDisabled={Object.keys(action).length}
              
              options={merchants.map(el=>({label:el.comp_name,value:el.merchant_id}))}
              isClearable={false}
              onChange={el=>values.merchant_id=el.value}
              />
               </FormGroup> :null
              } 
             <FormGroup tag={Col} md='6' sm='12'>
            <label>Currency</label>
             <Select
              theme={selectThemeColors}
              className="react-select"
              classNamePrefix="select"
              isDisabled={Object.keys(action).length}
              value={{label:values.currency,value:values.currency}}
              options={currencies.map(el=>({label:el.currency,value:el.currency}))}
              isClearable={false}

              onChange={el=>{values.currency=el.value;
        
              }}
              />
             </FormGroup>
             <FormGroup tag={Col} md='6' sm='12'>
               <label>Fee</label>
               <Field
                name={'fee'}
                id={'fee'}
                value={values.fee }
                className={`form-control ${errors.fee && touched.fee && "is-invalid"}`}
                />
                <ErrorMessage name="fee" component="div" className="field-error text-danger" />

             </FormGroup>
             <FormGroup tag={Col} md='4' sm='12'>
               <label>Fixed Fee</label>
               <Field
                name={'fixed_fee'}
                id={'fixed_fee'}
                value={values.fixed_fee}
                className={`form-control ${errors.fixed_fee && touched.fixed_fee && "is-invalid"}`}
                />
                <ErrorMessage name="fixed_fee" component="div" className="field-error text-danger" />

             </FormGroup>
             <FormGroup tag={Col} md='4' sm='12'>
               <label>Fee Type</label>
               <Input
                name={'fee_type'}
                type='select'
                id={'fee_type'}
                value={Number(values.fee_type)}
                onChange={el=>values.fee_type=el.target.value}
                >
                  <option value={0} label="Domestic" />
                  <option value={1} label="SWIFT" />
                </Input>
              
              

             </FormGroup>
             <FormGroup tag={Col} md='4' sm='12'>
               <label>Forex Fee</label>
               <Field
                name={'fx_fee'}
                id={'fx_fee'}
                value={values.fx_fee}

                className={`form-control ${errors.fx_fee && touched.fx_fee && "is-invalid"}`}
                />
                <ErrorMessage name="fx_fee" component="div" className="field-error text-danger" />
                 
              
              

             </FormGroup>
             </Row>
             <Row>
            <Col md={4} sm={12} >
             <Button.Ripple       disabled={loading|| action.id?userDetails?.role_type!='admin' && !userDetails['transfer']?.includes('U'):userDetails?.role_type!='admin' && !userDetails['transfer']?.includes('C') }    type="submit" className='mr-1' color="primary" outline >
            {loading ? <Spinner size="sm" color="primary" className="mr-1" /> : null}
            {action.id? "Update":"Add"}</Button.Ripple>
            <Button.Ripple onClick={e=>{e.preventDefault(); setForm(false);setAction({})} } className='mr-1' color="secondary" outline disabled={loading}>Close</Button.Ripple>
            </Col>
             </Row>
           </Form>

        )
          
        }
      </Formik>
    </CardBody>
  </Card>


)


}

const Header=({setMerchant,merchants})=>{ 
  return(
    <Card>
      <CardBody>      <Col md='6'>
        <Label>Merchant</Label>
       <Input
       type='select'
        onChange={e=>setMerchant(e.target.value)}
       >
        <option value={0} label="select --" />
       {merchants.map(e=>
        <option value={e.merchant_id} label={e.comp_name}/>
        )}
       </Input> 
      </Col>
      </CardBody>

    </Card>
  )
}