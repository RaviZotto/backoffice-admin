import React, {
  Fragment,
  useEffect,
  useState,
  createContext,
  useMemo,
} from "react";
import PerfectScrollBar from "react-perfect-scrollbar";
import { Button, Card, Row } from "reactstrap";
import { PlusCircle } from "react-feather";
import CardsModal from "./CardsModal";
import DebitsCards from "./DebitsCards";
import Table from "./Table";
import moment from "moment";
import CustomHeader from "./Table/Header"
import { useDispatch, useSelector } from "react-redux";
import Loading from "@core/components/spinner/Loading-spinner";
import {
  GET_PARTNER,
  UPDATE_MERCHANT,
} from "../../redux/actions/storeMerchant.";
import notification from "../../components/notification";
import { Axios, getAllTransactions, getFlairCardTransactions, getGiftsCards } from "../../Api";
import { GET_CLIENTS, GET_CURRENCY } from "../../redux/actions/transfer";
import { graphqlServerUrl } from "../../App";


export const GiftCard = createContext(null);

const Cards = (props) => {
  const dispatch = useDispatch();
  const { merchants, partners } = useSelector((state) => state.merchant);
  const { clients, currencies } = useSelector((state) => state.transfer);

  const [cards, setCards] = useState([]);
  const [loading, setLoading] = useState(true);
  const [refetch, setRefetch] = useState(false);
  const [merchant, setMerchant] = useState(0);
  // const [partner, setPartner] = useState(0);
  const [modal, setModal] = useState(false);
  // const [statusQuery, setStatusQuery] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const [selectedCard, setSelectedCard] = useState({});
  // const [transactions, setTransaction] = useState([]);
  const [dataRow, setDataRow] = useState([]);
  const[cardCategories,setCardCategories] = useState([]);
  const [loader, setLoader] = useState({ l1: false, l2: false });



  // useEffect(() => {
  //   if (merchant) {
  //     (async () => {
  //       try {
  //         setLoader({ ...loader, l2: true });
  //         const merchantObj = merchants.find(el => el.merchant_id == merchant);
  //         const { transactions: transArr } = await getFlairCardTransactions({ merchant_id: merchantObj.api_key, ...dates });
  //         setTransaction(transArr.length ? transArr : []);
  //         setLoader({ ...loader, l2: false });
  //       } catch (e) {
  //         setLoader({ ...loader, l2: false });
  //         notification({ title: "Error", message: typeof e == 'object' ? e.message : e, type: "error" });
  //       }
  //     })()

  //   }
  // }, [dates])




  useEffect(() => {
    if (loading || refetch) {
      if (!currencies.length) dispatch(GET_CURRENCY())
      if (!merchants.length) dispatch(UPDATE_MERCHANT(() => setLoading(false)));
      // if (!partners.length) dispatch(GET_PARTNER(() => setLoading(false)));
      if (!clients.length) dispatch(GET_CLIENTS(() => setLoading(false)));
    }
   const loadGifCardsContent=async()=>{
    try{
    if (merchant || refetch) {
      setLoader({ ...loader, l1: true })

       const {cards}= await  getGiftsCards(merchant);
       const {data:{card_categories}} = await Axios.get(`${graphqlServerUrl}/admin/getCardCategory?id=${merchant}`);

       setCardCategories(card_categories||[]);
       setCards(cards || []); setRefetch(false); setLoader({ ...loader, l1: false }) 
    }
    }catch(e) {
          notification({ title: 'Error', message: e.message, type: 'error' }); setRefetch(false); setLoader({ ...loader, l1: false })
        }
    }
  
  loadGifCardsContent();
  }, [refetch, merchant]);

  const handleSelect = (el) => { if (el.id == selectedCard.id) setSelectedCard({}); else setSelectedCard(el); }

  const giftCardMemo = useMemo(
    () => ({

    
      merchants,
      loader,
      dataRow,
      setDataRow,
      // transactions,
      // setTransaction,
      handleSelect,
      merchants,
      // partners,
      // setPartner,


      // setStatusQuery,
      searchQuery,
      currencies,
      setSearchQuery,
      // partner,
      merchant,
    
      setMerchant,
    
      cardCategories,
      setCardCategories,
      clients,
      setLoader,
      cards,
      setCards
    }),
    [  dataRow,cards,  clients, loader, currencies,   searchQuery,  merchant, merchants]
  );

  return (
    <GiftCard.Provider value={giftCardMemo}>

      <>

        <Button.Ripple
          color="primary"
          onClick={() => setModal(!modal)}
          className="ml-1 mb-1"
        >
          {" "}
          <PlusCircle size={12} className="mr-2" /> Add Cards
        </Button.Ripple>



        <CardsModal
          modalOpen={dataRow.id || modal}
          handleModal={() => {
            if (!dataRow.id) {
              setModal(!modal);
            } else { setDataRow({}) }
          }}
          refetch={() => setRefetch(true)}
        />

        <CustomHeader />

        {!loader.l1 ?
             <Table />
          : <Loading className="zIndex:10000" />}
    
      </>

    </GiftCard.Provider>
  );
};

export default Cards;
