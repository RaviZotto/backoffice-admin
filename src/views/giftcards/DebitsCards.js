
import React, { useContext } from "react";
import {CheckCircle, Edit, MoreVertical} from 'react-feather'
import {Card,CardBody, CardHeader, DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import { Button } from "reactstrap";
import "./index.scss";

import logo2 from "../../assets/images/logo/master-card2.png";
import { GiftCard } from ".";
import { Axios } from "../../Api";
import { graphqlServerUrl } from "../../App";

const DebitsCards = ({
  data,
  
  
}) => {

  const {handleSelect,selectedCard ,setDataRow,setCards,cards} = useContext(GiftCard);
  return (
    <div className='mr-1'
    onClick={()=>handleSelect(data)}
    >
     
      <Card
    
        className={ selectedCard.id==data.id? "card-congratulations text-bg-white":"text-bg-black" }
        style={{ width: "450px", cursor: "pointer" }}
      >
       
        <CardBody>
          <div className="d-flex flex-column">
          <div>Balance</div>
            <div className="d-flex justify-content-between">
          
            <h1 >   {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency:(data.currency?data.currency:'EUR'),
          })
            .format(data.balance)
            .replace(/^(\D+)/, "$1 ")}</h1>
             <UncontrolledDropdown className="chart-dropdown">
        <DropdownToggle
          color=""
          className="bg-transparent btn-sm border-0 p-50"
        >
          <MoreVertical size={18} className="cursor-pointer" color={selectedCard.id==data.id? "white":'black'} />
        </DropdownToggle>
        <DropdownMenu top>
          <DropdownItem
            className="w-100 d-flex text-center"
       
            onClick={(e) =>{
              setDataRow(data);
            
            }}
          >
           Edit
          </DropdownItem>
       
          <DropdownItem
            className="w-100 d-flex text-center"
       
            onClick={async(e) =>{
            try{  
            await  Axios.delete(`${graphqlServerUrl}/admin/deleteClientCard?id=${data.id}`);
            setCards(cards.filter(el=>el.id!=data.id));  
          }catch(e){
          
            }

       
            
            }}
          >
           Delete
          </DropdownItem>
             
          
  
        </DropdownMenu>
      </UncontrolledDropdown>
            </div>
            <div className="d-flex justify-content-between my-3">
              <div>
              <img src={  logo2} />
              </div>
              {/* <h3 style={{ color: "white", letterSpacing: "3px" }}>**** **** **** {data.truncated_pan}</h3> */}
            </div>
            <div className="d-flex">
              <div className="d-flex flex-column">
                <div className="font-small-2">VALID THRU</div>
                <h5 >{data.expiry_date}</h5>
              </div>
              <div className="d-flex flex-column mx-3">
                <div className="font-small-2">CARD HOLDER</div>
                <h5 >{data.customer_name}</h5>
              </div>
              <div className="d-flex flex-column">
                <div className="font-small-2">CARD STATUS</div>
                <h5 >
                  <CheckCircle size={15} style={{ marginRight: "0.5rem" }} />
                  Active
                </h5>
              </div>
            </div>
          </div>
        </CardBody>
      </Card>
    </div>
  );
};
export default DebitsCards;
