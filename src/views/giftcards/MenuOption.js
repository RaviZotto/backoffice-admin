import React, { useContext } from 'react'
import { MoreVertical } from 'react-feather'
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from "reactstrap";
import { GiftCard } from ".";
import { Axios } from "../../Api";
import { graphqlServerUrl } from "../../App";
const MenuOption = ({data}) => {
    const {cards,setCards,setDataRow} = useContext(GiftCard);
    return (
        <UncontrolledDropdown className="chart-dropdown">
            <DropdownToggle
                color=""
                className="bg-transparent btn-sm border-0 p-50"
            >
                <MoreVertical size={18} className="cursor-pointer" color={'black'} />
            </DropdownToggle>
            <DropdownMenu top>
                <DropdownItem
                    className="w-100 d-flex text-center"

                    onClick={(e) => {
                        setDataRow(data);

                    }}
                >
                    Edit
                </DropdownItem>

                <DropdownItem
                    className="w-100 d-flex text-center"

                    onClick={async (e) => {
                        try {
                            await Axios.delete(`${graphqlServerUrl}/admin/deleteClientCard?id=${data.id}`);
                            setCards(cards.filter(el => el.id != data.id));
                        } catch (e) {

                        }



                    }}
                >
                    Delete
                </DropdownItem>



            </DropdownMenu>
        </UncontrolledDropdown>
    )
}

export default MenuOption;



