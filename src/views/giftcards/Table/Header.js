import { Row,Card, Col, Button, Input, Label } from "reactstrap";
import Flatpickr from "react-flatpickr";
import { Search } from "react-feather";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import React,{ useContext, useState } from "react";
import moment from "moment";
import { GiftCard } from "..";
import { useSelector } from "react-redux";
import { getAllTransactions } from "../../../Api";
import notification from "../../../components/notification";


const CustomHeader = () => {
   const { dates, setTransaction,  setDates ,merchants,merchant, setMerchant,partners,setPartner,partner,setStatusQuery,setSearchQuery } = useContext(GiftCard);
  //   const [state, setState] = useState({
  //   start_date: dates.start_date,
  //   end_date: dates.end_date,
  // });

  return (
    <Card className="p-1 pr-0">
     <Row className=" justify-content-between mb-1  align-items-center">
     {/* <Col md={6} sm={12} className='ml-0 mb-1 mb-md-0'>
       <Label>Partner</Label>
     <Input
      onChange={e=>setPartner(e.target.value)}
      type='select'
      name='parnter'
      value={partner}
      >
        <option value={0} label="select--" />
        {partners.map(el=><option value={el.id} label={el.comp_name}  />)}
        </Input>
        </Col> */}
        <Col md={6} sm={12} className='mr-0' >
        
      <Input
      type='select' 
      name='merchant'
      onChange={e=>setMerchant(e.target.value)}
      value={merchant}
      >
        <option value={0} label="select Merchant--" />
        {merchants.map(el=><option value={el.merchant_id} label={el.comp_name}  />)}
        </Input>
        </Col>
        <Col md={6} sm={12}>
        <Input
          id="search-invoice"
          type="text"
          onChange={(e) => setSearchQuery(e.target.value)}
          placeholder="Search"
        />
        </Col>
     </Row>
    {/* <div className="row row-flex-wrap pr-sm-0">
      <div className="col-md-2 col-6  ">
        <Flatpickr
          className="form-control"
           value={dates.start_date}
          onChange={(e) =>
            setState({
              ...state,
              start_date: moment(e[0]).format("YYYY-MM-DD"),
            })
          }
        />
      </div>
      <div className="  col-md-2 col-6   ">
        <Flatpickr
          className="form-control "
           value={dates.end_date}
          onChange={(e) =>
            setState({ ...state, end_date: moment(e[0]).format("YYYY-MM-DD") })
          }
        />
      </div>
      <div className=" col-12 col-md-1 ">
        <Button
          className="w-100 mb-2 mt-1 mb-md-0 mt-md-0  "
          color="primary"
          // disabled={state.start_date==dates.start_date && state.end_date==dates.end_date}
           onClick={() => {setDates({ ...state })
    
          } }
        >
          <Search size={17} />
        </Button>
      </div>

      <div className="col-12 col-md-2  mb-sm-0 mb-1 ml-md-auto">
        <Input
          id="search-invoice"
          type="text"
          onChange={(e) => setSearchQuery(e.target.value)}
          placeholder="Search"
        />
      </div>
     
    </div> */}
  </Card>
  );
};

export default CustomHeader;
