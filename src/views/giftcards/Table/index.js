import React,{ useContext, Fragment, useEffect, useState } from "react";
import CustomHeader from "./Header";
import Loading from "@core/components/spinner/Loading-spinner";
// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge } from "reactstrap";
import moment from "moment";
import MenuOption from '../MenuOption'
import Avatar from "@components/avatar";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import { GiftCard } from "..";
import { formatMoney } from "../../../utility/Utils";
const Table = () => {
const{cards,searchQuery}=useContext(GiftCard);


const renderClient = (row, i) => {
  const stateNum = i % 6,
    states = ["light-success", "light-danger", "light-warning", "light-info", "light-primary", "light-secondary"],
    color = states[stateNum];

  return <Avatar color={color || "primary"} className="mr-1" content={row.customer_name || "John Doe"} initials />;
};

const columns = [
  {
    name: "name",
    selector: "customer_name",
    sortable: true,
    minWidth: "110px",
    cell: (row,i) => {
    
      return (
        <Fragment>
           {renderClient(row, i)}
       <span className='text-wrap'>{row.customer_name}</span>
        </Fragment>
      );
    },
  },
  {
    name: "email",
    selector: "email",
    minWidth: "110px",
    sortable: true,
    cell: (row) => (
      <Fragment>
       <p>{row.email}</p>
      </Fragment>
    ),
  },

  {
    name: "Card ID",
    selector: "card_id",
    sortable: false,
    minWidth: "130px",
    cell: (row) => {
      return <div className="font-small-3">{row.card_id}</div>;
    },
  },
  {
    name: "balance",
    selector: "balance",
    sortable: false,
    minWidth: "100px",
    cell: (row) => {
      return <div className="font-small-3">{formatMoney(row.balance)}</div>;
    },
  },
  {
    name: "currency",
    selector: "currency",
    sortable: false,
    minWidth: "200px",
    cell: (row) => {
      return <div className="font-small-3">{row.currency}</div>;
    },
  },
  {
    name: "Valid Thru",
    selector: "expiry_date",
    sortable: false,
    minWidth: "200px",
    cell: (row) => {
      return <div className="font-small-3">{row.expiry_date}</div>;
    },
  },
  {
    name: "Action",
    selector: "action",
    sortable: false,
    minWidth: "200px",
    cell: (row) => {
      return <MenuOption data={row} />;
    },
  },
  

];

  const dataToRender = () => {

    let tempData = cards||[];
    // if(merchant){
    //   const merchantObj = merchants.find(e=>e.merchant_id == merchant);

    //   tempData=tempData.filter(el=>el.merchant_id==merchantObj.api_key);}
 
  // if(selectedCard.id)tempData=tempData.filter(el=>el.card_id==selectedCard.card_id||el.card_id==selectedCard.card_code);    
  // console.log(tempData);
 if (searchQuery)
     console.log(searchQuery);
      tempData = tempData.filter((el) => {
        let temp = JSON.parse(JSON.stringify(el));
        let sq = searchQuery.toLowerCase();
      
        let searchFields = [
         
          "customer_name",
           "client_id",
          "card_id",
           "expiry_date",
          "balance",
          
        ];
        for (let field of searchFields)
          if (`${temp[field]}`.includes(sq) || `${temp[field]}`.startsWith(sq))
            return true;
        return false;
      });
   
    return tempData;
  };

  return (
    
    <div className="invoice-list-wrapper">
      
    <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>

    </div>
  );
};

export default Table;
