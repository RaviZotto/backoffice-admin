import AvForm from "availity-reactstrap-validation-safe/lib/AvForm";
import React, { useContext, useEffect, useState } from "react";
import {
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  ModalFooter,
  Row,
  Col,
} from "reactstrap";
import FormGroup from "reactstrap/lib/FormGroup";
import Label from "reactstrap/lib/Label";
import { X } from "react-feather";
import AvInput from "availity-reactstrap-validation-safe/lib/AvInput";
import "@styles/react/libs/react-select/_react-select.scss";
import Spinner from "reactstrap/lib/Spinner";
import {  phoneCodes } from "../../utility/Utils";
import notification from "../../components/notification";
import { GiftCard } from ".";
import { saveClientCards, updateClientCards } from "../../Api";
import moment from "moment";
const CardsModal = ({ modalOpen, handleModal, refetch }) => {
  const { clients, merchants, setMerchant, merchant,currencies,cardCategories,setDataRow,dataRow } = useContext(GiftCard);
  //console.log(modalOpen);
  // alert(modalOpen)
  const [client, setClient] = useState(0);
  useEffect(()=>{
  if(dataRow.id){
    const clientObj=clients.find(el=>el.client_id==dataRow.client_id);
    setClient(clientObj?clientObj:{});
  }else{
    setClient({});
  }

  },[dataRow])


  const CloseBtn = (
    <X className="cursor-pointer" size={15} onClick={handleModal} />
  );

  const [loader, setLoader] = useState(false);
  const handleSumbit = async (event, errors, value) => {
    if (errors.length) return;
    event.preventDefault();
    try {
    
      setLoader(true);
      if(!dataRow.id){
        const dateFormat = moment(value.expiry_date,'YYYY-MM-DD');
        const year = dateFormat.format('YY');
        const month = dateFormat.format('MM');
        value.expiry_date=month+year;
      let merchantObj=merchants.find(e=>e.merchant_id==merchant)||{};
      //console.log(merchantObj);
      value.merchant_id=merchantObj.api_key;
       
       await saveClientCards(value);
      }else{
        let updateObj={};
         value.mobile=value.phone_code+value.phone;
         delete value.phone_code;
         delete value.phone;
        for (const [key, val] of Object.entries(value)) {
          if(value[key]!=dataRow[key])updateObj[key]=val;
        }
     
        
        await updateClientCards(dataRow.id,value);
      }
       setLoader(false);
      //  refetch();
      handleModal();
    } catch (e) {
      notification({ title: "Error", message: e.toString(), type: "error" });
      setLoader(false);
      //console.log(e);
    }
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={handleModal}
      //   className='middle-lg'
      //   modalClassName='modal-slide-in'
      contentClassName="pt-0"
      className="modal-dialog-centered"
    >
      <ModalHeader className="" toggle={handleModal} close={CloseBtn} tag="div">
     {   (!dataRow.id?"Add":"Update")+" Card"}
      </ModalHeader>
      <AvForm onSubmit={handleSumbit}>
        <ModalBody>
          <Row>
         {  !dataRow.id?<FormGroup tag={Col} md='12'> 
            <Label>Merchant</Label>
              <AvInput
                type="select"
                required
                name="merchant_id"
                onChange={(e) =>{ setMerchant(e.target.value)}}
              
              >
                <option value={null} label="select merchant--" />
                {merchants.map((el) => (
                  <option label={el.comp_name} value={el.merchant_id}  />
                ))}
              </AvInput>


             
            </FormGroup>:null}
            <FormGroup tag={Col}>
            <Label>Card Category</Label>
              <AvInput
                type="select"
                
                name="category_id"
                value={dataRow.category_id}
              >
                <option value={null} label="select category --" />
                {cardCategories.map((el) => (
                  <option label={el.category_name} value={el.category_id}  />
                ))}
              </AvInput>
              <Label>Customer Name</Label>
              <AvInput
                type="text"
                value={client.client_name}
                className="form-control"
                name="customer_name"
              />
              <Label>Phone Code</Label>
              <AvInput type="select" name="phone_code" value={client.phone_code} required>
                <option value={null} label="select--" />
                {phoneCodes.map((el) => (
                  <option value={el.value} label={el.label} />
                ))}
              </AvInput>
              <Label>Currency</Label>
              <AvInput
              required
              name="currency"
              type="select"
              value={dataRow.currency}
              >
                <option value={null} label="select currency"/>
                {currencies.map((el) => <option value={el.currency} label={el.currency} />)}
              </AvInput>
              <Label>Expiry date</Label>
              <AvInput
               name='expiry_date'
               type={dataRow.id?'number':'date'}
               value={
                    dataRow.expiry_date
                 }
               
              />

        
            </FormGroup>
            <FormGroup tag={Col}>
              <Label>Clients</Label>
              <AvInput
                type="select"
                required
                name="client_id"
                value={dataRow.client_id}
                onChange={(e) => {
                  let exist = clients.find(
                    (el) => el.client_id == e.target.value
                  );
                  //console.log(exist);
                  setClient(exist ? exist : {});
                }}
              >
                <option value={null} label="select clients--" />
                {clients
                  .filter((e) => e.merchant_id == merchant)
                  .map((el) => (
                    <option label={el.client_name} value={el.client_id} />
                  ))}
              </AvInput>
              <Label>Email</Label>
              <AvInput
                value={client.client_email}
                required
                type="email"
                name="email"
              />

              <Label>Phone</Label>
              <AvInput required type="text" name="phone"  value={client.phone} />
             <Label>Balance</Label>
             <AvInput required  defaultValue={Number(dataRow.balance)?dataRow.balance:'0'} type="text" name="balance" />
             <Label>Card Number</Label>
              <AvInput
              value={dataRow.card_code}
               name='card_code'
               type='number'
               required
              />
            </FormGroup>
          </Row>
        </ModalBody>

        <ModalFooter>
          <div className="d-flex justify-content-center">
            <Button.Ripple
              className="mr-2"
              type="submit"
              color={"primary"}
              // disabled={loader|| userDetails.role_type!='admin' && !userDetails['merchant'].includes('C')}
            >
              {loader ? <Spinner size="sm" className="mr-1" /> : null}{" "}
              <span>{dataRow.id?'Update':'Add'}</span>
            </Button.Ripple>
            <Button.Ripple color={"danger"} onClick={handleModal}>
              Close
            </Button.Ripple>
          </div>
        </ModalFooter>
      </AvForm>
    </Modal>
  );
};
export default CardsModal;
