import React, { createContext, useEffect, useMemo, useState } from 'react'
import { getPaymentGateway } from '../../Api';
import notification from '../../components/notification';
import Loading from "@core/components/spinner/Loading-spinner";
import Table from './Table';
export const FeeSection=createContext();
function index() {
    const [gateway,setGateway]=useState([]);
      // const [transaction,setTransaction]=useState(false);
    const[loading,setLoading]=useState(true);
   
    useEffect(()=>{
     if(loading){
         
         getPaymentGateway(({err,res})=>{
            if(res)setGateway(res);
            else notification({title:'Error',message:err,type:'error'}) ;
            //console.log(gateway);
            
            setLoading(false);
            
        })
     }
     },[loading])
    const provider=useMemo(() => (
        {
         setGateway,
         gateway,
      }
    ), [gateway,setGateway])
    if(loading) return <Loading/>
    return (
        <FeeSection.Provider value={provider}>
        <Table provider={provider} />
       
        </FeeSection.Provider>
    )
}

export default index
