import React,{ useContext, Fragment, useEffect, useState } from "react";
import CustomHeader from "./Header";

// ** Third Party Components
import { ChevronDown, Eye, Edit,Trash } from "react-feather";
import DataTable from "react-data-table-component";
import { Card, Badge } from "reactstrap";
import Loading from "@core/components/spinner/Loading-spinner";
// ** Styles
import "@styles/react/apps/app-invoice.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import { merchantAllDetails } from "../../Merchant";
import { useReactiveVar } from "@apollo/client";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { UPDATE_MERCHANT } from "../../../redux/actions/storeMerchant.";

const Table = ({provider}) => {
  // const {setTransferModal,setTransactionModal,transferModal,transactionModal}=provider;
  const {transaction,merchants}=useSelector(state=>state.merchant);
  const dispatch =useDispatch();
 const history =useHistory();
  const store=useReactiveVar(merchantAllDetails); 
 const[merchantAll,setMerchantAll]=useState([]);
 const [loading,setLoading]=useState(true);
useEffect(()=>{
  
 dispatch(UPDATE_MERCHANT(()=>setLoading(false)));
},[])
 //  useEffect(()=>{
//      if(store.length){
//      //console.log(store,merchantAllDetails());
//      setMerchantAll([...store]);
//      }
//  },[store,merchantAllDetails().length])
useEffect(()=>{
  // //console.log(transaction,merchants,'123')
transaction.length&&setMerchantAll(transaction);

},[transaction,merchants])

  const columns = [
    {
      name: "Name",
      selector: "name",
      sortable: true,
      minWidth: "110px",
      cell: (row) => {
        // let date = moment(row.date_of_transaction);
        return (
          <Fragment>
         <span>{row.name}</span>
          </Fragment>
        );
      },
    },
    {
      name: "Phone",
      selector: "phone",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.phone_code+row.phone}</span>
        </Fragment>
      ),
    },
    {
      name: "Email",
      selector: "email",
      minWidth: "110px",
      sortable: true,
      cell: (row) => (
        <Fragment>
         
          <span>{row.email}</span>
        </Fragment>
      ),
    },

    {
      name: "Action",
      minWidth: "110px",
      selector: "",
      sortable: true,
      cell: (row) => (
        <div className="column-action d-flex align-items-center">
           <Eye  key={0} size={15} className="cursor-pointer "  onClick={e=>history.push(`/merchant-fee-manage?merchant_id=${row.merchant_id}`)} />
          
         </div>
      ),
    },
  ];

  const dataToRender = () => {

    let tempData=merchantAll;
 
    return tempData;
  };

if(loading) return <Loading/>

  return (
    <div className="invoice-list-wrapper">
      {/* <CustomHeader  /> */}
      <Card>
        <div className="invoice-list-dataTable">
          <DataTable
            className="react-dataTable"
            noHeader
            pagination
            columns={columns}
            responsive={true}
            sortIcon={<ChevronDown />}
            data={dataToRender()}
          />
        </div>
      </Card>
    </div>
  );
};

export default Table;
