import React from 'react';
import { Nav, NavItem, NavLink } from "reactstrap";
import { User, Settings } from "react-feather";

const Tabs = ({ activeTab, toggleTab }) => {
  return (
    <Nav className="nav-left" pills vertical>
      <NavItem>
        <NavLink active={activeTab === "1"} onClick={() => toggleTab("1")}>
          <User size={18} className="mr-1" />
          <span className="font-weight-bold">Transaction Fee</span>
        </NavLink>
      </NavItem>
      <NavItem>
   
      </NavItem> 
    </Nav>
  );
};

export default Tabs;
