import {
    Modal,
    ModalBody,
    ModalHeader,
    Button,
     Input,
    Col,
    Table,
  } from "reactstrap";
  import React, { useContext, useEffect, useState } from "react";
  import { PlusCircle, RefreshCcw, X, Trash } from "react-feather";
  import { addOrUpdateTransferFee, deleteTransferfee, getPaymentGateway, getTransactionFee, getTransferFee } from "../../../Api";
  import notification from "../../../components/notification";
  import Loading from "@core/components/spinner/Loading-spinner";
  import FormGroup from "reactstrap/lib/FormGroup";
  import CardText from "reactstrap/lib/CardText";
  import AvForm from "availity-reactstrap-validation-safe/lib/AvForm";
  import CardFooter from "reactstrap/lib/CardFooter";
  import AvField from "availity-reactstrap-validation-safe/lib/AvField";
  import Row from "reactstrap/lib/Row";
  import Spinner from "reactstrap/lib/Spinner";
  import queryString from 'querystring';
  function TransactionFee() {
    const [modal, setModal] = useState(false);
    const [transactionOpt, setTransactionOpt] = useState([]);
    const [loading, setLoading] = useState(true);
    const [refetch, setRefetch] = useState(false);
    const [gateway, setGateway] = useState([]);
    const merchant = queryString.parse(window.location.search);
    useEffect(() => {
      if (loading || refetch) {
        //console.log(gateway);
        getTransferFee(merchant['?merchant_id'],({ err, res }) => {
          if (err) {
            notification({ title: "Error", message: err, type: "error" });
            return;
          }
          res.sort((a, b) => a.gateway - a.gateway);
          setTransactionOpt(res);
          getPaymentGateway(({ err, res }) => {
            if (err)
              return notification({
                title: "Error",
                type: "error",
                message: err,
              });
            //console.log(res);
            setGateway(res);
            setRefetch(false);
            setLoading(false);
          });
        });
      }
    }, [refetch]);
  
    if (loading) return <Loading />;
    return (
      <div className="card">
        <Button.Ripple
          onClick={(e) => setModal(!modal)}
          color="primary"
          className="mt-1"
        >
          <PlusCircle size={15} /> <span>Add Fee </span>
        </Button.Ripple>
        <AddFee modalOpen={modal} refetch={el=>setRefetch(true)}  handleModal={(el) => setModal(!modal)}  merchant={merchant}  transactionOpt={transactionOpt}/>
        <FormComponent data={transactionOpt} refetch={el=>setRefetch(true)} />
      </div>
    );
  }
  
  const FormComponent = ({ data ,refetch}) => {
    const [state,setState]=useState(data.map(el=>({id:el.id,fee_type:el.fee_type,fee:el.fee})));
    const updateTxnfee=({id,fee,fee_type})=>{
        
        //console.log(id,fee,fee_type,'1',state);
        let dataObj=data.find(res=>res.id==id);
        //console.log(fee_type);
         if(dataObj.fee_type!=fee_type || dataObj.fee!=fee){
             addOrUpdateTransferFee({data:{id,fee,fee_type}},({err,res})=>{
             if(err)notification({type:'error',title:'Error',message:err})
             else notification({type:'success',message:'Successfully updated!'});
             refetch()
            })
            
         }
        // //console.log(id,fixed_fee,fee)
    }

    const deleteTransfee=(id)=>{
        deleteTransferfee(id,({err,res})=>{
            if(err)notification({type:'error',title:'Error',message:err})
             else notification({type:'success',message:'Successfully updated!'});
             refetch()
        })
    }

    return (
      <Table responsive className='mt-1'>
        <thead>
       
          <th>Currency</th>
          <th>Fee</th>
          <th>Fee Type</th>
          <th>Update</th>
          <th>Delete</th>
        </thead>
       {data.length?<tbody>
          { data.map((res,i)=> 
          <tr key={res.id}>
            
            <td>
              <CardText>{res.currency}</CardText>
            </td>
  
            <td className='w-25 w-sm-100'>
              <Input type='number' name="fee" defaultValue={res.fee}  onChange={el=>{  let newArr=[...state];  newArr[i]={...newArr[i],fee:el.target.value};   setState([...newArr]) }  } />
            </td>
  
            <td className='w-75'>
              <Input type='select' name="fee_type" defaultValue={res.fee_type}  onChange={el=>{  let newArr=[...state];  newArr[i]={...newArr[i],fee_type:el.target.value};   setState([...newArr]) }  } >
                <option label={'International'} value={1} />
                <option label={'Domestic'} value={0} />
                </Input>
            </td>
  
            <td>
              <RefreshCcw  className='cursor-pointer' onClick={el=>updateTxnfee({...state.find(el=>el.id==res.id)})}/>
            </td>
  
            <td>
              <Trash className='cursor-pointer' onClick={el=>deleteTransfee(res.id)} />
            </td>
          </tr>
  )}
        </tbody>:<tbody>No Fees Added Yet</tbody>}
      </Table>
    );
  };
  
  export default TransactionFee;
  
  const AddFee = ({ modalOpen, handleModal,transactionOpt,merchant,refetch }) => {
    const CloseBtn = (
      <X className="cursor-pointer" size={15} onClick={handleModal} />
    );
    const[loading,setLoading]=useState(false);
    const onSubmit=(event,error,values)=>{
        if(!error.length){
            setLoading(true);
           addOrUpdateTransferFee({data:{...values},merchant_id:merchant['?merchant_id'],action:'add'},({err,res})=>{
               if(err)notification({type:'error',title:"Error",message:err})
               else {notification({type:'success',message:'Successfully added'});handleModal();refetch()}
               setLoading(false);
           })
        }
    }
    return (
      <Modal
        isOpen={modalOpen}
        toggle={handleModal}
        contentClassName="pt-0"
        className="modal-dialog-centered"
      >
        <ModalHeader
          className="mb-0 pb-0"
          toggle={handleModal}
          close={CloseBtn}
          tag="div"
        >
          
        </ModalHeader>
        <AvForm onSubmit={onSubmit}>
        <ModalBody>
          <Row>
              <FormGroup tag={Col} md='12' >
               <AvField
               required
              
               label='Currency'
               name='currency'
               type='select'
               >
                    <option label='select--' value='' defaultChecked/>
                   {["EUR","GBP","BDT","DKK","BMD","USD"].filter(el=>{
                       //console.log(transactionOpt.find(el1=>el1.currency==el));
                       return !transactionOpt.find(el1=>el1.currency==el)
                   }).map(res=>
                       <option label={res} value={res} />
                   )}
  
               </AvField>
          
              </FormGroup>
              <FormGroup tag={Col}>
              <AvField
              required
              type='select'
               pal
              label='Fee Type'
               name='fee_type'
              >
                   <option label='select--' value='' defaultChecked/>
                   <option label='Domestic' value={0} defaultChecked/>
                   <option label='International' value={1} defaultChecked/>
             </AvField>
              <AvField
              type='number'
              required
              label='Fee'
               name='fee'
              />
             </FormGroup>
          </Row>
        </ModalBody>
        <CardFooter className='mb-1 pb-1'>
        <div className='float-right mb-1'>
            <Button.Ripple type='submit' color='primary' disabled={loading} >
                {loading?<Spinner size={'sm'} />:null}Update
            </Button.Ripple>
        </div>
        </CardFooter>
        </AvForm>
      </Modal>
    );
  };
  