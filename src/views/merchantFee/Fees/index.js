import React, { useState } from 'react'
import { Row, Col, TabContent, TabPane, Card, CardBody,Button } from "reactstrap";
import TransactionFee from './TransactionFee';
import Tabs from './Tabs';
import { ArrowLeft } from 'react-feather';
import { useHistory } from 'react-router';
function index() {
    const [activeTab, setActiveTab] = useState("1");
    const toggleTab = (el) => setActiveTab(el);
    const history =useHistory()
    return (
        <Row>
        <Col className="mb-2 mb-md-0" md="3">
          <Tabs activeTab={activeTab} toggleTab={toggleTab} />
        </Col>
        <Col md="9">
          <Card>
            <CardBody>
                <Button.Ripple onClick={el=>history.goBack()} color='primary' ><ArrowLeft size={15}/> Go Back</Button.Ripple>
              <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                  <TransactionFee />
                </TabPane>
               
              </TabContent>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
}

export default index
