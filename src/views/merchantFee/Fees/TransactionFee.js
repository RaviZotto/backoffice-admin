import {
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  Label,
  Input,
  Col,
  Table,
} from "reactstrap";
import React, { useContext, useEffect, useState } from "react";
import { PlusCircle, RefreshCcw, X, Trash } from "react-feather";
import {
  addOrUpdateTransactionFee,
  Axios,
  deleteTransactionfee,
  getPaymentGateway,
  getTransactionFee,
} from "../../../Api";
import notification from "../../../components/notification";
import Loading from "@core/components/spinner/Loading-spinner";
import FormGroup from "reactstrap/lib/FormGroup";
import CardText from "reactstrap/lib/CardText";
import AvForm from "availity-reactstrap-validation-safe/lib/AvForm";
import CardFooter from "reactstrap/lib/CardFooter";
import AvField from "availity-reactstrap-validation-safe/lib/AvField";
import Row from "reactstrap/lib/Row";
import Spinner from "reactstrap/lib/Spinner";
import queryString from "querystring";
import { countries } from "../../../utility/extras";
import { currencyCodes, userDetails } from "../../../utility/Utils";
import { AxiosError } from "axios";
import { graphqlServerUrl } from "../../../App";
import { GET_CURRENCY } from "../../../redux/actions/transfer";
import { useDispatch,useSelector } from "react-redux";

function TransactionFee() {
  const dispatch = useDispatch();
  const [modal, setModal] = useState(false);
  const [transactionOpt, setTransactionOpt] = useState([]);
  const [loading, setLoading] = useState(true);
  const [refetch, setRefetch] = useState(false);
  const [gateway, setGateway] = useState([]);
  const merchant = queryString.parse(window.location.search);
  const {currencies}= useSelector(state => state.transfer);
  const saveDefaultTransactionFee = (id) => {
    Axios.post(`${graphqlServerUrl}/admin/setDefaultTransactionFee`, id)
      .then((res) => {
        if (res.status == 200 && res.data) {
          notification({ message: "Default Added", type: "success" });
          setRefetch(true);
        }
      })
      .catch((e: AxiosError) => {
        notification({
          title: "Error",
          message: e.response ? e.response.data : e.toString(),
          type: "error",
        });
      });
  };
  useEffect(() => {
    if (!userDetails)
      userDetails = JSON.parse(localStorage.getItem("userDetails"));
    if(!currencies.length)dispatch(GET_CURRENCY())
  });
  useEffect(() => {
    if (loading || refetch) {
      //console.log(gateway);
      getTransactionFee(merchant["?merchant_id"], ({ err, res }) => {
        if (err) {
          notification({ title: "Error", message: err, type: "error" });
          return;
        }
        res.sort((a, b) => b.gateway - a.gateway);
        setTransactionOpt(res);
        getPaymentGateway(({ err, res }) => {
          if (err)
            return notification({
              title: "Error",
              type: "error",
              message: err,
            });
          //console.log(res);
          setGateway(res);
          setRefetch(false);
          setLoading(false);
        });
      });
    }
  }, [refetch]);

  if (loading) return <Loading />;
  return (
    <div className="card">
      <Button.Ripple
        onClick={(e) => setModal(!modal)}
        color="primary"
        className="mt-1"
        disabled={
          userDetails.role_type &&
          userDetails.role_type != "admin" &&
          !userDetails["merchant"].includes("C")
        }
      >
        <PlusCircle size={15} /> <span>Add Fee </span>
      </Button.Ripple>
      {!transactionOpt.length ? (
        <Button.Ripple
          onClick={(e) =>
            saveDefaultTransactionFee({ id: merchant["?merchant_id"] })
          }
          color="primary"
          className="mt-1"
          disabled={
            userDetails.role_type &&
            userDetails.role_type != "admin" &&
            !userDetails["merchant"].includes("U")
          }
        >
          Save Default Transaction Fee
        </Button.Ripple>
      ) : null}
      <AddFee
        refetch={(el) => setRefetch(true)}
        merchant={merchant}
        modalOpen={modal}
        handleModal={(el) => setModal(!modal)}
        currencies={currencies}
        gateway={gateway}
      />
      <FormComponent
        merchant={merchant}
        data={transactionOpt}
        refetch={(el) => setRefetch(true)}
      />
    </div>
  );
}

const FormComponent = ({ data, refetch }) => {
  const updateTxnfee = ({ id, fee, fixed_fee }) => {
    //console.log(id,fee,fixed_fee,'1',state);
    let dataObj = data.find((res) => res.id == id);

    if (dataObj && (dataObj.fixed_fee != fixed_fee || dataObj.fee != fee)) {
      console.log(fixed_fee,dataObj,fee);
      addOrUpdateTransactionFee(
        { data: { id, fee, fixed_fee } },
        ({ err, res }) => {
          if (err)
            notification({ type: "error", title: "Error", message: err });
          else
            notification({ type: "success", message: "Successfully updated!" });
          refetch();
        }
      );
    }
    // //console.log(id,fixed_fee,fee)
  };
  const deleteTxnFee = (id) => {
    deleteTransactionfee(id, ({ err, res }) => {
      if (err) notification({ type: "error", title: "Error", message: err });
      else notification({ type: "success", message: "Successfully updated!" });
      refetch();
    });
  };

  const [state, setState] = useState(
    data.map((res) => ({ fixed_fee: res.fixed_fee, fee: res.fee, id: res.id }))
  );
  return (
    <Table responsive className="mt-1">
      <thead>
        <th>Gateway</th>
        <th>Currency</th>
        <th>Fixed Fee</th>
        <th>Fee</th>
        <th>Update</th>
        <th>Delete</th>
      </thead>
      {data.length ? (
        <tbody>
          {data.map((res, i) => (
            <tr key={i}>
              <td>
                <CardText>{res.gateway}</CardText>
              </td>

              <td>
                <CardText>{res.currency}</CardText>
              </td>

              <td style={{ minWidth: "100px" }}>
                <Input
                  name="fixed_fee"
                  type="number"
                  className="text-center"
                  defaultValue={res.fixed_fee}
                  onChange={(el) => {
                    let newArr = [...state];
                    newArr[i] = { ...newArr[i], fixed_fee: el.target.value };
                    setState([...newArr]);
                  }}
                />
              </td>

              <td className="" style={{ minWidth: "100px" }}>
                <Input
                  name="fee"
                  type="number"
                  className="text-center"
                  defaultValue={res.fee}
                  onChange={(el) => {
                    let newArr = [...state];
                    newArr[i] = { ...newArr[i], fee: el.target.value };
                    setState([...newArr]);
                  }}
                />
              </td>
              <td>
                <Button.Ripple
                  color="primary"
                  onClick={(el) =>
                    updateTxnfee({ ...state.find((el) => el.id == res.id) })
                  }
                  disabled={
                    userDetails.role_type &&
                    userDetails.role_type != "admin" &&
                    !userDetails["merchant"].includes("U")
                  }
                >
                  <RefreshCcw size={15} className="cursor-pointer" />
                </Button.Ripple>
              </td>

              <td>
                <Button.Ripple
                  color="primary"
                  onClick={(el) => deleteTxnFee(res.id)}
                  disabled={
                    userDetails.role_type &&
                    userDetails.role_type != "admin" &&
                    !userDetails["merchant"].includes("D")
                  }
                >
                  <Trash size={15} className="cursor-pointer" />
                </Button.Ripple>
              </td>
            </tr>
          ))}
        </tbody>
      ) : (
        <tbody className="align-items-center">No fees added yet</tbody>
      )}
    </Table>
  );
};

export default TransactionFee;

const AddFee = ({
  modalOpen,
  handleModal,
  gateway,
  merchant,
  refetch,
  currencies,
}) => {
  const CloseBtn = (
    <X className="cursor-pointer" size={15} onClick={handleModal} />
  );
  const [loading, setLoading] = useState(false);
  const onSubmit = (event, error, values) => {
    event.preventDefault();
    if (!error.length) {
      setLoading(true);
      addOrUpdateTransactionFee(
        {
          data: { ...values },
          merchant_id: merchant["?merchant_id"],
          action: "add",
        },
        ({ err, res }) => {
          console.log(res, err);
          if (err)
            notification({ type: "error", message: err, title: "Error" });
          else {
            notification({
              type: "success",
              message: "Successfully update",
              title: "Transaction Fee",
            });
          }
          setLoading(false);
          handleModal();
          refetch();
        }
      );
    }
  };
  return (
    <Modal
      isOpen={modalOpen}
      toggle={handleModal}
      contentClassName="pt-0"
      className="modal-dialog-centered"
    >
      <ModalHeader
        className="mb-0 pb-0"
        toggle={handleModal}
        close={CloseBtn}
        tag="div"
      ></ModalHeader>
      <AvForm onSubmit={onSubmit}>
        <ModalBody>
          <Row>
            <FormGroup tag={Col}>
              <AvField required label="Currency" name="currency" type="select">
                <option label="select--" value="" defaultChecked />
                {currencies.map((res) => (
                  <option label={res.currency} value={res.currency} />
                ))}
              </AvField>
              <AvField type="select" label="Gateway" name="gateway">
                <option label="select--" value="" defaultChecked />
                {gateway.map((res) => (
                  <option
                    label={res.gateway_name.toUpperCase()}
                    value={res.gateway_name.toUpperCase()}
                  />
                ))}
              </AvField>
            </FormGroup>
            <FormGroup tag={Col}>
              <AvField
                required
                type="number"
                label="Fixed Fee"
                name="fixed_fee"
              />
              <AvField type="number" required label="Fee" name="fee" />
            </FormGroup>
          </Row>
        </ModalBody>
        <CardFooter className="mb-1 pb-1">
          <div className="float-right mb-1">
            <Button.Ripple type="submit" color="primary" disabled={loading}>
              {loading ? <Spinner size="sm" className="mr-1" /> : null}Add
            </Button.Ripple>
          </div>
        </CardFooter>
      </AvForm>
    </Modal>
  );
};
