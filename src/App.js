// ** Router Import
import React, { useEffect } from 'react';
import {
  ApolloClient, InMemoryCache, ApolloProvider, makeVar,
} from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { createUploadLink } from 'apollo-upload-client';
import { setContext } from '@apollo/client/link/context';
import Router from './router/Router';
import ErrorBoundary from './ErrorBoundary';
import Buggy from './Buggy';
// import { useQuery } from "@apollo/client";
// import { GET_MERCHANT } from "@graphql/queries";
// import Loading from "./@core/components/spinner/Fallback-spinner";
import notification from './components/notification';

    export const graphqlServerUrl = "https://paymentz.z-pay.co.uk";
// export const graphqlServerUrl = 'http://localhost:3006';

const basename = process.env.REACT_APP_BASENAME;
export const AdminDetails = makeVar({});
const errorLink = onError(({ graphQLErrors, response }) => {
  if (response) response.errors = null;
  if (graphQLErrors)graphQLErrors.map(({ message }) => {
    //console.log(message))
   })
  if (graphQLErrors) {
    for (const err of graphQLErrors) {
      //console.log(err.extensions);
      switch (err.extensions.code) {
        case 'UNAUTHENTICATED':
           notification({type: 'error', message: err.message,title: 'Error'})
          // if (window.location.pathname !== `${basename}/login`) {
          //   window.location.href = `${process.env.REACT_APP_HOST}${basename}/login`;
          // }
          break;
        case 'INTERNAL_SERVER_ERROR':
          //console.log(graphQLErrors[0]);
          break;

        default:
          notification({
            type: 'error',
            title: graphQLErrors[0].message,
          });
      }
    }
  }
  // if (networkError) notification({ type: "error", title: "No Internet!", message: "Check your network connection" });
});

const httpLink = createUploadLink({ uri: `${graphqlServerUrl}/merchant` });

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = JSON.parse(localStorage.getItem('token_item'));
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token ? token.token : null}` : '',
    },
  };
});

const client = new ApolloClient({
  link: errorLink.concat(authLink.concat(httpLink)),
  cache: new InMemoryCache(),
});

// const IntializeApp = () => {
//   const { loading, error } = useQuery(GET_MERCHANT);

localStorage.removeItem('temp_token');
//   if (error) localStorage.removeItem("token");

//   if (loading) return <Loading />;
//   return <Router />;
// };

// const App = (props) => (
//   //
//   //   <IntializeApp />
//   // </ApolloProvider>
// );

const App = (props) => {
 localStorage.removeItem('temp_token');
  return (
    <ApolloProvider client={client}>

      <Router />
     
    </ApolloProvider>
  );
};

export default App;
