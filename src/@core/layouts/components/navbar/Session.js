
import React,{useState} from 'react'
import {Button, Modal,Spinner , ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import axios from 'axios';
import { graphqlServerUrl } from '../../../../App';





const refreshToken=async({setLoading,setOpenModal,setTimer,timer,timerRef})=>{ 
  try{
    setLoading(true);
    console.log(JSON.parse(localStorage.getItem('token_item')),'2343');
    const auth_creds= JSON.parse(localStorage.getItem('token_item'))||{};
   const {refresh_token:refreshToken}=auth_creds;
  const {data} = await axios.get(`${graphqlServerUrl}/admin/refreshToken`,{headers: {'Content-Type': 'application/json','authorization':`Bearer ${refreshToken}`}});
  
    localStorage.setItem('token_item',JSON.stringify({...auth_creds,...data}));
    setOpenModal(false);
    
    if(timerRef.current)window.clearTimeout(timerRef.current);
    setTimer(Math.floor(data.access_expire_date-Date.now()/1000))
    setLoading(false);
     timerRef.current=setInterval(()=>{
        timer&&setTimer(t=>t-1);
     },1000);
  
     
  }catch(e){
    if(e.response&&(e.response.status==401 || e.response.statusText=="unauthorized")){
       localStorage.clear();window.location.href='/login';
       
    }
  }
}


function Session({openModal,setOpenModal,timer,setTimer,timerRef}) {
  const [loading,setLoading]=useState(false); 
  return (
        <Modal isOpen={openModal } backdrop="static" >
          <ModalHeader>
           <p>This is for the logout session</p>
          </ModalHeader>
          <ModalBody>
            {!timer?<p>Logging out .....</p>:
          <p>Your Session is about to expire in <span style={{color: 'red'}}>{timer}</span> sec,  Do you want continue session ?</p>}
          </ModalBody>
          <ModalFooter>
           <Button.Ripple  disabled={loading || !timer} color="primary" onClick={()=>refreshToken({setLoading,setOpenModal,timer,setTimer,timerRef})}>{loading?<Spinner  size="sm" />:"Yes"}</Button.Ripple>
          
          </ModalFooter>
        </Modal>
            
    
    )
}

export default Session
