// ** React Imports
import React, { useEffect, useRef, useState,Fragment } from "react";
import { Link, useHistory } from "react-router-dom";

// ** Custom Components
import Avatar from "@components/avatar";

// ** Utils
import { isUserLoggedIn } from "@utils";

// ** Store & Actions
import { useDispatch, useSelector } from "react-redux";
import { handleLogout } from "@store/actions/auth";

// ** Third Party Components
import {
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
} from "reactstrap";
import { User, Power } from "react-feather";

// ** Default Avatar Image
import defaultAvatar from "@src/assets/images/icons/user_image.png";
import { graphqlServerUrl } from "../../../../App";
import { AdminDetails } from "../../../../views/Settings";
import { getBase64FromUrl } from "../../../../utility/Utils";
import SessionModal from './Session';

const converSectoFormat=(sec)=>{
 
return new Date(sec * 1000).toISOString().substr(11, 8)
}






const UserDropdown = () => {
  // ** Store Vars
  const { adminDetails } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const history = useHistory();
  // ** State
  const [openModal,setOpenModal]=useState(false );
  const local = JSON.parse(localStorage.getItem("userDetails"));
  
  const [userName, setUserName] = useState(local && local["name"]);
  const [userAvatar, setUserAvatar] = useState(
    localStorage.getItem("image")||  defaultAvatar
  );
  const timerRef=useRef(null);
  const logoRef = useRef();
  const [count,setCount]=useState(()=>{
    const {access_expire_date} = JSON.parse(localStorage.getItem('token_item'))||{};
    return access_expire_date?Math.floor(access_expire_date-Date.now()/1000):0});
  
useEffect(()=>{
  if(timerRef.current)window.clearTimeout(timerRef.current);
  timerRef.current=setInterval(()=>{
      count&&setCount(t=>t-1);  
  },1000);
},[]);




  //** Vars
  const refreshImage = () => {
    getBase64FromUrl(  `${graphqlServerUrl}/images/admin/${
      AdminDetails().id
    }.png?t=${Date.now()}`)
    .then(res=>{
      if(res){
        console.log(res,'123')
      logoRef.current &&
      (logoRef.current.children[0].src = `${graphqlServerUrl}/images/admin/${
        AdminDetails().id
      }.png?t=${Date.now()}`);
    }else {setUserAvatar(defaultAvatar)}
    })
    .catch(err=>setUserAvatar(defaultAvatar));
    //  //console.log(logoRef.current.children,logoRef.current.children[0].src);
      (logoRef.current.children[0].src = `${graphqlServerUrl}/images/admin/${
        AdminDetails().id
      }.png?t=${Date.now()}`);
  };




  useEffect(() => {
    if(count==10)setOpenModal(true);
    if(count==0){dispatch(handleLogout()); window.clearInterval(timerRef.current); window.location.href='/login'}
    // count&&setTimeout(()=>setCount(count=>count-1),1000)
    return ()=>{}
  },[count]);

  useEffect(() => {
    


    window.addEventListener("image", (el) => {
      el.preventDefault();

      refreshImage();
    });






    //console.log(adminDetails);

    if (typeof adminDetails == "object") {
      const image =  localStorage.getItem("image")
      getBase64FromUrl( image )
      .then(res=>{setUserAvatar(res?res:defaultAvatar)})
      .catch(err=>setUserAvatar(defaultAvatar));
      adminDetails["name"] && setUserName(adminDetails["name"]);
    }
  }, [adminDetails && adminDetails[
    "name"]]);

  return (
    <Fragment>
      <SessionModal openModal={openModal} setOpenModal={setOpenModal} setTimer={setCount} timerRef={timerRef} timer={count} />
    <UncontrolledDropdown tag="li" className="dropdown-user nav-item">
      <DropdownToggle
        href="/"
        tag="a"
        className="nav-link dropdown-user-link"
        onClick={(e) => e.preventDefault()}
      >
        <span className='mr-1' >{converSectoFormat(count)}</span>
        <div className="user-nav d-sm-flex d-none">
          <span className="user-name font-weight-bold">
            {userName || "John Doe"}
          </span>
          <span className="user-status">
            {(local && local["role_type"]) || "Admin"}
          </span>
         
        </div>
        <Avatar
          img={userAvatar}
          ref={logoRef}
          imgHeight="40"
          imgWidth="40"
          status="online"
        />
      </DropdownToggle>
      <DropdownMenu right>
        <DropdownItem
          tag={Link}
          to="#"
          onClick={(e) => history.push("/settings")}
        >
          <User size={14} className="mr-75" />
          <span className="align-middle">Profile</span>
        </DropdownItem>

        <DropdownItem
          tag={Link}
          to="/login"
          onClick={() => dispatch(handleLogout())}
        >
          <Power size={14} className="mr-75" />
          <span className="align-middle">Logout</span>
        </DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
    </Fragment>
  );
};

export default UserDropdown;
