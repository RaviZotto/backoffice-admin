// ** React Imports
import React, { useRef, useState } from 'react';
import { useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import {writeFileSync} from 'fs';
// ** Third Party Components
import { Disc, X, Circle } from 'react-feather'

// ** Config
import logo from "@src/assets/images/logo/logo.png";
import logoShort from "@src/assets/images/logo/logo-short.png";
import themeConfig from '@configs/themeConfig'
import { graphqlServerUrl } from '../../../../../App';

const VerticalMenuHeader = props => {
  // ** Props
  const [url,setUrl]=useState(require('@src/assets/images/logo/Zottologo.png')|| localStorage.getItem('image'));
  const { menuCollapsed, setMenuCollapsed, setMenuVisibility, setGroupOpen, menuHover } = props
  const logoRef=useRef();

 const refreshImg=()=>{
  logoRef.current&&(logoRef.current.src=`${graphqlServerUrl}/images/admin/1.png?t=${Date.now()}`);

 }



  // ** Reset open group
 useEffect(()=>{
  
  window.addEventListener('image',el=>{
    el.preventDefault();
    //console.log(el);
    refreshImg()
  });
//   setInterval(()=>{
//     refreshImg()
// },2000 )
 },[])

  useEffect(() => {
    if (!menuHover && menuCollapsed) setGroupOpen([])
  }, [menuHover, menuCollapsed])

  // ** Menu toggler component
  const Toggler = () => {
    if (!menuCollapsed) {
      return (
        <Disc
          size={20}
          data-tour='toggle-icon'
          className='text-primary toggle-icon d-none d-xl-block'
          onClick={() => setMenuCollapsed(true)}
        />
      )
    } else {
      return (
        <Circle
          size={20}
          data-tour='toggle-icon'
          className='text-primary toggle-icon d-none d-xl-block'
          onClick={() => setMenuCollapsed(false)}
        />
      )
    }
  }

  return (
    <div className='navbar-header'>
      <ul className='nav navbar-nav flex-row'>
        <li className='nav-item mr-auto'>
          <NavLink to='/' className='navbar-brand'>
            <span className='brand-logo' >
               { !menuCollapsed?
                <img  src={logo} alt="logo" />:<img className='mt-0' src={logoShort} alt="logo"/>
               }
            </span>
            
          </NavLink>
        </li>
        <li className='nav-item nav-toggle'>
          <div className='nav-link modern-nav-toggle cursor-pointer'>
            <Toggler />
            <X onClick={() => setMenuVisibility(false)} className='toggle-icon icon-x d-block d-xl-none' size={20} />
          </div>
        </li>
      </ul>
    </div>
  )
}

export default VerticalMenuHeader
