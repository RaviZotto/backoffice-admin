
import React from 'react';
import {Modal,Button,ModalHeader,ModalFooter,ModalBody} from 'reactstrap';
function Index({open,onPress,onCancel,title}){

return (
<div className='basic-modal'>
      
        <Modal isOpen={open} toggle={() => onCancel(!open)}>
          <ModalHeader toggle={() => onCancel(!open)}>Basic Modal</ModalHeader>
          <ModalBody>
           <h5>{title}</h5>
          </ModalBody>
          <ModalFooter className='d-flex justify-content-end space-x-1'>
            <Button color='primary' onClick={() =>onPress }>
              Accept
            </Button>
            <Button color='secondary' onClick={() =>onCancel(!open)}/>
            cancel
          </ModalFooter>
        </Modal>
        </div>
        );
}
export default Index;